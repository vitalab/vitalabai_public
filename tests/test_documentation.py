import importlib
import inspect
import pkgutil
import re
import sys
import warnings
from itertools import compress

import pytest

modules = ['VITALabAI.model.classification',
           'VITALabAI.dataset.classification',
           'VITALabAI.model.semanticsegmentation',
           'VITALabAI.dataset.semanticsegmentation',
           'VITALabAI.project']

#  These modules are inserted manually because we do not want to recursively list all
#  modules in the VITALabAI module.
all_modules = ['VITALabAI.VITALabAiAbstract',
               'VITALabAI.VITALabAiDatasetAbstract',
               'VITALabAI.VITALabAiKerasAbstract',
               'VITALabAI.VITALabAiPytorchAbstract',
               'VITALabAI.VITALabAiPytorchDatasetWrapper']

accepted_name = []
accepted_module = []

# Functions or classes with less than 'MIN_CODE_SIZE' lines can be ignored
MIN_CODE_SIZE = 10

ARGS = 'Args:'
RAISES = 'Raises:'
RETURN = 'Returns:'


def handle_class(name, member):
    if is_accepted(name, member):
        return
    if member.__doc__ is None and not member_too_small(member):
        raise ValueError("{} class doesn't have any documentation".format(name),
                         member.__module__, inspect.getmodule(member).__file__)
    for n, met in inspect.getmembers(member):
        if inspect.isfunction(met):
            handle_method(n, met)


def handle_function(name, member):
    if is_accepted(name, member) or member_too_small(member):
        # We don't need to check this one.
        return
    doc = member.__doc__
    if doc is None:
        raise ValueError("{} function doesn't have any documentation".format(name),
                         member.__module__, inspect.getmodule(member).__file__)

    args = list(inspect.signature(member).parameters.keys())
    assert_args_presence(args, doc, member, name)
    assert_function_style(name, member, doc, args)
    assert_doc_style(name, member, doc)


def assert_doc_style(name, member, doc):
    if name == '__init__':
        return
    lines = doc.split("\n")
    first_line = lines[0]
    if len(first_line.strip()) == 0:
        raise ValueError(
            "{} the documentation should be on the first line.".format(name),
            member.__module__)


def assert_function_style(name, member, doc, args):
    code = inspect.getsource(member)
    has_return = re.findall(r"\s*return \S+", code, re.MULTILINE)
    if has_return and RETURN not in doc:
        innerfunction = [inspect.getsource(x) for x in member.__code__.co_consts if
                         inspect.iscode(x)]
        return_in_sub = [ret for code_inner in innerfunction for ret in
                         re.findall(r"\s*return \S+", code_inner, re.MULTILINE)]
        if len(return_in_sub) < len(has_return):
            raise ValueError("{} needs a 'Returns' section".format(name),
                             member.__module__)

    has_raise = re.findall(r"^\s*raise \S+", code, re.MULTILINE)
    if has_raise and "# Raises" not in doc:
        innerfunction = [inspect.getsource(x) for x in member.__code__.co_consts if
                         inspect.iscode(x)]
        raise_in_sub = [ret for code_inner in innerfunction for ret in
                        re.findall(r"\s*raise \S+", code_inner, re.MULTILINE)]
        if len(raise_in_sub) < len(has_raise):
            raise ValueError("{} needs a 'Raises:' section".format(name),
                             member.__module__)

    if len(args) > 0 and ARGS not in doc:
        raise ValueError("{} needs a 'Args:' section".format(name),
                         member.__module__)

    assert_blank_before(name, member, doc, [ARGS, RAISES, RETURN])


def assert_blank_before(name, member, doc, keywords):
    doc_lines = [x.strip() for x in doc.split('\n')]
    for keyword in keywords:
        if keyword in doc_lines:
            index = doc_lines.index(keyword)
            if doc_lines[index - 1] != '':
                raise ValueError(
                    "{} '{}' should have a blank line above.".format(name, keyword),
                    member.__module__)


def is_accepted(name, member):
    if 'VITALabAI' not in str(member.__module__):
        return True
    return name in accepted_name or member.__module__ in accepted_module


def member_too_small(member):
    code = inspect.getsource(member).split('\n')
    return len(code) < MIN_CODE_SIZE


def assert_args_presence(args, doc, member, name):
    if 'self' in args:
        args.remove('self')
    args_not_in_doc = [arg not in doc for arg in args]
    if any(args_not_in_doc):
        raise ValueError(
            "{} {} arguments are not present in documentation ".format(name, list(
                compress(args, args_not_in_doc))), member.__module__)
    words = doc.replace('*', '').split()
    # Check arguments styling
    styles = [arg + ":" not in words for arg in args]
    if any(styles):
        raise ValueError(
            "{} {} are not style properly 'argument': documentation".format(
                name,
                list(compress(args, styles))),
            member.__module__)

    # Check arguments order
    indexes = [words.index(arg + ":") for arg in args]
    if indexes != sorted(indexes):
        raise ValueError(
            "{} arguments order is different from the documentation".format(name),
            member.__module__)


def handle_method(name, member):
    if name in accepted_name or member.__module__ in accepted_module:
        return
    handle_function(name, member)


def handle_module(mod):
    for name, mem in inspect.getmembers(mod):
        if inspect.isclass(mem):
            handle_class(name, mem)
        elif inspect.isfunction(mem):
            handle_function(name, mem)
        elif 'VITALabAI' in name and inspect.ismodule(mem):
            # No recursion as of now.
            pass


def get_all_modules(modules, all_modules):
    """
    This method recursively searches for all modules starting from the modules listed
    in the modules param and adds them to the all_modules list.

    Args:
        modules: list of modules to recursively search.
        all_modules: list of modules to which found modules are added before it is returned

    Returns:
        List of all modules to be tested for documentation.
    """

    def list_submodules(list_name, package):

        for loader, module_name, is_pkg in pkgutil.iter_modules(package.__path__,
                                                                package.__name__ + '.'):
            list_name.append(module_name)

            if is_pkg:
                module_name = importlib.import_module(module_name)
                list_submodules(list_name, module_name)

    for mod in modules:
        package = importlib.import_module(mod)
        list_submodules(all_modules, package)

    return all_modules


@pytest.mark.skipif(sys.version_info < (3, 3), reason="requires python3.3")
def test_doc():
    for mod in get_all_modules(modules, all_modules):
        try:
            mod = importlib.import_module(mod)
            handle_module(mod)
        except ImportError as e:
            if 'torch' in str(e):
                warnings.warn(UserWarning("Module {} will not be tested because {}".format(mod, e)))
            else:
                ImportError(e)


if __name__ == '__main__':
    pytest.main([__file__])
