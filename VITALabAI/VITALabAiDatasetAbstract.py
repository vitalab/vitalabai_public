#
# Author: Clement ZOTTI (clement.zotti@usherbrooke.ca), Frédéric B-Charron, Thierry Judge
#

"""
VITALalAiDatasetAbstract class
"""


class VITALabAiDatasetAbstract():
    """
    This class is the abstract class to generate and get the dataset.
    All dataset projects have to comply to this interface.
    """

    def get_train_set(self):
        """ Get the training set
        Returns:
            keras.utils.Sequence or torch.Dataloader
        """
        raise NotImplementedError

    def get_validation_set(self):
        """ Get the training set
        Returns:
            keras.utils.Sequence or torch.Dataloader
        """
        raise NotImplementedError

    def get_test_set(self):
        """ Get the training set
        Returns:
            keras.utils.Sequence or torch.Dataloader
        """
        raise NotImplementedError

    def get_classes(self):
        """ Get the list of classes
        Returns:
            List of classes
        """
        raise NotImplementedError

    def get_num_classes(self):
        """Get the number of classes
        Returns:
            Int, number of classes
        """
        return len(self.get_classes())

    def get_input_shape(self):
        """Get the input shape of the dataset
        Returns:
            Tuple, shape of an input (H,W,C)
        """
        raise NotImplementedError

    def __iter__(self):
        """
        This method is used to get the iterator for the dataset.
        """
        return self

    def __next__(self):
        """
        This method is called by python3 loop iteration process.
        """
        return self.next()

    def next(self):
        """
        This method is called by the current interface and used for iteration on the data from the dataset.
        """
        pass
