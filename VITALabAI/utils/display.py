import os

import numpy as np
import pygame


class Display:
    """Class to handle the pygame display system."""

    def __init__(self, save, img_per_row):
        """
        Args:
            save: string, Where to save the screenshot, if empty nothing is saved.
            img_per_row: int, number of images to display per row
        """
        if save:
            if not os.path.isdir(save):
                os.makedirs(save)
        self.save = save
        self.screen = None
        self.img_per_row = img_per_row

    def pyinit(self, size):
        """Initialize the pygame display system given the size.

        Args:
            size: tuple or list, Size should be a two element tuple/list.

        # Raises:
            ValueError if size is not list or tuple of length 2
        """
        pygame.init()
        if not isinstance(size, (tuple, list)):
            ValueError("The parameter 'size' should be a tuple/list.")
        if len(size) != 2:
            ValueError(
                "The parameter 'size' should be a {} of len 2.".format(type(size)))
        self.screen = pygame.display.set_mode(size, 0, 32)

    def pydisplay(self, images, epoch):
        """Method to display images from the list given in parameter.

        Args:
            images: list or tuple, It's a list of 2D RGB images.
            epoch: int, number of the current epoch used to save images

        # Raises:
            ValueError if images are not numpy array, list or tuple
        """
        if not self.screen:
            num_img = len(images)
            size_img = images[0].shape
            rows = int(np.ceil(num_img / 2) * size_img[0])
            cols = size_img[1] * 2
            self.pyinit((rows, cols))

        row, col = 0, 0

        for i in range(images.shape[0]):
            image = images[i]

            if not isinstance(image, (np.ndarray, list, tuple)):
                raise ValueError(
                    "Image should be a numpy array, a list or a tuple.")

            if i != 0 and i % self.img_per_row == 0:
                row += image.shape[0]
                col = 0
            image = (np.clip(image, 0, 1) * 255).astype(np.uint8)
            self.screen.blit(
                pygame.surfarray.make_surface(image), (col, row))
            col += image.shape[1]
        pygame.display.update()

        if self.save:
            pygame.image.save(self.screen,
                              "{}/screenshot_{:015}.png".format(self.save, epoch))

    def __call__(self, *images):
        """ See pydisplay method. """
        self.pydisplay(images)
