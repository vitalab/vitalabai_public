from keras.engine import Layer
import keras.backend as K


class Softmax_along_axis(Layer):
    """
    Layer class Softmax for keras.
    You can choose on which axis you want to perform the softmax
    """
    def __init__(self, axis=1, **kwargs):
        """
        Initializer
        Parameters
        ----------
        axis: int
            Axis to perform the softmax.
        kwargs: other params
        """
        self.axis = axis
        super(Softmax_along_axis, self).__init__(**kwargs)

    def build(self, input_shape):
        """
        Used in keras model.
        See keras.engine.Layer class.
        """
        pass

    def call(self, pre_output, mask=None):
        """
        This method performs the chosen backend computation of the softmax.
        See keras.engine.Layer class.
        """
        return across_channel_softmax(pre_output=pre_output, axis=self.axis)

    def compute_output_shape(self, input_shape):
        return self.get_output_shape_for(input_shape)

    def get_output_shape_for(self, input_shape):
        """
        Generate the output shape for this layer.
        See keras.engine.Layer class.
        """
        len_shape = len(input_shape)
        axis_index = self.axis % len_shape
        return input_shape


def across_channel_softmax(pre_output, axis=1):
    """
    Function that computes a softmax along the channel axis.
    
    Parameters
    ----------
    pre_output: graph.variable
        Output of the previous layer.

    Returns
    -------
    softmax computed along channel axis.
    """
    e_tensorbc01 = K.exp(pre_output - K.max(pre_output, axis=axis, keepdims=True))
    p_y_given_x = e_tensorbc01 / K.sum(e_tensorbc01, axis=axis, keepdims=True)
    return p_y_given_x
