import logging
import os
import re
import sys
from os.path import join as pjoin

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

logging.basicConfig(format='%(asctime)-15s : %(message)s',
                    level=logging.DEBUG)


def get_logger(name):
    return logging.getLogger(name)


def conv_int(i):
    """ Convert string into int if i is convertible.

    Args:
        i: str, String to be converted into int.

    Returns:
        int if possible, otherwise the string itself.
    """
    return int(i) if i.isdigit() else i


def natural_order(sord):
    """ Split the sord string into a list of string and int.
    Use it as the key parameter of the sorted() function.

    Ex:

    ['1','10','2'] -> ['1','2','10']

    ['abc1def','ab10d','b2c','ab1d'] -> ['ab1d','ab10d', 'abc1def', 'b2c']

    Args:
        sord: str, String that needs to be sorted.

    Returns:
        List of string and int.

    """
    if isinstance(sord, tuple):
        sord = sord[0]
    return [conv_int(c) for c in re.split(r'(\d+)', sord)]


def write_summary_to_file(model, filename):
    """Write the summary of a Keras model to a text file for viewing."""
    orig_stdout = sys.stdout
    with open(filename, 'w') as f:
        sys.stdout = f
        model.summary()
        sys.stdout = orig_stdout


def print_confusion_matrix(confusion_matrix, normalize=False, class_names=None, figsize=(10, 7), fontsize=11,
                           cmmin=None, cmmax=None, norm=None):
    """Prints a confusion matrix, as returned by sklearn.metrics.confusion_matrix, as a heatmap.

    Args:
        confusion_matrix: numpy.ndarray, The numpy.ndarray object returned from a call to
                        sklearn.metrics.confusion_matrix. Similarly constructed ndarrays can also be used.
        normalize: bool, Whether to normalize data or not.
        class_names: list, An ordered list of class names, in the order they index the given confusion matrix.
        figsize: tuple, A 2-long tuple, the first value determining the horizontal size of the outputted
                        figure, the second determining the vertical size. Defaults to (10,7).
        fontsize: int, Font size for axes labels. Defaults to 14.
        cmmin: scalar, The min colorbar value. If None, suitable min values is automatically chosen by the Normalize
                        instance (defaults to the respective min/max values of C in case of the default linear scaling).
        cmmax: scalar, The max colorbar value. If None, suitable max values is automatically chosen by the Normalize
                        instance (defaults to the respective min/max values of C in case of the default linear scaling).
        norm: a matplotlib Normalize instance, scales the data values to the canonical colormap range [0, 1] for mapping
                        to colors. By default, the data range is mapped to the colorbar range using linear scaling.

    Returns:
        matplotlib.figure.Figure: The resulting confusion matrix figure
    """
    if normalize:
        confusion_matrix = (
                confusion_matrix.astype('float') / (confusion_matrix.sum(axis=1) + 1e-21)[:, np.newaxis] * 100).astype(
            np.int32)
    df_cm = pd.DataFrame(
        confusion_matrix, index=class_names, columns=class_names,
    )
    fig = plt.figure(figsize=figsize)
    try:
        heatmap = sns.heatmap(df_cm, annot=True, fmt="d", vmin=cmmin, vmax=cmmax, cmap='Blues', norm=norm,
                              cbar=True)
    except ValueError:
        raise ValueError("Confusion matrix values must be integers.")

    heatmap.yaxis.set_ticklabels(heatmap.yaxis.get_ticklabels(), rotation=0, ha='right', fontsize=fontsize)
    heatmap.xaxis.set_ticklabels(heatmap.xaxis.get_ticklabels(), rotation=45, ha='right', fontsize=fontsize)
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    return fig


def get_nonexistent_path(fname_path):
    """Get the path to a filename which does not exist by incrementing path.

    Examples
    --------
    get_nonexistant_path('/etc/issue')
    '/etc/issue_1'

    Args:
        fname_path: string, path to increment

    Returns:
        non existent file path
    """
    if not os.path.exists(fname_path):
        os.makedirs(fname_path)
        return fname_path
    filename, file_extension = os.path.splitext(fname_path)
    i = 1
    new_fname = "{}_{}{}".format(filename, i, file_extension)
    while os.path.exists(new_fname):
        i += 1
        new_fname = "{}_{}{}".format(filename, i, file_extension)
    os.mkdir(new_fname)
    return new_fname


def create_model_directories(name, subdirectories, nonexistent=True):
    """ Create the folder architecture to save everything.

    Args:
        name: string, name of the parent directory
        subdirectories: tuple of strings, name and names of the subdirectories
        nonexistent: bool, whether to create a non existent path to parent directory or not

    Returns:
        tuple containing new name and subdirectory paths in same order as arg
    """
    if nonexistent:
        name = get_nonexistent_path(name)

    subdir_paths = [name]

    for subdir in subdirectories:
        if not os.path.exists(pjoin(name, subdir)):
            os.mkdir(pjoin(name, subdir))

        subdir_paths.append(pjoin(name, subdir))

    return tuple(subdir_paths)
