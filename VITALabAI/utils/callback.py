import io
import os

import tensorflow as tf
import keras.callbacks
import keras.backend as kback
import matplotlib.pyplot as plt


class TensorBoardPlot(keras.callbacks.TensorBoard):
    """This callback produces TensorBoard logs in the given log_dir, and also produces plots.

    Add plots using add_plot(). A subfolder will be created for each run, eg.: ./logs/run0000, ./logs/run0001 ...
    """

    def __init__(self, log_dir='./logs', histogram_freq=0, write_graph=True, write_images=False):
        # Create a new subfolder for this run
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)  # First make sure the containing folder is there
        decimals = 4
        all_subdirs_numbers = [int(d[-decimals:]) for d in os.listdir(log_dir)]
        if len(all_subdirs_numbers) == 0:
            run_number = 0
        else:
            run_number = max(all_subdirs_numbers) + 1
        log_dir = os.path.join(log_dir, 'run' + str(run_number).zfill(decimals))

        super(TensorBoardPlot, self).__init__(log_dir, histogram_freq, write_graph, write_images)
        self.log_history = None
        self.plots = []
        self.required_metrics = []

    def add_plot(self, title, ylabel, metrics, metric_labels, xlabel='Epoch'):
        """Add a plot to be shown in the summary at each epoch

        Parameters
        ----------
        title: string
            Title of the chart.
        xlabel: string
            Label of the x axis.
        ylabel: string
            Label of the y axis, eg.: 'Accuracy'
        metrics: list of strings
            List containing metric names, eg.: ['acc', 'val_acc']
        metric_labels: list of strings
            Readable names for the metrics, eg.: ['Training', 'Validation']
        """
        self.plots.append({
            'title': title,
            'xlabel': xlabel,
            'ylabel': ylabel,
            'metrics': metrics,
            'metric_labels': metric_labels
        })
        self.required_metrics += list(metrics)

    def on_epoch_end(self, epoch, logs=None):
        if logs is None:
            return

        super(TensorBoardPlot, self).on_epoch_end(epoch, logs)

        if self.log_history is None:
            # Check that required metrics are present
            for m in self.required_metrics:
                if m not in logs:
                    raise Exception('Metric {} is not in the logs'.format(m))
            # Build history dict
            self.log_history = {m: [] for m in self.required_metrics}

        # Update history
        for entry in self.log_history:
            self.log_history[entry].append(logs[entry])

        # Add TensorBoard image summaries
        for plot_desc in self.plots:
            plot_image = self._gen_plot(plot_desc)
            self._add_summary(plot_desc['title'], plot_image, epoch)
        self.writer.flush()

    def _add_summary(self, name, image, epoch):
        summary_op = tf.summary.image(name.replace(' ', '_') + '_' + str(epoch).zfill(3), image, max_outputs=1)
        summary = kback.get_session().run(summary_op)
        self.writer.add_summary(summary, epoch)

    def _gen_plot(self, plot_desc):
        """Create a pyplot plot and save to buffer, then convert to TF image."""

        data = [self.log_history[l] for l in plot_desc['metrics']]

        dpi = 75
        plt.figure(dpi=dpi, figsize=(512 / dpi, 240 / dpi))
        plt.title(plot_desc['title'])
        plt.xlabel(plot_desc['xlabel'])
        plt.ylabel(plot_desc['ylabel'])
        xticks = range(1, len(next(iter(data))) + 1)  # Epochs from 1 to the last completed epoch
        plt.xticks(xticks)  # Put axis x ticks on epoch number integer
        lines = []
        for data_line, label in zip(data, plot_desc['metric_labels']):
            plot_line, = plt.plot(xticks, data_line, label=label)
            lines.append(plot_line)
        plt.legend(handles=lines)
        plt.grid(linestyle='dotted')
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        buf.seek(0)

        # Convert PNG buffer to TF image
        image = tf.image.decode_png(buf.getvalue(), channels=4)

        # Add the batch dimension
        image = tf.expand_dims(image, 0)

        plt.close()

        return image
