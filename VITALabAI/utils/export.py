import numpy as np


def save_nii_file(array, output_filepath,
                  affine=np.eye(4),
                  header=None,
                  zoom=(1, 1, 1),
                  dtype=np.float32):
    """ Save a 3d array in nifti format.

    Args:
        array: ndarray, The 3d image.
        output_filepath: string, The output filename. Must end in ".nii" or ".nii.gz".
        affine: ndarray, The affine transformation.
        header: NiftiHeader, The Nifti header.
        zoom: list or tuple, Define the voxel size.
        dtype: numpy.dtype, Type of data to save into the nifti format.
    """
    import nibabel as nib
    img = nib.Nifti1Image(array.astype(dtype), affine, header=header)
    img.header.set_zooms(zoom)
    img.set_data_dtype(dtype)
    nib.save(img, output_filepath)


def save_mhd_file(array, output_filepath, origin=(0, 0, 0), spacing=(1, 1, 1), dtype=np.float32):
    """ Saves a 2d array in mhd format.

    Args:
        array: ndarray, the 2d image.
        output_filepath: string, the output filename. Must end in ".mhd".
        origin: list or tuple, define the center of the image.
        spacing: list or tuple, define the voxel size.
        dtype: numpy.dtype, Type of data to save into the mhd format.
    """
    import SimpleITK
    seg = SimpleITK.GetImageFromArray(array.astype(dtype))
    seg.SetOrigin(origin)
    seg.SetSpacing(spacing)
    SimpleITK.WriteImage(seg, output_filepath)
