# -*- coding: utf-8 -*-

from keras import backend as K
from keras.engine.topology import Layer

if K.backend() == 'theano':
    import theano
    import theano.tensor as T

    class MergeCenter(Layer):
        """ Merge center layer. """

    class MergeCenter(Layer):

        def __init__(self, output_shape, axis=-1, **kwargs):
            super(MergeCenter, self).__init__(**kwargs)
            self.axis = axis
            self.out_shape = tuple(output_shape)

        def pad_to_bounding_box(self, img, pos):
            """ Pad

            Args:
                img:
                pos:

            Returns:

            """
            maxi = T.constant(205, dtype='int32')
            mini = T.constant(51, dtype='int32')
            res = T.zeros(self.out_shape[:2] + (3,))

            o_h = T.cast(K.max((K.min((pos[0], maxi)), mini)), dtype='int32')
            o_w = T.cast(K.max((K.min((pos[1], maxi)), mini)), dtype='int32')
            sub_tensor = res[o_h - 50:o_h + 50, o_w - 50:o_w + 50, :]
            res = T.set_subtensor(sub_tensor, img)
            return res

        def call(self, x, mask=None):
            """ call merge center

            Args:
                x: list, inputs (pred, prior, centers)
                mask: None

            Returns:
                merged outputs

            """
            pred = x[0]  # Image predicted by the network
            prior = x[1]  # Prior shape to stack with the prediction
            pos = x[2]  # position to center the prior shape

            res, _ = theano.scan(
                self.pad_to_bounding_box, sequences=(prior, pos))
            res = K.concatenate([pred, res])

            return res

        def compute_output_shape(self, input_shape):
            return input_shape[0][:-1] + (7,)

elif K.backend() == "tensorflow":
    import tensorflow as tf

    class MergeCenter(Layer):
        """ Merge center layer. """


    class MergeCenter(Layer):
        def __init__(self, output_shape, axis=-1, **kwargs):
            super(MergeCenter, self).__init__(**kwargs)
            self.axis = axis
            self.out_shape = tuple(output_shape)

        def pad_to_bounding_box(self, _old, new):
            """ Pad

            Args:
                _old:
                new:

            Returns:

            """
            prior, center = new

            maxi = tf.constant(205, tf.int32)
            mini = tf.constant(51, tf.int32)

            # center = tf.Print(center, [center,], "C: ")
            # welcome to tf.scan we need to remove the padding added to keep
            # the shape consistant between the input and output
            prior = prior[78:256 - 78, 78:256 - 78, :]

            # Constrain the center in the image feature map
            # feature map expected to be (None, 256, 256, c)
            idx_r = tf.maximum(tf.minimum(tf.cast(center[0], tf.int32), maxi), mini)
            idx_c = tf.maximum(tf.minimum(tf.cast(center[1], tf.int32), maxi), mini)

            # Compute the padding needed to pad the prior to (256, 256, 3)
            min_r = idx_r - 50
            max_r = idx_r + 50

            min_c = idx_c - 50
            max_c = idx_c + 50

            # Compute the padding on X, Y and X
            pr = tf.reshape(tf.stack([min_r, 255 - max_r], axis=0), [1, -1])
            pc = tf.reshape(tf.stack([min_c, 255 - max_c], axis=0), [1, -1])
            pz = tf.reshape(tf.stack([0, 0], axis=0), [1, -1])
            paddings = tf.cast(tf.concat([pr, pc, pz], axis=0), tf.int32)

            padded = tf.pad(prior, paddings)
            # Need to set the shape, without tf is going crazy with a shape inconsistency
            # padded = tf.Print(padded, [tf.shape(padded),], 's:')
            padded.set_shape((256, 256, 3))
            return [padded, center]

        def call(self, x, mask=None):
            """ call merge center

            Args:
                x: list, inputs (pred, prior, centers)
                mask: None

            Returns:
                merged outputs

            """
            pred, prior, centers = x
            centers = tf.cast(centers, tf.int32)
            centers = tf.cast(centers, tf.float32)

            # 100 is the dimension of the shape prior (15, 100, 100, 3)
            self.pad = (self.out_shape[0] - 100) // 2

            # Padding on the prior needed to have consistency in shape for tf.scan
            padded_prior = tf.pad(prior,
                                  tf.constant([[0, 0], [self.pad, self.pad], [self.pad, self.pad], [0, 0]]))

            res = tf.scan(self.pad_to_bounding_box,
                          [padded_prior, centers],
                          initializer=[padded_prior[0], centers[0]],
                          infer_shape=False)

            # Discard the res[1] which contains the centers
            aligned_prior = res[0]

            # in my notebook the same function outputs (None, 256, 256, 3)
            aligned_prior.set_shape((None, 255, 255, 3))
            aligned_prior = tf.pad(aligned_prior, tf.constant([[0, 0], [0, 1], [0, 1], [0, 0]]))

            res = K.concatenate([pred, aligned_prior])
            return res

        def compute_output_shape(self, input_shape):
            return input_shape[0][:-1] + (7,)

        def get_config(self):
            base_config = super(MergeCenter, self).get_config()
            base_config['output_shape'] = self.out_shape
            base_config['axis'] = self.axis
            return base_config
