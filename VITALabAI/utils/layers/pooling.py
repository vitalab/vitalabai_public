"""
WARNING: the following keras layers can only be used with the Tensorflow backend.
"""
from keras import backend as K
from keras.layers import Layer


class MaxPoolingWithArgmax2D(Layer):
    """ MaxPooling layer that saves the max indices.
    """
    def __init__(self, pool_size=(2, 2), strides=(2, 2), padding='same', **kwargs):
        """ Layer constructor

        Args:
            pool_size: pooling window size, see keras.layers.MaxPooling2D.
            strides: pooling steps, see keras.layers.MaxPooling2D.
            padding: padding to use with pooling, see keras.layers.MaxPooling2D.
            **kwargs: standard keras layer arguments.
        """
        super(MaxPoolingWithArgmax2D, self).__init__(**kwargs)
        self.padding = padding

        if type(pool_size) == int:
            pool_size = (pool_size, pool_size)
        self.pool_size = pool_size

        if type(strides) == int:
            strides = (strides, strides)
        self.strides = strides

    def call(self, inputs, **kwargs):
        """ Applies MaxPooling to the input and saves the max indices,
        this essentially wraps tf.max_pool_with_argmax in a keras layer

        Args:
            inputs: data to maxpool
            **kwargs: for keras layer compatibility.

        Returns:
            The maxpooled input and the mask for the used indices.
        """
        padding = self.padding
        pool_size = self.pool_size
        strides = self.strides
        ksize = [1, pool_size[0], pool_size[1], 1]
        padding = padding.upper()
        strides = [1, strides[0], strides[1], 1]
        output, argmax = K.tf.nn.max_pool_with_argmax(inputs, ksize=ksize, strides=strides, padding=padding)
        argmax = K.cast(argmax, K.floatx())
        return [output, argmax]

    def compute_output_shape(self, input_shape):
        """ Computes the output shape for this layer with regards to the input shape.

        Args:
            input_shape: input's shape.

        Returns:
            Layer's output shape.
        """
        ratio = (1, 2, 2, 1)
        output_shape = [dim // ratio[idx] if dim is not None else None for idx, dim in enumerate(input_shape)]
        output_shape = tuple(output_shape)
        return [output_shape, output_shape]

    def compute_mask(self, inputs, mask=None):
        return 2 * [None]


class MaxUnpooling2D(Layer):
    """ Upsampling layer using saved max indices.
    """
    def __init__(self, size=(2, 2), **kwargs):
        """
        Layer constructor

        Args:
            size: max pooling window size.
            **kwargs: standard keras layer arguments.
        """
        super(MaxUnpooling2D, self).__init__(**kwargs)
        if type(size) == int:
            size = (size, size)
        self.size = size

    def call(self, inputs, output_shape=None):
        """ Implemented from https://github.com/tensorflow/tensorflow/issues/2169

        Args:
            inputs: layer inputs, where the input[0] is the data to upsample
                and inputs[1] is the mask with the indices returned by MaxPoolingWithArgmax2D.
            output_shape: layer output shape.

        Returns:
            Upsampled inputs.
        """
        updates, mask = inputs[0], inputs[1]
        with K.tf.variable_scope(self.name):
            mask = K.cast(mask, 'int32')
            input_shape = K.tf.shape(updates, out_type='int32')
            # compute the new shape
            if output_shape is None:
                output_shape = (input_shape[0], input_shape[1] * self.size[0],
                                input_shape[2] * self.size[1], input_shape[3])

            # compute the indices for batch, height, width and feature maps
            one_like_mask = K.ones_like(mask, dtype='int32')
            batch_shape = K.concatenate([[input_shape[0]], [1], [1], [1]], axis=0)
            batch_range = K.reshape(K.tf.range(output_shape[0], dtype='int32'), shape=batch_shape)
            b = one_like_mask * batch_range
            y = mask // (output_shape[2] * output_shape[3])
            x = (mask // output_shape[3]) % output_shape[2]
            feature_range = K.tf.range(output_shape[3], dtype='int32')
            f = one_like_mask * feature_range

            # transpose indices & reshape update values to one dimension
            updates_size = K.tf.size(updates)
            indices = K.transpose(K.reshape(K.stack([b, y, x, f]), [4, updates_size]))
            values = K.reshape(updates, [updates_size])
            ret = K.tf.scatter_nd(indices, values, output_shape)
            return ret

    def compute_output_shape(self, input_shape):
        """ Computes the output shape for this layer with regards to the input shape.

        Args:
            input_shape: input's shape.

        Returns:
            Layer's output shape.
        """
        mask_shape = input_shape[1]
        return mask_shape[0], mask_shape[1] * self.size[0], mask_shape[2] * self.size[1], mask_shape[3]
