##  ISPRS - Dataset
### [2D Semantic Labeling Contest](http://www2.isprs.org/commissions/comm3/wg4/semantic-labeling.html)
Two airborne image datasets, consisting of very high resolution treue ortho photo (TOP) tiles and
corresponding digital surface models (DSMs - [def](https://en.wikipedia.org/wiki/Digital_elevation_model))
derived from dense image matching techniques. Both areas cover urban scenes. While Vaihingen is a 
relatively small village with many detached buildings and small multi story buildings, Potsdam shows a 
typical historic city with large building blocks, narrow streets, and structures.

Each dataset has been classified manually into six common land cover classes. Groundtruth is available for 
half of the dataset while the remaining annotations have not been released for evaluation fairness purposes.
Participants shall use all data with ground truth for training or internal evaluation of their method.

|       Classes       |  R  |  G  |  B  |
|:-------------------:|:---:|:---:|:---:|
| Impervious surfaces | 255 | 255 | 255 |
|       Building      |  0  |  0  | 255 |
|    Low vegetation   |  0  | 255 | 255 |
|         Tree        |  0  | 255 |  0  |
|         Car         | 255 | 255 |  0  |
|  Clutter/background | 255 |  0  |  0  |

The clutter/background class includes water bodies (a river section visible in only two images) and other 
objects that look very different from everything else (e.g., containers, tennis courts, swimming pools) and
 that are usually not of interest in semantic object classification in urban scenes. Please note that
 participants must submit labels for all classes (including the clutter/background class). For instance,
 it is not possible to submit only classification results for the category building.

DSM are one-band 32-bit floating points values TIFF files. They contain height values.

### [Vaihingen data](http://www2.isprs.org/commissions/comm3/wg4/2d-sem-label-vaihingen.html)
The dataset contains 33 images (of different sizes), the ground sampling distance of both, the TOP and the 
DSM, is 9 cm. The TOP are 8 bit TIFF files with three bands: near infrared, red and green.

| area | Nb - col | Nb - row | ground truth |
|:----:|:--------:|:--------:|:------------:|
| 1    | 1919     | 2569     |       x      |
| 2    | 2428     | 2767     |       -      |
| 3    | 2006     | 3007     |       x      |
| 4    | 1887     | 2557     |       -      |
| 5    | 1887     | 2557     |       x      |
| 6    | 1887     | 2557     |       -      |
| 7    | 1887     | 2557     |       x      |
| 8    | 1887     | 2557     |       -      |
| 10   | 1887     | 2557     |       -      |
| 11   | 1893     | 2566     |       x      |
| 12   | 1922     | 2575     |       -      |
| 13   | 2818     | 2558     |       x      |
| 14   | 1919     | 2565     |       -      |
| 15   | 1919     | 2565     |       x      |
| 16   | 1919     | 2565     |       -      |
| 17   | 2336     | 1281     |       x      |
| 20   | 1866     | 2315     |       -      |
| 21   | 1903     | 2546     |       x      |
| 22   | 1903     | 2546     |       -      |
| 23   | 1903     | 2546     |       x      |
| 24   | 1903     | 2546     |       -      |
| 26   | 2995     | 1783     |       x      |
| 27   | 1917     | 3313     |       -      |
| 28   | 1917     | 2567     |       x      |
| 29   | 1917     | 2563     |       -      |
| 30   | 1934     | 2563     |       x      |
| 31   | 1980     | 2555     |       -      |
| 32   | 1980     | 2555     |       x      |
| 33   | 1581     | 2555     |       -      |
| 34   | 1388     | 2555     |       x      |
| 35   | 2805     | 1884     |       -      |
| 37   | 1996     | 1995     |       x      |
| 38   | 3816     | 2550     |       -      |

### [Potsdam data](http://www2.isprs.org/commissions/comm3/wg4/2d-sem-label-potsdam.html)
The dataset contains 38 images (all images have dimension 6000 x 6000), the ground sampling distance of 
both, the TOP and the DSM, is 5 cm. The TOP are 8 bit TIFF files with three or four bands: IRRG (IR-R-G), 
RGB(R-G-B) and RGBIR (R-G-B-IR).


| area | ground truth |
|:----:|:------------:|
| 2 10 |       x      |
| 2 11 |       x      |
| 2 12 |       x      |
| 2 13 |       -      |
| 2 14 |       -      |
| 3 10 |       x      |
| 3 11 |       x      |
| 3 12 |       x      |
| 3 13 |       -      |
| 3 14 |       -      |
| 4 10 |       x      |
| 4 11 |       x      |
| 4 12 |       x      |
| 4 13 |       -      |
| 4 14 |       -      |
| 4 15 |       -      |
| 5 10 |       x      |
| 5 11 |       x      |
| 5 12 |       x      |
| 5 13 |       -      |
| 5 14 |       -      |
| 5 15 |       -      |
| 6 07 |       x      |
| 6 08 |       x      |
| 6 09 |       x      |
| 6 10 |       x      |
| 6 11 |       x      |
| 6 12 |       x      |
| 6 13 |       -      |
| 6 14 |       -      |
| 6 15 |       -      |
| 7 07 |       x      |
| 7 08 |       x      |
| 7 09 |       x      |
| 7 10 |       x      |
| 7 11 |       x      |
| 7 12 |       x      |
| 7 13 |       -      |
