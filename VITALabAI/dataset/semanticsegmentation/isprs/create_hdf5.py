"""
create_hdf5.py
author: Charles Authier (charles.authier@usherbrooke.ca)
date: 23/09/2018

Create a hdf5 fil for all images given, in the dimension given.
"""
from skimage import io
from skimage.util.shape import view_as_windows
from os import listdir
from os.path import isfile, join, exists
from tqdm import tqdm
from VITALabAI.utils.utils import natural_order

import argparse
import h5py
import numpy as np
import re


def generate_list_directory(path):
    """ Generate the list of image from the path. """
    if not exists(path):
        return []
    path = sorted([f for f in listdir(path) if (isfile(join(path, f)) and f.endswith(".tif"))],
                  key=natural_order)
    return path


def extract_number(name):
    """ Extract all numbers in the string(name) and
    convert a single number into a double.
    5 -> 05
    """
    return ['{:0>2}'.format(i) for i in re.findall(r'[0-9]+', name)]


def good_name(l, answer):
    """ This method searches for a name with the same number then the answer.

    Args:
        l: list, List of string with the name, description of the content follows by numbers.
        answer: list, List of the images numbers.
    """
    return [i for i in l if np.array_equal(extract_number(i), answer)][0]


def same_img(gt_name, img_list, dsm_list, no_bound_list):
    """ This method finds all the images that match the given gt_name.

    Args:
        gt_name: str, Name of the image that we want to complete.
            (Gt because not all images have a gt and we want all images with a gt)
        img_list: list, List of all images name.
        dsm_list: list, List of all DSM images.
        no_bound_list: list, List of gt name with no boundary.

    Returns:
        The name of img, dsm and no boundary image that match the gt.
    """
    gt = extract_number(gt_name)
    img_name = good_name(img_list, gt)
    dsm_name = good_name(dsm_list, gt)
    no_bound_name = good_name(no_bound_list, gt)
    num = '_'.join(str(x) for x in gt)
    return gt_name, img_name, dsm_name, no_bound_name, num


def make_label(y):
    """This method swaps classes indices as follows :
    7 (no class) -> 1 (Impervious surfaces)
    6 (Clutter/background) -> 5 (Car)
    5 (Car) -> Null
    4 (Tree) -> 6 (Clutter/background)
    3 (Low vegetation) -> 3 (Low vegetation)
    2 (Building) -> 4 (Tree)
    1 (Impervious surfaces) -> 2 (Building)
    0 (boundary) -> 0 (boundary)

    Args:
        y: array, NxN array with values between 0 and 7.

    Returns:
        y_classified: array, NxN array with values between 0 and 6.
    """
    y_classified = np.copy(y)

    # value 0 and 3 don't need to be swaps
    y_classified[y == 7] = 1
    y_classified[y == 6] = 5
    y_classified[y == 4] = 6
    y_classified[y == 2] = 4
    y_classified[y == 1] = 2

    return y_classified


def gt2one_hot(arr_rgb):
    """This method converts an RGB GT into a one hot.

    Args:
        arr_rgb: array, An array of NxMx3 dimension, the third dimension appears like (255, 255, 255).
            Decimal equivalents (255, 255, 255)->(1, 1, 1)*(4, 2, 1)->4 + 2 + 1 = 7

    Returns:
        out: array, A one hot with the dimension NxMx6.
    """
    decimal = (arr_rgb == 255).dot([4, 2, 1])
    return make_label(decimal)


def tiles(img, x, y, overlap):
    """This method separates images in tiles of wanted dimension.
    Attention: if the dimension doesn't fit the original image,
    the number of tiles will be rounded down.

    Args:
        img: array, Images.
        x: int, Dimension wanted, x must be equal to y, to be a square.
        y: int, Dimension wanted, x must be equal to y, to be a square.
        overlap: float, Percentage of overlapping in decimal.

    Returns:
         out: array, An array with the dimension (M, X, Y, N),
            where M is the number of tiles and N the number of channels.
    """
    if img.ndim != 2:
        r = img.shape[-1]
        dimension = (x, y, r)
    else:
        r = 1
        dimension = (x, y)
    step = int(np.ceil(x * (1 - (overlap / 100))))
    tiles = view_as_windows(img, dimension, step=step)
    return np.resize(tiles, (tiles.shape[0] * tiles.shape[1], x, y, r))


def generate_dataset(img_path, dsm_path, bound_path, no_bound_path, name, x, y, overlap=0):
    """A method that generates each dataset, train, valid and test.

    Args:
        img_path: string, The path where we can find the images from the downloaded dataset.
        dsm_path: string, The path where we can find the DSM from the downloaded dataset.
        bound_path: string, The path where we can find the ground truth with boundary.
        no_bound_path: string, The path where we can find the ground truth without boundary.
        name: string, Name of the hdf5 file to generate.
        x: int, Dimension wanted, x must be equal to y, to be a square.
        y: int, Dimension wanted, x must be equal to y, to be a square.
        overlap: float, Percentage of overlapping in decimal.

    # Raises:
        ValueError: if dataset is not Potsdam or Vaihingen
    """
    # Images information
    if 'Potsdam' in img_path:
        nb_channels = 5
    elif 'Vaihingen' in img_path:
        nb_channels = 4
    else:
        raise ValueError(
            'The dataset {} may not be compatible with this model, try:'
            ' Potsdam or Vaihingen dataset'
        )
    # List all files in paths
    img_list = generate_list_directory(img_path)
    dsm_list = generate_list_directory(dsm_path)
    bound_list = generate_list_directory(bound_path)
    no_bound_list = generate_list_directory(no_bound_path)

    with h5py.File(name, "w") as h5f:
        # Create each group
        grp_train = h5f.create_group("train")
        grp_valid = h5f.create_group("valid")
        grp_test = h5f.create_group("test")

        mean_img = np.zeros(nb_channels)
        std_img = np.zeros(nb_channels)

        for c in np.arange(nb_channels):
            channel_l = []
            # For each image with a ground truth
            for name in bound_list:
                bound_name, img_name, dsm_name, no_bound_name, num = \
                    same_img(name, img_list, dsm_list, no_bound_list)
                # Read and adapt all image proprieties
                if c == nb_channels - 1:
                    img = io.imread(join(dsm_path, dsm_name))
                    channel_l.append(img.flatten())
                else:
                    img = io.imread(join(img_path, img_name))
                    channel_l.append(img[..., c].flatten())
            # Store information
            mean_img[c] = np.mean(np.concatenate(np.array(channel_l)))
            std_img[c] = np.std(np.concatenate(np.array(channel_l)))

        # For each images with a ground truth
        for name in tqdm(bound_list, desc='images'):
            bound_name, img_name, dsm_name, no_bound_name, num =\
                same_img(name, img_list, dsm_list, no_bound_list)

            # Read and adapt all image properties
            img = io.imread(join(img_path, img_name)).astype('float64')
            dsm = io.imread(join(dsm_path, dsm_name))
            # transform in one hot vector
            bound = gt2one_hot(
                io.imread(join(bound_path, bound_name))
            )
            no_bound = gt2one_hot(
                io.imread(join(no_bound_path, no_bound_name))
            )
            # normalization - colors
            for i in np.arange(nb_channels - 1):
                img[..., i] = (img[..., i] - mean_img[i]) / std_img[i]
            # normalization - dsm
            dsm = (dsm - mean_img[-1]) / std_img[-1]

            # Create Tiles from images
            img = tiles(img, x, y, overlap)
            dsm = tiles(dsm, x, y, overlap)
            bound = tiles(bound, x, y, overlap)
            no_bound = tiles(no_bound, x, y, overlap)

            # Set the random to dispatch
            np.random.seed(2003)
            idx = np.random.permutation(img.shape[0])
            # Dispatch images, 25% in test, 25% in validation and 50% in train
            idx_test, idx_val, idx_train =\
                np.split(idx, [len(idx) // 4, len(idx) // 2])

            # Training samples
            for i in tqdm(idx_train, desc='train'):
                d = np.concatenate(  # (img, dsm, gt, gt_no_boundary)
                    (img[i], dsm[i], bound[i], no_bound[i]), axis=2
                )
                grp_train.create_dataset(  # NumOfImage_NumOfTile
                    "{num}_{index}".format(num=num, index=str(i).zfill(3)),
                    data=d
                )

            # Validation samples
            for i in tqdm(idx_val, desc='validation'):
                d = np.concatenate(  # (img, dsm, gt, gt_no_boundary)
                    (img[i], dsm[i], bound[i], no_bound[i]), axis=2
                )
                grp_valid.create_dataset(  # NumOfImage_NumOfTile
                    "{num}_{index}".format(num=num, index=str(i).zfill(3)),
                    data=d
                )

            # Testing samples
            for i in tqdm(idx_test, desc='test'):
                d = np.concatenate(  # (img, dsm, gt, gt_no_boundary)
                    (img[i], dsm[i], bound[i], no_bound[i]), axis=2
                )
                grp_test.create_dataset(  # NumOfImage_NumOfTile
                    "{num}_{index}".format(num=num, index=str(i).zfill(3)),
                    data=d
                )


def parse_args():
    """ Get the args to create the dataset.

    Returns:
        args

    """
    aparser = argparse.ArgumentParser()
    aparser.add_argument("images_path", help="Path with the images", type=str)
    aparser.add_argument("dsm_path", help="Path with the DSMs", type=str)
    aparser.add_argument("bound_path", help="Path with the ground truth with boundary", type=str)
    aparser.add_argument("no_bound_path", help="Path with the ground truth with no boundary", type=str)
    aparser.add_argument(
        "--destination_path", type=str, dest="dest_path", help="Path to store the database in hdf5", default=''
    )
    aparser.add_argument(
        "--dimension", type=int, dest="dim", help="Dimension of the images who will store", default=224
    )
    aparser.add_argument("--overlap", type=int, dest="overlap", help="Percentage of Overlap between images", default=0)
    args = aparser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    if args.images_path.find('Vaihingen') != -1:
        city = 'Vaihingen'
    elif args.images_path.find('Potsdam') != -1:
        city = 'Potsdam'
    else:
        raise ValueError('The dataset may not be compatible.')

    out_file = join(
        args.dest_path,
        "{c}_d{d}_o{o}.h5".format(c=city, d=args.dim, o=str(args.overlap))
    )

    generate_dataset(
        args.images_path, args.dsm_path, args.bound_path, args.no_bound_path, out_file,
        args.dim, args.dim, overlap=args.overlap
    )
