# -*- coding: utf-8 -*-
"""
preprocess.py
author: clement zotti (clement.zotti@usherbrooke.ca)
"""

import numpy as np
from keras.utils import to_categorical

from VITALabAI.utils.dataset import centered_resize


class PreProcessing(object):
    """This is the basic class for all preprocessing classes."""
    name = None

    def __call__(self, data):
        """ All PreProcessing subclasses must override this method.
        This is called by the prediction part of this pipeline.

        Args:
            data: ndarray, Data to preprocess.

        Returns:
            ndarray data where preprocessing is applied

        # Raises:
            NotImplementedError
        """
        raise NotImplementedError("Abstract class.")


class PreQuantilePercent(PreProcessing):
    """Quantile normalization class."""
    name = 'quantile'

    def __init__(self, percent=96):
        """Initializer.

        Args:
            percent: int, Above this percent the data are considered outliers.
        """
        self.percent = percent

    def __call__(self, data):
        """ Processing the percentile normalization.
        Set all outlier values higher than a given percentage to the highest
        acceptable value.

        Args:
            data: ndarray, numpy array data to apply the preprocessing.

        Returns:
            ndarray data without outlier
        """
        tresh = np.percentile(data, self.percent)
        idx = data > tresh
        data[idx] = data.min()
        data[idx] = data.max()
        return data


class PreProcessSTD(PreProcessing):
    """Standard deviation normalization class. """
    name = 'std'

    def __call__(self, data):
        """Processing the standard deviation normalization.

        Args:
            data: ndarray, Data to preprocess.

        Returns:
            ndarray data preprocessed
        """

        # If data are all zeros
        if np.all(data == 0):
            return data

        data = data.astype(np.float32)
        data = data - data.mean()

        std = data.std()
        # Avoid division by zero
        if std < 1e-5:
            return data

        return data / std


class PreProcessNonZeroSTD(PreProcessing):
    """ Non zero standard deviation normalization class. """
    name = 'nonZeroSTD'

    def __call__(self, data):
        """ Processing the standard deviation normalization, ignoring the 0s.

        Args:
            data: ndarray, Data to preprocess.

        Returns:
            ndarray data preprocessed
        """

        non_zeros = data != 0

        data[non_zeros] = PreProcessSTD()(data[non_zeros])

        return data


class PreGammaCorrection(PreProcessing):
    """Apply a gamma correction for each slices in the 3D volume.
    """
    name = 'GammaCorrection'

    def __init__(self, min=0.5, max=1.5):
        self.min = min
        self.max = max
        self.rng = np.random.RandomState(1337)

    def __call__(self, data):
        """ Preprocessing the data with a gamma correction for every slices.

        Args:
            data: ndarray, Data to process.

        Returns:
            ndarray of the data processed
        """
        # Get the gammas from a uniform distribution
        gammas = self.rng.uniform(self.min, self.max,
                                  data.shape[0])[:, np.newaxis, np.newaxis]
        return np.power(data, gammas)


class PreProcessResizeSeg(PreProcessing):
    """ Resize segmentation map
    """
    name = 'resizeSeg'

    def __init__(self, size, num_classes):
        super().__init__()
        self.size = (size, size) if type(size) == int else size
        self.num_classes = num_classes

    def __call__(self, data):
        """ Resize the segmentation map.

        Args:
            data: ndarray, 3d segmentation map to resize.

        Returns:
            resized segmentation map
        """

        pred_resized = to_categorical(data, self.num_classes)
        pred_resized = centered_resize(pred_resized, self.size)

        # Need to redo the background class due to resize
        # that set the image border to 0
        if pred_resized.ndim == 4:
            summed = np.clip(pred_resized[:, :, :, 1:].sum(axis=-1), 0, 1)
            pred_resized[:, :, :, 0] = np.abs(1 - summed)
        elif pred_resized.ndim == 3:
            summed = np.clip(pred_resized[:, :, 1:].sum(axis=-1), 0, 1)
            pred_resized[:, :, 0] = np.abs(1 - summed)

        return pred_resized.argmax(axis=-1)


class PreProcessRescaleIntensity(PreProcessing):
    """Intensity normalization between [0, 1] """
    name = 'rescaleIntensity'

    def __call__(self, data):
        """Rescale intensity to [0,1]

        Args:
            data: ndarray, Data to preprocess.

        Returns:
            ndarray data preprocessed
        """

        # If data are all zeros
        if np.all(data == 0):
            return data

        data = data.astype(np.float32)
        data = data - data.min()

        # Avoid division by zero
        if data.max() < 1e-5:
            return data

        return data / data.max()
