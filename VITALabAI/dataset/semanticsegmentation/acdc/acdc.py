import h5py
import numpy as np
from keras.utils import Sequence

from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract
from VITALabAI.utils.dataset import centered_resize


class VITALAcdc(VITALabAiDatasetAbstract):
    """Wrapper for the ACDC dataset.
    """
    TRAIN_KEY = 'train'
    VAL_KEY = 'valid'
    TEST_KEY = 'test'
    PRIOR_KEY = 'prior'

    def __init__(self, path, input_size: int = 256, batch_size=10, seed=None, augmentation_transformer=None,
                 use_prior=False, preproc_fn=()):
        """
        Args:
            path: string, path to the hdf5 file to read.
            input_size: int, Size of the image.
            batch_size: int, Size of the batches to return.
            seed: object, seed for the permutations.
            augmentation_transformer: AugmentationTransformer to perform online data augmentation.
            use_prior: bool, whether to use a prior with data in each batch.
            preproc_fn: preprocessing functions to be applied to each batch.
        """
        self.input_size = input_size
        self.batch_size = batch_size
        self.path = path
        self.nb_classes = 4
        self.augmentation_transformer = augmentation_transformer
        self.use_prior = use_prior
        self.preproc_fn = preproc_fn

        np.random.seed(seed)

        self.prior = None
        self.train_list, self.val_list, self.test_list = self.load_data()

    def get_classes(self):
        return list(range(self.nb_classes))

    def get_input_shape(self):
        return (self.input_size, self.input_size, 1)

    def load_data(self):
        """ List of images from the HDF5 file and save them in 3 set lists.

        Returns:
            tuple of list containing path to files for each set
        """

        def get_set_list(set_key, file):
            set_list = []
            f_set = file[set_key]
            for key in list(f_set.keys()):
                patient = f_set[key]
                img = patient['img']
                gt = patient['gt']

                assert img.shape[0] == gt.shape[0]

                for i in range(img.shape[0]):
                    k = '{}/{}'.format(set_key, key)
                    set_list.append((k, i))
            return set_list

        with h5py.File(self.path, 'r') as f:
            train_list = get_set_list(self.TRAIN_KEY, f)
            train_list = np.random.permutation(train_list)

            val_list = get_set_list(self.VAL_KEY, f)
            test_list = get_set_list(self.TEST_KEY, f)

            if self.use_prior:
                self.prior = np.array(f['prior'])

        return train_list, val_list, test_list

    def get_train_set(self):
        return ACDCSequence(self.train_list, self.path, self.input_size, self.batch_size, prior=self.prior,
                            augmentation_transformer=self.augmentation_transformer,
                            preproc_fn=self.preproc_fn)

    def get_validation_set(self):
        return ACDCSequence(self.val_list, self.path, self.input_size, self.batch_size, prior=self.prior,
                            preproc_fn=self.preproc_fn)

    def get_test_set(self):
        return ACDCSequence(self.test_list, self.path, self.input_size, self.batch_size, prior=self.prior,
                            preproc_fn=self.preproc_fn)

    def get_set_for_prediction(self, set):
        return ACDCTestSequence(set, self.path, self.input_size, self.prior, self.preproc_fn)


class ACDCTestSequence(Sequence):
    """ Sequence of images for Keras.
        This sequence does not use a shuffled list, instead this sequence returns one patient at a time for
        predicting and saving results.
    """

    def __init__(self, set, file_path, input_size, prior, preproc_fn=()):
        """
        Args:
            set: string, which set to load ('train', 'validation', 'test')
            file_path: string, path to the hdf5 dataset file
            input_size: int, size of the image
            prior: array, prior
            preproc_fn: preprocessing functions to be applied on the data
        """
        self.file_path = file_path
        self.input_size = input_size
        self.preproc_fn = preproc_fn
        self.prior = prior
        self.set = set

        with h5py.File(self.file_path, 'r') as f:
            self.key_list = list(f[self.set].keys())

    def __len__(self):
        return int(np.floor(len(self.key_list)))

    def __getitem__(self, idx):
        """This methos loads are returns slices from one patient.

        Args:
            idx: int, Index of the sequence

        Returns:
            img_name: string, name of the lamb in the dataset
            x: array, Array of slices
            y: array, Array of gt
            prior: array, if prior is Used
            voxel_size: list, dimensions of the voxels for the lamb

        """
        img_name = self.key_list[idx]
        key = '{}/{}'.format(self.set, img_name)

        with h5py.File(self.file_path, 'r') as f:
            img, gt = get_data(key, f)
            voxel_size = f[key].attrs.get('voxel_size', [1, 1, 1])

        img = preprocess_channel(img, self.preproc_fn)

        img = centered_resize(img, (self.input_size, self.input_size))
        gt = centered_resize(gt, (self.input_size, self.input_size))

        # Need to redo the background class due to resize
        # that set the image border to 0
        summed = np.clip(gt[:, :, :, 1:].sum(axis=-1), 0, 1)
        gt[:, :, :, 0] = np.abs(1 - summed)

        if self.prior is not None:
            prior = resampling_slices(img, self.prior)
            return img_name, img, gt, prior, voxel_size

        return img_name, img, gt, voxel_size


class ACDCSequence(Sequence):
    """ Sequence of images for Keras."""

    def __init__(self, key_list, file_path, input_size, batch_size, prior, augmentation_transformer=None,
                 preproc_fn=()):
        """
        Args:
            key_list: list, list of pairs of keys and idexes for each slice
            file_path: string, path to the hdf5 dataset file
            input_size: int, size of the image
            batch_size: int, Batch size to stack the images
            prior: array, prior
            augmentation_transformer: object, to be applied on batches for online data augmentation
            preproc_fn: preprocessing functions to be applied on the data
        """
        self.key_list = key_list
        self.batch_size = batch_size
        self.file_path = file_path
        self.input_size = input_size
        self.augmentation_transformer = augmentation_transformer
        self.preproc_fn = preproc_fn
        self.prior = prior

    def __len__(self):
        return int(np.ceil(len(self.key_list) / self.batch_size))

    def __getitem__(self, idx):
        """This methos loads are returns slices from one patient.

        Args:
            idx: int, Index of the sequence

        Returns:
            x: array, Array of slices
            y: array, Array of gt
            prior: array, if prior is Used
        """
        keys = self.key_list[idx * self.batch_size:(idx + 1) * self.batch_size]

        x = []
        y = []
        priors = []

        with h5py.File(self.file_path, 'r') as f:
            for key, i in keys:
                img, gt = get_data(key, f)

                if self.prior is not None:
                    prior = resampling_slices(img, self.prior)
                    prior = prior[int(i)]
                    priors.append(prior)

                img = img[int(i)]
                img = preprocess_channel(np.copy(img), self.preproc_fn)

                gt = gt[int(i)]

                img = centered_resize(img, (self.input_size, self.input_size))
                gt = centered_resize(gt, (self.input_size, self.input_size))

                # Need to redo the background class due to resize
                # that set the image border to 0
                summed = np.clip(gt[:, :, 1:].sum(axis=-1), 0, 1)
                gt[:, :, 0] = np.abs(1 - summed)

                x.append(img)
                y.append(gt)

        if self.augmentation_transformer is not None:
            self.augmentation_transformer.apply_on_lists(x, y)

        if self.prior is not None:
            return np.array(x), np.array(y), np.array(priors)
        else:
            return np.array(x), np.array(y)


def get_data(key, file):
    img = np.array(file['{}/img'.format(key)])
    gt = np.array(file['{}/gt'.format(key)])
    return img, gt


def preprocess_channel(img, preproc_fn):
    """This method preprocesses an image by channel.

    Args:
        img: hdf5 data, img in 3d like this ('s', 0, 1, 'c'):
                - 's': number of slices.
                - 0, 1: x and y dim.
                - 'c': number of channels.
        preproc_fn: list of functions used for preprocessing

    Returns:
        ndarray of the image with preprocesses applied on each channel.
    """

    # Convert the image in float32 (GPU limitation)
    img = np.array(img).astype(np.float32)

    # Preprocessing over each channel
    for c in range(img.shape[-1]):
        for pre in preproc_fn:
            img[..., c] = pre(img[..., c])

    return img


def resampling_slices(img, prior):
    """This method is doing a linear interpolation along the first axis of the img ndarray.

    Args:
        img: ndarray, The 4D ground truth image
        prior: ndarray, The 4D prior shape

    Returns:
        ndarray of the prior shape mapped into the img space.
    """
    space = np.linspace(0, prior.shape[0] - 1, num=img.shape[0])
    return prior[space.astype(np.int32)]


if __name__ == '__main__':
    from matplotlib import pyplot as plt
    import argparse

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the dataset", type=str, required=True)
    args = aparser.parse_args()

    ds = VITALAcdc(path=args.path, batch_size=1, use_prior=True)
    sequence = ds.get_train_set()

    img, gt, prior = sequence[0]

    img = img.squeeze()
    gt = gt.squeeze()
    prior = prior.squeeze()

    seg = np.argmax(gt, axis=-1)
    prior = np.argmax(prior, axis=2)

    img = img.squeeze()

    f, (ax1, ax2, ax3) = plt.subplots(1, 3)
    ax1.imshow(img)
    ax2.imshow(seg)
    ax3.imshow(prior)
    plt.show(block=False)

    plt.figure(2)
    plt.imshow(img, cmap='gray')
    plt.imshow(seg, alpha=0.2)
    plt.show()
