"""
augmentation_transformer.py
author: Laurence Fournier
"""

from random import randint

import matplotlib.pyplot as plt
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
from skimage.morphology import binary_dilation, disk


class AugmentationTransformer:
    """ Class to augment acdc dataset batches using random transformations on each image and segmentation
    classes.
    """

    def __init__(self, rotation_range=60, width_shift_range=0.1, height_shift_range=0.1, zoom_range=0.5,
                 dilation_range=2):
        """
        Args:
            rotation_range: maximum rotation in degrees (clockwise or counter clockwise from vertical)
            width_shift_range: maximum horizontal shift proportion (left or right)
            height_shift_range: maximum vertical shift proportion (down or up)
            zoom_range: maximum maximum zoom out proportion (in or out)
            dilation_range: maximum dilation width in pixels
        """

        self.transformer = ImageDataGenerator(
            fill_mode="constant",
            cval=0,
            rotation_range=rotation_range,
            width_shift_range=width_shift_range,
            height_shift_range=height_shift_range,
            zoom_range=zoom_range,
            data_format="channels_last"
        )

        self.dilation_range = dilation_range

    def apply(self, x, y):
        """ Transform a image and its segmentation.

        Args:
            x: image (ndarray of shape (_,_,1)
            y: segmentation classes (ndarray of shape (_,_,4))

        Returns:
             transformed image and the transformed segmentation classes
        """

        assert y.sum() == y.shape[0] * y.shape[1], "Segmentation classes should be mutually exclusive"

        # Draw a random rotation and shift transformation
        transform = self.transformer.get_random_transform(x.shape)

        # cval = 0 is a valid background for the image and most segmentation classes (except the background class)
        xt = self.transformer.apply_transform(x, transform)
        segmentation_classes = self.transformer.apply_transform(y[:, :, 1:4], transform)

        # Compute the background class as the complement of other class (this fix a bug where some pixels had no class)
        background = 1 - segmentation_classes.any(2)[:, :, None]
        yt = np.concatenate((background, segmentation_classes), axis=2)

        assert yt.sum() == yt.shape[0] * yt.shape[1], "Segmentation classes should be mutually exclusive"

        # Draw random dilation sizes (dil) for lv, myo and rv such that
        # 0 <= dil_lv <= dil_myo <= dil_rv <= dilation_range
        # This way, no class will disappear if we subtract from its dilation the dilatations of the other classes.
        dil_rv = randint(0, self.dilation_range)
        dil_myo = randint(0, dil_rv)
        dil_lv = randint(0, dil_myo)

        # Construct dilation elements
        element_rv = disk(dil_rv, dtype=bool)
        element_myo = disk(dil_myo, dtype=bool)
        element_lv = disk(dil_lv, dtype=bool)

        background, rv, myo, lv = np.rollaxis(yt.astype(bool), 2)

        # Apply dilations and substractions
        lv = binary_dilation(lv, element_lv)
        myo = np.logical_and.reduce((binary_dilation(myo, element_myo), np.logical_not(lv)))
        rv = np.logical_and.reduce((binary_dilation(rv, element_rv), np.logical_not(myo), np.logical_not(lv)))

        background = np.logical_not(np.logical_or.reduce((lv, myo, rv)))
        yt = np.stack((background, rv, myo, lv), axis=2).astype(int)

        assert yt.sum() == yt.shape[0] * yt.shape[1], "Segmentation classes should be mutually exclusive"

        return xt, yt

    def apply_on_lists(self, xs, ys):
        """ Like apply but on the list of images and the list of segmentation classes.

        Args:
            xs: list of images
            ys: list of segmentation classes

        Returns:
            the list of transformed images and the list of transformed segmentation classes
        """
        return map(list, zip(*map(self.apply, xs, ys)))

    @staticmethod
    def _visualize_transformation(x, xt, y, yt):
        """ Utility function to visualize image and classes before or after transformation when debugging.

        Args:
            x: image
            xt: image after transformation
            y: classes
            yt: image after transformation
        """

        x = x[:, :, 0]
        background, rv, myo, lv = np.rollaxis(y, 2)
        sum = y.sum(axis=2)
        sumclass = y[:, :, 1:4].sum(axis=2)

        xt = xt[:, :, 0]
        background_t, rv_t, myo_t, lv_t = np.rollaxis(yt, 2)
        sum_t = y.sum(axis=2)
        sumclass_t = y[:, :, 1:4].sum(axis=2)

        plt.figure(figsize=(16, 12))

        ax = None
        for name, i in zip(['x', 'myo', "lv", "rv", "sumclass", "background", "sum"],
                           [1, 7, 8, 9, 13, 14, 15]):
            ax = plt.subplot(3, 6, i, sharex=ax, sharey=ax)
            ax.set_title(name)
            plt.imshow(eval(name))

        ax = None
        for name, i in zip(['xt', 'myo_t', "lv_t", "rv_t", "sumclass_t", "background_t", "sum_t"],
                           [4, 10, 11, 12, 16, 17, 18]):
            ax = plt.subplot(3, 6, i, sharex=ax, sharey=ax)
            ax.set_title(name)
            plt.imshow(eval(name))

        plt.show()
