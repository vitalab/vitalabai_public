import argparse
import os
from glob import glob
from os.path import basename

import h5py
import nibabel as nib
import numpy as np
from natsort import natsorted
from scipy import ndimage
from scipy.ndimage.interpolation import rotate

from VITALabAI.utils.dataset import centered_resize

try:
    from tqdm import tqdm
except ImportError as ie:
    print("The package 'tqdm' is not installed, "
          "if you want a progress bar install it.")

    def tqdm(iterable, *args, **kwargs):
        """ Mock function """
        return iterable

PRIOR_SIZE = 100
PRIOR_HALF_SIZE = PRIOR_SIZE // 2

ROTATIONS = [-60, -45, -15, 0, 15, 45, 60]


def generate_list_directory(path):
    """ Generate the list of nifti images from the path.

    Args:
        path: string, path to nifti images
    """
    if not os.path.exists(path):
        return []

    path = os.path.join(path, '*', '*')
    paths = natsorted(glob(path))
    paths = [path for path in paths if not path.endswith("Info.cfg")]
    paths = [path for path in paths if path.find('_4d') < 0]
    return paths


def _to_categorical(matrix, nb_classes):
    """ Transform a matrix containing interger label class into a matrix containing categorical class labels.
    The last dim of the matrix should be the category (classes).

    Args:
        matrix: ndarray, A numpy matrix to convert into a categorical matrix.
        nb_classes: int, number of classes

    Returns:
        A numpy array representing the categorical matrix of the input.
    """
    return matrix == np.arange(nb_classes)[np.newaxis, np.newaxis, np.newaxis, :]


def _mass_center(imgs):
    """ Function to extract the center of masses of a 3D ground truth image.

    Args:
        imgs: images
    """
    centers = np.array([ndimage.measurements.center_of_mass(img[:, :, 3])
                        for img in imgs])
    # Need to fix the Nan when the ground truth slice is a slice of zeros.
    # Set it to center 256 // 2 = 128
    centers[np.isnan(centers)] = 128
    return centers.astype(np.int16)


def _generate_centered_prob_map(image, shape, center, label):
    """ Function to extract the information from the ground truth image given the centers.

    Args:
        image: np.array, Numpy array of the ground truth.
        shape: np.array, Shape of the desired prior image.
        center: np.array, Numpy array of shape (slices, 2).
        label: int, Which label to extract from the ground truth.

    Returns:
        Array of shape (slices, 100, 100, 1)
    """
    res = np.zeros(shape)
    # Nearest neightbour slice index between the number of slice
    # of the image and the ground truth
    space = np.linspace(0, shape[0] - 1, num=image.shape[0]).astype(np.int32)
    for i, (s, c) in enumerate(zip(space, center)):
        res[s] += image[i, c[0] - PRIOR_HALF_SIZE:c[0] + PRIOR_HALF_SIZE,
                        c[1] - PRIOR_HALF_SIZE:c[1] + PRIOR_HALF_SIZE,
                        label:label + 1]
    return res


def generate_probability_map(h5f, group):
    """ Generate the probability map from all unrotated training exemples.

    Args:
    h5f: hdf5 File, Handle of the hdf5 file containing all the dataset.
    group: hdf5 File, Group where to create the prior shape (train, valid or test) train should be the
        default.
    """
    keys = [key for key in group.keys() if key.endswith("_0")]
    images = [centered_resize(group[k]['gt'][:], (256, 256)) for k in keys]
    images_center = [_mass_center(img) for img in images]

    prior_shape = np.array([15, PRIOR_SIZE, PRIOR_SIZE, 1])

    label0 = np.array([
        _generate_centered_prob_map(np.copy(img), prior_shape, center, 0)
        for img, center in tqdm(zip(images, images_center),
                                desc='Background', total=len(images))])
    label0 = label0.sum(axis=0)

    label1 = np.array([
        _generate_centered_prob_map(np.copy(img), prior_shape, center, 1)
        for img, center in tqdm(zip(images, images_center),
                                desc='Right ventricle', total=len(images))])
    label1 = label1.sum(axis=0)

    label2 = np.array([
        _generate_centered_prob_map(np.copy(img), prior_shape, center, 2)
        for img, center in tqdm(zip(images, images_center),
                                desc='Myocardium', total=len(images))])
    label2 = label2.sum(axis=0)

    label3 = np.array([
        _generate_centered_prob_map(np.copy(img), prior_shape, center, 3)
        for img, center in tqdm(zip(images, images_center),
                                desc='Left ventricle', total=len(images))])
    label3 = label3.sum(axis=0)

    p_img = np.concatenate((label0, label1, label2, label3), axis=-1)
    p_img /= p_img.sum(axis=-1, keepdims=True).astype(np.float32)
    h5f.create_dataset('prior', data=p_img[:, :, :, 1:])


def create_database_structure(group, data_augmentation, data_ed,
                              gt_ed, data_es, gt_es):
    """ Create the dataset for the End-Systolic and End-Diastolic phases and if some data augmentation is
    involve we create also the rotation for each phase.

    Args:
        group: hdf5 Group, Group where we add each image by its name and the rotation associated to it.
        data_augmentation: bool, Enable/Disable data augmentation.
        data_ed: string, Path of the nifti diastolic MRI image.
        gt_ed: string, Path of the nifti diastolioc MRI segmentation of data_ed.
        data_es: string, Path of the nifti systolic MRI image.
        gt_es: string, Path of the nifti systolic MRI segmentation of data_ed.

    """
    p_name = data_ed.split(os.path.sep)[-2]

    ni_img = nib.load(data_ed)

    ed_img = ni_img.get_data().astype(np.float32)
    ed_img = ed_img.transpose(2, 0, 1)[..., np.newaxis]

    if gt_ed:
        ni_img = nib.load(gt_ed)
        edg_img = ni_img.get_data()
        edg_img = edg_img.transpose(2, 0, 1)[..., np.newaxis]
        edg_img = _to_categorical(edg_img, 4).astype(np.uint8)

    ni_img = nib.load(data_es)
    es_img = ni_img.get_data().astype(np.float32)
    es_img = es_img.transpose(2, 0, 1)[..., np.newaxis]

    if gt_es:
        ni_img = nib.load(gt_es)
        esg_img = ni_img.get_data()
        esg_img = esg_img.transpose(2, 0, 1)[..., np.newaxis]
        esg_img = _to_categorical(esg_img, 4).astype(np.uint8)

    if data_augmentation:
        iterable = ROTATIONS
    else:
        iterable = [0, ]

    for rot in iterable:
        r_name = "{}_ED_{}".format(p_name, rot)
        patient = group.create_group(r_name)
        patient.attrs['voxel_size'] = ni_img.header.get_zooms()

        # ED gate with gt
        r_img = rotate(ed_img, rot, axes=(1, 2), reshape=False)
        r_img = np.clip(r_img, ed_img.min(), ed_img.max())
        r_img[np.isclose(r_img, 0.)] = 0.
        patient.create_dataset('img', data=r_img)

        if gt_ed:
            r_img = rotate(edg_img, rot, axes=(1, 2), output=np.uint8,
                           reshape=False)
            patient.create_dataset('gt', data=r_img)

        r_name = "{}_ES_{}".format(p_name, rot)
        patient = group.create_group(r_name)

        # ES gate with gt
        r_img = rotate(es_img, rot, axes=(1, 2), reshape=False)
        r_img = np.clip(r_img, es_img.min(), es_img.max())
        r_img[np.isclose(r_img, 0.)] = 0.
        patient.create_dataset('img', data=r_img)
        patient.attrs['voxel_size'] = ni_img.header.get_zooms()

        if gt_ed:
            r_img = rotate(esg_img, rot, axes=(1, 2), output=np.uint8,
                           reshape=False)
            patient.create_dataset('gt', data=r_img)


def generate_dataset(path, name, data_augmentation=False):
    """ Function that generates each dataset, train, valid and test.

    Args:
        path: string, Path where we can find the images from the downloaded ACDC challenge.
        name: string, Name of the hdf5 file to generate.
        data_augmentation: bool, Enable/Disable data augmentation.

    # Raises:
        ValueError if names don't match
    """
    if data_augmentation:
        print("Data augmentation enabled, rotation "
              "from -40 to 40 by step of 10.")
    rng = np.random.RandomState(1337)

    # get training examples
    train_paths = generate_list_directory(os.path.join(path, 'training'))
    # We have 4 path, path_ED, path_gt_ED, path_ES and path_gt_ES
    train_paths = np.array(list(zip(train_paths[0::4], train_paths[1::4],
                                    train_paths[2::4], train_paths[3::4])))

    # 20 is the number of patients per group
    indexes = np.arange(20)

    train_idxs = []
    valid_idxs = []
    # 5 is the number of groups
    for i in range(5):
        start = i * 20
        idxs = indexes + start
        rng.shuffle(idxs)
        t_idxs = idxs[:int(indexes.shape[0] * 0.75)]
        v_idxs = idxs[int(indexes.shape[0] * 0.75):]
        train_idxs.append(t_idxs)
        valid_idxs.append(v_idxs)

    train_idxs = np.array(train_idxs).flatten()
    valid_idxs = np.array(valid_idxs).flatten()
    valid_paths = train_paths[valid_idxs]
    train_paths = train_paths[train_idxs]

    # get testing examples
    test_paths = generate_list_directory(os.path.join(path, 'testing_with_gt'))
    test_paths = np.array(list(zip(test_paths[1::5], test_paths[2::5],
                                   test_paths[3::5], test_paths[4::5])))

    with h5py.File(name, "w") as h5f:

        # Training samples ###
        group = h5f.create_group("train")
        for p_ed, g_ed, p_es, g_es in tqdm(train_paths, desc='Training'):
            # Find missmatch in the zip
            if p_ed != g_ed.replace('_gt', '') or \
                            p_es != g_es.replace('_gt', ''):
                raise ValueError(("File name don't match: ",
                                  "{} instead of {}, ".format(p_ed, g_ed),
                                  "{} instead of {}.".format(p_es, g_es)))

            create_database_structure(group, data_augmentation,
                                      p_ed, g_ed,
                                      p_es, g_es)

        # Generate the probability map from the ground truth training examples
        generate_probability_map(h5f, group)

        # Validation samples ###
        group = h5f.create_group("valid")
        for p_ed, g_ed, p_es, g_es in tqdm(valid_paths, desc='Validation'):
            # Find missmatch in the zip
            if p_ed != g_ed.replace('_gt', '') or \
                            p_es != g_es.replace('_gt', ''):
                raise ValueError(("File name don't match: ",
                                  "{} instead of {}, ".format(p_ed, g_ed),
                                  "{} instead of {}.".format(p_es, g_es)))

            create_database_structure(group, False, p_ed, g_ed, p_es, g_es)

        # Testing samples ###
        group = h5f.create_group("test")
        for p_ed, g_ed, p_es, g_es in tqdm(test_paths, desc='Testing'):
            # Find missmatch in the zip
            if basename(p_ed) != basename(g_ed).replace('_gt', '') or \
                            basename(p_es) != basename(g_es).replace('_gt', ''):
                raise ValueError(("File name don't match: ",
                                  "{} instead of {}, ".format(p_ed, g_ed),
                                  "{} instead of {}.".format(p_es, g_es)))
            create_database_structure(group, data_augmentation,
                                      p_ed, g_ed,
                                      p_es, g_es)


def main():
    """ Main function where we define the argument for the script. """
    parser = argparse.ArgumentParser(
        description=("Script to create the ACDC dataset "
                     "hdf5 file from the directory. "
                     "The given directory need to have two "
                     "directory inside, 'training' and 'testing'."))
    parser.add_argument('path', type=str,
                        help="Path of the ACDC dataset.")
    parser.add_argument('name', type=str,
                        help="Name of the generated hdf5 file.")
    parser.add_argument('-d', '--data_augmentation', action='store_true',
                        help="Add data augmentation (rotation).")
    args = parser.parse_args()
    generate_dataset(args.path, args.name, args.data_augmentation)


if __name__ == "__main__":
    main()
