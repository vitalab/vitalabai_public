# -*- coding: utf-8 -*-
"""
utils.py
author: clement.zotti@usherbrooke.ca
"""

import nibabel as nib
import numpy as np


def save_nii_file(array, output_filepath,
                  affine=np.eye(4),
                  header=None,
                  zoom=(1, 1, 1),
                  dtype=np.float32):
    """ Save a 3d array in nifti format.

    Args:
        array: ndarray, The 3d image.
        output_filepath: string, The output filename. Must end in ".nii" or ".nii.gz".
        affine: ndarray, The affine transformation.
        header: NiftiHeader, The Nifti header.
        zoom: list or tuple, Define the voxel size.
        dtype: numpy.dtype, Type of data to save into the nifti format.
    """

    img = nib.Nifti1Image(array.astype(dtype), affine, header=header)
    img.header.set_zooms(zoom)
    img.set_data_dtype(dtype)
    nib.save(img, output_filepath)
