import argparse

import numpy as np
from keras.models import load_model
from tqdm import tqdm

from VITALabAI.dataset.semanticsegmentation.acdc.augmentation_transformer import AugmentationTransformer
from VITALabAI.dataset.semanticsegmentation.acdc.preprocess import PreProcessSTD, PreQuantilePercent
from VITALabAI.dataset.semanticsegmentation.acdc.acdc import VITALAcdc
from VITALabAI.model.acdc.layers_utils import VAESampling, VAELatentMean, VAELatentLogVar


def generate_latent_space(file_path, pretrained_model, mean_var, output_path, batch_size, nb_epochs):
    """ Generate a file containing all points encoded by encoder layer
        path of output file is use as param to acdc_kneighbours

        number of genreated latent space point is batch_size * nb_epoch

    Args:
        file_path: hdf5 path containing ACDC data : use full dataset with ground truth
        pretrained_model: path of a valid pretrained vae model with valid encoder layer
        mean_var: flag if model need VAELatentMean, VAELatentLogVar to be loaded
        output_path: path of output .npz containing all data encoded by input pretrained model
        batch_size: size of a batch from training dataset we want to encode
        nb_epochs: number of epochs whe we select a batch and encode it
    """

    augmentation_transformer = AugmentationTransformer(rotation_range=60, width_shift_range=0,
                                                       height_shift_range=0, zoom_range=0.1,
                                                       dilation_range=2)
    preproc = (PreProcessSTD(), PreQuantilePercent())

    dataset = VITALAcdc(path=file_path, batch_size=batch_size,
                        augmentation_transformer=augmentation_transformer,
                        preproc_fn=preproc)

    sequence = dataset.get_train_set()

    if mean_var:
        model = load_model(pretrained_model, {"VAESampling": VAESampling,
                                              "VAELatentMean": VAELatentMean,
                                              "VAELatentLogVar": VAELatentLogVar}, compile=False)
    else:
        model = load_model(pretrained_model, {"VAESampling": VAESampling}, compile=False)

    encoder = model.get_layer("vae_encoder")
    encoder.trainable = False

    array = []

    for epoch in range(nb_epochs):
        print("\n\nepoch ", epoch + 1, "/", nb_epochs)
        for i in tqdm(range(len(sequence))):
            _, gt = sequence[i]

            array.append(encoder.predict_on_batch(gt)[:, :, 0])

    np.savez(output_path, latent_space=np.vstack(array))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Generate lower dimensionality data by applying encoder layer of a '
                    'given model on ACDC cardiac images with data augment')

    parser.add_argument('--path', type=str, help='Absolute path of ACDC dataset with hdf5 formatting',
                        required=True)
    parser.add_argument('--model', type=str, help='Absolute path of ACDC pretrained vae model', required=True)
    parser.add_argument('--output', type=str, nargs='?', default='latent_space_dataset',
                        help='Relative path of output file')
    parser.add_argument('--meanvar', action='store_true',
                        help='flag if model needs log, and mean to be loaded')

    parser.add_argument('--batch_size', type=int, nargs='?', default=50,
                        help='Size of batches generated randomly from ACDC training data')
    parser.add_argument('--epochs', type=int, nargs='?', default=75,
                        help='Number of time lower dimensionality data is generated from batches of ACDC '
                             'training data')
    args = parser.parse_args()

    generate_latent_space(args.path, args.model, args.meanvar, args.output, args.batch_size, args.epochs)
