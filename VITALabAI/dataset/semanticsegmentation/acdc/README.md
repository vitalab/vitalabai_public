# ACDC project

### Contributors

* Clément Zotti (clement.zotti@usherbrooke.ca)

### Description

This folder contains the necessary file to use the ACDC dataset: [ACDC](www.creatis.insa-lyon
.fr/Challenge/acdc/) 2017 dataset (Automatic Cardiac Delineation Challenge). 

### Requirements


* keras (with --no-deps)
* Tensorflow
* scikit-image
* pandas
* nibabel
* medpy (python3 version should be install with pip github link)
* tqdm
* natsort
* [pygpu](https://github.com/Theano/libgpuarray) (0.6.2)
* pygame (optional)


# Generate the dataset 


The file `VITALabAI/dataset/acdc/acdcdatasetgenerator.py` can be used to
generate the dataset into the hdf5 format.

### How to use `acdcdatasetgenerator.py`

```bash
python3 acdcdatasetgenerator.py path/of/the/raw_MRI_nifti_data output.hdf5 -d
```
#### Parameters explanation

* `path/of/the/data `, the parent directory of all the pathologies.
* `output.hdf5`, save the dataset into this hdf5 file.
* `-d`, add data augmentation to the dataset (rotation -60 to 60).
