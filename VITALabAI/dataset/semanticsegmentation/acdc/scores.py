# -*- coding: utf-8 -*-
"""
scores.py
author: clement zotti (clement.zotti@usherbrooke.ca)

Schematic view of the class heritage:

BaseScore
    |  SaveNifti
    |  MergeScoreIntoCsv
    |    |  BaseCsvScore
    |    |    |  BasicMetrics
    |    |    |     | PrecisionScore
    |    |    |     | RecallScore
    |    |    |     | DiceScore
    |    |    |  BaseDistance
    |    |    |     | HausdorffScore
    |    |    |     | ASSDScore


"""
import os
from os.path import join

import numpy as np
import pandas as pd
from medpy.metric.binary import hd, dc, assd, precision, recall
from skimage.morphology import binary_erosion
from tqdm import tqdm

from VITALabAI.utils import utils
from .utils import save_nii_file

LOGGER = utils.get_logger(__file__)


class BaseScore(object):
    """ Base class used for each score class.
    """
    _name = None
    voxel = None

    def __call__(self, h5file, which_set):
        """Must be implemented in child class
        It takes the h5py file containing all the predictions made by the
        model and also the groundtruth for each prediction.

        The hdf5 file is ordered like this:
            /Name/[GT,PRED]

        Args:
            h5file: hdf5, h5py file containing Ground Truth and Prediction.
            which_set: string, Tell to the scores classes which set is going to be processed.

        # Raises:
            NotImplementedError
        """
        raise NotImplementedError("Abstract class.")


class SaveNifti(BaseScore):
    """ This class provides the ability to save the h5py into nifti format.
    Saves only the first modality of the image
    """
    _name = 'Nifti'

    def __init__(self, save_path, name='Nifti'):
        self._name = name
        self.save_path = save_path

    def __call__(self, h5file, which_set):
        """ See BaseScore.__call__()

        Args:
            h5file: h5 file containing predictions
            which_set: string, set to convert to Nifti
        """

        # Iterator on the hdf5 file for each predicted images.
        save_images = join(self.save_path, which_set)
        if not os.path.isdir(save_images):
            os.makedirs(save_images)

        for name in tqdm(h5file.keys(), desc="Save nifti"):
            # Each of these names 'pred_m', 'pred_post', ... are defined during
            # the prediction task. We only get the results of the useful ones
            # and save into nifti format.
            pred = h5file[name]['pred_m'][:].squeeze()
            pred_post = h5file[name]['pred_post'][:].squeeze()
            ground = h5file[name]['gt_m'][:].squeeze()
            img = h5file[name]['image'][:].squeeze()
            voxel = h5file[name].attrs.get('voxel_size', [1, 1, 1])

            # Remove the rotation from the name
            name = name.replace("_0", "")

            # We save it in "0, 1, s"
            if len(pred.shape) < 3:
                pred = pred[np.newaxis]
            pred = pred.transpose((1, 2, 0))
            if len(pred_post.shape) < 3:
                pred_post = pred_post[np.newaxis]
            pred_post = pred_post.transpose((1, 2, 0))
            if len(ground.shape) < 3:
                ground = ground[np.newaxis]
            ground = ground.transpose((1, 2, 0))
            if len(img.shape) < 3:
                img = img[np.newaxis]
            img = img.transpose((1, 2, 0))

            datas = [pred, pred_post, ground, img]
            tags = ["prediction", "post_prediction",
                    "groundtruth", "image"]
            for data, tag in zip(datas, tags):
                save_nii_file(data,
                              os.path.join(save_images,
                                           "{}_{}.nii.gz".format(name, tag)),
                              zoom=voxel)

            for classe in range(1, pred_post.max() + 1):
                tmp_classe = np.copy(pred_post)
                tmp_classe[tmp_classe != classe] = 0
                tmp_classe = np.clip(tmp_classe, 0, 1).astype(np.uint8)
                img_path = join(save_images,
                                "{}_C_{}.nii.gz".format(name, classe))
                save_nii_file(tmp_classe, img_path, zoom=voxel, dtype=np.uint8)

    @staticmethod
    def _generate_contour(self, pred):
        """ Method to generate the contour of the prediction used for visualisation.

        Args:
            pred: ndarray, The prediction made by the model.

        Returns:
            A list of array with a contour for each class.
        """

        labels_contour = []
        # Get the maximum label contained in the image
        for i in range(1, pred.max() + 1):
            # Choose only the current class and discard the other
            lbl = (pred == i).astype(np.int)

            # Iterate on each slice to generate the contour
            for j in range(lbl.shape[-1]):
                lbl[:, :, j] -= binary_erosion(lbl[:, :, j])
            labels_contour.append(lbl)
        return labels_contour


class MergeScoreIntoCsv(BaseScore):
    """ Basic class to merge each score that can fit into a csv.
    The scores usually contain all the metrics, distances or numerical results.
    """

    def __init__(self, list_score, save_path, ext_name=""):
        """ Initializer.

        Args:
            list_score: list, This is a list of BaseCsvScore classes.
            save_path: string, path to save the csv
            ext_name: string, A suffix added inside the csv file name.
        """
        self.list_score = list_score
        self.ext_name = ext_name
        self.save_path = save_path

    def __call__(self, h5file, which_set):
        """ See BaseScore.__call__()

        Args:
            h5file: h5 file containing predictions
            which_set: string, set to convert to Nifti

        Returns:
            None
        """

        if which_set == 'test':
            return
        # Get all the scores given at object creation from the list_score
        # variable
        columns = ["{}{}".format(score._name, grp)
                   for score in self.list_score
                   for grp in ["RV", "MYO", "LV"]]

        # Generate the path to save the csv file
        path = 'merge_score{}_{}.csv'.format(self.ext_name, which_set)
        path = os.path.join(self.save_path, path)

        rows = list(h5file.keys())
        res = []

        # Iterate through image from the result hdf5 file
        for name in tqdm(h5file.keys(), desc="Generate CSV"):
            # Remove channel dim (always 1) don't need in prediction
            preds = np.array(h5file[name]['pred_post'],
                             dtype=np.uint8).squeeze()
            gts = np.array(h5file[name]['gt_m'],
                           dtype=np.uint8).squeeze()

            # Iterate on each BaseCsvScore object and set the voxel attribute
            voxel = h5file[name].attrs.get('voxel_size', [1, 1, 1])

            # Iterate on each BaseCsvScore object and compute the score
            metrics = [f([preds, gts], voxel) for f in self.list_score]
            metrics = [
                value
                for metric in metrics
                for value in metric
            ]
            res.append(np.array(metrics))
        # Create the DataFrame and save it
        df = pd.DataFrame(res, index=rows, columns=columns)
        print("ED: {}".format(df.iloc[::2].describe()))
        print("ES: {}".format(df.iloc[1::2].describe()))
        df.to_csv(path)


class BaseCsvScore():
    """ Base class to have the score saved into its own csv file.
    """

    def __call__(self, data, voxel_size):
        """ Must be implemented in child class
        It takes the h5py file containing all the predictions made by the
        model and also the groundtruth for each prediction.

        The hdf5 file is ordered like this:
            /Name/[GT,PRED]

        Args:
            data: tuple or list, the 1st element is the prediction and the 2nd is the groundtruth
            voxel_size: list or tuple, Size of the voxel used in the image.

        Returns:
            image score for the data and voxel score
        """
        return self.image_score(data, voxel_size)

    def image_score(self, data, voxel_size):
        """ Method should be implemented in each child of this class.

        Args:
            data: tuple, the 1st element is the prediction and the 2nd is the groundtruth.
            voxel_size: list or tuple, Size of the voxel used in the image.

        Returns:
            The score of the prediction.

        # Raises:
            NotImplementedError
        """
        raise NotImplementedError("Abstract class.")


class BasicMetrics(BaseCsvScore):
    """Base class for metrics.
    """

    def image_score(self, data, voxel_size):
        """ Compute the metric score for a 3d image.

        Args:
            data: list, tuple A list or tuple of only two elements, first is the prediction, and second is the
                groundtruth.
            voxel_size: list or tuple, Size of the voxel used in the image.

        Returns:
            The metric score.
        """
        preds = data[0]
        gts = data[1]
        res = []

        # Compute the metric for each class in the image
        for i in range(1, np.amax(gts) + 1):
            tgt = np.copy(gts)
            tpred = np.copy(preds)
            tgt[tgt != i] = 0
            tpred[tpred != i] = 0
            res.append(self.metrics(tgt, tpred))
        res = np.array(res)
        return res


class PrecisionScore(BasicMetrics):
    """ Precision class.
    """

    def __init__(self, name="Precision"):
        self._name = name
        self.metrics = precision


class RecallScore(BasicMetrics):
    """ Recall class.
    """

    def __init__(self, name='Recall'):
        self._name = name
        self.metrics = recall


class DiceScore(BasicMetrics):
    """ Dice class.
    """

    def __init__(self, name='Dice'):
        self._name = name
        self.metrics = dc


class BaseDistance(BaseCsvScore):
    """ Base class for distance scores.
    """

    def image_score(self, data, voxel_size):
        """ Compute the metric distance for a 3d image.

        Args:
            data: list, tuple, A list or tuple of only two elements, first is the prediction, and second is
                the groundtruth.
            voxel_size: list or tuple, Size of the voxel used in the image.

        Returns:
            The metric distance score.
        """
        res = []
        if data[1].sum() != 0 and data[0].sum() != 0:
            img = data[0]
            gt = data[1]

            # Compute the metric distance for each class in the image
            for i in range(1, np.amax(gt) + 1):
                tgt = np.copy(gt)
                tpred = np.copy(img)
                tgt[tgt != i] = 0
                tpred[tpred != i] = 0

                res.append(self.distance(tpred, tgt, voxel_size))
        res = np.array(res)
        return res


class HausdorffScore(BaseDistance):
    """ Hausdorff class.
    """

    def __init__(self, name='Hausdorff'):
        self._name = name
        self.distance = hd


class ASSDScore(BaseDistance):
    """ ASSD class.
    """

    def __init__(self, name="ASSD"):
        self._name = name
        self.distance = assd


class BlandAltman(object):
    """ Specific class to compute metric of the ACDC challenge. """

    def __init__(self, csv_file, columns, name="BlandAltman", ):
        self._name = name
        self._csv_file = csv_file
        self._columns = columns

    def __call__(self):
        pass
