import os
from os.path import join as pjoin
import nibabel as nib
import numpy as np
import h5py
from tqdm import tqdm


def create_dataset(path, name, set_split):
    """ Create the hdf5 file for the lamb2018 dataset

    Args:
        path: string, path to the lamb2018 dataset
        name: string, path for the hdf5 file to be created
        set_split: list containing 3 values, train split
    """

    assert len(set_split) == 3
    assert np.sum(set_split) == 1

    dir_list = os.listdir(path)

    with h5py.File(name, "w") as h5f:
        h5f.create_group("train")
        h5f.create_group("validation")
        h5f.create_group("test")

        for dir in tqdm(dir_list):

            g = np.random.choice(a=['train', 'validation', 'test'], p=set_split)
            group = h5f.get(g)

            dir_path = pjoin(path, dir)
            path_content = os.listdir(dir_path)

            # Files in the dataset are not always named the same way
            cuisse_path = [s for s in path_content if "cuisse" in s]
            longe_path = [s for s in path_content if "lo" in s]

            cuisse_path = pjoin(path, dir_path, cuisse_path[0])
            longe_path = pjoin(path, dir_path, longe_path[0])
            img_path = pjoin(path, dir_path, '{}.nii.gz'.format(dir))

            ni_img = nib.load(img_path)
            img = ni_img.get_data()
            img = img.transpose(2, 0, 1)[..., np.newaxis]

            cuisse = nib.load(cuisse_path).get_data()
            cuisse = cuisse.transpose(2, 0, 1)[..., np.newaxis]

            longe = nib.load(longe_path).get_data()
            longe = longe.transpose(2, 0, 1)[..., np.newaxis]

            gt = np.concatenate((longe, cuisse), axis=-1)
            background = 1 - gt.any(-1)[:, :, :, None]
            gt = np.concatenate((background, gt), axis=-1)

            data = np.concatenate((img, gt), axis=-1)

            dataset = group.create_dataset(dir, data=data, compression='gzip')
            dataset.attrs['voxel_size'] = ni_img.header.get_zooms()


if __name__ == '__main__':
    import argparse

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the dataset", type=str, required=True)
    aparser.add_argument("--name", help="Name of the hdf5 file to generate ", type=str)
    args = aparser.parse_args()

    set_split = [0.7, 0.1, 0.2]

    create_dataset(args.path, args.name, set_split)
