import h5py
import numpy as np
from keras.utils import Sequence

from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract
from skimage.transform import resize


class VITALLamb2018(VITALabAiDatasetAbstract):
    """Wrapper for the Lamb dataset."""
    TRAIN_KEY = 'train'
    VAL_KEY = 'validation'
    TEST_KEY = 'test'

    def __init__(self, path, input_size: int = 512, batch_size=4, seed=None, augmentation_transformer=None):
        """
        Args:
            path: string, path to the hdf5 file to read.
            input_size: int, Size of the image
            batch_size: int, Size of the batches to return.
            seed: object, seed for the permutations
            augmentation_transformer: object, to be applied on batches for online data augmentation
        """
        self.input_size = input_size
        self.batch_size = batch_size
        self.path = path
        self.nb_classes = 3
        self.augmentation_transformer = augmentation_transformer

        np.random.seed(seed)

        self.prior = None
        self.train_list, self.val_list, self.test_list = self.load_data()

        print(self.train_list.shape)
        print(self.val_list.shape)
        print(self.test_list.shape)

    def get_classes(self):
        return list(range(self.nb_classes))

    def get_input_shape(self):
        return (self.input_size, self.input_size, 1)

    def load_data(self):
        """ Read list of images from the HDF5 file and save them in a list

        Returns:
            tuple of lists containing path to files for each set
        """

        def get_set_list(set_key, file, no_back=False):
            set_list = []
            f_set = file[set_key]
            for key in list(f_set.keys()):
                patient = f_set[key]
                n_slices = patient.shape[0]

                for i in range(n_slices):

                    if no_back:
                        gt_backround = patient[i, :, :, 1]
                        if np.sum(gt_backround) == gt_backround.shape[0] * gt_backround.shape[1]:
                            continue

                    k = '{}/{}'.format(set_key, key)
                    set_list.append((k, i))

            return set_list

        with h5py.File(self.path, 'r') as f:
            train_list = get_set_list(self.TRAIN_KEY, f)
            train_list = np.random.permutation(train_list)

            val_list = get_set_list(self.VAL_KEY, f)
            val_list = np.random.permutation(val_list)

            test_list = get_set_list(self.TEST_KEY, f)
            test_list = np.random.permutation(test_list)

        return train_list, val_list, test_list

    def get_train_set(self):
        return LambSequence(self.train_list, self.path, self.input_size, self.batch_size,
                            augmentation_transformer=self.augmentation_transformer)

    def get_validation_set(self):
        return LambSequence(self.val_list, self.path, self.input_size, self.batch_size)

    def get_test_set(self):
        return LambSequence(self.test_list, self.path, self.input_size, self.batch_size)

    def get_set_for_prediction(self, set):
        return LambTestSequence(set, self.path, self.input_size)


class LambSequence(Sequence):
    """ Sequence of images for Keras."""
    def __init__(self, key_list, file_path, input_size, batch_size, augmentation_transformer=None):
        """

        Args:
            key_list: list, list of pairs of keys and idexes for each slice
            file_path: string, path to the hdf5 dataset file
            input_size: int, size of the image
            batch_size: int, Batch size to stack the images
            augmentation_transformer: object, to be applied on batches for online data augmentation
        """
        self.key_list = key_list
        self.batch_size = batch_size
        self.file_path = file_path
        self.input_size = input_size
        self.augmentation_transformer = augmentation_transformer

    def __len__(self):
        return int(np.ceil(len(self.key_list) / self.batch_size))

    def __getitem__(self, idx):
        """ This method loads a batch from the dataset file and resizes the images

        Args:
            idx: int, Index of the sequence

        Returns:
            x: array, Array of slices
            y: array, Array of gt

        """
        keys = self.key_list[idx * self.batch_size:(idx + 1) * self.batch_size]

        x = []
        y = []

        with h5py.File(self.file_path, 'r') as f:
            for key, i in keys:
                patient = f[key]
                slice = np.array(patient[int(i)])

                img = slice[:, :, 0][..., None]
                gt = slice[:, :, 1:]

                img = resize(img, (self.input_size, self.input_size), mode='constant', preserve_range=True)
                gt = resize(gt, (self.input_size, self.input_size), mode='constant', preserve_range=True)

                x.append(img)
                y.append(gt)

        if self.augmentation_transformer is not None:
            self.augmentation_transformer.apply_on_lists(x, y)

        return np.array(x), np.array(y)


class LambTestSequence(Sequence):
    """ Sequence of images for Keras.
    This sequence does not use a shuffled list, instead this sequence returns one lamb at a time for
    prediction and saving results.
    """
    def __init__(self, set, file_path, input_size):
        """
        Args:
            set: string, which set to load ('train', 'validation', 'test')
            file_path: string, path to the hdf5 dataset file
            input_size: int, size of the image
        """
        self.file_path = file_path
        self.input_size = input_size
        self.set = set

        with h5py.File(self.file_path, 'r') as f:
            self.key_list = list(f[self.set].keys())

    def __len__(self):
        return int(np.floor(len(self.key_list)))

    def __getitem__(self, idx):
        """This methos loads are returns slices from one lamb.

        Args:
            idx: int, Index of the sequence

        Returns:
            img_name: string, name of the lamb in the dataset
            x: array, Array of slices
            y: array, Array of gt
            voxel_size: list, dimensions of the voxels for the lamb

        """
        img_name = self.key_list[idx]
        key = '{}/{}'.format(self.set, img_name)

        with h5py.File(self.file_path, 'r') as f:
            patient = np.array(f[key])
            voxel_size = f[key].attrs.get('voxel_size', [1, 1, 1])

        img = patient[:, :, :, 0][..., None]
        gt = patient[:, :, :, 1:]

        x = []
        y = []

        for i in range(img.shape[0]):
            x.append(resize(img[i], (self.input_size, self.input_size), mode='constant', preserve_range=True))
            y.append(resize(gt[i], (self.input_size, self.input_size), mode='constant', preserve_range=True))

        return img_name, np.array(x), np.array(y), voxel_size


if __name__ == '__main__':
    from matplotlib import pyplot as plt
    import argparse

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the dataset", type=str, required=True)
    args = aparser.parse_args()

    ds = VITALLamb2018(path=args.path, batch_size=1)
    sequence = ds.get_test_set()

    img, gt = sequence[0]

    img = img.squeeze()
    gt = gt.squeeze()

    f, (ax11, ax12, ax13) = plt.subplots(1, 3)
    ax11.imshow(gt[:, :, 0], cmap='gray')
    ax12.imshow(gt[:, :, 1], cmap='gray')
    ax13.imshow(gt[:, :, 2], cmap='gray')
    plt.show(block=False)

    seg = np.argmax(gt, axis=-1)

    img = img.squeeze()

    f2, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(img)
    ax2.imshow(seg)
    plt.show(block=False)

    plt.figure(3)
    plt.imshow(img, cmap='gray')
    plt.imshow(seg, alpha=0.2)
    plt.show()


