# -*- coding: utf-8 -*-
import os

from glob import glob

import numpy as np

from scipy.ndimage import imread

from keras.utils import generic_utils

from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract


class TinyImageNetDataset(VITALabAiDatasetAbstract):
    """
    This is a tiny subsample of the imagenet dataset, only 200 classes and 500 samples per class for training.
    Only training and validation sets are available.
    Training contains 100,000 samples and validation contains 10,000 samples.
    Images are 64x64x3 (RGB) and sometimes 64x64x1 (Grayscale).
    See: https://tiny-imagenet.herokuapp.com/ for more information about the dataset.
    """

    def __init__(self, path):
        """
        Initializer
        Parameters
        ----------
        path: string
            Path to the dataset directory or to the numpy array file
        """
        self.path = path

    def generate_dataset(self):
        """
        This method is used to generate the dataset from the data, like taking all the images
        inside one big hdf5 file, numpy file or something else.
        """

        # Check if a numpy array was provided
        if os.path.isfile(self.path):
            f = np.load(self.path)
            # ('b', 0, 1, 'c') -> ('b', 'c', 0, 1)
            self.X_train = self._transpose(f['X_train'])
            self.y_train = f['y_train']
            # ('b', 0, 1, 'c') -> ('b', 'c', 0, 1)
            self.X_valid = self._transpose(f['X_valid'])
            self.y_valid = f['y_valid']
            return

        train_path = os.path.join(self.path, 'train')
        classes = os.listdir(train_path)

        # Generate the one hot vector for the classes
        self.one_hot = {}
        for i, n_c in enumerate(classes):
            vec = np.zeros(len(classes))
            vec[i] = 1
            self.one_hot[n_c] = vec

        # load the training data
        X_train = []
        y_train = []
        print("Load training samples.")
        p_bar = generic_utils.Progbar(len(classes) * 500)
        for class_dir in classes:
            p_img = os.path.join(self.path, 'train', class_dir, 'images', '*')
            # Find all the images of one class and load them
            for img in glob(p_img):
                p_bar.update(p_bar.seen_so_far + 1, force=True)
                iimg = imread(img)
                # Some images are grayscale so we add 3 channels
                if len(iimg.shape) < 3:
                    iimg = iimg[..., np.newaxis]
                    iimg = np.repeat(iimg, 3, axis=-1)

                X_train.append(iimg)
                y_train.append(self.one_hot[class_dir])

        # ('b', 0, 1, 'c') -> ('b', 'c', 0, 1)
        self.X_train = self._transpose(X_train)
        self.y_train = np.array(y_train)

        # load the validation data
        X_valid = []
        y_valid = []
        valid_classes = os.path.join(self.path, "val", "val_annotations.txt")
        df_valid = np.loadtxt(valid_classes, delimiter=',',
                              skiprows=1,
                              usecols=(0, 1),
                              dtype=np.string_)

        val_images = os.path.join(self.path, "val", "images", "*")
        images = glob(val_images)

        print("Load validation samples.")
        p_bar = generic_utils.Progbar(len(images))
        for img in images:
            p_bar.update(p_bar.seen_so_far + 1, force=True)
            iimg = imread(img)
            # Same as the training images
            if len(iimg.shape) < 3:
                iimg = iimg[..., np.newaxis]
                iimg = np.repeat(iimg, 3, axis=-1)

            X_valid.append(iimg)
            # Get the image filename
            idx = os.path.basename(img)
            # find the class associated to the filename
            res = df_valid[df_valid[:, 0] == idx.encode()]
            # Get the string out of it
            val = res[0, 1].decode('utf-8')
            y_valid.append(self.one_hot[val])

        # ('b', 0, 1, 'c') -> ('b', 'c', 0, 1)
        self.X_valid = self._transpose(X_valid)
        self.y_valid = np.array(y_valid)

    def _transpose(self, arr):
        """
        Transpose a ndarray dimension (shuffle_axis).
        Parameters
        ----------
        arr: ndarray or list
            Apply the .transpose((0, 3, 1, 2)) on this input

        Returns
        -------
        Ndarray where the transpose((0, 3, 1, 2)) method is applied.
        """
        return np.array(arr, copy=False).transpose((0, 3, 1, 2))

    def preprocess_dataset(self):
        """
        This method is used when you want to preprocess all the data from the dataset.
        We need to preprocess the file during training time as it is too big to fit float32 or float64 values into memory.
        """
        pass

    def __iter__(self):
        """
        This method is used to get the iterator for the dataset.
        """
        return self

    def __next__(self):
        """
        This method is called by python3 loop iteration process.
        """
        return self.next()

    def next(self):
        """
        This method is called by the current interface and used for iteration on the data from the dataset.
        """
        pass

    def get_entire_dataset(self):
        """
        This method returns all the data from the dataset into a numpy array.

        Returns
        -------
        numpy.ndarray with all the data.
        """
        return (self.X_train, self.y_train, self.X_valid, self.y_valid)
