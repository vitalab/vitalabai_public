## Database from Saint-Etienne CHU 

CAMUS : Cardiac Acquisitions for Multi-structure Ultrasound Segmentation (also a famous French writer / philosopher)

*** Private ! (but soon to be public) ***

- Ultrasound 2D images of the heart at end diastoly (ED) and end systoly (ES) on 2CH and 4CH views. It contains as of yet sequences from 500 patients. It may be completed with additional data with the intention of organizing a challenge.

- This database is the result of a one-year collaboration between Olivier Bernard, Sarah Leclerc, and Florian Espinoza. It has been involved in a collaborative study between the universities of Lyon (France), NTNU (Norway), Sherbrooke (Canada) and Leuven (Belgium).

- The data can be efficently split in 10 balanced subfolds with respect to the ejection fraction and the image quality (see .txt files).

The dataset itself is available on the VITALab shared folder.

Contact Olivier BERNARD (olivier.bernard@creatis.insa-lyon.fr) 
	   or Sarah LECLERC (sarah.leclerc@insa-lyon.fr)

# create_us_stetienne_hdf5.py

```bash
python3 create_camus_hdf5.py datapath hdf5name size_im
```
datapath : path to the CAMUS dataset
hdf5name : default name for subfold HDF5 files
size_im : constant image size to which all images are resized to

Default :
```bash
python3 create_camus_hdf5.py data datahdf5fold 256
```