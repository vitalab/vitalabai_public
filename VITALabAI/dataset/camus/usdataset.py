# -*- coding: utf-8 -*-
"""
usdataset.py
author: Sarah LECLERC from Clement Zotti's code

This file is used to get the data from the hdf5 file and feed it to
a model for training, validation and testing.
This class is an iterator.
"""

import h5py
import numpy as np
from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract
from VITALabAI.utils import utils
LOGGER = utils.get_logger(__file__)

class USDataset(VITALabAiDatasetAbstract):
    """
    This class enables to manipulate the data from a hdf5 file
    """

    def __init__(self, path, temporal, batch_size=10):
        """
        Initializer
        :param path: string : path to the dataset (hdf5 file)
        :param image_size: tuple : image constant size
        :param temporal: bool : whether to process ED and ES jointly
        :param batch_size: int : Number of patients per batch
        """

        self.nb_classes = 4
        self.temporal = temporal
        self.batch_size = int(batch_size)
        self.pos_data = {
            'train': [],
            'valid': [],
            'test': []
        }
        self.cache = {'names': [], 'data': {}}
        self._f = h5py.File(path, 'r')

        k = list(list(self._f["train"].items())[0][1].values())
        self.image_size = k[0].shape[0:2]

        self.rng = np.random.RandomState(707)

        # Initialize count
        self._idx_set = {
            'train': 0,
            'valid': 0,
            'test': 0
        }

        self._current_count = 0
        self.im_count = 0
        self.which_set = 'test'

        # Set the right function for the iteration
        self.next_iteration = self._next_training_minibatch
        self._get_all_positions()

    def _get_all_positions(self):
        """
        This method shuffles the data of each subfold
        """

        for dataset in ["train", "valid", "test"]:
            for img in self._f[dataset].keys():
                self.pos_data[dataset].append(img)

            # Randomize positions
            tmp_arr = self.pos_data[dataset]
            self.rng.shuffle(tmp_arr)
            self.pos_data[dataset] = tmp_arr

    def select_set(self, which_set):
        """
        This function selects the chosen set and correspond iterator.
        :param which_set: string : set to iterate on ('train', 'valid', or 'test')
        """

        LOGGER.info('Switch to {}'.format(which_set))

        # Switch next function
        if which_set == 'test':
            self.next_iteration = self._next_testing_image
            self._test_iter = iter(self._f[which_set].keys())
        else:
            self.next_iteration = self._next_training_minibatch

            # Save the count for the previous dataset
            self._idx_set[self.which_set] = self._current_count

            # Set the count from the saved state
            self._current_count = self._idx_set[self.which_set]
            self.current_seen = 0

        # Change the dataset
        self.which_set = which_set
        self._data = self._f[self.which_set]

    def get_num_examples(self):
        """
        This function gives the numbers of images per epoch.
        :return int: Number of elements per epoch
        """

        return len(self.pos_data[self.which_set])

    def _next_training_minibatch(self):
        """
        This function gets the next training/validation example from the dataset file.
        :return tuple : Two arrays of dim (number_example, x, y, channel), the first one is the image,
        the second one is the target
        """

        if self.current_seen >= self.get_num_examples():
            raise StopIteration()

        instants = ['ES', 'ED']
        views = ['2CH', '4CH']
        lower = self._current_count
        upper = self._current_count + self.batch_size
        pos = self.pos_data[self.which_set][lower:upper]
        self._current_count += len(pos)

        # Loop back to the beginning of the dataset
        if (len(pos) != self.batch_size) and (self.current_seen + len(pos) < self.get_num_examples()):
            self._current_count = 0
            lower = self._current_count
            upper = self._current_count + (self.batch_size - len(pos))
            pos = np.concatenate(
                (pos, self.pos_data[self.which_set][lower:upper]))
            self._current_count += upper

        self.current_seen += len(pos)

        x_res = []
        y_res = []

        # get all 2 images of every patients views of the minibatch

        if self.temporal:
            for name in pos:
                x_img2 = np.zeros((self.image_size[0], self.image_size[1], 2), np.dtype('float32'))
                y_img2 = x_img2.copy()

                for view in views:
                    for c, instant in enumerate(instants):

                        # Get the data from the dataset
                        x_img, info = self._get_data(name, 'im_' + str(view) + '_' + str(instant), view)
                        y_img, _ = self._get_data(name, 'gt_' + str(view) + '_' + str(instant), view)
                        x_img2[:, :, c] = x_img[:, :, 0]
                        y_img2[:, :, c] = y_img[:, :, 0]

                    x_res.append(x_img2)
                    y_res.append(y_img2)

        else:
            for name in pos:
                for view in views:
                    for instant in instants:

                        # Get the data from the dataset
                        x_img, info = self._get_data(name, 'im_' + str(view) + '_' + str(instant), view)
                        y_img, _ = self._get_data(name, 'gt_' + str(view) + '_' + str(instant), view)

                        x_res.append(x_img)
                        y_res.append(y_img)

        return x_res, y_res

    def _next_testing_image(self):
        """
        This function gets the next testing example from the dataset file.
        :return: tuple : Image name + two arrays of dim (number_example, x, y, channel), the first one is the image,
        the second one is the target if provided
        """

        # keep count between patient images
        # may be improved later on
        if self.im_count == 4:  # reset
            self.im_count = 0

        count = self.im_count
        if count == 0:
            self.img_name = next(self._test_iter)  # iterate to a new patient
        img_name = self.img_name

        if count >= 2:
            view = '4CH'
        else:
            view = '2CH'

        if self.temporal:
            instants = ['ES', 'ED']
            img = np.zeros((self.image_size[0], self.image_size[1], 2), np.dtype('<f4'))
            gt = np.zeros((self.image_size[0], self.image_size[1], 2), np.dtype('<f4'))

            for c, instant in enumerate(instants):
                # Get the data from the dataset
                x_img, info = self._get_data(img_name, 'im_' + str(view) + '_' + str(instant), view)
                y_img, _ = self._get_data(img_name, 'gt_' + str(view) + '_' + str(instant), view)
                img[:, :, c] = x_img[:, :, 0]
                gt[:, :, c] = y_img[:, :, 0]
            count += 2

        else:

            if count == 0 or count == 2 :
                instant = "ED"
            else :
                instant = "ES"

            # access the corresponding view and instant image
            img, info = self._get_data(img_name, 'im_' + str(view) + '_' + str(instant), view)
            gt, _ = self._get_data(img_name, 'gt_' + str(view) + '_' + str(instant), view)

            count += 1

        self.im_count = count
        return img_name, img, gt, info

    def next(self):
        """
        This method is called by the current interface and used
        for iteration on the data from the dataset.
        :return: function : the iterator function
        """

        return self.next_iteration()

    def _get_data(self, name, dtype, view):
        """
        This method extracts the data from the dataset.
        :param  name: string : name of the image to extract from the dataset
        :param  dtype: string : which value you want to extract ('img' or 'gt')
        :return img : ndarray : image
        """

        if type(name) is np.bytes_:
            name = name.decode('utf-8')

        # If we don't have the data in memory, we go get it
        if name not in self.cache['names']:
            # We add the image to the cache
            self.cache['names'].append(name)
            self.cache['data'][name] = {}

        # Extract the image from the dataset file
        img = self._data['{0}/'.format(name) + str(dtype)]
        img = np.array(img)
        self.cache['data'][name][dtype] = img
        info = self._data['{0}/'.format(name) + "info_" + view]
        info = np.array(info)

        return img, info

    def generate_dataset(self):
        pass

    def get_entire_dataset(self):
        pass

    def preprocess_dataset(self):
        pass