""""
This script does the following tasks :
- load the data from the CAMUS database
- resize images to a given size
- split it into train, test and validation sets for a selection of subfolds
- save them in hdf5 files for further use
"""

""""
Use :
- Simply run :  python3 create_camus_hdf5.py datapath hdf5name size_im
    like :      python3 create_camus_hdf5.py data datahdf5fold 256
"""

import argparse
import os
from os.path import join

import SimpleITK
import h5py
import numpy as np
import scipy.misc as sc
import skimage.io as io
from PIL import Image
from tqdm import tqdm


# Author : Sarah LECLERC (sarah.leclerc@insa-lyon.fr)

########################################################################################################################
#                                                    Functions
########################################################################################################################


def load_and_process_mhd_data(filename, save=0):
    """
    This function loads a mhd image and returns a numpy array
    :param filename: string : path to the image
    :param save: boolean : flag for saving the image in png format
    :param preprocess: boolean : flag for preprocessing the data before saving
    :return: im_array : numpy array with channels as the last dimension
    """
    # load image and save info
    image = SimpleITK.ReadImage(filename)
    info = [image.GetSize(), image.GetOrigin(), image.GetSpacing(),
            image.GetDirection()]

    # create numpy array from the .mhd file and corresponding image
    im_array = np.squeeze(io.imread(filename, plugin='simpleitk'))
    img = Image.fromarray(im_array, 'L')

    # options
    if save:
        base, dir = os.path.basename(filename), os.path.dirname(filename)
        fn = os.path.splitext(base)[0]
        img.save(os.path.join(dir, fn + ".png"))

    # padding to have even dimensions
    if im_array.shape[0] % 2 == 1:
        im_array = np.pad(im_array, ((0, 1), (0, 0)), mode='edge')
    if im_array.shape[1] % 2 == 1:
        im_array = np.pad(im_array, ((0, 0), (0, 1)), mode='edge')

    return im_array, info


def get_arguments():
    """
    This function is used to create the arguments parser.
    :return: im_array : An argument parser object, similar to a dictionary.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("datafolder", type=str, default="data", help="Folder in which the patient directories are stored")
    parser.add_argument("hdf5name", type=str, default="datahdf5.hdf5",
                        help="name of the hdf5 file in which to save the data")
    parser.add_argument("size_im", type=int, default=256, help ="the size to which images are resized to")
    args = parser.parse_args()
    return args


def main(num_subfold):

    # Parse arguments
    args = get_arguments()
    hdf5name = args.hdf5name

    # Folder with the .mhd dataset
    data_folder = args.datafolder
    size = args.size_im

    f = h5py.File(hdf5name + str(num_subfold) + '.hdf5', 'w')

    # read test train valid details in the correspond .txt files
    list_path = join(data_folder, 'listSubGroups','subGroup')
    id_train = get_subgroup(list_path + str(num_subfold) + '_training.txt')
    id_test = get_subgroup(list_path + str(num_subfold) + '_testing.txt')
    id_valid = get_subgroup(list_path + str(num_subfold) + '_validation.txt')

    # save in hdf5
    print("Train : ")
    write_group_hdf5(f, 'train', id_train, data_folder, size)
    print("Test : ")
    write_group_hdf5(f, 'test', id_test, data_folder, size)
    print("Valid : ")
    write_group_hdf5(f, 'valid', id_valid, data_folder, size)

    f.close()


def get_subgroup(file):
    """
    This function reads PatientIds in a txt file.
    :param file: string : path to the txt file
    :return: patient_list : int[] : list of patientIds
    """
    patient_list = []
    with open(file, 'r') as f:
        for line in f:
            patient_id = int(line.replace('patient', ''))
            patient_list.append(patient_id)
    return patient_list


def write_group_hdf5(hdf, group_name, patient_list, data_folder, size):
    """
    This function writes in a hdf5 the data for a given subset.
    :param hdf: string : hdf5filename
    :param group_name: string : hdf5 group to store the patient data under
    :param patient_list: int[] : list of patientIds
    :param data_folder: string : path to the mhd data
    :param size: int : new image size
    """

    hdf.create_group(group_name)
    gpp = hdf[group_name]
    for patient_id in tqdm(patient_list, total=len(patient_list), unit="Patients"):
        data_x = np.zeros((4, size, size, 1), dtype=np.float32)
        data_y = np.zeros((4, size, size, 1), dtype=np.float32)
        counter = 0
        informations = []
        for view in ['4CH', '2CH']:
            for mode in ['ED', 'ES']:

                # Open ultrasound image
                patient_str_id = 'patient' + str(patient_id).zfill(4)
                filename = patient_str_id + '_' + view + '_' + mode + '.mhd'
                filepath = join(data_folder, patient_str_id, filename)
                us_image, info = load_and_process_mhd_data(filepath, 0)
                us_image = sc.imresize(us_image, (size, size), interp='nearest')
                us_image = us_image / 255
                data_x[counter, :, :, 0] = us_image

                # Open segmentation
                filename = patient_str_id + '_' + view + '_' + mode + '_gt.mhd'
                filepath = join(data_folder, patient_str_id, filename)
                segmentation, _ = load_and_process_mhd_data(filepath, 0)
                segmentation = sc.imresize(segmentation, (size, size), interp='nearest')
                segmentation[segmentation > 3] = 3  # in case the LV borders labels were set at 4
                segmentation = segmentation
                data_y[counter, :, :, 0] = segmentation

                counter += 1

            informations.append(info)

        # Write to hdf5
        gpp.create_group(patient_str_id)
        gp = gpp[patient_str_id]
        im_4CH_ED, im_4CH_ES, im_2CH_ED, im_2CH_ES = data_x[0], data_x[1], data_x[2], data_x[3]
        gt_4CH_ED, gt_4CH_ES, gt_2CH_ED, gt_2CH_ES = data_y[0], data_y[1], data_y[2], data_y[3]
        info_2CH, info_4CH = informations

        gp.create_dataset(name='im_4CH_ED', data=im_4CH_ED, dtype=np.float32, compression="gzip", compression_opts=4)
        gp.create_dataset(name='im_4CH_ES', data=im_4CH_ES, dtype=np.float32, compression="gzip", compression_opts=4)
        gp.create_dataset(name='im_2CH_ED', data=im_2CH_ED, dtype=np.float32,  compression="gzip", compression_opts=4)
        gp.create_dataset(name='im_2CH_ES', data=im_2CH_ES, dtype=np.float32,  compression="gzip", compression_opts=4)
        gp.create_dataset(name='gt_4CH_ED', data=gt_4CH_ED, dtype=np.float32, compression="gzip", compression_opts=4)
        gp.create_dataset(name='gt_4CH_ES', data=gt_4CH_ES, dtype=np.float32, compression="gzip", compression_opts=4)
        gp.create_dataset(name='gt_2CH_ED', data=gt_2CH_ED, dtype=np.float32, compression="gzip", compression_opts=4)
        gp.create_dataset(name='gt_2CH_ES', data=gt_2CH_ES, dtype=np.float32, compression="gzip", compression_opts=4)

        info_2CH = [item for sublist in info_2CH for item in sublist]
        info_4CH = [item for sublist in info_4CH for item in sublist]
        gp.create_dataset(name='info_2CH', data=info_4CH, dtype=np.float64)
        gp.create_dataset(name='info_4CH', data=info_2CH, dtype=np.float64)

sel = range(1, 11)  # fold dataset to create
for i in sel:
    print ("Processing subfold {}".format(i))
    main(i)
