import keras.backend as K
import numpy as np
from keras.utils import Sequence
from skimage.transform import resize


def image_normalization(shape=(224, 224), mean=128., std=128.):
    """Normalize and resize the images. ImageDataGenerator or torchvision should be prefered.

    Args:
        shape: (int+), shape to resize the image to.
        mean: int, mean of the images to substract.
        std:  int, std of the images to normalize.

    Returns:
        function which normalize a batch of images.
    """

    def function(X, data_format='channels_last'):
        """
        Function that normalizes a batch of images.
        Args:
            X: ndarray, a batch of images.
            data_format: Data format to use.
                One of 'channels_last', 'channels_first'.

        Returns:

        """
        X = X.astype(np.float32)
        X = (X - mean) / std
        X = np.array([resize(x, shape, mode='constant') for x in X])
        if data_format == 'channels_first':
            X = X.transpose((0, 2, 3, 1))
        return X

    return function


class NumpySequence(Sequence):
    """Create a Sequence from a numpy array
    Args:
        X: Array, Input data
        y: Array, target data
        batch_size: Size of the batch to return
        preprocessing_function: function that takes an input.
    """

    def __init__(self, X, y, batch_size, preprocessing_function=None):
        self.X, self.y = X, y
        self.batch_size = batch_size
        self.preprocessing_function = preprocessing_function or (lambda x, **kwargs: x)

    def __len__(self):
        return int(np.ceil(len(self.X) / self.batch_size))

    def __getitem__(self, idx):
        X, y = (self.X[idx * self.batch_size:(idx + 1) * self.batch_size],
                self.y[idx * self.batch_size:(idx + 1) * self.batch_size])
        return self.preprocessing_function(X, data_format=K.image_data_format()), y
