import numpy as np
from keras.datasets import mnist
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split

from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract
from VITALabAI.dataset.classification.utils import image_normalization, NumpySequence


class VITALMnist(VITALabAiDatasetAbstract):
    """Wrapper for the MNIST dataset.
    """

    def __init__(self, input_size: int=28, batch_size=8, val_split=0.1, seed=1337):
        """
        Args:
            input_size: int, Size of the image
            batch_size: int, Size of the batches to return.
            val_split: float, Proportion of the train set to use for the validation set
            seed: object, seed for the RNG
        """
        self.input_size = input_size
        self.batch_size = batch_size
        (x_train, y_train), (self.X_test, self.y_test) = mnist.load_data()
        y_train, self.y_test = map(to_categorical, [y_train, self.y_test])
        self.X_train, self.X_val, self.y_train, self.y_val = \
            train_test_split(x_train, y_train, test_size=val_split, random_state=seed)
        self.X_train, self.X_val, self.X_test = map(lambda x: x[..., np.newaxis],
                                                    [self.X_train,
                                                     self.X_val,
                                                     self.X_test])

    def get_classes(self):
        return list(range(10))

    def get_input_shape(self):
        return (self.input_size, self.input_size, 1)

    def get_train_set(self):
        # No data augmentation
        return NumpySequence(self.X_train, self.y_train, self.batch_size,
                             image_normalization((self.input_size, self.input_size)))

    def get_validation_set(self):
        return NumpySequence(self.X_val, self.y_val, self.batch_size,
                             image_normalization((self.input_size, self.input_size)))

    def get_test_set(self):
        return NumpySequence(self.X_test, self.y_test, self.batch_size,
                             image_normalization((self.input_size, self.input_size)))

    def get_entire_dataset(self):
        return (self.X_train, self.y_train), (self.X_val, self.y_val), (self.X_test, self.y_test)
