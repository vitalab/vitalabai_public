import pickle

import numpy as np
from keras.utils import to_categorical

from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract
from VITALabAI.dataset.classification.utils import NumpySequence


class VITALMioVectors(VITALabAiDatasetAbstract):
    """Dataset to handel the feature vectors created by Mio Tcd networks
    """

    def __init__(self, path, bath_size=16, use_validation=False):
        """
        Args:
            path: string, path to the pickle file containing the vectors
                Must correspond to ((X_train, y_train), (X_test, y_test)) or
                ((self.X_train, self.y_train), (self.X_val, self.y_val), (self.X_test, self.y_test))
            bath_size: int, size of the batches to return
            use_validation: bool, if True a validation set is used
        """
        self.path = path
        self.use_validation = use_validation
        self.X_train = None
        self.y_train = None
        self.X_val = None
        self.y_val = None
        self.X_test = None
        self.y_test = None
        self.nb_class = 11
        self.classes = ["articulated_truck", "bicycle", "bus", "car", "motorcycle",
                        "non-motorized_vehicle", "pedestrian", "pickup_truck",
                        "single_unit_truck", "work_van", "background"]

        self.batch_size = bath_size
        self.load_data()

    def load_data(self):
        """Load the data from the pickle file
        """

        print('Loading data')
        with open(self.path, 'rb') as f:
            vec_tuple = pickle.load(f, encoding='latin1')

        if len(vec_tuple) == 3:  # New version
            ((self.X_train, self.y_train),
             (self.X_val, self.y_val), (self.X_test, self.y_test)) = vec_tuple

            if self.use_validation:
                val_idx = np.random.choice(len(self.X_val), len(self.X_val))
                self.X_val = self.X_val[val_idx]
                self.y_val = self.y_val[val_idx]
                self.y_val = to_categorical(self.y_val, self.nb_class)
            else:
                self.X_train = np.append(self.X_train, self.X_val, axis=0)
                self.y_train = np.append(self.y_train, self.y_val, axis=0)

        else:  # Old version, no validation set
            ((self.X_train, self.y_train), (self.X_test, self.y_test)) = vec_tuple

        train_idx = np.random.choice(len(self.X_train), len(self.X_train))
        self.X_train = self.X_train[train_idx]
        self.y_train = self.y_train[train_idx]

        test_idx = np.random.choice(len(self.X_test), len(self.X_test))
        self.X_test = self.X_test[test_idx]
        self.y_test = self.y_test[test_idx]

        self.y_train = to_categorical(self.y_train, self.nb_class)
        self.y_test = to_categorical(self.y_test, self.nb_class)

    def get_classes(self):
        return self.classes

    def get_input_shape(self):
        return self.X_train.shape[1]

    def get_train_set(self):
        return NumpySequence(self.X_train, self.y_train, self.batch_size)

    def get_validation_set(self):
        if self.use_validation:
            return NumpySequence(self.X_val, self.y_val, self.batch_size)

    def get_test_set(self):
        return NumpySequence(self.X_test, self.y_test, self.batch_size)
