import os

from torchvision import transforms
from torchvision.datasets import CIFAR10

from VITALabAI.VITALabAiPytorchDatasetWrapper import VITALabAiPytorchDatasetWrapper


def get_cifar10_dataset(input_size=32):
    """Factory method that produces a dataset compatible with VITALabAiDatasetAbstract and pytorch

    Args:
        input_size: int, size of the images in the dataset

    Returns:
        Cifar10 Pytorch dataset
    """

    # Transforms (data preprocessing and augmentation)
    # Taken from https://github.com/pytorch/tutorials/blob/master/beginner_source/blitz/cifar10_tutorial.py
    norm_mean = [0.49139968, 0.48215827, 0.44653124]
    norm_std = [0.24703233, 0.24348505, 0.26158768]
    norm_transform = transforms.Normalize(norm_mean, norm_std)
    train_transform = transforms.Compose([
        transforms.RandomCrop(32, padding=4),
        transforms.Resize(input_size),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        norm_transform
    ])
    test_transform = transforms.Compose([
        transforms.Resize(input_size),
        transforms.ToTensor(),
        norm_transform
    ])

    cifar_root = os.environ.get('CIFAR', '/tmp/data')
    train_dataset = CIFAR10(cifar_root, train=True, transform=train_transform, download=True)
    test_dataset = CIFAR10(cifar_root, train=False, transform=test_transform, download=True)
    # IMPORTANT NOTE: the validation set uses the training transform!

    dataset = VITALabAiPytorchDatasetWrapper(train_dataset=train_dataset,
                                             test_dataset=test_dataset,
                                             validation=0.1,
                                             batch_size=32,
                                             num_classes=10)
    return dataset
