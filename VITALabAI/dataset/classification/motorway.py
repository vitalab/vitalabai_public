import os

import cv2
import numpy as np
from keras.utils import to_categorical, Sequence
from keras_preprocessing.image import ImageDataGenerator
from matplotlib import pyplot as plt
from scipy.misc import imread, imresize
from skimage.transform import resize
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import normalize

from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract


class VITALMotorway(VITALabAiDatasetAbstract):
    """VITALMotorway handles the IO for the Motorway image dataset.
    """
    TOTAL_NUM_IMG = 400  # the Motorway dataset contains 400 images
    CLASS_0_RANGE = range(0, 100)
    CLASS_1_RANGE = range(100, 200)
    CLASS_2_RANGE = range(200, 300)
    CLASS_3_RANGE = range(300, 400)

    def __init__(self, data_path, input_size: int = 224, batch_size: int = 8,
                 val_split=0.1, test_split=0.2, preprocess: bool = True, seed=1337,
                 use_da=True, **kwargs):
        """
        Args:
            data_path: string, Path to the images files.
            input_size: int, Size of the image
            batch_size: int, Size of the batches to return.
            val_split: float, Proportion of the train set to be used in the validation set
            test_split: float, Proportion of the dataset to be used in the test set
            preprocess: bool, when true, subtract the average color to each image
            (optional).
            seed: object, seed for the RNG
            use_da: bool, when True data augmentation is applied to the training set
            **kwargs:
        """
        self.input_size = input_size
        self.batch_size = batch_size

        self.data_file = data_path
        self.test_split = test_split
        self.input_size = input_size
        self.preprocess = preprocess
        self.use_da = use_da

        (x_train, y_train), (self.X_test, self.y_test) = self._load_data()

        y_train, self.y_test = map(to_categorical, [y_train, self.y_test])

        self.X_train, self.X_val, self.y_train, self.y_val = \
            train_test_split(x_train, y_train, test_size=val_split, random_state=seed)

        self.classes = ['empty', 'heavy', 'jam', 'fluid']

    def get_classes(self):
        return self.classes

    def get_input_shape(self):
        return (self.input_size, self.input_size, 3)

    def get_train_set(self):
        return MotorwaySequence(self.X_train, self.y_train, self.batch_size,
                                self.input_size, use_da=self.use_da)

    def get_validation_set(self):
        return MotorwaySequence(self.X_val, self.y_val, self.batch_size,
                                self.input_size, use_da=False)

    def get_test_set(self):
        return MotorwaySequence(self.X_test, self.y_test, self.batch_size,
                                self.input_size, use_da=False)

    def _load_data(self):
        """ This method returns all the data from the dataset into a numpy array after
        performing a random permutation.

        Returns:
            Tuple of ndarray with all the data.
        """

        self.all_imgs = np.zeros((self.TOTAL_NUM_IMG, self.input_size, self.input_size,
                                  3))
        print('Loading files...')

        for i in range(self.TOTAL_NUM_IMG):
            img_name = os.path.join(self.data_file,
                                    'allImages',
                                    '%06d.jpg' % (i + 1))
            self.all_imgs[i, :, :, :] = imresize(imread(img_name),
                                                 (self.input_size,
                                                  self.input_size)).astype(np.float32)

        self.all_labels = np.zeros(self.TOTAL_NUM_IMG)
        self.all_labels[self.CLASS_1_RANGE] = 1
        self.all_labels[self.CLASS_2_RANGE] = 2
        self.all_labels[self.CLASS_3_RANGE] = 3
        print('Done loading files')

        if self.preprocess:
            self.preprocess_dataset()

        rand_perm = np.random.permutation(self.TOTAL_NUM_IMG)
        train_size = np.uint32((1 - self.test_split) * self.TOTAL_NUM_IMG)

        X_train = self.all_imgs[rand_perm[:train_size], :, :, :]
        X_test = self.all_imgs[rand_perm[train_size:], :, :, :]

        Y_train = self.all_labels[rand_perm[:train_size]]
        Y_test = self.all_labels[rand_perm[train_size:self.TOTAL_NUM_IMG + 1]]

        return (X_train, Y_train), (X_test, Y_test)

    def preprocess_dataset(self):
        """
        This method is used when you want to preprocess all the data from the
        dataset. Here the average color is subtracted to each pixel.
        """

        mean_col = np.mean(self.all_imgs, (0, 1, 2))
        self.all_imgs -= mean_col[np.newaxis, np.newaxis, np.newaxis, :]

    def get_dataset_preprocessed_with_cnn(self, f):
        """ This method returns all the data into a numpy array after converting the
        images of the dataset into features output with the cnn model and performing a
        random permutation.

        Args:
            f: a keras backend CNN function.  Typically, f is a CNN without the last softmax
            layer.

        Returns:
            Tuple of ndarray with all the data preprocessed with the cnn

        """
        features = np.zeros((self.TOTAL_NUM_IMG, 4096))
        for i in range(self.TOTAL_NUM_IMG):
            im = self.all_imgs[i, :, :]
            # Make the image have the right shape for keras
            im = np.expand_dims(im, axis=0)
            features[i][:] = f([0, im])[0]

        # L2 normalize the feature vector
        all_cnn_features = normalize(features, norm='l2', axis=1)

        rand_perm = np.random.permutation(self.TOTAL_NUM_IMG)
        train_size = np.uint32((1 - self.test_split) * self.TOTAL_NUM_IMG)

        X_train = all_cnn_features[rand_perm[:train_size], :]
        X_test = all_cnn_features[rand_perm[train_size:], :]

        Y_train = self.all_labels[rand_perm[:train_size]]
        Y_test = self.all_labels[rand_perm[train_size:self.TOTAL_NUM_IMG + 1]]

        return (X_train, Y_train), (X_test, Y_test)

    def visualize_images(self):
        """Shows a batch of original images and augmented images(if use_da).
        """

        train_sequence_da = self.get_train_set()

        X, y, O = train_sequence_da.getitem_for_visualisation()

        def concat_images(X, y):
            font = cv2.FONT_HERSHEY_SIMPLEX
            img = None
            for i in range(len(X)):
                im = X[i]
                cv2.putText(im, self.get_classes()[np.argmax(y[i])], (120, 200), font, 1, (0, 0, 255), 2,
                            cv2.LINE_AA)
                if img is None:
                    img = im
                else:
                    img = np.concatenate((img, im), 1)
            return img

        img_da = concat_images(X, y)
        img_orig = concat_images(O, y)

        img = np.concatenate((img_orig, img_da), 0)
        img = img * 127 / 255  # rescale to floats between 0 and 1 for visualization

        plt.imshow(img)
        plt.show()


class MotorwaySequence(Sequence):
    """Create a Sequence from the list of file names.
    """
    def __init__(self, X, y, batch_size, input_size, use_da):
        """ Args:
                X: Array, Input data
                y: Array, Target data
                batch_size: int, Size of the batch to return
                input_size: int, size of returned images
                use_da: bool, if true each is augmented using keras' ImageDataGenerator

        """
        self.X, self.y = X, y
        self.batch_size = batch_size
        self.input_size = input_size
        self.use_da = use_da
        self.imgaug = ImageDataGenerator(width_shift_range=0.1,
                                         height_shift_range=0.1,
                                         horizontal_flip=True,
                                         cval=0, fill_mode='constant',
                                         rotation_range=5,
                                         zoom_range=0.2)

    def __len__(self):
        return int(np.ceil(len(self.X) / self.batch_size))

    def __getitem__(self, idx):
        """ Returns batch of augmented and standardized data (if use_da = True)

        Args:
            idx: index of the batch

        Returns:
            tuple of data and labels

        """
        X, y = (self.X[idx * self.batch_size:(idx + 1) * self.batch_size],
                self.y[idx * self.batch_size:(idx + 1) * self.batch_size])

        X = np.array([
            resize(x, (self.input_size, self.input_size), mode='constant')
            for x in X])

        X /= 127

        if self.use_da:
            for i in range(len(X)):
                X[i] = self.imgaug.random_transform(X[i])

        return X, y

    def getitem_for_visualisation(self, idx=0):
        """ Returns batch with original images (no da) and augmented images for visualisation.

        Args:
            idx: batch index

        Returns:
            tuple of augmented images, labels and original images

        """
        X, y = (self.X[idx * self.batch_size:(idx + 1) * self.batch_size],
                self.y[idx * self.batch_size:(idx + 1) * self.batch_size])

        X = np.array([
            resize(x, (self.input_size, self.input_size), mode='constant')
            for x in X])

        X /= 127
        O = X.copy()

        if self.use_da:
            for i in range(len(X)):
                X[i] = self.imgaug.random_transform(X[i])

        return X, y, O


if __name__ == '__main__':
    import argparse

    """ This script is used to visalize data augmentation
    Example usage of this script:

    python motorway.py --path=$MOTORWAY_DATASET --batch_size=4
    """

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to motorway dataset", type=str)
    aparser.add_argument("--batch_size", help="Number of images to visualize", type=int, default=4)

    args = aparser.parse_args()

    ds = VITALMotorway(batch_size=args.batch_size, data_path=args.path, use_da=True, preprocess=False)
    ds.visualize_images()
