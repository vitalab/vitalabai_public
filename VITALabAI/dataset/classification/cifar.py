from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract
from VITALabAI.dataset.classification.utils import image_normalization, NumpySequence
from keras.datasets import cifar10, cifar100
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split


class VITALCifar10(VITALabAiDatasetAbstract):
    """Wrapper for the CIFAR10
    """

    def __init__(self, input_size: int = 32, seed=1337, batch_size: int = 8,
                 val_split=0.1):
        """
        Args:
            input_size: int, Size of the image
            seed: object, seed for the RNG
            batch_size: int, Size of the batches to return.
            val_split: float, Proportion of the train set to be used in the validation set
        """
        self._get_data()

        self.input_size = input_size
        self.batch_size = batch_size

        y_train, self.y_test = map(to_categorical, [self.y_train, self.y_test])
        self.X_train, self.X_val, self.y_train, self.y_val = train_test_split(
            self.x_train, y_train, test_size=val_split, random_state=seed)

    def _get_data(self):
        self.nb_classes = 10
        (self.x_train, self.y_train), (self.X_test, self.y_test) = cifar10.load_data()

    def get_classes(self):
        return list(range(self.nb_classes))

    def get_input_shape(self):
        return (self.input_size, self.input_size, 3)

    def get_train_set(self):
        # No data augmentation
        return NumpySequence(self.X_train, self.y_train, self.batch_size,
                             image_normalization((self.input_size, self.input_size)))

    def get_validation_set(self):
        return NumpySequence(self.X_val, self.y_val, self.batch_size,
                             image_normalization((self.input_size, self.input_size)))

    def get_test_set(self):
        return NumpySequence(self.X_test, self.y_test, self.batch_size,
                             image_normalization((self.input_size, self.input_size)))


class VITALCifar100(VITALCifar10):
    """Wrapper for the CIFAR100
    """

    def _get_data(self):
        self.nb_classes = 100
        (self.x_train, self.y_train), (self.X_test, self.y_test) = cifar100.load_data()
