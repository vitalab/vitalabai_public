import csv
import os
import os.path as path
from collections import defaultdict

import numpy as np
from keras.utils import to_categorical, Sequence
from keras_preprocessing.image import ImageDataGenerator
from skimage.io import imread
from skimage.transform import resize

from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract

pjoin = path.join
listdir = os.listdir


class VITALMioTCD(VITALabAiDatasetAbstract):
    """
        VITALMioTCD handles the IO from the MioTCD-Classification challenge.
        Please visit the tcd.miovision.com project page for more details.
    """

    def __init__(self, data_path, input_size: int = 321, batch_size: int = 8,
                 scale=1., shift=(0., 0., 0.), use_bgr=False,
                 uniform_sampling=False, use_da=True, **kwargs):
        """
        Args:
            data_path: string, path to the MIO-TCD classification folder
            input_size: int, this parameter resizes it all to the same size (optional).
            batch_size: int, Size of the batches to return.
            scale: float, float to scale the images
            shift: tuple, tuple to shift the images
            use_bgr: bool, Whether or not to change from rgb to bgr.
            uniform_sampling: bool, Whether or not to sample uniformly from the dataset.
            use_da: bool, if True, data augmentation is applied to the training set.
            **kwargs:
        """
        self.input_size = input_size
        self.batch_size = batch_size
        self.datas = defaultdict(dict)

        self.use_da = use_da

        self.data_path = data_path
        self.input_size = input_size

        self.nb_class = 11
        self.classes = ["articulated_truck", "bicycle", "bus", "car", "motorcycle",
                        "non-motorized_vehicle", "pedestrian", "pickup_truck",
                        "single_unit_truck", "work_van", "background"]

        self.preprocess_scale = scale
        self.preprocess_shift = shift
        self.use_bgr = use_bgr
        self.uniform_sampling = uniform_sampling
        self.use_validation = True

        self._generate_dataset()

    def get_classes(self):
        return self.classes

    def get_input_shape(self):
        return (self.input_size, self.input_size, 3)

    def get_train_set(self):
        return MIOSequence(self.X_train, self.Y_train, self.batch_size, self.input_size,
                           self.preprocess_scale, self.preprocess_shift, self.use_bgr, use_da=self.use_da)

    def get_validation_set(self):
        return MIOSequence(self.X_valid, self.Y_valid, self.batch_size, self.input_size,
                           self.preprocess_scale, self.preprocess_shift, self.use_bgr, use_da=False)

    def get_test_set(self):
        return MIOSequence(self.X_test, self.Y_test, self.batch_size, self.input_size,
                           self.preprocess_scale, self.preprocess_shift, self.use_bgr, use_da=False)

    def _generate_dataset(self):
        """ Get the paths from the dataset, if `uniform_sampling`, create the structure
        to handle this case as well.
        """

        def get_class_dict(subfolder):
            """Returns a dict that maps from class_index to a list of filenames"""
            img_filenames_by_class_idx = defaultdict(list)
            for class_name in self.classes:
                filtered_filenames = [pjoin(self.data_path, subfolder, class_name, x)
                                      for x in listdir(pjoin(self.data_path,
                                                             subfolder, class_name))
                                      if x.endswith('.jpg')]
                class_index = self.classes.index(class_name)
                img_filenames_by_class_idx[class_index] = filtered_filenames
            return img_filenames_by_class_idx

        def get_test_class_dict(csvfile):
            img_filenames_by_class_idx = defaultdict(list)
            with open(pjoin(self.data_path, csvfile), "r") as f:
                reader = csv.reader(f, delimiter=",")
                for row in reader:
                    class_index = self.classes.index(row[1])
                    filename = pjoin(self.data_path, 'test', row[0] + '.jpg')
                    img_filenames_by_class_idx[class_index].append(filename)
            return img_filenames_by_class_idx

        def dict_to_lists(d):
            """Returns two lists (x, y), where x contains all samples
            and y the class_index of the corresp. samples"""
            x, y = [], []
            for idx, filenames in d.items():
                x += filenames
                # a list with `idx` repeated `len(filenames)` times
                y += [idx] * len(filenames)
            return np.array(x), np.array(y)

        def dict_to_uniform_lists(d):
            """Returns two lists (x, y), where x contains an equal number of samples from
            each class and y the class_index of the corresp. samples.
            Samples from smaller classes are duplicated"""
            x, y = [], []

            max_key = max(d, key=lambda x: len(d[x]))  # Get longest list in dict

            for i in range(len(d[max_key])):
                for class_idx, class_filenames in d.items():
                    x.append(class_filenames[i % len(class_filenames)])
                    y.append(class_idx)
            return np.array(x), np.array(y)

        # Get list of filenames for each class
        train_img_filenames_by_class_idx = get_class_dict('train')
        test_img_filenames_by_class_idx = get_test_class_dict('gt_test.csv')

        # Create the tuple (X,Y for train and test)
        if self.uniform_sampling:
            self.X_train, self.Y_train = \
                dict_to_uniform_lists(train_img_filenames_by_class_idx)
            self.X_test, self.Y_test = \
                dict_to_uniform_lists(test_img_filenames_by_class_idx)
        else:
            self.X_train, self.Y_train = dict_to_lists(train_img_filenames_by_class_idx)
            self.X_test, self.Y_test = dict_to_lists(test_img_filenames_by_class_idx)

        # Separate train set into 2 subsets to create the validation set
        self.n_train = int(0.8 * len(self.X_train))
        self.n_valid = len(self.X_train) - self.n_train
        self.n_test = len(self.X_test)

        # Before shuffling, the samples are sorted by class
        shuffled_idx = np.random.permutation(len(self.X_train))
        train_idx = shuffled_idx[:self.n_train]
        val_idx = shuffled_idx[self.n_train:]
        self.X_valid = self.X_train[val_idx]
        self.X_train = self.X_train[train_idx]  # Note that this line must be after the former

        self.Y_valid = self.Y_train[val_idx]
        self.Y_train = self.Y_train[train_idx]  # Note that this line must be after the former

        # Set targets to categorical (one-hot)
        self.Y_train = to_categorical(self.Y_train, self.nb_class)
        self.Y_test = to_categorical(self.Y_test, self.nb_class)
        self.Y_valid = to_categorical(self.Y_valid, self.nb_class)

    def set_preprocessing(self, scale, shift, use_bgr):
        self.preprocess_scale = scale
        self.preprocess_shift = shift
        self.use_bgr = use_bgr

    def preprocess_image(self, x):
        """ Preprocess image. Used while iterating through a directory.

        Args:
            x: image to preprocess

        Returns:
            preprocessed image
        """
        x = resize(x, (self.input_size, self.input_size), mode='constant')

        if self.use_bgr:
            x = x[:, :, ::-1]

        x *= self.preprocess_scale
        x += self.preprocess_shift

        return x

    def visualize_images(self):
        """Shows a batch of original images and augmented images(if use_da).
        """
        import cv2
        from matplotlib import pyplot as plt

        train_sequence = self.get_train_set()

        X, y, O = train_sequence.getitem_for_visualisation()

        def concat_images(X, y):
            font = cv2.FONT_HERSHEY_SIMPLEX
            img = None
            for i in range(len(X)):
                im = X[i]
                cv2.putText(im, self.get_classes()[np.argmax(y[i])], (120, 200), font, 1, (0, 0, 255), 2,
                            cv2.LINE_AA)
                if img is None:
                    img = im
                else:
                    img = np.concatenate((img, im), 1)
            return img

        img_da = concat_images(X, y)
        img_orig = concat_images(O, y)
        img = np.concatenate((img_orig, img_da), 0)

        if self.use_bgr:
            img = img[:, :, ::-1]

        plt.imshow(img)
        plt.show()


class MIOSequence(Sequence):
    """Create a Sequence from the list of file names."""

    def __init__(self, X, y, batch_size, input_size, scale, shift, use_bgr, use_da):
        """
            Args:
                X: Array, Input data
                y: Array, Target data
                batch_size: int, Size of the batch to return
                input_size: int, size of returned images
                scale: float, Float to scale the images
                shift: tuple, Tuple to shift the images
                use_bgr: bool, Whether or not to change from rgb to bgr.
                use_da: bool, if True, data augmentation is applied to each batch
        """
        self.X, self.y = X, y
        self.batch_size = batch_size
        self.input_size = input_size
        self.preprocess_scale = scale
        self.preprocess_shift = shift
        self.use_bgr = use_bgr
        self.use_da = use_da
        self.imgaug = ImageDataGenerator(width_shift_range=0.1,
                                         height_shift_range=0.1,
                                         horizontal_flip=True,
                                         cval=0, fill_mode='constant',
                                         rotation_range=5,
                                         zoom_range=0.2)

    def __len__(self):
        return int(np.ceil(len(self.X) / self.batch_size))

    def __getitem__(self, idx):
        """ This method loads the images, creates a batch and preprocesses it.

        Args:
            idx: int, index of the sequence

        Returns:
            tuple of images and labels
        """
        X, y = (self.X[idx * self.batch_size:(idx + 1) * self.batch_size],
                self.y[idx * self.batch_size:(idx + 1) * self.batch_size])

        X = np.array([
            resize(imread(file_name), (self.input_size, self.input_size), mode='constant')
            for file_name in X])

        if self.use_bgr:
            X = X[:, :, :, ::-1]
        X *= self.preprocess_scale
        X += self.preprocess_shift

        if self.use_da:
            for i in range(len(X)):
                X[i] = self.imgaug.random_transform(X[i])

        return X, y

    def getitem_for_visualisation(self, idx=0):
        """ Returns batch with original images (no da) for visualisation.

        Args:
            idx: batch index

        Returns:
            tuple of augmented images, labels and original images

        """
        X, y = (self.X[idx * self.batch_size:(idx + 1) * self.batch_size],
                self.y[idx * self.batch_size:(idx + 1) * self.batch_size])

        X = np.array([
            resize(imread(file_name), (self.input_size, self.input_size), mode='constant')
            for file_name in X])

        if self.use_bgr:
            X = X[:, :, :, ::-1]
        X *= self.preprocess_scale
        X += self.preprocess_shift

        O = X.copy()

        if self.use_da:
            for i in range(len(X)):
                X[i] = self.imgaug.random_transform(X[i])

        return X, y, O


if __name__ == '__main__':
    import argparse

    """ This script is used to visalize data augmentation
    Example usage of this script:

    python mio_tcd.py --path=$MIO_CLASSIFICATION_PATH --batch_size=4
    """

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to motorway dataset", type=str)
    aparser.add_argument("--batch_size", help="Number of images to visualize", type=int, default=4)

    args = aparser.parse_args()

    ds = VITALMioTCD(batch_size=args.batch_size, data_path=args.path)
    ds.visualize_images()
