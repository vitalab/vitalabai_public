import abc
import warnings
from datetime import datetime
from typing import Callable, Dict, Type

import torch
import torch.nn
import torch.optim
from ignite.engine import Engine, Events, create_supervised_trainer, create_supervised_evaluator
from ignite.handlers import ModelCheckpoint
from ignite.metrics import Loss, Metric

from VITALabAI.VITALabAiAbstract import VITALabAiAbstract, Backend
from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract


class VITALabAiPytorchAbstract(VITALabAiAbstract, metaclass=abc.ABCMeta):
    """Base class for a PyTorch model"""

    def __init__(self, dataset: VITALabAiDatasetAbstract,
                 loss_fn: torch.nn.Module,
                 optimizer_factory: Callable[[torch.nn.Module], torch.optim.Optimizer],
                 metrics: Dict[str, Metric]=None,
                 use_cuda: bool = True):
        """
        Args:
            dataset: A dataset that yield pytorch tensors
            loss_fn: the objective function.
            optimizer_factory: a Callable that takes a torch.nn.Module and returns a
                               torch.nn.Optimizer. See optimizer_setup().
            metrics: a dict that maps metric names to ignite.metrics objects
            use_cuda: bool, If True, the GPU is used

        Example:
            ```
            class MyNet(VITALabAiPytorchAbstract):
                ...
            dataset = ...
            loss_fn = torch.nn.CrossEntropyLoss()
            model_trainer = MyNet(dataset=dataset,
                loss_fn=loss_fn,
                optimizer_factory=optimizer_setup(torch.optim.Adam, lr=1e-3),
                metrics={'loss': Loss(loss_fn), 'accuracy': CategoricalAccuracy()}
                )
            model_trainer.train(max_epochs=10)
            ```
        """

        super().__init__(dataset)  # builds the model -> self.model

        device_name = 'cuda' if use_cuda else 'cpu'
        if use_cuda and not torch.cuda.is_available():
            warnings.warn("CUDA is not available. Suppress this warning by passing "
                          "use_cuda=False to {}()."
                          .format(self.__class__.__name__), RuntimeWarning)
            device_name = 'cpu'

        self.device = torch.device(device_name)
        self.loss_fn = loss_fn
        self.optimizer = optimizer_factory(self.model)
        self.metrics = metrics

    def get_backend(self):
        return Backend.PYTORCH

    def train(self, max_epochs, start_epoch=0, **kwargs):
        """Build ignite engines for training and evaluation, setup events,
        setup checkpointing, launch training.

        Args:
            max_epochs: int, maximum number of epochs
            start_epoch: int, epoch to start training at
            **kwargs: dict with keywords to be used.
        """

        metrics = self.metrics if self.metrics is not None else {'loss': Loss(self.loss_fn)}
        trainer = create_supervised_trainer(self.model, self.optimizer, self.loss_fn,
                                            self.device)
        evaluator = create_supervised_evaluator(self.model, metrics=metrics,
                                                device=self.device)

        # Events ----------------------------------------------
        @trainer.on(Events.ITERATION_COMPLETED)
        def log_training_loss(trainer: Engine):
            if trainer.state.iteration % 100 == 0:
                total_iters = trainer.state.iteration
                iters_per_epoch = len(trainer.state.dataloader)
                epochs = trainer.state.epoch - 1
                progress = int((total_iters - epochs * iters_per_epoch) / iters_per_epoch * 100)
                print("Epoch {} {}%  Loss: {}".format(trainer.state.epoch, progress, trainer.state.output))

        @trainer.on(Events.EPOCH_COMPLETED)
        def compute_metrics(trainer):
            # Training metrics
            print('Evaluating on training set...', end='\r', flush=True)
            # NOTE: Predicts on whole training set
            evaluator.run(self.dataset.get_train_set())
            metrics = evaluator.state.metrics
            metrics_str = '  '.join(['{}: {:.2f}'.format(n, v) for n, v in metrics.items()])
            print("Training metrics   - Epoch: " + str(trainer.state.epoch) + "  " + metrics_str)
            # Validation metrics
            print('Evaluating on validation set...', end='\r', flush=True)
            evaluator.run(self.dataset.get_validation_set())
            metrics = evaluator.state.metrics
            metrics_str = '  '.join(['{}: {:.2f}'.format(n, v) for n, v in metrics.items()])
            print("Validation metrics - Epoch: " + str(trainer.state.epoch) + "  " + metrics_str)

        # Checkpoint saving -----------------------------------
        def checkpoint_score(_):
            val_loss = evaluator.state.metrics['loss']  # val_loss is computed last
            return -val_loss

        run_id = self.__class__.__name__ + '_' + datetime.now().strftime("%m-%d_%H-%M-%S_%f")
        ckpt_handler = ModelCheckpoint('weights', run_id, score_function=checkpoint_score,
                                       score_name='loss', save_as_state_dict=True)
        trainer.add_event_handler(Events.EPOCH_COMPLETED, ckpt_handler, {'model': self.model})

        # Launch training -------------------------------------------
        print('Launching training...')
        trainer.run(self.dataset.get_train_set(), max_epochs=max_epochs)

    def predict(self, only_compute_metrics=False, **kwargs):
        """Build ignite engine for predicting and launch it

        Args:
            only_compute_metrics: bool, if True only the metrics are returned
            **kwargs: dict with keywords to be used.

        Returns:
            predictions or metrics
        """

        metrics = self.metrics if self.metrics is not None else {'loss': Loss(self.loss_fn)}
        predictor = create_supervised_evaluator(self.model, metrics=metrics, device=self.device)
        pred_batches = []

        def store_prediction(engine):
            y_pred, y = engine.state.output
            pred_batches.append(y_pred)

        if not only_compute_metrics:
            predictor.add_event_handler(Events.ITERATION_COMPLETED, store_prediction)

        # Predict on all test set
        predictor.run(self.dataset.get_test_set())

        # Print metrics
        metrics = predictor.state.metrics
        metrics_str = '  '.join(['{}: {:.2f}'.format(n, v) for n, v in metrics.items()])
        print("Test  " + metrics_str)

        if only_compute_metrics:
            return metrics
        else:
            # Concatenate predictions and return them
            return torch.cat(pred_batches, dim=0)

    def predict_on_batch(self, batch):
        """Predict on a single batch"""
        with torch.no_grad():
            return self.model.forward(batch)

    def evaluate(self):
        """Compute metrics on test set and return them

        Returns: A dict that maps metric names to average values
        """
        print('Predicting on test set...')
        return self.predict(only_compute_metrics=True)

    def save(self, filename=None):
        """Save the model"""
        filename = filename if filename is not None else self.model.__class__.__name__ + '.pt'
        torch.save(self.model.state_dict(), filename)

    def load_model(self, filepath, **kwargs):
        raise NotImplementedError('VITALabAI does not support loading a model '
                                  'architecture with PyTorch. Please use load_weights().')

    def load_weights(self, filepath, **kwargs):
        """Load the model's weights"""
        self.model.load_state_dict(torch.load(filepath))


def optimizer_setup(optimizer_class: Type[torch.optim.Optimizer], **hyperparameters) ->\
        Callable[[torch.nn.Module], torch.optim.Optimizer]:
    """Creates a factory method that can instantiate optimizer_class with the given
    hyperparameters

    Why this? torch.optim.Optimizer takes the model's parameters as an argument. Thus,
    we  cannot pass an Optimizer to the VITALabAiPytorchAbstract constructor.

    Args:
        optimizer_class: optimizer used to train the model
        **hyperparameters: hyperparameters for the model

    Returns:
        function to setup the optimizer
    """

    def f(optimized_model):
        return optimizer_class(optimized_model.parameters(), **hyperparameters)

    return f
