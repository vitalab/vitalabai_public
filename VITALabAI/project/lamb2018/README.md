# Lamb project

### Contributors

* Thierry Judge (thierry.judge@usherbrooke.ca)

### Description

This project consists of the segmentation of CTscan images of lamb. The goal is to segment the thigh and the 
loin from the lamb. 

# Dataset

To use this model, a HDF5 dataset containing the training, validation and testing data must have been 
created. 

To create the dataset use the file VITALabAI/dataset/semanticsegmentation/lamb2018/generatedataset.py as 
follows:


```bash
python generatedataset.py --path='path/to/folder/cointaing/lamb/files --name'name/of/hdf5'
```



# Models 

------------------------------------------------------------------------------

## UNET

### How to use `lamb_unet.py`

```bash
python lamb_unet.py --path='path/of/output.hdf5'
```

### Parameters explanation

* `--path`, path of the h5 file that contains the lamb2018 dataset.
* `--batch_size`, is optional (default `32`), choose the size of the batch feeded to the network.
* `--nb_epochs`, is optional (default `100`), number of epoch to train on the dataset.
* `--weight`, is optional (default None), path to a pretrained weights.
* `--testonly`, is optional (default `False`). This options allows to skip the training.
* `--display`, is optional (defautl `False`), display the output segmentation map.
* `--name`, is optional (default is current folder), path where you want to save all the data.
* `--lr`, is optional (default `3e-4`), learning rate.
* `--patience`, is optional (default `10`), Number of epochs for which the loss did not improve for early stopping

