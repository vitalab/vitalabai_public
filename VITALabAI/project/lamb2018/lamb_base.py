import h5py
from os.path import join
import numpy as np
import os

from keras.callbacks import TensorBoard, ReduceLROnPlateau, EarlyStopping, ModelCheckpoint
from tqdm import tqdm

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.dataset.acdc.utils import save_nii_file
from VITALabAI.project.lamb2018.visualcallback import VisualCallback
from VITALabAI.utils.vitalutils import create_model_directories
import csv


class LambBase(VITALabAiKerasAbstract):
    """Base model to handle the Lamb dataset segmentation."""

    def __init__(self, dataset: VITALabAiDatasetAbstract, loss_fn=None, optimizer=None, metrics=None,
                 nb_feature_maps=32, alpha=1e-5, name=None, display=False):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            nb_feature_maps: int, Number of feature maps at beginning of the network, automatic scaling for
                            the following layers.
            alpha: float, Alpha in the regularizer
            name: string, name of the model
            display: bool, if True predictions are displayed at the en of each epoch
        """
        self.nb_feature_maps = nb_feature_maps
        self.alpha = alpha
        super().__init__(dataset, loss_fn, optimizer, metrics)

        self.display = display
        self.name = self.__class__.__name__ if name is None else name
        self.name, self.checkpoint_dir, self.logs_dir, self.screen_dir, self.results_dir = \
            create_model_directories(self.name, ["ModelCheckpoint",
                                                 "LOGS",
                                                 "SCREEN",
                                                 "RESULTS"])

    def build_model(self):
        raise NotImplementedError("Abstract class")

    def predict_and_save(self, sequence, f5h):
        raise NotImplementedError("Abstract class.")

    def evaluate(self, **kwargs):
        """Evaluate the model and save the results in hdf5 files for each set.

        Args:
            **kwargs: additional parameters
        """

        print("Testing on train set...")
        train_value = self.model.evaluate_generator(self.dataset.get_train_set(),
                                                    verbose=1)
        print("Train : Loss: {}, Accuracy: {}, , Dice: {}".format(train_value[0], train_value[1],
                                                                  train_value[2]))
        print("Testing on validation set...")
        val_value = self.model.evaluate_generator(self.dataset.get_validation_set(),
                                                  verbose=1)
        print("Val : Loss: {}, Accuracy: {}, , Dice: {}".format(val_value[0], val_value[1],
                                                                val_value[2]))
        print("Testing on test set...")
        test_value = self.model.evaluate_generator(self.dataset.get_test_set(),
                                                   verbose=1)
        print("Test : Loss: {}, Accuracy: {}, Dice: {}".format(test_value[0], test_value[1],
                                                               test_value[2]))

        with open(join(self.results_dir, 'results.csv'), 'w') as csv_file:
            file_writer = csv.writer(csv_file, delimiter=',')
            file_writer.writerow(['Set', 'loss', 'accuracy', 'dice'])
            file_writer.writerow(['Train', str(train_value[0]), str(train_value[1]), str(train_value[2])])
            file_writer.writerow(['Val', str(val_value[0]), str(val_value[1]), str(val_value[2])])
            file_writer.writerow(['Test', str(test_value[0]), str(test_value[1]), str(test_value[2])])

        print("Saving train set predictions...")
        with h5py.File(join(self.results_dir, 'TRAIN_PREDICTION'), "w") as f5h:
            self.predict_and_save(self.dataset.get_set_for_prediction('train'), f5h)

        print("Saving validation set predictions...")
        with h5py.File(join(self.results_dir, 'VALID_PREDICTION'), "w") as f5h:
            self.predict_and_save(self.dataset.get_set_for_prediction('validation'), f5h)

        print("Saving test set predictions...")
        with h5py.File(join(self.results_dir, 'TEST_PREDICTION'), "w") as f5h:
            self.predict_and_save(self.dataset.get_set_for_prediction('test'), f5h)

    def save_to_h5f(self, group, img, gt, pred, v_size):
        """ This method saves the data from one lamb into a HDF5 group.

        Args:
            group: hdf5 group, group to save the data in
            img: array, input slices
            gt: array, gt slices
            pred: array, predicted slices
            v_size: list, voxel size
        """
        group.create_dataset("image", data=img, compression='gzip')
        group.create_dataset("gt_c", data=gt, compression='gzip')
        group.create_dataset("pred_c", data=pred, compression='gzip')

        gt = gt.argmax(axis=-1)
        pred = pred.argmax(axis=-1)
        group.create_dataset("gt_m", data=gt, compression='gzip')
        group.create_dataset("pred_m", data=pred, compression='gzip')

        group.attrs['voxel_size'] = v_size

    def export_results(self):
        """This method exports results from the HDF5 files to nifti format for each set."""
        print("Exporting results...")
        train_set = join(self.results_dir, "TRAIN_PREDICTION")
        valid_set = join(self.results_dir, "VALID_PREDICTION")
        test_set = join(self.results_dir, "TEST_PREDICTION")

        nii_images = join(self.results_dir, "NIFTI")
        if not os.path.isdir(nii_images):
            os.makedirs(nii_images)

        for dataset, which_set in zip([train_set, valid_set, test_set], ["train", "valid", "test"]):
            with h5py.File(dataset, "r") as h5f:
                self.save_nifti(nii_images, which_set, h5f)

    def save_nifti(self, path, set, h5file):
        """This method loads the data from a hdf5 file and saves it to nifiti format.

        Args:
            path: string, path to save the nifiti files to.
            set: string, set being saved to nifiti.
            h5file: h5py File, file to be loaded.
        """
        save_images = join(path, set)
        if not os.path.isdir(save_images):
            os.makedirs(save_images)

        for name in tqdm(h5file.keys(), desc="Save nifti"):
            # Each of these names 'pred_m', ... are defined during
            # the prediction task. We only get the results of the useful ones
            # and save into nifti format.

            save_path = join(save_images, name)
            if not os.path.isdir(save_path):
                os.makedirs(save_path)

            pred = h5file[name]['pred_m'][:].squeeze()
            ground = h5file[name]['gt_m'][:].squeeze()
            img = h5file[name]['image'][:].squeeze()
            voxel = h5file[name].attrs.get('voxel_size', [1, 1, 1])

            # We save it in "0, 1, s"
            if len(pred.shape) < 3:
                pred = pred[np.newaxis]
            pred = pred.transpose((1, 2, 0))

            if len(ground.shape) < 3:
                ground = ground[np.newaxis]
            ground = ground.transpose((1, 2, 0))

            if len(img.shape) < 3:
                img = img[np.newaxis]
            img = img.transpose((1, 2, 0))

            datas = [pred, ground, img]
            tags = ["prediction", "groundtruth", "image"]
            for data, tag in zip(datas, tags):
                save_nii_file(data,
                              os.path.join(save_path,
                                           "{}_{}.nii.gz".format(name, tag)),
                              zoom=voxel)

            for classe in range(1, pred.max() + 1):
                tmp_classe = np.copy(pred)
                tmp_classe[tmp_classe != classe] = 0
                tmp_classe = np.clip(tmp_classe, 0, 1).astype(np.uint8)
                img_path = join(save_path,
                                "{}_C_{}.nii.gz".format(name, classe))
                save_nii_file(tmp_classe, img_path, zoom=voxel, dtype=np.uint8)

    def get_callbacks(self, early_stopping, reduce_lr, patience, learning_rate):
        """Returns callbacks frequently used for the lamb models.

        Args:
            early_stopping: bool, whether to use early stopping with patience = patience.
            reduce_lr: bool, whether to use lr reduction with patience = patience // 2.
            patience: int, patience for early_stopping and reduce_lr
            learning_rate: float, learning rate used to determine min_lr for reduce_lr

        Returns:
            list of callback to be used during training.
        """
        tensorboard = TensorBoard(self.logs_dir, write_graph=True)

        min_lr = learning_rate / 20
        reduce_lr_callback = ReduceLROnPlateau(monitor='val_loss', factor=0.5, verbose=1,
                                               patience=patience // 2, min_lr=min_lr)

        early_stopping_callback = EarlyStopping(monitor='val_loss', patience=patience, verbose=1)

        details = self.__class__.__name__
        details += '_{epoch:03d}' + '_{val_loss:.4f}.h5'

        model_saver = ModelCheckpoint(join(self.checkpoint_dir, details), monitor='val_loss',
                                      save_best_only=True,
                                      save_weights_only=True,
                                      verbose=1)

        list_callback = [tensorboard, model_saver]
        if early_stopping:
            list_callback.append(early_stopping_callback)
        if reduce_lr:
            list_callback.append(reduce_lr_callback)

        if self.display:
            list_callback.append(
                VisualCallback(self.get_validation_set(), save_dir=join(self.name, "SCREEN")))

        return list_callback
