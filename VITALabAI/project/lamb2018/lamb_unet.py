import argparse

import keras
from tqdm import tqdm

from VITALabAI.dataset.semanticsegmentation.lamb2018.lamb2018 import VITALLamb2018
from VITALabAI.model.semanticsegmentation.losses import dice_crossentropy_loss, dice_coef
from VITALabAI.model.semanticsegmentation.unet import VITALUnet
from VITALabAI.project.lamb2018.lamb_base import LambBase


class LambUnet(LambBase):
    """Unet model for Lamb dataset segmentation."""

    def build_model(self):
        return VITALUnet(self.dataset, nb_feature_maps=self.nb_feature_maps).get_model()

    def predict_and_save(self, sequence, f5h):
        """ This method iterates through a sequence and saves predictions.

        Args:
            sequence: Keras Sequence, sequence to iterate through.
            f5h: h5py file, file in which predictions and other data is saved.
        """
        for i in tqdm(range(len(sequence))):
            name, img, gt, v_size = sequence[i]

            pred = self.model.predict(img)
            group = f5h.create_group(name)
            self.save_to_h5f(group, img, gt, pred, v_size)


if __name__ == '__main__':
    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the dataset", type=str, required=True)
    aparser.add_argument("--weight", type=str, dest="weight", help="Weights h5 file", default=None)
    aparser.add_argument("--batch_size", type=int, dest="batch_size", help="Batch size", default=4)
    aparser.add_argument("--patience", type=int, dest="patience",
                         help="number of epochs for which the loss did not improve for early stopping",
                         default=10)
    aparser.add_argument("--lr", type=float, dest="lr", help="learning rate", default=0.0001)
    aparser.add_argument("--name", type=str, dest="name", help="Model path", default=None)
    aparser.add_argument("--nb_epochs", type=int, dest="nb_epochs", help="Number of epochs", default=100)
    aparser.add_argument("--display", dest="display", action="store_true", help="Display results during "
                                                                                "training")
    aparser.add_argument("--testonly", action='store_true', dest="testonly",
                         help="Skip training and do test phase")

    args = aparser.parse_args()

    ds = VITALLamb2018(path=args.path, batch_size=args.batch_size)

    optim = keras.optimizers.Adam(lr=args.lr)
    losses = dice_crossentropy_loss

    model = LambUnet(ds, loss_fn=losses, optimizer=optim, metrics=['accuracy', dice_coef],
                     display=args.display, name=args.name)

    if args.weight is not None:
        print("Loading weights...")
        model.load_weights(args.weight)
        print("Done.")

    callbacks = model.get_callbacks(early_stopping=True, reduce_lr=True, patience=args.patience,
                                    learning_rate=args.lr)

    print("Model create at: {}".format(model.name))

    if not args.testonly:
        model.train(epochs=args.nb_epochs, callbacks=callbacks, workers=1)

    model.save()
    model.evaluate()
    model.export_results()
