import random

import numpy as np
from keras.callbacks import Callback

from VITALabAI.utils.display import Display


class VisualCallback(Callback):
    """Keras Callback to visualize segmentation results at each epoch end."""

    def __init__(self, sequence, save_dir=None):
        """
        Args:
            sequence: Keras Sequence, sequence to get images for prediction.
            save_dir: string, if not None, directory to save screenshots of the predictions.
        """
        super().__init__()
        self.sequence = sequence
        self.img_per_row = min(sequence.batch_size, 3)
        self.display = Display(save_dir, img_per_row=self.img_per_row)

    def on_epoch_end(self, epoch, logs=None):
        """ Method called at the end of each epoch.
        Updates the visual display with new predictions.

        Args:
            epoch: int, current epoch
            logs: None
        """
        idx = random.randint(0, len(self.sequence) - 1)

        x, gt = self.sequence[idx]
        seg = self.model.predict(x)

        seg = seg[:self.img_per_row, :, :, 1:]
        gt = gt[:self.img_per_row, :, :, 1:]

        z = np.zeros_like(seg)

        seg = np.concatenate((seg, z[:, :, :, :1]), axis=-1)
        gt = np.concatenate((gt, z[:, :, :, :1]), axis=-1)

        data = np.concatenate((seg, gt))

        self.display.pydisplay(data, epoch)
