# MioTCD Classification

This module provide utilities to create a model for **MioTCD Classification Challenge**
Mio-tcd is one of the largest dataset devoted to traffic analytic ever made. It can be downloaded here : [tcd.miovision.com]

------

This module provides the following models

- [X] Resnet50 [Deep Residual Learning for Image Recognition](https://arxiv.org/abs/1512.03385)
- [X] VGG19 [Very Deep Convolutional Networks for Large-Scale Image Recognition](https://arxiv.org/abs/1409.1556)
- [X] Xception [Xception: Deep Learning with Depthwise Separable Convolutions](https://arxiv.org/abs/1610.02357)
- [X] AlexNet [ImageNet Classification with Deep Convolutional Neural Networks] (https://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf)
- [X] InceptionV3 [Rethinking the Inception Architecture for Computer Vision](http://arxiv.org/abs/1512.00567)
- [X] SVM taking _deep feature vectors_ as input (Using scikit-learn's SGDClassifier)
- [ ] GoogleNet [Going Deeper with Convolutions] (https://arxiv.org/abs/1409.4842)
- [ ] DAC (Deep active contours) (Not published yet)

------

## Results

------

The models are saved locally at $VITAL_ROOT/VITALabAI/model/mio_tcd.
This module requires **Keras >=2.0.0** since it needs keras.applications.


## How to run the models 

First, go inside the `project/mio_tcd` folder.

**NOTE:** For an up-to-date list of options to use with a model, see the function `parse_args()` inside its script.

Important options:

* `--epochs <int>` Maximum number of epochs
* `--weights <path>` Specify a hdf5 weight file to load
* `--batch-size <path>` Size of each training batch
* `--uniform` Activate uniform sampling
* `--patience` Number of epochs for which the loss did not improve for early stopping
* `--no_da` Disable data augmentation
* `--testonly` Skip training and predict on the provided weights (use in conjuction with `--weight`)
* `--make-vec` Convert the dataset into _deep feature vectors_. Use with a trained network.
    * The model must implement the get_embedding_model method.

### Xception

Note that Xception is only available with the **Tensorflow** backend since it uses depthwise convolutions.

```bash
python train_mio_tcd_classification.py --path=$MIO_CLASSIFICATION_PATH --model=xception
```

### Resnet50

```bash
python train_mio_tcd_classification.py --path=$MIO_CLASSIFICATION_PATH --model=resnet
```

### AlexNet

```bash
python train_mio_tcd_classification.py --path=$MIO_CLASSIFICATION_PATH --model=alexnet

```

### VGG16

```bash
python train_mio_tcd_classification.py --path=$MIO_CLASSIFICATION_PATH --model=vgg16
```

### VGG19

```bash
python train_mio_tcd_classification.py --path=$MIO_CLASSIFICATION_PATH --model=vgg19
```

### VGGBN

```bash
python train_mio_tcd_classification.py --path=$MIO_CLASSIFICATION_PATH --model=vgg19bn
```

### Inception V3

```bash
python train_mio_tcd_classification.py --path=$MIO_CLASSIFICATION_PATH --model=inceptionv3
```

### DenseNet

```bash
python train_mio_tcd_classification.py --path=$MIO_CLASSIFICATION_PATH --model=densenet
```

### SVM

```bash
python train_mio_tcd_classification.py --path=PATH_TO_VECTORS --model=svm
```

* `--path` is the `.pkl` file containing the "vectorized" dataset. Those are on the share in the folder 
`mio-tcd/classification_challenge/vectors`.

## TensorBoard

Example for launching Tensorboard for MioXception:

```bash
cd VITALabAI/logs
tensorboard --logdir .
```