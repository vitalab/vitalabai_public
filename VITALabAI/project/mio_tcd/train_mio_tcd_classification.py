import argparse
import datetime
from os.path import join

import keras
from keras.callbacks import TensorBoard, EarlyStopping, CSVLogger, ModelCheckpoint

from VITALabAI.dataset.classification.mio_tcd import VITALMioTCD
from VITALabAI.dataset.classification.mio_tcd_vectors import VITALMioVectors
from VITALabAI.model.classification.alexnet import VITALAlexnet
from VITALabAI.model.classification.cnn import VITALCnn
from VITALabAI.model.classification.densenet import VITALDenseNet
from VITALabAI.model.classification.inceptionv3 import VITALInceptionV3
from VITALabAI.model.classification.resnet import VITALResnet
from VITALabAI.model.classification.svm import VITALSvm
from VITALabAI.model.classification.vgg16 import VITALVGG16
from VITALabAI.model.classification.vgg19 import VITALVGG19
from VITALabAI.model.classification.vggbn import VITALVGG19BN
from VITALabAI.model.classification.xception import VITALXception
from VITALabAI.project.mio_tcd.mio_tcd_classification_utils import *

"""
Example usage of this script:

    python train_mio_tcd_classification.py --path=$MIO_CLASSIFICATION_PATH --model=cnn

    Note: when using the svm model, the path must be to a pickle file containing the vectors.
        (generated using the --make-vec flag)
"""
if __name__ == '__main__':
    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to mio_tcd dataset", type=str)
    aparser.add_argument("--model", help="Model to train", choices=['cnn', 'vgg16', 'resnet', 'alexnet',
                                                                    'inceptionv3', 'vgg19', 'vgg19bn',
                                                                    'xception', 'svm', 'densenet'],
                         type=str, default='vgg16')
    aparser.add_argument("--no_da", dest="use_da", action='store_false', help="Disable data augmentation")
    aparser.add_argument("--batch-size", type=int, dest="batch_size", help="Batch size", default=30)
    aparser.add_argument("--weights", type=str, dest="weights", help="Weights h5 file", default=None)
    aparser.add_argument("--epochs", type=int, dest="epochs", help="Number of training epochs", default=200)
    aparser.add_argument("--patience", type=int, dest="patience",
                         help="number of epochs for which the loss did not improve for early stopping",
                         default=10)
    aparser.add_argument("--make-vec", dest="make_vec", action='store_true', help="Generate feature vectors")
    aparser.add_argument("--testonly", action='store_true', dest="testonly",
                         help="Skip training and do test phase")
    aparser.add_argument("--uniform", action='store_true', dest="uniform", help="Activate uniform sampling")

    args = aparser.parse_args()

    if args.model == 'svm':
        ds = VITALMioVectors(path=args.path)
        model = VITALSvm(ds)
        model.train()
        model.evaluate()
        exit()

    ds = VITALMioTCD(data_path=args.path, batch_size=args.batch_size, use_da=args.use_da, input_size=200,
                     uniform_sampling=args.uniform)

    optim = keras.optimizers.adam(lr=0.0001)
    if args.model == 'vgg16':
        model = VITALVGG16(ds, optimizer=optim, loss_fn='categorical_crossentropy', metrics=['accuracy'],
                           weights='imagenet')
    elif args.model == 'resnet':
        model = VITALResnet(ds, optimizer=optim, loss_fn='categorical_crossentropy', metrics=['accuracy'],
                            weights='imagenet')
    elif args.model == 'alexnet':
        ds.input_size = 227
        model = VITALAlexnet(ds, optimizer=optim, loss_fn='categorical_crossentropy',
                             metrics=['accuracy'], weights=args.weights)
    elif args.model == 'inceptionv3':
        model = VITALInceptionV3(ds, optimizer=optim, loss_fn='categorical_crossentropy',
                                 metrics=['accuracy'], weights=args.weights)
    elif args.model == 'vgg19':
        model = VITALVGG19(ds, optimizer=optim, loss_fn='categorical_crossentropy',
                           metrics=['accuracy'], weights=args.weights)
    elif args.model == 'vgg19bn':
        model = VITALVGG19BN(ds, optimizer=optim, loss_fn='categorical_crossentropy',
                             metrics=['accuracy'], pretrained=True)
    elif args.model == 'xception':
        model = VITALXception(ds, optimizer=optim, loss_fn='categorical_crossentropy',
                              metrics=['accuracy'], weights=args.weights)
    elif args.model == 'densenet':
        ds.input_size = 221
        model = VITALDenseNet(ds, optimizer=optim, loss_fn='categorical_crossentropy',
                              metrics=['accuracy'], weights=args.weights)
    else:  # CNN'
        model = VITALCnn(ds, optimizer='adam', loss_fn='categorical_crossentropy',
                         metrics=['accuracy'])

    ds.set_preprocessing(**model.get_preprocessing())

    now = datetime.datetime.now()
    name = "{}_{}{}".format(args.model, now.strftime("%Y-%m-%d-%H:%M"), ("_da" if args.use_da else ""))
    tensor_board = TensorBoard('logs/{}/'.format(name), write_graph=True,
                               write_images=True)

    early_stop = EarlyStopping(monitor='val_loss', patience=args.patience, verbose=1)

    details = args.model + '_{epoch:03d}on' + str(args.epochs) + '_{val_loss:.4f}.h5'
    if not os.path.exists("ModelCheckpoint"):
        os.mkdir("ModelCheckpoint")
    model_saver = ModelCheckpoint(join("ModelCheckpoint", details), monitor='val_loss', save_best_only=True,
                                  save_weights_only=True, verbose=1)

    initial_epoch = 0
    if args.weights is not None:
        print("Loading weights: {}".format(args.weights))
        model.load_weights(args.weights)
        _, file = os.path.split(args.weights)
        initial_epoch = int(file.replace(args.model, '')[1:4])

    if not args.testonly:
        model.train(epochs=args.epochs, callbacks=[early_stop,
                                                   tensor_board,
                                                   CSVLogger(name + '.csv'),
                                                   model_saver],
                    workers=1, initial_epoch=initial_epoch)

    train_value, val_value, test_value = model.evaluate(save_errors=False)

    with open('results.csv', 'a') as csv_file:
        file_writer = csv.writer(csv_file, delimiter=',')
        file_writer.writerow([now.strftime("%Y-%m-%d-%H:%M"), args.model, 'da=' + str(args.use_da)])
        file_writer.writerow(['Epochs:', str(early_stop.stopped_epoch)])
        file_writer.writerow(['Set', 'loss', 'accuracy'])
        file_writer.writerow(['Train', str(train_value[0]), str(train_value[1])])
        file_writer.writerow(['Val', str(val_value[0]), str(val_value[1])])
        file_writer.writerow(['Test', str(test_value[0]), str(test_value[1])])

    generate_challenge_csv(model, os.path.join(args.path, 'test'))

    if args.make_vec:
        if not os.path.exists("vectors"):
            os.mkdir("vectors")
        make_vectors(model, 'vectors/mio_{}_featvects.pkl'.format(args.model))

    model.save()
