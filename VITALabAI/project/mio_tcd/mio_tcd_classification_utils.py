import csv
import os
import pickle

import numpy as np
from skimage.io import imread
from tqdm import tqdm

from VITALabAI import VITALabAiAbstract


def generate_challenge_csv(estimator: VITALabAiAbstract, test_dir_path, csv_path=None):
    """Generate a csv with the prediction results, in the following format:
        file-name1-without-ext,class-nameA
        file-name2-without-ext,class-nameB
        ...

    Args:
        estimator: VITALabAiAbstract with the weights already loaded.
        test_dir_path: Path containing the images
        csv_path: Path and filename of the csv file to produce
    """
    print('Generation challenge csv...')
    images, result = predict_from_directory(estimator, test_dir_path)

    classes = estimator.dataset.get_classes()
    csv_path = csv_path or estimator.__class__.__name__ + '_challenge.csv'

    images = [os.path.splitext(i)[0] for i in images]
    result_array = np.argmax(result, axis=1)
    results_str = [classes[l] for l in result_array]
    with open(csv_path, 'w') as file:
        csv.writer(file).writerows(zip(images, results_str))


def predict_from_directory(estimator: VITALabAiAbstract, path):
    """Predict on images inside a single directory. Useful for testing in a challenge setting.

    Args:
        estimator: VITALabAiAbstract with the weights already loaded.
        path: Path to the directory containing images

    Returns:
    A tuple containing:
        images:  list of the filenames of the images, relative to the given path.
        results: score matrix of shape (N, K) where N is the total number of images and K the number of
                classes.
    """
    images = os.listdir(path)

    def generator():
        while True:
            for i in images:
                im = imread(os.path.join(path, i))
                if im is not None:
                    yield np.expand_dims(estimator.dataset.preprocess_image(im), axis=0)

    return images, estimator.model.predict_generator(generator(), len(images), verbose=1)


def make_vectors(estimator: VITALabAiAbstract, out_file):
    """Make vectors with the last layer of the estimator

    Args:
        estimator: VITALabAiAbstract model that implements get_embedding_model.
        out_file: Name of the file to write the vectors to
    """
    try:
        embedding_model = estimator.get_embedding_model()
    except AttributeError as e:
        print(e)
        print("Choose a model that implements the get_embedding_model method")
        return

    print('Making vectors...')
    print('Converting training samples...')
    x_train_vectors = []
    y_train = []
    seq = estimator.dataset.get_train_set()
    for i in tqdm(range(len(seq))):
        x, y = seq[i]
        y_train.extend(np.argmax(y, -1).ravel())
        x_train_vectors.extend(embedding_model.predict_on_batch(x))

    y_train = np.array(y_train)

    x_train_vectors = np.array(x_train_vectors)
    x_train_vectors = x_train_vectors.astype(np.float16)  # Convert to float16 to save disk space

    print('Converting val samples...')
    x_val_vectors = []
    y_val = []
    seq = estimator.dataset.get_validation_set()
    for i in tqdm(range(len(seq))):
        x, y = seq[i]
        y_val.extend(np.argmax(y, -1).ravel())
        x_val_vectors.extend(embedding_model.predict_on_batch(x))

    y_val = np.array(y_val)
    x_val_vectors = np.array(x_val_vectors)
    x_val_vectors = x_val_vectors.astype(np.float16)  # Convert to float16 to save disk space

    print('Converting test samples...')
    x_test_vectors = []
    y_test = []
    seq = estimator.dataset.get_test_set()
    for i in tqdm(range(len(seq))):
        x, y = seq[i]
        y_test.extend(np.argmax(y, -1).ravel())
        x_test_vectors.extend(embedding_model.predict_on_batch(x))

    y_test = np.array(y_test)
    x_test_vectors = np.array(x_test_vectors)
    x_test_vectors = x_test_vectors.astype(np.float16)  # Convert to float16 to save disk space

    print('Saving vectors to ' + out_file)
    with open(out_file, 'wb') as f:
        pickle.dump(((x_train_vectors, y_train),
                     (x_val_vectors, y_val),
                     (x_test_vectors, y_test)), f, pickle.HIGHEST_PROTOCOL)
