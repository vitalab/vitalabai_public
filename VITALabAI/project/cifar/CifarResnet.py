#
# Author: Pierre-Marc Jodoin (pierre-marc.jodoin@usherbrooke.ca)
#

"""
Cifar10resNet :
 the "6n+2" version from section 4.2 of paper

[1]Kaiming He, Xiangyu Zhang, Shaoqing Ren, Jian Sun
Deep Residual Learning for Image Recognition
http://arxiv.org/abs/1512.03385

but modified by Pai Peng (pengpai_sh@163.com) according to paper
Kaiming He, Xiangyu Zhang, Shaoqing Ren, and Jian Sun
Identity Mappings in Deep Residual Networks
https://arxiv.org/pdf/1603.05027.pdf

The Residual Blocks refer to
https://github.com/keunwoochoi/residual_block_keras/blob/master/residual_blocks.py
(Keunwoo Choi).
Note that his implementation has already used "pre-activation" proposed in the
new version of ResNet paper "Identity Mappings in Deep Residual Networks".
1. Set nb_residual_blocks = 3 to obtain a 20-layer ResNet, testing accuracy is 89.4%
    (loss: 0.31668), training with 36 hours (75 epochs).
2. Set nb_residual_blocks = 5 to obtain a 32-layer ResNet, testing accuracy is 90.8%
    (loss: 0.28545), training with 58 hours (75 epochs).
3. Set nb_residual_blocks = 8 to obtain a 50-layer ResNet, testing accuracy is 90.89%
    (loss: 0.28211), training with 94 hours (75 epochs).
"""

from keras.layers import Dense, Activation, Flatten, Conv2D, AveragePooling2D, Input
from keras.layers.merge import add
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.models import Sequential
from keras.regularizers import l2

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.model.classification.classification_utils import *


class CifarResnet(VITALabAiKerasAbstract):
    """
    Estimator for a Resnet model.

    Notes:
        For more information on the parameters, see keras.Model.compile.
    """

    def __init__(self, dataset: VITALabAiDatasetAbstract, nb_residual_blocks: int = 3,
                 loss_fn=None, optimizer=None, metrics=None):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            nb_residual_blocks: int, the number of res-block.
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
        """

        self.nb_residual_blocks = nb_residual_blocks
        super().__init__(dataset, loss_fn=loss_fn,
                         optimizer=optimizer, metrics=metrics)

    def evaluate(self, save_errors=False):
        """Evaluate the current model as a Classification task."""
        return evaluate(self, save_errors=save_errors)

    def build_model(self):
        """ Build the CIFAR-10 resnet with a series of residual blocks as described in [1].

        Returns:
            keras.Model object *not* compiled.
        """
        model = Sequential()  # it's a CONTAINER, not MODEL

        # 1st layer is a 3x3 conv, output shape: (32, 32, 16)
        model.add(Conv2D(16, (3, 3), strides=(1, 1), padding='same',
                         input_shape=(self.dataset.get_input_shape())))

        # 1st 2n layer, 16 filters, output shape: (32, 32, 16)
        for i in range(2 * self.nb_residual_blocks):
            model.add(self.building_residual_block(input_shape=(32, 32, 16),
                                                   n_feature_maps=16,
                                                   kernel_sizes=(3, 3),
                                                   n_skip=2))

        # 2nd 2n layer, 32 filters, output shape: (16, 16, 32)
        for i in range(2 * self.nb_residual_blocks):
            # expand dimensions and half the output shape size
            if i == 0:
                model.add(self.building_residual_block(input_shape=(32, 32, 16),
                                                       n_feature_maps=32,
                                                       kernel_sizes=(3, 3),
                                                       n_skip=2,
                                                       subsample=(2, 2)))
            else:
                model.add(self.building_residual_block(input_shape=(16, 16, 32),
                                                       n_feature_maps=32,
                                                       kernel_sizes=(3, 3),
                                                       n_skip=2))

        # 3rd 2n layer, 64 filters, output shape: (8, 8, 64)
        for i in range(2 * self.nb_residual_blocks):
            # expand dimensions and half the output shape size
            if i == 0:
                model.add(self.building_residual_block(input_shape=(16, 16, 32),
                                                       n_feature_maps=64,
                                                       kernel_sizes=(3, 3),
                                                       n_skip=2,
                                                       subsample=(2, 2)))
            else:
                model.add(self.building_residual_block(input_shape=(8, 8, 64),
                                                       n_feature_maps=64,
                                                       kernel_sizes=(3, 3),
                                                       n_skip=2))

        # global averaging layer
        model.add(AveragePooling2D(pool_size=(8, 8)))

        model.add(BatchNormalization(axis=3))
        model.add(Activation('relu'))

        # Classifier
        model.add(Flatten())
        model.add(Dense(self.dataset.get_num_classes()))
        model.add(Activation('softmax'))
        print("Model built.")

        model.summary()

        return model

    def building_residual_block(self, input_shape, n_feature_maps,
                                kernel_sizes=None, n_skip=2,
                                subsample=None):
        """ [1] Building block of layers for residual learning.
            Code based on
            https://github.com/ndronen/modeling/blob/master/modeling/residual.py
            but modification of (perhaps) incorrect relu(f)+x thing and it's
            for conv layer (more details in the
            "Identity Mappings in Deep Residual Networks" paper)
        [2] MaxPooling is used instead of strided convolution to make it easier
            to set size(output of short-cut) == size(output of conv-layers).
            If you want to remove MaxPooling,
               i) change (border_mode in Convolution2D in shortcut),
                   'same'-->'valid'
               ii) uncomment ZeroPadding2D in conv layers.
                   (Then the following Conv2D is not the first layer of this
                   container anymore, so you can remove the input_shape in the
                   line 101, the line with comment #'OPTION' )
        [3] It can be used for both cases whether it subsamples or not.
        [4] In the short-cut connection, I used 1x1 convolution to increase
            #channel. It occurs when expand_channels == True

        Args:
            input_shape: (None, num_channel, height, width)
            n_feature_maps: number of feature maps. In ResidualNet it increases
                            whenever image is downsampled.
            kernel_sizes: list or tuple, (3,3) or [3,3] for example
            n_skip: number of layers to skip
            subsample: tuple, (2,2) or (1,2) for example.

        Returns:
            residual block: keras.Model object
        """
        # ***** VERBOSE_PART *****
        print('   - New residual block with')
        print('      input shape:', input_shape)
        print('      kernel size:', kernel_sizes)
        # expand_channels == True when num_channels increases.
        #    E.g. the very first residual block (e.g.1->64,3->128,128->256,...)
        expand_channels = not (input_shape[0] == n_feature_maps)
        if expand_channels:
            print('      - Input channels: %d ---> num feature maps on : %d'
                  % (input_shape[0], n_feature_maps))
        if subsample is not None:
            print('      - with subsample:', subsample)
        kernel_row, kernel_col = kernel_sizes
        # set input
        x = Input(shape=input_shape)
        # ***** SHORTCUT PATH *****
        if subsample is not None:  # subsample (+ channel expansion if needed)
            shortcut_y = Conv2D(n_feature_maps, (kernel_row, kernel_col),
                                strides=subsample,
                                kernel_regularizer=l2(0.0001),
                                padding='same')(x)
        elif expand_channels:
            # channel expansion only (the very first layer of the whole networks)
            shortcut_y = Conv2D(n_feature_maps, (1, 1),
                                kernel_regularizer=l2(0.0001),
                                padding='same')(x)
        else:
            # if no subsample and no channel expansion
            # there's nothing to add on the shortcut.
            shortcut_y = x

        # ***** CONVOLUTION_PATH *****
        conv_y = x
        for i in range(n_skip):
            conv_y = BatchNormalization(axis=1)(conv_y)
            conv_y = Activation('relu')(conv_y)
            if i == 0 and subsample is not None:  # [Subsample at layer 0 if needed]
                conv_y = Conv2D(n_feature_maps, (kernel_row, kernel_col),
                                strides=subsample,
                                kernel_regularizer=l2(0.0001),
                                padding='same')(conv_y)
            else:
                conv_y = Conv2D(n_feature_maps, (kernel_row, kernel_col),
                                kernel_regularizer=l2(0.0001),
                                padding='same')(conv_y)
        # output
        y = add([shortcut_y, conv_y])
        block = Model(inputs=x, outputs=y)
        print('        -- model was built.')
        return block


if __name__ == '__main__':
    from VITALabAI.dataset.classification.cifar import VITALCifar10
    from keras.callbacks import EarlyStopping

    ds = VITALCifar10(batch_size=16)
    model = CifarResnet(ds, nb_residual_blocks=3, optimizer='adam',
                        loss_fn='categorical_crossentropy',
                        metrics=['accuracy'])

    early_stop = EarlyStopping(monitor='val_loss', patience=10, verbose=1)

    model.train(epochs=200, callbacks=[early_stop])
    model.evaluate()
