import argparse

import keras
from keras.callbacks import EarlyStopping

from VITALabAI.dataset.classification.cifar import VITALCifar10
from VITALabAI.dataset.classification.cifar import VITALCifar100
from VITALabAI.model.classification.cnn import VITALCnn
from VITALabAI.model.classification.vgg16 import VITALVGG16
from VITALabAI.project.cifar.CifarResnet import CifarResnet

"""
Example usage of this script:

    python train_cifar.py --dataset=cifar10 --model=cnn
"""
if __name__ == '__main__':
    aparser = argparse.ArgumentParser()
    aparser.add_argument("--dataset", help="Which dataset to train on", choices=['cifar10', 'cifar100'],
                         type=str, default='cifar10')
    aparser.add_argument("--model", help="Model to train", choices=['cnn', 'vgg16', 'resnet'],
                         type=str, default='cnn')
    aparser.add_argument("--nb-residual-blocks", type=int, help="Number of residual blocks in the model",
                         default=3)
    aparser.add_argument("--batch-size", help='Batch size used during training', type=int, default=128)
    aparser.add_argument("--nb-epoch", help="Number of epochs the model will train", type=int, default=20)

    args = aparser.parse_args()

    if args.dataset == 'cifar100':
        ds = VITALCifar100(batch_size=args.batch_size)
    else:
        ds = VITALCifar10(batch_size=args.batch_size)

    if args.model == 'vgg16':
        optim = keras.optimizers.Adam(lr=0.0001)
        ds.input_size = 48  # Image must be at least 48x48 to use VGG16
        model = VITALVGG16(ds, optimizer=optim, loss_fn='categorical_crossentropy',
                           metrics=['accuracy'], weights='imagenet')
    elif args.model == 'resnet':
        model = CifarResnet(ds, optimizer='adam', loss_fn='categorical_crossentropy',
                            metrics=['accuracy'], nb_residual_blocks=args.nb_residual_blocks)
    else:
        model = VITALCnn(ds, optimizer='adam', loss_fn='categorical_crossentropy',
                         metrics=['accuracy'])

    early_stop = EarlyStopping(monitor='val_loss', patience=10, verbose=1)

    model.train(epochs=args.nb_epoch, callbacks=[early_stop])
    model.evaluate(save_errors=True)
