# Training classification on Cifar dataset




# Basic CNN model

```bash
python train_cifar.py --dataset=cifar10 --model=cnn
```

The following parameters are available:

- `--batch-size` changes the batch_size of training and testing.
- `--nb-epoch` changes the number of epochs for training.
- `--dataset` dataset to train on, choices are: 'cifar10', 'cifar100'.



# cifar10 version of Resnet model as described in section 4.2 of the [original paper](https://arxiv.org/pdf/1512.03385v1.pdf)

```bash
python train_cifar.py --dataset=cifar10 --model=resnet
```

The following parameters are available:

- `--batch-size` changes the batch_size of training and testing.
- `--nb-epoch` changes the number of epochs for training.
- `--nb-residual-blocks` changes the number of residual blocks in the model (Only for resnet model).
- `--dataset` dataset to train on, choices are: 'cifar10', 'cifar100'.


# VGG16

```bash
python train_cifar.py --dataset=cifar10 --model=vgg16
```

The following parameters are available:

- `--batch-size` changes the batch_size of training and testing.
- `--dataset` dataset to train on, choices are: 'cifar10', 'cifar100'.
- `--nb-epoch` changes the number of epochs for training.