from collections import defaultdict
from enum import Enum

from VITALabAI.project.bar.utils import VARIATIONAL_METHODS


class TrainingSchedule(object):
    class EventType(Enum):
        Slow = 0
        Fast = 1
        Test = 2
        Prune = 3
        Finetune = 4
        Stop = 5

    def __init__(self):
        self.round_idx = -1  # -1 means before the first pruning
        self.schedule = defaultdict(set)
        self.handlers = None
        self.fine_epoch = None
        self.end_epoch = None
        self.is_slow = False
        self.is_finetuning = False
        self.epoch_idx = 0

    def add_event(self, epoch: int, event_type: EventType):
        self.schedule[epoch].add(event_type)

        if event_type == self.EventType.Stop:
            self.end_epoch = epoch
        elif event_type == self.EventType.Finetune:
            self.fine_epoch = epoch

    def set_handlers(self, on_slow, on_fast, on_prune, on_finetune, on_test, on_stop):
        self.handlers = {
            TrainingSchedule.EventType.Slow: on_slow,
            TrainingSchedule.EventType.Fast: on_fast,
            TrainingSchedule.EventType.Prune: on_prune,
            TrainingSchedule.EventType.Finetune: on_finetune,
            TrainingSchedule.EventType.Test: on_test,
            TrainingSchedule.EventType.Stop: on_stop,
        }

    def catch_up(self, epoch):
        if self.epoch_idx >= epoch:
            return
        while self.epoch_idx < epoch:
            self.step(dry=True)

    def step(self, dry=False):
        assert len(self.schedule) > 0 and len(self.handlers) > 0
        events = self.schedule[self.epoch_idx]

        for e in [e for e in TrainingSchedule.EventType if e in events]:
            if e == self.EventType.Prune:
                self.round_idx += 1
            elif e == self.EventType.Finetune:
                self.is_finetuning = True
            elif e == self.EventType.Slow:
                self.is_slow = True
            elif e == self.EventType.Fast:
                self.is_slow = False

            if not dry:
                self.handlers[e](self.round_idx)

        self.epoch_idx += 1

    @staticmethod
    def from_intervals(pairs):
        s = TrainingSchedule()
        cur_ep = 0
        for interval, event in pairs:
            cur_ep += interval
            s.add_event(cur_ep, event)
        return s

    @staticmethod
    def make_bayesian_schedule(n_train, n_finetune, n_slow):
        # TrainFast -> Finetune -> Test+Stop
        return TrainingSchedule.from_intervals([
            (n_train, TrainingSchedule.EventType.Finetune),
            (n_finetune, TrainingSchedule.EventType.Slow),
            (n_slow, TrainingSchedule.EventType.Test),
            (0, TrainingSchedule.EventType.Stop)
        ])

    @staticmethod
    def make_infobot_schedule(n_train, n_finetune, n_slow):
        # TrainFast -> Prune+Finetune -> Test+Stop
        return TrainingSchedule.from_intervals([
            (n_train, TrainingSchedule.EventType.Prune),
            (0, TrainingSchedule.EventType.Finetune),
            (n_finetune, TrainingSchedule.EventType.Slow),
            (n_slow, TrainingSchedule.EventType.Test),
            (0, TrainingSchedule.EventType.Stop)
        ])

    @staticmethod
    def make_nonbayesian_schedule(n_train, n_trainslow, n_rounds, n_retrain, n_retrainslow):
        # TrainFast -> TrainSlow -> Test -> [Prune+TrainFast -> TrainSlow -> Test] x n_rounds -> Stop
        intervals_and_events = [
            (n_train, TrainingSchedule.EventType.Slow),
            (n_trainslow, TrainingSchedule.EventType.Test),
        ]
        for _ in range(n_rounds):
            intervals_and_events += [
                (0, TrainingSchedule.EventType.Prune),
                (0, TrainingSchedule.EventType.Fast),
                (n_retrain, TrainingSchedule.EventType.Slow),
                (n_retrainslow, TrainingSchedule.EventType.Test)
            ]
        intervals_and_events += [(0, TrainingSchedule.EventType.Stop)]
        return TrainingSchedule.from_intervals(intervals_and_events)

    @staticmethod
    def make_traditional_schedule(n_train, n_slow):
        # TrainFast -> TrainSlow -> Test+Stop
        return TrainingSchedule.from_intervals([
            (n_train, TrainingSchedule.EventType.Slow),
            (n_slow, TrainingSchedule.EventType.Test),
            (0, TrainingSchedule.EventType.Stop)
        ])

    @staticmethod
    def from_args(args):
        if args.method == 'full':
            training_schedule = TrainingSchedule.make_traditional_schedule(args.ep_train, args.ep_trainslow)
        elif args.method == 'infobo':
            training_schedule = TrainingSchedule.make_infobot_schedule(args.ep_train, args.ep_finetune,
                                                                       args.ep_trainslow)
        elif args.method == 'morph':
            # TrainFast -> Prune+Finetune+TrainFast -> TrainSlow -> Test+Stop
            training_schedule = TrainingSchedule.from_intervals([
                (args.ep_train, TrainingSchedule.EventType.Prune),
                (0, TrainingSchedule.EventType.Finetune),
                (args.ep_retrain, TrainingSchedule.EventType.Slow),
                (args.ep_retrainslow, TrainingSchedule.EventType.Test),
                (0, TrainingSchedule.EventType.Stop)
            ])
        elif args.method in VARIATIONAL_METHODS:
            training_schedule = TrainingSchedule.make_bayesian_schedule(args.ep_train, args.ep_finetune,
                                                                        args.ep_trainslow)
        else:
            training_schedule = TrainingSchedule.make_nonbayesian_schedule(
                args.ep_train, args.ep_trainslow, args.n_rounds, args.ep_retrain, args.ep_retrainslow)

        return training_schedule
