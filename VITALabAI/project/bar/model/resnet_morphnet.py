from collections import defaultdict, OrderedDict
import math

import torch
import torch.nn as nn
import torch.utils.model_zoo as model_zoo
from VITALabAI.project.bar.model.layers import convert_state_dict


class StaticGraph(object):
    def __init__(self, root=None):
        self.graph = defaultdict(list)
        self.last = root

    def connect(self, src, dest):
        self.check_compatible(src, dest)
        self.graph[src].append(dest)
        self.last = dest

    def connect_to_last(self, dest):
        self.connect(self.last, dest)

    def connect_residual(self, main_flow, downs):
        # main_flow is a list of conv modules
        if downs is not None:
            self.check_compatible(self.last, downs)
            self.graph[self.last].append(downs)
        for c in main_flow:
            self.connect_to_last(c)

    def __getitem__(self, item):
        return self.graph[item]

    def __contains__(self, item):
        return item in self.graph

    @staticmethod
    def check_compatible(first, second):
        assert first.weight.size(0) == second.weight.size(1)


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, in_width, width1, width2, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = nn.Conv2d(in_width, width1, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(width1)
        self.activ = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(width1, width2, kernel_size=3, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(width2)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.activ(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.activ(out)

        return out


class ResNetCifarMorphNet(nn.Module):
    def __init__(self, block, layers, layer_widths, num_classes=1000, init_weights=True):
        self.layer_widths = iter(layer_widths)
        super(ResNetCifarMorphNet, self).__init__()
        self.conv1 = nn.Conv2d(3, next(self.layer_widths), kernel_size=3, stride=1, padding=1, bias=False)
        self.inplanes = self.conv1.out_channels
        self.bn1 = nn.BatchNorm2d(self.conv1.out_channels)
        self.activ = nn.ReLU(inplace=True)
        self.layer1 = self._make_layer(layers[0])
        self.layer2 = self._make_layer(layers[1], stride=2)
        self.layer3 = self._make_layer(layers[2], stride=2)
        self.avgpool = nn.AdaptiveAvgPool2d(output_size=1)  # Global Avg Pool
        self.fc = nn.Linear(self.inplanes, num_classes)

        if init_weights:
            for m in self.modules():
                if isinstance(m, nn.Conv2d):
                    n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                    m.weight.data.normal_(0, math.sqrt(2. / n))
                elif isinstance(m, nn.BatchNorm2d):
                    m.weight.data.fill_(1)
                    m.bias.data.zero_()

        # Build static graph -- for surgery
        self.static_graph = StaticGraph(root=self.conv1)
        assert block is BasicBlock
        for l in [self.layer1, self.layer2, self.layer3]:
            for b in l.children():
                downs = next(b.downsample.children()) if b.downsample is not None else None
                self.static_graph.connect_residual([b.conv1, b.conv2], downs)
        self.static_graph.connect_to_last(self.fc)

    def _make_layer(self, n_blocks, stride=1):
        # Make downsample conv for first block if necessary
        downsample = None
        b1_width1 = next(self.layer_widths)
        b1_width2 = next(self.layer_widths)
        if stride != 1 or self.inplanes != b1_width2:
            conv_layer = nn.Conv2d(self.inplanes, b1_width2, kernel_size=1, stride=stride, bias=False)
            if hasattr(conv_layer, 'keep_alive'):
                conv_layer.keep_alive = True
            downsample = nn.Sequential(
                conv_layer,
                nn.BatchNorm2d(b1_width2),
            )

        layers = []
        layers.append(BasicBlock(self.inplanes, b1_width1, b1_width2, stride, downsample))
        self.inplanes = b1_width2
        for i in range(1, n_blocks):
            width1 = next(self.layer_widths)
            width2 = next(self.layer_widths)
            layers.append(BasicBlock(self.inplanes, width1, width2))
            self.inplanes = width2

        return nn.Sequential(*layers)

    def load_state_dict(self, sd, **kwargs):
        new_sd = OrderedDict()
        for k, v in sd.items():
            new_sd[k.replace('normalize', 'bn')] = v  # Backwards compat.
        super(ResNetCifarMorphNet, self).load_state_dict(new_sd, **kwargs)

    @staticmethod
    def get_widths_from_state_dict(sd):
        return [w.shape[0] for n, w in sd.items() if 'bn' in n and 'weight' in n]

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.activ(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)

        return x

    def get_new_widths(self, image_size, budget):
        def get_width(gamma_vector):
            return torch.sum(torch.abs(gamma_vector) > 1e-10).item()

        image_area = image_size * image_size
        # 1. Compute current widths (gamma != 0)
        widths = [get_width(self.bn1.weight.data)]
        used_resources = widths[0] * image_area
        for l in [self.layer1, self.layer2, self.layer3]:
            res_gammas = []
            for b_idx, b in enumerate(l):
                widths += [get_width(b.bn1.weight.data), None]
                res_gammas.append(b.bn2.weight.data)
            # Harmonize residual connections
            res_gammas = torch.stack(res_gammas)
            l_inf_norm, _ = torch.max(res_gammas, dim=0)
            harmonized_w = get_width(l_inf_norm)
            for l_idx in range(len(l)):
                widths[-1 - l_idx * 2] = harmonized_w
            # Add block resources
            used_resources += sum(widths[-len(l) * 2:]) * image_area
            used_resources += harmonized_w * image_area  # downsample conv of first block
            # Prepare for next layer
            image_area /= 4
        # 2. Find width multiplier (according to budget)
        multiplier = budget / used_resources
        # 3. Scale widths
        new_widths = [math.floor(w * multiplier) for w in widths]
        # 4. return new widths
        return new_widths

    def get_default_resources(self, image_size):
        _, default_resources = self.get_default_widths(input_side=image_size, more_info=True)
        return default_resources

    def get_volume_estimation(self, image_size):
        # for loss
        image_area = image_size * image_size
        volume_estimation = torch.sum(torch.abs(self.bn1.weight)) * image_area
        for l in [self.layer1, self.layer2, self.layer3]:
            for b in l:
                volume_estimation += torch.sum(torch.abs(b.bn1.weight)) * image_area
                volume_estimation += torch.sum(torch.abs(b.bn2.weight)) * image_area
            image_area /= 4
        return volume_estimation

    @staticmethod
    def get_default_widths(input_side=0, more_info=False):
        w = 12
        cfg = [(4, 16 * w), (4, 32 * w), (4, 64 * w)]
        image_size = input_side * input_side

        widths = [16]
        used_resources = image_size * widths[0]
        for blocks_in_layer, width_in_layer in cfg:
            widths += [width_in_layer] * 2 * blocks_in_layer
            used_resources += image_size * width_in_layer * (2 * blocks_in_layer + 1)
            image_size /= 4
        if not more_info:
            return widths
        else:
            return widths, used_resources


class Bottleneck(nn.Module):
    def __init__(self, in_width, width1, width2, width3, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(in_width, width1, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(width1)
        self.conv2 = nn.Conv2d(width1, width2, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(width2)
        self.conv3 = nn.Conv2d(width2, width3, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(width3)
        self.activ = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.activ(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.activ(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.activ(out)

        return out


class ResNetMorphNet(nn.Module):
    def __init__(self, layers, layer_widths, num_classes=1000, init_weights=True):
        self.layer_widths = iter(layer_widths)
        super(ResNetMorphNet, self).__init__()
        self.conv1 = nn.Conv2d(3, next(self.layer_widths), kernel_size=7, stride=2, padding=3, bias=False)
        self.inplanes = self.conv1.out_channels
        self.bn1 = nn.BatchNorm2d(self.conv1.out_channels)
        self.activ = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(layers[0])
        self.layer2 = self._make_layer(layers[1], stride=2)
        self.layer3 = self._make_layer(layers[2], stride=2)
        self.layer4 = self._make_layer(layers[3], stride=2)
        self.avgpool = nn.AdaptiveAvgPool2d(output_size=1)  # Global Avg Pool
        self.fc = nn.Linear(self.inplanes, num_classes)

        if init_weights:
            for m in self.modules():
                if isinstance(m, nn.Conv2d):
                    n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                    m.weight.data.normal_(0, math.sqrt(2. / n))
                elif isinstance(m, nn.BatchNorm2d):
                    m.weight.data.fill_(1)
                    m.bias.data.zero_()

    def _make_layer(self, n_blocks, stride=1):
        # Make downsample conv for first block if necessary
        downsample = None
        b1_width1 = next(self.layer_widths)
        b1_width2 = next(self.layer_widths)
        b1_width3 = next(self.layer_widths)
        if stride != 1 or self.inplanes != b1_width3:
            conv_layer = nn.Conv2d(self.inplanes, b1_width3, kernel_size=1, stride=stride, bias=False)
            if hasattr(conv_layer, 'keep_alive'):
                conv_layer.keep_alive = True
            downsample = nn.Sequential(
                conv_layer,
                nn.BatchNorm2d(b1_width3),
            )

        layers = []
        layers.append(Bottleneck(self.inplanes, b1_width1, b1_width2, b1_width3, stride, downsample))
        self.inplanes = b1_width3
        for i in range(1, n_blocks):
            width1 = next(self.layer_widths)
            width2 = next(self.layer_widths)
            width3 = next(self.layer_widths)
            layers.append(Bottleneck(self.inplanes, width1, width2, width3))
            self.inplanes = width3

        return nn.Sequential(*layers)

    def load_state_dict(self, sd, **kwargs):
        new_sd = OrderedDict()
        for k, v in sd.items():
            new_sd[k.replace('normalize', 'bn')] = v  # Backwards compat.
        super(ResNetMorphNet, self).load_state_dict(new_sd, **kwargs)

    @staticmethod
    def get_widths_from_state_dict(sd):
        return [w.shape[0] for n, w in sd.items() if 'bn' in n and 'weight' in n]

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.activ(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)

        return x

    def get_new_widths(self, image_size, budget):
        def get_width(gamma_vector):
            return torch.sum(torch.abs(gamma_vector) > 1e-10).item()

        image_area = image_size * image_size
        image_area /= 4  # First conv has stride=2
        # 1. Compute current widths (gamma != 0)
        widths = [get_width(self.bn1.weight.data)]
        used_resources = widths[0] * image_area
        image_area /= 4
        for l_idx, l_blocks in enumerate([self.layer1, self.layer2, self.layer3, self.layer4]):
            res_gammas = []
            for b_idx, b in enumerate(l_blocks):
                widths += [get_width(b.bn1.weight.data), get_width(b.bn2.weight.data), None]
                res_gammas.append(b.bn3.weight.data)
            # Harmonize residual connections
            res_gammas = torch.stack(res_gammas)
            l_inf_norm, _ = torch.max(res_gammas, dim=0)
            harmonized_w = get_width(l_inf_norm)
            for b_idx in range(len(l_blocks)):
                widths[-1 - b_idx * 3] = harmonized_w
            # Add block resources
            used_resources += widths[-len(l_blocks) * 3] * image_area
            if l_idx > 0:
                image_area /= 4
            used_resources += sum(widths[-len(l_blocks) * 3 + 1:]) * image_area
            used_resources += harmonized_w * image_area  # downsample conv of first block
        # 2. Find width multiplier (according to budget)
        multiplier = budget / used_resources
        # 3. Scale widths
        new_widths = [math.floor(w * multiplier) for w in widths]
        # 4. return new widths
        return new_widths

    def get_default_resources(self, image_size):
        _, default_resources = self.get_default_widths(input_side=image_size, more_info=True)
        return default_resources

    def get_volume_estimation(self, image_size):
        # for loss
        # Note that we do not prune the "Residual 1x1 conv", as it is not indicated to do so in the paper.
        image_area = image_size * image_size
        image_area /= 4
        volume_estimation = torch.sum(torch.abs(self.bn1.weight)) * image_area
        image_area /= 4
        for l_idx, l_blocks in enumerate([self.layer1, self.layer2, self.layer3, self.layer4]):
            for b_idx, b in enumerate(l_blocks):
                volume_estimation += torch.sum(torch.abs(b.bn1.weight)) * image_area
                if l_idx > 0 and b_idx == 0:
                    image_area /= 4
                volume_estimation += torch.sum(torch.abs(b.bn2.weight)) * image_area
                volume_estimation += torch.sum(torch.abs(b.bn3.weight)) * image_area
        return volume_estimation

    @staticmethod
    def get_default_widths(input_side=0, more_info=False):
        cfg = [(3, 64), (4, 128), (6, 256), (3, 512)]
        image_area = input_side * input_side

        widths = [64]
        image_area /= 4
        used_resources = image_area * widths[0]
        image_area /= 4
        for l_idx, (blocks_in_layer, width_in_layer) in enumerate(cfg):
            widths += [width_in_layer, width_in_layer, width_in_layer * 4] * blocks_in_layer
            used_resources += image_area * width_in_layer
            if l_idx > 0:
                image_area /= 4
            used_resources += image_area * width_in_layer * (2 * blocks_in_layer - 1)
            used_resources += image_area * width_in_layer * 4 * (blocks_in_layer + 1)
        if not more_info:
            return widths
        else:
            return widths, used_resources


def wide_resnet_morphnet(widths=None, **kwargs):
    # Remove useless keys
    for k in ['pretrained']:
        kwargs.pop(k, None)

    if widths is None:
        widths = ResNetCifarMorphNet.get_default_widths()

    model = ResNetCifarMorphNet(BasicBlock, [4, 4, 4], widths, **kwargs)
    return model

def resnet50_morphnet(widths=None, **kwargs):
    pretrained = kwargs['pretrained']

    # Remove useless keys
    for k in ['pretrained']:
        kwargs.pop(k, None)

    if widths is None:
        widths = ResNetMorphNet.get_default_widths()

    model = ResNetMorphNet([3, 4, 6, 3], widths, **kwargs)

    if pretrained:
        sd = model_zoo.load_url('https://download.pytorch.org/models/resnet50-19c8e357.pth')
        convert_state_dict(sd, model)
        if model.fc.weight.shape != sd['fc.weight'].shape:
            sd['fc.weight'] = model.fc.weight.data
            sd['fc.bias'] = model.fc.bias.data
        model.load_state_dict(sd)
    return model
