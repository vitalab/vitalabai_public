from collections import defaultdict, OrderedDict
import math

import torch
import torch.nn as nn
import torch.utils.model_zoo as model_zoo
from VITALabAI.project.bar.model.layers import convert_state_dict, PrunableBatchNorm2d, sparse_modules


class StaticGraph(object):
    def __init__(self, root=None):
        self.graph = defaultdict(list)
        self.last = root

    def connect(self, src, dest):
        self.check_compatible(src, dest)
        self.graph[src].append(dest)
        self.last = dest

    def connect_to_last(self, dest):
        self.connect(self.last, dest)

    def connect_residual(self, main_flow, downs):
        # main_flow is a list of conv modules
        if downs is not None:
            self.check_compatible(self.last, downs)
            self.graph[self.last].append(downs)
        for c in main_flow:
            self.connect_to_last(c)

    def __getitem__(self, item):
        return self.graph[item]

    def __contains__(self, item):
        return item in self.graph

    @staticmethod
    def check_compatible(first, second):
        assert first.weight.size(0) == second.weight.size(1)


class ModuleInjection:
    pruning_method = None
    prunable_modules = []

    @staticmethod
    def convert_conv(old_conv_instance, new_conv_class):
        m = old_conv_instance
        return new_conv_class(m.in_channels, m.out_channels, m.kernel_size, m.stride, m.padding, m.dilation, 1, m.bias)

    @staticmethod
    def make_prunable(conv_module: nn.Conv2d, bn_module: nn.BatchNorm2d, prune_before_bn=True):
        """Make a (conv, bn) sequence prunable.

        The results in the paper were produced with prune_before_bn=True. However, using prune_before_bn=False is highly
        recommended, since it significantly simplifies the creation of the final pruned architecture.

        :param conv_module: A Conv2d module
        :param bn_module: The BatchNorm2d module following the Conv2d above
        :param prune_before_bn: Whether the pruning gates will be applied before or after the Batch Norm
        :return: a pair (conv, bn) that can be trained to
        """

        if ModuleInjection.pruning_method == 'full':
            # Method full means training without pruning
            return conv_module, bn_module

        if prune_before_bn:
            # Apply the pruning gates BEFORE Batch Norm
            sparse_conv_class = sparse_modules[ModuleInjection.pruning_method]
            new_conv = ModuleInjection.convert_conv(conv_module, sparse_conv_class)
            new_bn = bn_module

            ModuleInjection.prunable_modules.append(new_conv)
        else:
            # Apply the pruning gates AFTER Batch Norm

            if ModuleInjection.pruning_method not in ['louiz', 'bar']:
                raise NotImplementedError('PrunableBatchNorm2d only supports methods louiz and bar.')

            new_conv = conv_module

            # Convert bn to PrunableBatchNorm
            new_bn = PrunableBatchNorm2d.from_batchnorm(bn_module, conv_module=conv_module, copy_params=False)

            ModuleInjection.prunable_modules.append(new_bn)

        return new_conv, new_bn


def conv3x3(in_planes, out_planes, stride=1):
    """3x3 convolution with padding"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride, padding=1, bias=False)


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = nn.BatchNorm2d(planes)
        self.activ = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.downsample = downsample
        self.stride = stride

        self.conv1, self.bn1 = ModuleInjection.make_prunable(self.conv1, self.bn1)
        self.conv2, self.bn2 = ModuleInjection.make_prunable(self.conv2, self.bn2)

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.activ(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.activ(out)

        return out


class MixedBlock(nn.Module):
    def __init__(self, f_delta, delta_idx, in_idx, f_res=None, res_idx=None, res_size=None):
        super(MixedBlock, self).__init__()
        self.f_delta = f_delta
        self.delta_idx = delta_idx
        self.in_idx = in_idx
        self.f_res = f_res
        self.res_idx = res_idx
        self.res_size = res_size
        self.activ = nn.ReLU(inplace=True)

        self.res_scatter_idx = None
        self.delta_scatter_idx = None

        if f_delta is None:
            self.forward = self.forward_without_delta
        else:
            self.forward = self.forward_with_delta

    def scatter_features(self, idx, src, final_size):
        if self.res_scatter_idx is None or self.res_scatter_idx.size(0) != src.size(0):
            scatter_idx = idx.new_empty(*src.size())
            scatter_idx.copy_(idx[None, :, None, None])
            self.res_scatter_idx = scatter_idx

        x = torch.zeros(src.size(0), final_size, src.size(2), src.size(3)).to(src)
        x.scatter_(dim=1, index=self.res_scatter_idx, src=src)
        return x

    def scatter_add_features(self, dst, idx, src):
        if self.delta_scatter_idx is None or self.delta_scatter_idx.size(0) != src.size(0):
            scatter_idx = idx.new_empty(*src.size())
            scatter_idx.copy_(idx[None, :, None, None])
            self.delta_scatter_idx = scatter_idx

        dst.scatter_add_(dim=1, index=self.delta_scatter_idx, src=src)

    def forward_with_delta(self, x):
        # x: (B, C, H, W)

        if self.f_res is None:
            x_alive = x.index_select(dim=1, index=self.in_idx)
            delta = self.f_delta.forward(x_alive)  # 3x3 conv
        else:
            delta = self.f_delta.forward(x)  # 3x3 conv

            res = self.f_res.forward(x)  # 1x1 conv
            x = self.scatter_features(self.res_idx, res, self.res_size)

        self.scatter_add_features(x, self.delta_idx, delta)
        
        return self.activ(x)

    def forward_without_delta(self, x):
        res = self.f_res.forward(x)  # 1x1 conv
        x = self.scatter_features(self.res_idx, res, self.res_size)

        return self.activ(x)

    @staticmethod
    def from_basic(block, delta_idx, in_idx, res_idx=None, res_size=None):
        f_delta = nn.Sequential(
            block.conv1,
            block.bn1,
            block.activ,  # nn.ReLU(inplace=True)
            block.conv2,
            block.bn2
        )
        return MixedBlock(f_delta, delta_idx, in_idx, block.downsample, res_idx, res_size)


class InwardPrunedLinear(nn.Module):
    def __init__(self, module, in_idx):
        super(InwardPrunedLinear, self).__init__()
        self.module = module
        self.in_idx = in_idx
        
    def forward(self, x):
        return self.module.forward(x[:, self.in_idx])


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, planes * self.expansion, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes * self.expansion)
        self.activ = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

        self.conv1, self.bn1 = ModuleInjection.make_prunable(self.conv1, self.bn1)
        self.conv2, self.bn2 = ModuleInjection.make_prunable(self.conv2, self.bn2)
        self.conv3, self.bn3 = ModuleInjection.make_prunable(self.conv3, self.bn3)

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.activ(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.activ(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.activ(out)

        return out


class ResNetCifar(nn.Module):
    def __init__(self, block, layers, width=1, num_classes=1000, init_weights=True):
        self.prunable_modules = []
        self.inplanes = 16
        super(ResNetCifar, self).__init__()
        self.conv1 = nn.Conv2d(3, 16, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(16)
        self.conv1, self.bn1 = ModuleInjection.make_prunable(self.conv1, self.bn1)
        self.activ = nn.ReLU(inplace=True)
        self.layer1 = self._make_layer(block, 16 * width, layers[0])
        self.layer2 = self._make_layer(block, 32 * width, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 64 * width, layers[2], stride=2)
        self.avgpool = nn.AdaptiveAvgPool2d(output_size=1)  # Global Avg Pool
        self.fc = nn.Linear(64 * width, num_classes)

        if init_weights:
            self.init_weights()

        # Build static graph -- for surgery
        self.static_graph = StaticGraph(root=self.conv1)
        assert block is BasicBlock
        for l_blocks in [self.layer1, self.layer2, self.layer3]:
            for b in l_blocks.children():
                downs = next(b.downsample.children()) if b.downsample is not None else None
                self.static_graph.connect_residual([b.conv1, b.conv2], downs)
        self.static_graph.connect_to_last(self.fc)

        # Build reverse static graph
        self.r_static_graph = {}
        prev = self.conv1
        self.groups = [[], [], []]
        assert block is BasicBlock
        for l_idx, l_blocks in enumerate([self.layer1, self.layer2, self.layer3]):
            for b in l_blocks:
                self.r_static_graph[b.conv1] = prev
                self.r_static_graph[b.conv2] = b.conv1
                self.groups[l_idx] += [b.conv2]
                if b.downsample is not None:
                    downsample_conv = next(b.downsample.children())
                    self.r_static_graph[downsample_conv] = prev
                    self.groups[l_idx] += [downsample_conv]
                prev = b.conv2

    def init_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            conv_module = nn.Conv2d(self.inplanes, planes * block.expansion, kernel_size=1, stride=stride, bias=False)
            bn_module = nn.BatchNorm2d(planes * block.expansion)
            if hasattr(bn_module, 'keep_alive'):
                bn_module.keep_alive = True
            downsample = nn.Sequential(*ModuleInjection.make_prunable(conv_module, bn_module))

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def load_state_dict(self, sd, **kwargs):
        new_sd = OrderedDict()
        for k, v in sd.items():
            new_sd[k.replace('normalize', 'bn')] = v  # Backwards compat.
        super(ResNetCifar, self).load_state_dict(new_sd, **kwargs)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.activ(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        
        return x

    def remove_orphans(self):
        def n_remaining(m):
            return (m.get_gates(stochastic=False).detach().cpu() > 0).sum().item()

        def is_all_pruned(m):
            return n_remaining(m) == 0

        num_removed = 0
        for l_blocks in [self.layer1, self.layer2, self.layer3]:
            for b in l_blocks:
                if hasattr(b.conv1, 'get_gates'):
                    m1, m2 = b.conv1, b.conv2
                else:
                    m1, m2 = b.bn1, b.bn2
                if is_all_pruned(m1) or is_all_pruned(m2):
                    num_removed += n_remaining(m1) + n_remaining(m2)
                    m1.log_alpha.data.fill_(-10.0)
                    m2.log_alpha.data.fill_(-10.0)
        print('Removed', num_removed, 'orphans')


class ResNet(nn.Module):
    def __init__(self, block, layers, width=1, num_classes=1000, produce_vectors=False, init_weights=True):
        self.prunable_modules = []
        self.frozen = []
        self.produce_vectors = produce_vectors
        self.inplanes = 64
        super(ResNet, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.conv1, self.bn1 = ModuleInjection.make_prunable(self.conv1, self.bn1)
        self.activ = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64 * width, layers[0])
        self.layer2 = self._make_layer(block, 128 * width, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256 * width, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512 * width, layers[3], stride=2)
        self.avgpool = nn.AdaptiveAvgPool2d(output_size=1)  # Global Avg Pool
        self.fc = nn.Linear(512 * block.expansion * width, num_classes)

        if init_weights:
            self.init_weights()

        # Build static graph -- for surgery
        self.static_graph = StaticGraph(root=self.conv1)
        assert block is Bottleneck
        for l in [self.layer1, self.layer2, self.layer3, self.layer4]:
            for b in l.children():
                downs = next(b.downsample.children()) if b.downsample is not None else None
                self.static_graph.connect_residual([b.conv1, b.conv2, b.conv3], downs)
        self.static_graph.connect_to_last(self.fc)

        # Build reverse static graph
        self.r_static_graph = {}
        prev = self.conv1
        self.groups = [[], [], [], []]
        assert block is Bottleneck
        for l_idx, l_blocks in enumerate([self.layer1, self.layer2, self.layer3, self.layer4]):
            for b in l_blocks:
                self.r_static_graph[b.conv1] = prev
                self.r_static_graph[b.conv2] = b.conv1
                self.r_static_graph[b.conv3] = b.conv2
                self.groups[l_idx] += [b.conv3]
                if b.downsample is not None:
                    downsample_conv = next(b.downsample.children())
                    self.r_static_graph[downsample_conv] = prev
                    self.groups[l_idx] += [downsample_conv]
                prev = b.conv3

    def init_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            conv_module = nn.Conv2d(self.inplanes, planes * block.expansion, kernel_size=1, stride=stride, bias=False)
            bn_module = nn.BatchNorm2d(planes * block.expansion)
            if hasattr(bn_module, 'keep_alive'):
                bn_module.keep_alive = True
            downsample = nn.Sequential(*ModuleInjection.make_prunable(conv_module, bn_module))

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.activ(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        feature_vectors = x.view(x.size(0), -1)
        x = self.fc(feature_vectors)

        if self.produce_vectors:
            return x, feature_vectors
        else:
            return x

    def remove_orphans(self):
        def n_remaining(m):
            return (m.get_gates(stochastic=False).detach().cpu() > 0).sum().item()

        def is_all_pruned(m):
            return n_remaining(m) == 0

        num_removed = 0
        for l_blocks in [self.layer1, self.layer2, self.layer3, self.layer4]:
            for b in l_blocks:
                if hasattr(b.conv1, 'get_gates'):
                    m1, m2, m3 = b.conv1, b.conv2, b.conv3
                else:
                    m1, m2, m3 = b.bn1, b.bn2, b.bn3
                if is_all_pruned(m1) or is_all_pruned(m2) or is_all_pruned(m3):
                    num_removed += n_remaining(m1) + n_remaining(m2) + n_remaining(m3)
                    m1.log_alpha.data.fill_(-10.0)
                    m2.log_alpha.data.fill_(-10.0)
                    m3.log_alpha.data.fill_(-10.0)
        print('Removed', num_removed, 'orphans')


def make_wide_resnet(num_classes, init_weights, pretrained=None):
    model = ResNetCifar(BasicBlock, [4, 4, 4], width=12, num_classes=num_classes, init_weights=init_weights)
    return model


def make_resnet50(num_classes, init_weights, pretrained):
    model = ResNet(Bottleneck, [3, 4, 6, 3], num_classes=num_classes, init_weights=init_weights)

    if pretrained:
        sd = model_zoo.load_url('https://download.pytorch.org/models/resnet50-19c8e357.pth')
        convert_state_dict(sd, model)
        if model.fc.weight.shape != sd['fc.weight'].shape:
            sd['fc.weight'] = model.fc.weight.data
            sd['fc.bias'] = model.fc.bias.data
        model.load_state_dict(sd)
    return model


def get_model(model, method, num_classes, init_weights=True, pretrained=False):
    """Returns the requested model, ready for training/pruning with the specified method.

    :param model: str, either wrn or r50
    :param method: str, see README.md for list
    :param num_classes: int, num classes in the dataset
    :param pretrained: bool, whether to use pretrained weights (only for r50)
    :param init_weights: bool, whether the weights should be initialized
    :return: A prunable ResNet model
    """

    # Model definitions have been changed so that prunable modules are injected when and where suitable.
    # Search in the code for ModuleInjection.make_prunable.
    ModuleInjection.pruning_method = method

    if model == 'wrn':
        net = make_wide_resnet(num_classes, init_weights)
    else:  # model == r50
        net = make_resnet50(num_classes, init_weights, pretrained)

    net.prunable_modules = ModuleInjection.prunable_modules
    return net
