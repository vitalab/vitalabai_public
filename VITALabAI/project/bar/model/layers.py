import abc
import torch
from torch.nn import Parameter
import torch.nn.functional as F


class SparseConvConfig(object):
    alpha_init = 0.0  # log_alpha is initialized around 0
    beta_init = 2. / 3
    gamma_init = -0.1
    zeta_init = 1.1
    fine_tune_mode = False
    stochastic_val = False
    init_gates = True
    minchan = True
    minchan_thresh = 0.0


class GatedLayer(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_gates(self, stochastic):
        raise NotImplementedError()

    @abc.abstractmethod
    def set_gates(self, g):
        raise NotImplementedError()


class MaskedConv2d(torch.nn.Conv2d, GatedLayer):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, groups=1,
                 bias=True):
        super(MaskedConv2d, self).__init__(in_channels, out_channels, kernel_size, stride, padding, dilation, groups,
                                           bias)

        self.num_gates = out_channels
        self.register_buffer('repetitions', torch.tensor([float('nan')], requires_grad=False))  # will contain H * W of feature maps
        if SparseConvConfig.init_gates:
            self.register_buffer('gates', torch.ones(self.num_gates, requires_grad=False))

    def forward(self, input):
        out = F.conv2d(input, self.weight, self.bias, self.stride,
                       self.padding, self.dilation, self.groups)

        self.repetitions.data[0] = out.size(2) * out.size(3)  # H * W

        z = self.gates.to(self.weight)
        out *= z[None, :, None, None]  # broadcast the mask to all samples in the batch, and all locations
        return out

    def get_gates(self, stochastic=False):
        return self.gates

    def set_gates(self, g):
        self.gates.copy_(g)

    def get_output_area(self):
        return self.repetitions.data.item()


class SparseConv2d(torch.nn.Conv2d, GatedLayer):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, groups=1,
                 bias=True):
        super(SparseConv2d, self).__init__(in_channels, out_channels, kernel_size, stride, padding, dilation, groups,
                                           bias)
        log_alpha_init = make_init_weights_for_sparse(out_channels)
        self.log_alpha = Parameter(log_alpha_init)

        beta, gamma, zeta = SparseConvConfig.beta_init, SparseConvConfig.gamma_init, SparseConvConfig.zeta_init

        for n, x in zip(('beta', 'gamma', 'zeta'), (torch.tensor([x], requires_grad=False) for x in (beta, gamma, zeta))):
            self.register_buffer(n, x)  # self.beta will be created (same for gamma, zeta)

        self.num_gates = out_channels
        self.register_buffer('repetitions', torch.tensor([float('nan')], requires_grad=False))  # will contain H * W of feature maps
        self.keep_alive = False

    @staticmethod
    def from_conv2d(m: torch.nn.Conv2d):
        newm = SparseConv2d(m.in_channels, m.out_channels, m.kernel_size, m.stride, m.padding, m.dilation, 1, m.bias)
        newm.weight.data.copy_(m.weight.data)
        if m.bias:
            newm.bias.data.copy_(m.bias.data)
        return newm

    def forward(self, input):
        out = F.conv2d(input, self.weight, self.bias, self.stride,
                       self.padding, self.dilation, self.groups)

        self.repetitions.data[0] = out.size(2) * out.size(3)  # H * W

        z = self.get_gates()
        out *= z[None, :, None, None]  # broadcast the mask to all samples in the batch, and all locations
        return out

    def get_gates(self, stochastic=None):
        if SparseConvConfig.minchan and self.keep_alive:
            self.undeadify()

        stochastic = stochastic if stochastic is not None else self.is_stochastic()
        if stochastic:
            u = torch.rand(self.num_gates).requires_grad_(False).to(self.weight)
            s = torch.sigmoid((torch.log(u) - torch.log(1.0 - u) + self.log_alpha) / self.beta)
        else:
            s = torch.sigmoid(self.log_alpha)

        s_bar = s * (self.zeta - self.gamma) + self.gamma
        z = torch.clamp(s_bar, min=0.0, max=1.0)
        return z

    def undeadify(self):
        # Keep at least one channel alive
        if (self.log_alpha.data < SparseConvConfig.minchan_thresh).sum() == self.num_gates:
            max_loc = self.log_alpha.data == self.log_alpha.data.max()
            self.log_alpha.data[max_loc] = SparseConvConfig.minchan_thresh

    def set_gates(self, g):
        raise RuntimeError('Cannot set gates, as they are learned.')

    def get_output_area(self):
        return self.repetitions.data.item()

    def is_stochastic(self):
        return not SparseConvConfig.fine_tune_mode and (self.training or SparseConvConfig.stochastic_val)


class PrunableBatchNorm2d(torch.nn.BatchNorm2d):
    def __init__(self, num_features, conv_module=None):
        super(PrunableBatchNorm2d, self).__init__(num_features)

        self.num_gates = num_features
        self.keep_alive = False

        self.log_alpha = Parameter(make_init_weights_for_sparse(num_features))

        beta, gamma, zeta = SparseConvConfig.beta_init, SparseConvConfig.gamma_init, SparseConvConfig.zeta_init
        for n, x in zip(('beta', 'gamma', 'zeta'), (torch.tensor([x], requires_grad=False) for x in (beta, gamma, zeta))):
            self.register_buffer(n, x)  # self.beta will be created (same for gamma, zeta)

        # Add forward hook to conv that stores the output size
        def save_output_size(module, in_tensor, out_tensor):
            module.output_area = out_tensor.size(2) * out_tensor.size(3)

        conv_module.register_forward_hook(save_output_size)
        self._conv_module = conv_module
        self.kernel_size, self.num_features, self.in_channels = (conv_module.kernel_size, conv_module.num_features,
                                                                 conv_module.in_channels)

    def forward(self, input):
        out = super(PrunableBatchNorm2d, self).forward(input)

        z = self.get_gates()
        out *= z[None, :, None, None]  # broadcast the mask to all samples in the batch, and all locations
        return out

    def get_gates(self, stochastic=None):
        if SparseConvConfig.minchan and self.keep_alive:
            self.undeadify()

        stochastic = stochastic if stochastic is not None else self.is_stochastic()
        if stochastic:
            u = torch.rand(self.num_gates).requires_grad_(False).to(self.weight)
            s = torch.sigmoid((torch.log(u) - torch.log(1.0 - u) + self.log_alpha) / self.beta)
        else:
            s = torch.sigmoid(self.log_alpha)

        s_bar = s * (self.zeta - self.gamma) + self.gamma
        z = torch.clamp(s_bar, min=0.0, max=1.0)
        return z

    def undeadify(self):
        # Keep at least one channel alive
        if (self.log_alpha.data < SparseConvConfig.minchan_thresh).sum() == self.num_gates:
            max_loc = self.log_alpha.data == self.log_alpha.data.max()
            self.log_alpha.data[max_loc] = SparseConvConfig.minchan_thresh

    def set_gates(self, g):
        raise RuntimeError('Cannot set gates, as they are learned.')

    def is_stochastic(self):
        return not SparseConvConfig.fine_tune_mode and (self.training or SparseConvConfig.stochastic_val)

    def get_output_area(self):
        return self._conv_module.output_area

    @staticmethod
    def from_batchnorm(bn_module, conv_module, copy_params=True):
        new_bn = PrunableBatchNorm2d(bn_module.num_features, conv_module=conv_module)

        if copy_params:
            raise NotImplementedError

        return new_bn


def make_init_weights_for_sparse(size):
    return torch.rand(size) * 0.01 + SparseConvConfig.alpha_init


class InfoBotConv2d(torch.nn.Conv2d, GatedLayer):
    """Compressing Neural Networks using the Variational Information Bottleneck, Dai et al. 2018"""

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, groups=1,
                 bias=True):
        super(InfoBotConv2d, self).__init__(in_channels, out_channels, kernel_size, stride, padding, dilation, groups,
                                            bias)
        self.mu = Parameter(torch.randn(out_channels) * 0.01 + 1.0)
        self.log_sigma = Parameter(torch.randn(out_channels) * 0.01 - 9.0)  # init value obtained by personal
        # communication with Bin Dai.

        self.num_gates = out_channels
        if SparseConvConfig.init_gates:
            self.register_buffer('gates', torch.ones(self.num_gates, requires_grad=False))
        self.register_buffer('repetitions', torch.tensor([float('nan')], requires_grad=False))  # will contain H * W of feature maps

    def forward(self, input):
        out = F.conv2d(input, self.weight, self.bias, self.stride,
                       self.padding, self.dilation, self.groups)

        self.repetitions.data[0] = out.size(2) * out.size(3)  # H * W

        eps = torch.randn(self.num_gates).requires_grad_(False).to(self.mu)

        if self.training:
            z = self.mu + eps * torch.exp(self.log_sigma)
        else:
            z = self.mu
        out *= z[None, :, None, None]  # broadcast the mask to all samples in the batch, and all locations

        g = self.gates.to(self.mu)
        g.requires_grad_(False)
        out *= g[None, :, None, None]
        return out

    def get_gates(self, stochastic=False):
        return self.gates

    def set_gates(self, g):
        self.gates.copy_(g)

    def get_output_area(self):
        return self.repetitions.data.item()


def get_convs(module, destination=None, prefix=''):
    """Get sparse conv modules and associate them to their path in the state dict"""

    if destination is None:
        destination = {}
    for name, child_module in module._modules.items():
        if child_module is not None:
            if type(child_module) in [SparseConv2d, InfoBotConv2d, MaskedConv2d]:
                destination[prefix + name] = child_module
            else:
                get_convs(child_module, destination, prefix + name + '.')
    return destination


def convert_state_dict(full_sd, sparse_net):
    """Convert a state dict from the 'full' method to a state dict that can be loaded into the given sparse model"""

    sparse_sd = sparse_net.state_dict()
    for conv_path, conv_module in get_convs(sparse_net).items():
        new_params = [p for p in conv_module._parameters.keys() if p not in ['weight', 'bias']]
        new_params += list(conv_module._buffers.keys())
        for p in new_params:
            param_path = conv_path + '.' + p
            full_sd[param_path] = sparse_sd[param_path]  # Add sparsification params


def fix_state_dict(checkpoint_sd, net):
    net_sd = net.state_dict()
    warning_msg = ''
    for conv_path, conv_module in get_convs(net).items():
        present_in_ckpt = {k[len(conv_path) + 1:] for k in checkpoint_sd.keys() if k.startswith(conv_path)}
        required = set(conv_module._parameters.keys()) | set(conv_module._buffers.keys())
        required.remove('bias')
        missing = required - present_in_ckpt
        for p in missing:
            param_path = conv_path + '.' + p
            checkpoint_sd[param_path] = net_sd[param_path]
            warning_msg += ' ' + param_path
    if warning_msg != '':
        warning_msg = 'WARNING: Found missing parameters and added them to state dict.' + warning_msg
        print(warning_msg)


sparse_modules = {
    'louiz': SparseConv2d,
    'bar': SparseConv2d,
    'infobo': InfoBotConv2d,
    'rand': MaskedConv2d,
    'vq': MaskedConv2d,
    'id': MaskedConv2d,
    'wsum': MaskedConv2d,
}
