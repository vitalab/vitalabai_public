import csv
import argparse
import os
from collections import defaultdict
from pathlib import Path

import numpy as np
from bokeh import plotting
import bokeh.layouts
import bokeh.models
import bokeh.transform
import bokeh.palettes


ap = argparse.ArgumentParser()
ap.add_argument('action', choices=['make', 'check'])
ap.add_argument('config_csv', type=str)
ap.add_argument('path', type=str)
args = ap.parse_args()


config = defaultdict(lambda: defaultdict(list))
# For full, bar, infobo, morph:
#   config[method][command_args] = (volume_ratio, test_accu)
# For rand, id, vq, wsum:
#   A single command produces all 4 results.
#   config[method][command_args] = [(0.5, test_accu), (0.25, test_accu), ...]


def parse_config():
    with open(args.config_csv) as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        for row in csvreader:
            method, command, volume_ratio, test_accu = row
            if method == 'method' or volume_ratio == '':
                continue  # header or empty line
            if method != '':
                cur_method = method
            if command != '':
                cur_command = command
            config[cur_method][cur_command].append((float(volume_ratio), float(test_accu)))


def make_commands():
    for method, v in config.items():
        for i, command_args in enumerate(v.keys()):
            path = os.path.join(args.path, f'{method}_{i}')
            print(f'{command_args} --ckpt-dir {path}')


def check_results():
    plots_file = Path(args.path) / 'repro_plots.html'
    plotting.output_file(plots_file)
    plots = []
    results = defaultdict(list)

    # Get results for all runs
    for p in Path(args.path).glob('*/'):
        if not p.is_dir():
            continue
        run_name = p.stem
        method = run_name.split('_')[0]

        with (p.absolute() / 'results.csv').open() as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
            for row in csvreader:
                _, volume_ratio, test_accu = row
                if method != 'full' and float(volume_ratio) == 1.0:
                    continue
                results[method].append((float(volume_ratio), float(test_accu)))

    print('======================')
    print('Reproducibility report')
    print('----------------------')

    # Check the results one method at a time
    ordered_methods = ['full', 'rand', 'vq', 'id', 'wsum', 'louiz', 'infobo', 'morph', 'bar']
    for method in ordered_methods:
        print('[ Method', method, ']')

        if method not in results:
            print('ERROR: Method results are missing!')
            continue

        if method in ['louiz', 'infobo']:
            # For these methods, sparsity is not directly controlled, so we use the area under the curve
            expected_unsorted = np.array(list(config[method].values())).squeeze()
            actual_unsorted = np.array(results[method])
            expected = np.sort(expected_unsorted, axis=0)
            actual = np.sort(actual_unsorted, axis=0)
            expected_auc = np.trapz(expected[:, 1], x=expected[:, 0])  # compute area under curve
            actual_auc = np.trapz(actual[:, 1], x=actual[:, 0])
            print('Expected AUC: {}   Actual AUC: {}   Diff: {}'.format(
                expected_auc, actual_auc, actual_auc - expected_auc))
        else:  # if method in bar, morph, etc
            expected_unsorted = np.array(list(config[method].values()))
            if method == 'full':
                expected_unsorted = expected_unsorted.squeeze(1)
            else:
                expected_unsorted = expected_unsorted.squeeze()
            actual_unsorted = np.array(results[method])
            expected = np.sort(expected_unsorted, axis=0)
            actual = np.sort(actual_unsorted, axis=0)
            if actual.shape[0] < expected.shape[0]:
                print('ERROR: Some results are missing! The training script may have crashed.')
                continue
            elif actual.shape[0] > expected.shape[0]:
                print('ERROR: There are more results than expected.')
                continue

            sparsity_diff = np.mean(actual[:, 0] - expected[:, 0])
            accu_diff = np.mean(actual[:, 1] - expected[:, 1])
            print('Sparsity mean diff:', sparsity_diff)
            print('Accuracy mean diff:', accu_diff)

        if method == 'full':
            x = ['Expected', 'Actual']
            y = [expected[0, 1], actual[0, 1]]
            source = bokeh.models.ColumnDataSource(data={'x': x, 'y': y})
            p = plotting.figure(x_range=x, plot_height=350, title="Full (unpruned)")
            p.vbar(x='x', top='y', width=0.9, source=source,
                   line_color='white', fill_color=bokeh.transform.factor_cmap('x', palette=['blue', 'red'], factors=x))  # bokeh.palettes.Spectral6
        else:
            p = plotting.figure(title=method.capitalize(), x_axis_label='Volume ratio', y_axis_label='Test accuracy', x_range=(0.0, 1.0), y_range=(0.0, 1.0))
            p.line(expected[:, 0], expected[:, 1], legend="Expected", line_width=2, line_color='blue')
            p.line(actual[:, 0], actual[:, 1], legend="Actual", line_width=2, line_color='red')
            p.scatter(expected[:, 0], expected[:, 1], color='blue')
            p.scatter(actual[:, 0], actual[:, 1], color='red')
            p.legend.location = 'bottom_right'
        plots.append(p)

    plotting.show(bokeh.layouts.column(*plots))
    print('\nPlots have been written to', plots_file)
    print('======================')


if __name__ == '__main__':
    parse_config()
    if args.action == 'make':
        make_commands()
    else:  # if args.action == 'check':
        check_results()
