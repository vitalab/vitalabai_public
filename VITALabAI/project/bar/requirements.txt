# Please install pytorch and torchvision according to the instructions on pytorch.org
numpy
scipy
scikit-learn
tqdm
matplotlib  # optional
visdom  # optional