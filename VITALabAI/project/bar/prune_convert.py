"""
Remove the unnecessary filters from a CNN pruned using main.py, then compare the converted net with the previous.
While main.py allows to determine which filters can be removed, those remain in the network's architecture. The
current script creates a new, lightweight architecture from the result of main.py.

NOTE: The arguments passed to this script are parsed in main.py (i.e. a dataset choice must be made).
"""

import time
from collections import OrderedDict

import torch
import torch.nn as nn

import VITALabAI.project.bar.model.resnet as resnet
from VITALabAI.project.bar.model.layers import SparseConvConfig
from VITALabAI.project.bar.main import ClassificationTraining


class IdentityModule(nn.Module):
    def forward(self, x):
        return x


def convert_cifar_resnet(net, insert_identity_modules=False):
    """Convert a ResNetCifar module (in place)

    Returns
    -------
        net: the mutated net
    """
    
    net.conv1, net.bn1 = convert_conv_bn(net.conv1, net.bn1, torch.ones(3).byte(), get_gates(net.bn1))
    in_gates = torch.ones(net.conv1.out_channels).byte()

    clean_res = True
    net.layer1, in_gates = convert_layer(net.layer1, in_gates, insert_identity_modules, clean_res)
    net.layer2, in_gates = convert_layer(net.layer2, in_gates, insert_identity_modules, clean_res)
    net.layer3, in_gates = convert_layer(net.layer3, in_gates, insert_identity_modules, clean_res)

    if clean_res:
        net.fc = convert_fc_head(net.fc, in_gates)
    else:
        net.fc = resnet.InwardPrunedLinear(convert_fc_head(net.fc, in_gates), mask2i(in_gates))

    return net


def convert_layer(layer_module, in_gates, insert_identity_modules, clean_res):
    """Convert a ResnetCifar layer (in place)

    Parameters
    ----------
        layer_module: a nn.Sequential
        in_gates: mask

    Returns
    -------
        layer_module: mutated layer_module
        in_gates: ajusted mask
    """

    previous_layer_gates = in_gates

    new_blocks = []
    for block in layer_module:
        new_block, in_gates = convert_block(block, in_gates)
        if new_block is None:
            if insert_identity_modules:
                new_blocks.append(IdentityModule())
        else:
            new_blocks.append(new_block)

    # Remove unused residual features
    if clean_res:
        print()
        cur_layer_gates = in_gates
        for block in new_blocks:
            if isinstance(block, IdentityModule):
                continue
            clean_block(block, previous_layer_gates, cur_layer_gates)  # in-place

    layer_module = nn.Sequential(*new_blocks)
    return layer_module, in_gates


def clean_block(mixed_block, previous_layer_alivef, cur_layer_alivef):
    """Remove unused res features (operates in-place)"""

    def clean_indices(idx, alive_mask=cur_layer_alivef):
        mask = i2mask(idx, alive_mask)
        mask = mask[mask2i(alive_mask)]
        return mask2i(mask)

    if mixed_block.f_res is None:
        mixed_block.in_idx = clean_indices(mixed_block.in_idx)
    else:
        mixed_block.in_idx = clean_indices(mixed_block.in_idx, alive_mask=previous_layer_alivef)
        mixed_block.res_size = cur_layer_alivef.sum().item()
        print('DOWNS ----- Res size: ', mixed_block.res_size)
        mixed_block.res_idx = clean_indices(mixed_block.res_idx)
        print('Res:  ', len(mixed_block.res_idx))
    mixed_block.delta_idx = clean_indices(mixed_block.delta_idx)

    print('In:   ', len(mixed_block.in_idx))
    print('Delta:', len(mixed_block.delta_idx))


def convert_block(block_module, in_gates):
    """Convert a Basic Resnet block (in place)

    Parameters
    ----------
        block_module: a BasicBlock
        in_gates: received mask

    Returns
    -------
        block_module: mutated block
        in_gates: out_gates of this block (in_gates for next block)
    """

    assert not hasattr(block_module, 'conv3')  # must be basic block

    b1_gates = get_gates(block_module.bn1)
    b2_gates = get_gates(block_module.bn2)

    delta_branch_is_pruned = b1_gates.sum().item() == 0 or b2_gates.sum().item() == 0
    
    # Delta branch
    if not delta_branch_is_pruned:
        block_module.conv1, block_module.bn1 = convert_conv_bn(block_module.conv1, block_module.bn1, in_gates, b1_gates)
        block_module.conv2, block_module.bn2 = convert_conv_bn(block_module.conv2, block_module.bn2, b1_gates, b2_gates)

    if block_module.downsample is not None:
        ds_gates = get_gates(block_module.downsample[1])
        ds_conv, ds_bn = convert_conv_bn(block_module.downsample[0], block_module.downsample[1], in_gates, ds_gates)
        ds_module = nn.Sequential(ds_conv, ds_bn)

        if delta_branch_is_pruned:
            mixed_block = resnet.MixedBlock(f_delta=None, delta_idx=None,
                                            f_res=ds_module,
                                            in_idx=mask2i(in_gates),
                                            res_idx=mask2i(ds_gates),
                                            res_size=len(b2_gates))
        else:
            block_module.downsample = ds_module
            mixed_block = resnet.MixedBlock.from_basic(block_module,
                                                       delta_idx=mask2i(b2_gates),
                                                       in_idx=mask2i(in_gates),
                                                       res_idx=mask2i(ds_gates),
                                                       res_size=len(b2_gates))
        in_gates = elementwise_or(ds_gates, b2_gates)
    else:
        if delta_branch_is_pruned:
            mixed_block = None
        else:
            mixed_block = resnet.MixedBlock.from_basic(block_module,
                                                       delta_idx=mask2i(b2_gates),
                                                       in_idx=mask2i(in_gates))
        in_gates = elementwise_or(in_gates, b2_gates)

    return mixed_block, in_gates


def convert_conv_bn(conv_module, bn_module, in_gates, out_gates):
    in_indices = mask2i(in_gates)  # indices of kept features
    out_indices = mask2i(out_gates)

    # Keep the good ones
    new_conv_w = conv_module.weight.data[out_indices][:, in_indices]

    new_conv = make_conv(new_conv_w, from_module=conv_module)
    new_bn = convert_bn(bn_module, out_indices)

    new_conv.out_idx = out_indices
    
    return new_conv, new_bn


def convert_fc_head(fc_module, in_gates):
    """Convert a the final FC module of the net

    Parameters
    ----------
        fc_module: a nn.Linear with weight tensor of size (out_f, in_f)
        in_gates: binary vector or list of size in_f

    Returns
    -------
        fc_module: mutated module
    """

    in_indices = mask2i(in_gates)
    new_weight_tensor = fc_module.weight.data[:, in_indices]
    return make_fc(new_weight_tensor, from_module=fc_module)


def convert_bn(bn_module, out_indices):
    z = bn_module.get_gates(stochastic=False)
    new_weight = bn_module.weight.data[out_indices] * z[out_indices]
    new_bias = bn_module.bias.data[out_indices] * z[out_indices]

    new_bn_module = nn.BatchNorm2d(len(new_weight))
    new_bn_module.weight.data.copy_(new_weight)
    new_bn_module.bias.data.copy_(new_bias)
    new_bn_module.running_mean.copy_(bn_module.running_mean[out_indices])
    new_bn_module.running_var.copy_(bn_module.running_var[out_indices])

    new_bn_module.out_idx = out_indices

    return new_bn_module


def make_bn(bn_module, kept_indices):
    new_bn_module = nn.BatchNorm2d(len(kept_indices))
    new_bn_module.weight.data.copy_(bn_module.weight.data[kept_indices])
    new_bn_module.bias.data.copy_(bn_module.bias.data[kept_indices])
    new_bn_module.running_mean.copy_(bn_module.running_mean[kept_indices])
    new_bn_module.running_var.copy_(bn_module.running_var[kept_indices])

    if hasattr(bn_module, 'out_idx'):
        new_bn_module.out_idx = bn_module.out_idx[kept_indices]
    else:
        new_bn_module.out_idx = kept_indices

    return new_bn_module


def make_conv(weight_tensor, from_module):
    # NOTE: No bias

    # New weight size
    in_channels = weight_tensor.size(1)
    out_channels = weight_tensor.size(0)

    # Other params
    kernel_size = from_module.kernel_size
    stride = from_module.stride
    padding = from_module.padding

    conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding, bias=False)
    conv.weight.data.copy_(weight_tensor)
    return conv


def make_fc(weight_tensor, from_module):
    in_features = weight_tensor.size(1)
    out_features = weight_tensor.size(0)
    fc = nn.Linear(in_features, out_features)
    fc.weight.data.copy_(weight_tensor)
    fc.bias.data.copy_(from_module.bias.data)
    return fc


def get_gates(module):
    return module.get_gates(stochastic=False) > 0


def elementwise_or(a, b):
    return (a + b) > 0


def mask2i(mask):
    assert mask.dtype == torch.uint8
    return mask.nonzero().view(-1)  # Note: do not use .squeeze() because single item becomes a scalar instead of 1-vec


def i2mask(i, from_tensor):
    x = torch.zeros_like(from_tensor)
    x[i] = 1
    return x


def test_convert(check_activations=False, do_full_epoch=True):
    sparsenet_training = ClassificationTraining()  # Note: script args are parsed in main.py
    if not isinstance(sparsenet_training.net, resnet.ResNetCifar):
        raise NotImplementedError

    before_log = OrderedDict()
    after_log = OrderedDict()

    def register_hook(log, m, block_name, m_name):
        def logger_hook(module, inp, out):
            log[block_name][m_name] = (module, out.clone())

        m.register_forward_hook(logger_hook)

    def forward_pass(batch_data):
        sparsenet_training.net.eval()

        SparseConvConfig.fine_tune_mode = True

        with torch.no_grad():
            images, targets = batch_data
            images, targets = images.to(sparsenet_training.device), targets.to(sparsenet_training.device)

            # compute output
            output = sparsenet_training.net.forward(images)

        SparseConvConfig.fine_tune_mode = sparsenet_training.training_schedule.is_finetuning

    if check_activations:
        for n, child in sparsenet_training.net.named_modules():
            if child.__class__.__name__ == 'BasicBlock':
                block = child
                before_log[n] = {}
                register_hook(before_log, block.conv1, n, 'conv1')
                register_hook(before_log, block.bn1, n, 'bn1')
                register_hook(before_log, block.conv2, n, 'conv2')
                register_hook(before_log, block.bn2, n, 'bn2')
                if block.downsample is not None:
                    register_hook(before_log, block.downsample[0], n, 'res_conv')
                    register_hook(before_log, block.downsample[1], n, 'res_bn')

    if not do_full_epoch:
        batch_data = next(iter(sparsenet_training.test_loader))

    print("TESTING SOFT-PRUNED...")
    t1 = time.time()
    if do_full_epoch:
        sparsenet_training.test_epoch()
    else:
        forward_pass(sparsenet_training, batch_data)
    softpruned_time = time.time() - t1
    print('Time:', softpruned_time)

    print("CONVERTING...", end='')
    sparsenet_training.net = convert_cifar_resnet(sparsenet_training.net, insert_identity_modules=check_activations)
    sparsenet_training.net.to(sparsenet_training.device)
    print('Done.')

    if check_activations:
        for n, child in sparsenet_training.net.named_modules():
            if child.__class__.__name__ == 'MixedBlock':
                block = child
                after_log[n] = {}
                if block.f_delta is not None:
                    register_hook(after_log, block.f_delta[0], n, 'conv1')
                    register_hook(after_log, block.f_delta[1], n, 'bn1')
                    register_hook(after_log, block.f_delta[3], n, 'conv2')
                    register_hook(after_log, block.f_delta[4], n, 'bn2')
                if block.f_res is not None:
                    register_hook(after_log, block.f_res[0], n, 'res_conv')
                    register_hook(after_log, block.f_res[1], n, 'res_bn')
            elif child.__class__.__name__ == 'IdentityModule':
                after_log[n] = None

    print("TESTING HARD-PRUNED...")
    t1 = time.time()
    if do_full_epoch:
        sparsenet_training.test_epoch()
    else:
        forward_pass(sparsenet_training, batch_data)
    hardpruned_time = time.time() - t1
    print('Time:', hardpruned_time)

    if check_activations:
        print("CHECKING FORWARD LOGS...")
        for (block1_name, block1), (block2_name, block2) in zip(before_log.items(), after_log.items()):
            print(block1_name.ljust(25), block2_name.ljust(25))

            if block2 is None:
                print('    xxx (Pruned block) xxx')
                continue

            intersect_keys = set(block1.keys()).intersection(set(block2.keys()))
            for k in intersect_keys:
                m1, v1 = block1[k]
                m2, v2 = block2[k]
                before_values = v1[:, m2.out_idx]
                checkresult = torch.abs(before_values - v2).max()
                print(''.ljust(25), k.ljust(25), str(v2.size(1)).rjust(4), '/', str(v1.size(1)).ljust(4), checkresult.item())


if __name__ == '__main__':
    test_convert()
