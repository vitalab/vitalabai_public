import numpy as np
import torch
import torch.nn.functional as F


def get_loss(sparsenet_training, args):
    def total_l0_loss():
        def alive_probability(m):
            """Probability of being alive for each feature map"""
            return torch.sigmoid(m.log_alpha - m.beta * torch.log(-m.gamma / m.zeta))

        def n_alive_approx(m):
            return torch.sum(alive_probability(m))

        l0_loss_conv = torch.zeros(1).to(sparsenet_training.device)
        for m in sparsenet_training.net.prunable_modules:
            rep = m.get_output_area() if args.method == 'bar' else 1.0
            if args.flops:
                k_area = 9 if m.kernel_size[0] == 3 else 1
                prev_n_alive_approx = 3 if m.in_channels == 3 else n_alive_approx(sparsenet_training.net.r_static_graph[m])
                l0_loss_conv += n_alive_approx(m) * prev_n_alive_approx * rep * k_area
            else:
                l0_loss_conv += n_alive_approx(m) * rep

        return l0_loss_conv

    def bar_criterion(pred, target, teacher_logits, tolerance=0.01):
        def sigmoid_progress_fn(p, a=args.sig_a):
            b = 1. / (1. + np.exp(a * 0.5))
            sigmoid_progress = 1. / (1. + np.exp(a * (0.5 - p)))
            sigmoid_progress = (sigmoid_progress - b) / (1. - 2. * b)  # shift+scale so that f(0)=0 and f(1)=1
            return float(sigmoid_progress)

        def exp_progress_fn(p, a=4.0):
            c = 1. - np.exp(-a)
            exp_progress = 1. - np.exp(-a * p)
            return float(exp_progress / c)

        if teacher_logits is not None:
            ce_loss = knowledge_distillation_loss(pred, target, teacher_logits, args.kdtemp, args.kdalpha)
        else:
            ce_loss = F.cross_entropy(pred, target)
        l0_loss_conv = total_l0_loss()
        lamb = args.lamb / args.batch_size

        expenses = get_l0_volume(sparsenet_training.net.prunable_modules, sparsenet_training.net, args.flops)

        # Progress
        final_budget = args.budget
        full_net = sparsenet_training.full_net_size * (1.0 + tolerance)
        p = sparsenet_training.cur_epoch_frac / sparsenet_training.training_schedule.fine_epoch
        if args.prfn == 'sig':
            p = sigmoid_progress_fn(p)
        elif args.prfn == 'exp':
            p = exp_progress_fn(p)
        cur_budget = (1 - p) * full_net + p * final_budget
        margin = full_net * args.margin_a  # 0.0001
        lower_bound = args.budget - margin
        budget_respect = (expenses - lower_bound) / (cur_budget - lower_bound)  # 0: good  1: on budget (asymptote)
        budget_respect = max(budget_respect, 0.0)
        upper_limit = 1e10

        if budget_respect < 1.0:
            lamb_mult = budget_respect ** 2 / (1.0 - budget_respect)
        else:
            lamb_mult = upper_limit

        lamb_mult = min(lamb_mult, upper_limit)
        loss = ce_loss + lamb * lamb_mult * l0_loss_conv
        return loss, float(ce_loss), expenses - args.budget

    def morphnet_criterion(pred, target):
        # resource usage: total activation volume (in paper this would be the args.flops)
        # sum for each layer (nonzero_filters * spatial_area)
        # use vector of abs(bn_gamma) as sparsity vector
        # sum_{for each layer} sum_{for each filter} abs(bn_gamma) * feature_map_area

        ce_loss = F.cross_entropy(pred, target)
        spars_loss = sparsenet_training.net.get_volume_estimation(sparsenet_training.data.insize)
        loss = ce_loss + args.lamb * spars_loss
        return loss, float(ce_loss), float(spars_loss)

    def info_bot_criterion(pred, target):
        ce_loss = F.cross_entropy(pred, target)

        kl_loss = 0
        for m in sparsenet_training.net.prunable_modules:
            gamma_i = args.lamb / torch.sqrt(torch.tensor(m.get_output_area(), dtype=torch.float32).to(sparsenet_training.device))
            kl_loss += gamma_i * m.get_output_area() * torch.sum(
                torch.log(1.0 + m.mu ** 2 / torch.exp(m.log_sigma) ** 2))

        loss = ce_loss + kl_loss  # type: torch.autograd.Variable
        return loss, float(ce_loss), float(kl_loss)

    def louizos_criterion(pred, target):
        ce_loss = F.cross_entropy(pred, target)
        l0_loss_conv = total_l0_loss()
        lamb = args.lamb / args.batch_size
        loss = ce_loss + lamb * l0_loss_conv  # type: torch.autograd.Variable
        return loss, float(ce_loss), float(l0_loss_conv)

    return {
        'louiz': louizos_criterion,
        'bar': bar_criterion,
        'infobo': info_bot_criterion,
        'morph': morphnet_criterion,
        'rand': ce_criterion,
        'vq': ce_criterion,
        'id': ce_criterion,
        'wsum': ce_criterion,
        'full': ce_criterion
    }[args.method]


def ce_criterion(pred, target, *args):
    ce_loss = F.cross_entropy(pred, target)
    return ce_loss, float(ce_loss), 0


def get_l0_volume(prunable_modules, net, use_flops):
    def n_alive(m):
        z = m.get_gates(stochastic=False)  # We are not in valid phase!!
        return (z > 0.0).long().sum().item()

    total = 0
    for m in prunable_modules:
        if use_flops:
            k_area = 9 if m.kernel_size[0] == 3 else 1
            prev_n_alive_approx = 3 if m.num_features == 3 else n_alive(net.r_static_graph[m])
            total += n_alive(m) * prev_n_alive_approx * m.get_output_area() * k_area
        else:
            total += n_alive(m) * m.get_output_area()

    return total


def knowledge_distillation_loss(outputs, labels, teacher_outputs, T, alpha):
    """
    NOTE: the KL Divergence for PyTorch comparing the softmaxs of teacher
    and student expects the input tensor to be log probabilities!
    """

    def full_cross_entropy(target_logits, pred_logits, T):
        """Cross-entropy for a target that is not one-hot"""
        p = F.softmax(target_logits / T, dim=1)
        log_q = F.log_softmax(pred_logits / T, dim=1)

        entropy = -torch.sum(p * log_q, dim=1)
        # Notes about F.kl_div
        # - arg `reduction` had a default value of 'mean' in PyTorch 1.1, but will change in the future
        # - This is actually KL(P||Q), the order of args does not match the math convention
        kl = F.kl_div(log_q, p, reduction='mean')
        return torch.mean(entropy + kl)  # mean over batch

    return full_cross_entropy(teacher_outputs, outputs, T) * (alpha * T * T) + \
           F.cross_entropy(outputs, labels) * (1. - alpha)
