"""
Setup instructions

1. Download MIO-TCD from tcd.miovision.com
2. Set the environment variable MIO_TCD_CLS_PATH to the folder that contains 'train/' and 'test/'
3. Download the logits *.pt file (see link in README) and put it in $MIO_TCD_CLS_PATH
"""

import os
from itertools import cycle

import torch
from torch.utils.data import Dataset
from torchvision.datasets.folder import pil_loader


class MiotcdClassification(Dataset):
    idx_to_label = [
        "articulated_truck",
        "bicycle",
        "bus",
        "car",
        "motorcycle",
        "non-motorized_vehicle",
        "pedestrian",
        "pickup_truck",
        "single_unit_truck",
        "work_van",
        "background"
    ]
    label_to_idx = {l: i for i, l in enumerate(idx_to_label)}
    # The following class weights are computed as: mean_log2_count / class_i_log2_count
    # Imo, logarithm better models class importance vs count than linear scaling
    class_weights = [1.0329786, 1.87954995, 0.99336996, 0.57960893, 1.49732424,
                     1.80484266, 1.2724412, 0.74747037, 1.21727145, 1.00870868,
                     0.6213602]

    def __init__(self, root, image_set, predict=False, transform=None, dataset_name='MioTCD', kd=False):
        self.root = os.path.join(root, image_set)
        list_file = os.path.join(root, 'gt_{}.csv'.format(image_set))

        self.predict = predict
        self.transform = transform
        self.name = dataset_name
        self.ids = list()
        self.targets = []
        self.file_ext = '.jpg'
        self.get_img_path = self.get_img_path_train if image_set == 'train' else self.get_img_path_test
        self.kd = kd

        self.load(list_file)
        self.kd_logits = torch.load(os.path.join(root, 'logits.pt')).cpu() if kd else None

    def load(self, list_file):
        # Open csv file
        # Columns:
        #   image file name (without .jpg), label string
        with open(list_file) as f:
            lines = f.readlines()

        for line in lines:
            fields = line.strip().split(',')
            filename = fields[0]
            class_idx = self.label_to_idx[fields[1]]
            self.ids.append(filename)
            self.targets.append(class_idx)

    def get_img_path_train(self, sample_id, target):
        return os.path.join(self.root, self.idx_to_label[target], sample_id + self.file_ext)

    def get_img_path_test(self, sample_id, target=None):
        return os.path.join(self.root, sample_id + self.file_ext)

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is class_index of the target class.
        """
        sample_id = self.ids[index]
        target = self.targets[index]
        imgpath = self.get_img_path(sample_id, target)
        img = pil_loader(imgpath)
        if self.transform is not None:
            img = self.transform(img)
        if self.predict:
            return img, target, sample_id
        else:
            if self.kd:
                return img, target, self.kd_logits[index]
            else:
                return img, target

    def __len__(self):
        return len(self.ids)


class MiotcdClassificationUniform(Dataset):
    idx_to_label = [
        "articulated_truck",
        "bicycle",
        "bus",
        "car",
        "motorcycle",
        "non-motorized_vehicle",
        "pedestrian",
        "pickup_truck",
        "single_unit_truck",
        "work_van",
        "background"
    ]
    label_to_idx = {l: i for i, l in enumerate(idx_to_label)}
    num_cls = len(idx_to_label)

    def __init__(self, root, image_set, predict=False, transform=None, dataset_name='MioTCD_Uniform'):
        self.root = os.path.join(root, image_set)
        list_file = os.path.join(root, 'gt_{}.csv'.format(image_set))

        self.predict = predict
        self.transform = transform
        self.name = dataset_name
        self.ids = list()
        self.targets = []
        self.file_ext = '.jpg'
        self.get_img_path = self.get_img_path_train if image_set == 'train' else self.get_img_path_test
        self.uniform_list = []

        self.load(list_file)

    def load(self, list_file):
        # Open csv file
        # Columns:
        #   image file name (without .jpg), label string
        with open(list_file) as f:
            lines = f.readlines()

        class_map = [[] for _ in range(self.num_cls)]

        for i, line in enumerate(lines):
            fields = line.strip().split(',')
            filename = fields[0]
            class_idx = self.label_to_idx[fields[1]]
            self.ids.append(filename)
            self.targets.append(class_idx)
            class_map[class_idx].append(i)

        cgens = [cycle(l) for l in class_map]
        smallest_class_len = len(class_map[5])  # non motorized vehicle

        def make_uniform_gen():
            for class_idx in cycle(range(self.num_cls)):
                yield next(cgens[class_idx])

        uniform_gen = make_uniform_gen()
        for _ in range(smallest_class_len * self.num_cls):
            self.uniform_list.append(next(uniform_gen))

    def get_img_path_train(self, index, target):
        return os.path.join(self.root, self.idx_to_label[target], self.ids[index] + self.file_ext)

    def get_img_path_test(self, index, target=None):
        return os.path.join(self.root, self.ids[index] + self.file_ext)

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is class_index of the target class.
        """
        index = self.uniform_list[index]
        sample_id = self.ids[index]
        target = self.targets[index]
        imgpath = self.get_img_path(sample_id, target)
        img = pil_loader(imgpath)
        if self.transform is not None:
            img = self.transform(img)
        if self.predict:
            return img, target, sample_id
        else:
            return img, target

    def __len__(self):
        return len(self.uniform_list)


class Crops4kClassification(Dataset):
    def __init__(self, root, shuffle=True, predict=False, transform=None, dataset_name='4kcrops'):
        self.root = os.path.join(root, 'images')
        list_file = os.path.join(root, 'gt.csv')
        with open(os.path.join(root, 'classmap.txt'), 'r') as f:
            self.idx_to_label = [l.strip() for l in f.readlines()]

        self.predict = predict
        self.transform = transform
        self.name = dataset_name
        self.ids = list()
        self.targets = []
        self.file_ext = '.jpg'

        self.load(list_file)

        if shuffle:
            shuffled_idx = torch.randperm(len(self.ids))
            self.ids = [self.ids[i] for i in shuffled_idx]
            self.targets = [self.targets[i] for i in shuffled_idx]

    def load(self, list_file):
        # Open csv file
        # Columns:
        #   image file name (without .jpg), class_idx
        with open(list_file) as f:
            lines = f.readlines()

        for line in lines:
            fields = line.strip().split(',')
            filename = fields[0]
            class_idx = int(fields[1])
            if class_idx > 10:
                continue  # MotorizedVehicle might appear as class 11, but should not train on it
            self.ids.append(filename)
            self.targets.append(class_idx)

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is class_index of the target class.
        """
        sample_id = self.ids[index]
        target = self.targets[index]
        imgpath = os.path.join(self.root, sample_id + self.file_ext)
        img = pil_loader(imgpath)
        if self.transform is not None:
            img = self.transform(img)
        if self.predict:
            return img, target, sample_id
        else:
            return img, target

    def __len__(self):
        return len(self.ids)
