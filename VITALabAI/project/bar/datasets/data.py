import os

import torch
import torch.utils.data as data
from torch.utils.data.sampler import SubsetRandomSampler
from torchvision import transforms, datasets

from VITALabAI.project.bar.datasets.miotcd import MiotcdClassification, MiotcdClassificationUniform
from VITALabAI.project.bar.datasets.cifar_custom import CIFAR10, CIFAR100
from VITALabAI.project.bar.datasets.tinyimagenet import TinyImageNet, TinyImageNetVal


class DataManager:
    def __init__(self, args):
        self.dataset_name = args.dataset
        self.model = args.model
        self.pruning_method = args.method
        self.batch_size = args.batch_size
        self.workers = args.workers
        self.kdoff = args.kdoff
        self.logits = args.logits or args.checklogits

        self.num_classes = {'c10': 10, 'c100': 100, 'tcd': 11, 'tin': 200}[self.dataset_name]
        (
            self.dataset,
            self.train_loader,
            self.val_loader,
            self.train_transform,
            self.val_transform,
            self.insize
        ) = self.prepare_data()
        (
            self.test_dataset,
            self.test_loader
        ) = self.prepare_test_data()

    @staticmethod
    def get_validation_set(dataset, validation_ratio=0.1):
        parent_dir = os.path.dirname(__file__)  # directory containing the script you are reading
        filename = os.path.join(parent_dir, '{}_valsplit.pkl'.format(dataset.__class__.__name__))
        try:
            train_idx, val_idx = torch.load(filename)
        except FileNotFoundError:
            print('WARNING: Generating new validation set')
            num_samples = len(dataset)
            num_val = int(num_samples * validation_ratio)
            shuffled_idx = torch.randperm(num_samples).long()
            train_idx = shuffled_idx[num_val:]
            val_idx = shuffled_idx[:num_val]
            torch.save((train_idx, val_idx), filename)
        return train_idx, val_idx

    def prepare_data(self):
        print('==> Preparing data...')

        use_kd = self.pruning_method == 'bar' and not self.kdoff

        if self.dataset_name in ['c10', 'c100']:
            # https://github.com/pytorch/tutorials/blob/master/beginner_source/blitz/cifar10_tutorial.py
            norm_mean = [0.49139968, 0.48215827, 0.44653124]
            norm_std = [0.24703233, 0.24348505, 0.26158768]
            norm_transform = transforms.Normalize(norm_mean, norm_std)
            train_transform = transforms.Compose([
                transforms.RandomCrop(32, padding=4),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                norm_transform
            ])
            val_transform = transforms.Compose([
                transforms.ToTensor(),
                norm_transform
            ])
            insize = 32
            dataset_download_path = os.environ['CIFAR']
            dataset_choice = {'c10': CIFAR10, 'c100': CIFAR100}[self.dataset_name]
            dataset = dataset_choice(root=dataset_download_path, train=True,
                                     download=True, transform=train_transform, knowledge_distillation=use_kd)
        elif self.dataset_name == 'tcd' or self.dataset_name == 'tcdu':
            # https://github.com/pytorch/examples/blob/master/imagenet/main.py
            norm_mean = [0.485, 0.456, 0.406]
            norm_std = [0.229, 0.224, 0.225]
            norm_transform = transforms.Normalize(norm_mean, norm_std)
            insize = 128
            train_transform = transforms.Compose([
                transforms.Resize((insize, insize)),
                transforms.RandomAffine(degrees=20.0, scale=(0.8, 1.2), shear=20.0),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                norm_transform,
            ])
            val_transform = transforms.Compose([
                transforms.Resize((insize, insize)),
                transforms.ToTensor(),
                norm_transform
            ])
            data_path = os.environ['MIO_TCD_CLS_PATH']
            MioDataset = MiotcdClassificationUniform if self.dataset_name == 'tcdu' else MiotcdClassification
            dataset = MioDataset(data_path, 'train', transform=train_transform, kd=use_kd)
        else:  # self.dataset_name == 'tin':
            norm_mean = [0.485, 0.456, 0.406]
            norm_std = [0.229, 0.224, 0.225]
            norm_transform = transforms.Normalize(norm_mean, norm_std)
            insize = 64
            train_transform = transforms.Compose([
                transforms.RandomAffine(degrees=20.0, scale=(0.8, 1.2), shear=20.0),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                norm_transform,
            ])
            val_transform = transforms.Compose([
                transforms.ToTensor(),
                norm_transform
            ])
            dataset = TinyImageNet(os.environ['TINYIMAGENET_PATH'], transform=train_transform, kd=use_kd)

        train_idx, val_idx = self.get_validation_set(dataset)
        train_sampler = SubsetRandomSampler(train_idx)
        val_sampler = SubsetRandomSampler(val_idx)
        train_loader = data.DataLoader(dataset, self.batch_size, num_workers=self.workers,
                                       sampler=train_sampler, pin_memory=True)
        val_loader = data.DataLoader(dataset, self.batch_size, num_workers=self.workers, sampler=val_sampler,
                                     pin_memory=True)

        return dataset, train_loader, val_loader, train_transform, val_transform, insize

    def prepare_test_data(self):
        print('==> Preparing TEST data...')

        if self.dataset_name in ['c10', 'c100']:
            dataset_download_path = os.environ['CIFAR']
            dataset_choice = {'c10': datasets.CIFAR10, 'c100': datasets.CIFAR100}[self.dataset_name]
            test_dataset = dataset_choice(root=dataset_download_path, train=False,
                                          download=True, transform=self.val_transform)
        elif self.dataset_name == 'tcd' or self.dataset_name == 'tcdu':
            data_path = os.environ['MIO_TCD_CLS_PATH']
            MioDataset = MiotcdClassificationUniform if self.dataset_name == 'tcdu' else MiotcdClassification
            test_dataset = MioDataset(data_path, 'test', predict=True, transform=self.val_transform)
        else:  # self.dataset_name == 'tin':
            test_dataset = TinyImageNetVal(os.environ['TINYIMAGENET_PATH'], self.dataset.class_to_idx,
                                           transform=self.val_transform)

        test_loader = data.DataLoader(test_dataset, self.batch_size, num_workers=self.workers,
                                      pin_memory=False)

        return test_dataset, test_loader

    def get_raw_dataloader(self):
        # Dataloader with unshuffled and untransformed data. Useful when producing teacher logits.
        return data.DataLoader(self.dataset, self.batch_size, num_workers=self.workers, pin_memory=False)

    def use_train_transform(self):
        self.dataset.transform = self.train_transform

    def use_val_transform(self):
        self.dataset.transform = self.val_transform
