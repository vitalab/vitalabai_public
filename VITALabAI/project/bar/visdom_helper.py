from collections import OrderedDict
import numpy as np


class VisdomHelper(object):
    def __init__(self, monitors, env_name='pytorch', dummy=False):
        try:
            import visdom
        except ImportError:
            print("Could not import visdom.")
            self.die()
            return
        
        if dummy:
            self.die()
            return

        self._vis = visdom.Visdom()

        if not self._vis.check_connection():
            print('ERROR: Could not connect to visdom server! Did you start it?\n    python -m visdom.server')
            self.die()
            return

        self.env = env_name
        self.monitors = monitors
        self._vis.close(env=self.env)
        self._vis.text('Initializing...', env=self.env, win='status')

    @staticmethod
    def make_dummy():
        print('WARNING: visdom is OFF')
        return VisdomHelper(None, dummy=True)

    @staticmethod
    def format_float(x):
        if 0.0001 < x < 1000:
            return '{:.6f}'.format(x)
        else:
            return '{:.4e}'.format(x)

    def show_dict(self, data, win):
        html = '<style>.k{padding-right:1em}</style><table>'
        for key, value in data.items():
            if type(value) is float:
                str_value = self.format_float(value)
            else:
                str_value = str(value)
            html += '<tr><td class="k">' + key + '</td><td>' + str_value + '</td></tr>'
        html += '</table>'
        self._vis.text(html, env=self.env, win=win)

    def update_status(self, epoch, phase, batch_idx, total_batches, ce_loss, l0_loss, loss, avg_loss, avg_accu):
        self.show_dict(OrderedDict([
            ('Epoch', epoch),
            ('Phase', phase),
            ('Batch', '{}/{}    {:.0f}%'.format(batch_idx, total_batches, batch_idx / total_batches * 100)),
            ('CE loss', ce_loss),
            ('L0 norm', l0_loss),
            ('Loss', loss),
            ('Avg loss', avg_loss),
            ('Avg accu', avg_accu)
        ]), win='status')

    def loss(self, loss_list, title, xlabel, win):
        n_hidden_iter = 20
        if len(loss_list) <= n_hidden_iter:
            pass
        else:
            x = np.arange(len(loss_list))[n_hidden_iter:]
            y = np.array(loss_list)[n_hidden_iter:]
            self._vis.line(y, x, env=self.env, win=win,
                      opts={'title': title, 'xlabel': xlabel, 'ylabel': 'Loss'})

    def loss_train_val(self):
        if self.monitors['loss_train'].num_epochs == 0:
            return
        layoutopts = {'plotly': {'yaxis': {'type': 'log', 'autorange': True}}}
        if self.monitors['loss_val'].num_epochs == 0:
            Y = np.array(self.monitors['loss_train'].epochs)
            X = np.arange(self.monitors['loss_train'].num_epochs)
            self._vis.line(Y, X, env=self.env, win='loss', opts=dict(title='Loss', xlabel='Epoch', ylabel='Loss',
                                                                layoutopts=layoutopts))
        else:
            Y = np.stack([self.monitors['loss_train'].epochs, self.monitors['loss_val'].epochs], axis=1)
            X = np.arange(self.monitors['loss_val'].num_epochs)
            self._vis.line(Y, X, env=self.env, win='loss',
                      opts=dict(title='Loss', xlabel='Epoch', ylabel='Loss', legend=['Train', 'Test'],
                                layoutopts=layoutopts))

    def accu_train_val(self):
        if self.monitors['accu_val'].num_epochs == 0:
            return
        Y = np.stack([self.monitors['accu_train'].epochs, self.monitors['accu_val'].epochs], axis=1)
        X = np.arange(self.monitors['accu_val'].num_epochs)
        self._vis.line(Y, X, env=self.env, win='accu',
                  opts=dict(title='Accuracy', xlabel='Epoch', ylabel='Accuracy', legend=['Train', 'Test']))

    def plot_compression(self):
        if self.monitors['alive_conv'].num_epochs == 0:
            return
        Y = np.array(self.monitors['alive_conv'].epochs)
        X = np.arange(self.monitors['alive_conv'].num_epochs)
        self._vis.line(Y, X, env=self.env, win='alive',
                  opts=dict(title='Alive neurons', xlabel='Epoch', ylabel='Alive ratio'))

        # Plot deficit
        if not 'l0_loss' in self.monitors or self.monitors['l0_loss'].num_epochs == 0:
            return
        n = self.monitors['alive_conv'].num_epochs
        self._vis.line(np.array(self.monitors['l0_loss'].epochs[-n:]), np.arange(n),
                  env=self.env, win='l0_loss', opts=dict(title='L0 loss', xlabel='Epoch', ylabel='L0 loss'))

    def plot_metrics(self):
        self.loss_train_val()
        self.accu_train_val()
        self.plot_compression()

    def image(self, img, win):
        self._vis.image(img, env=self.env, win=win)

    def images(self, img, win, **kwargs):
        self._vis.images(img, env=self.env, win=win, **kwargs)

    def line(self, y, x, win):
        self._vis.line(y, x, env=self.env, win=win)

    def die(self):
        """This method transforms the instance into a potato"""

        def nop(*args, **kwargs):
            pass

        for m in dir(self):
            if '__' not in m:
                setattr(self, m, nop)
