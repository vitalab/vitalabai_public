import numpy as np
import torch
from sklearn.cluster import KMeans
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.decomposition import PCA
from scipy.linalg import interpolative as intdep
from sklearn.cluster import AgglomerativeClustering


class ClusteringResult(object):
    def __init__(self, distances, code, centroids, flip_mask=None):
        self.distances = distances
        self.code = code
        self.centroids = centroids
        self.flip_mask = flip_mask


def random_pruning(conv_modules, ablated_ratio, graph=None):
    for m in conv_modules:
        num_ablations = int(ablated_ratio * m.num_gates)
        gates = m.get_gates()
        cur_ablated = torch.sum(gates == 0)
        num_new_ablations = num_ablations - cur_ablated
        still_alive = torch.nonzero(gates == 1.0).view(-1)
        choice = still_alive[torch.randperm(len(still_alive)).cuda()][:num_new_ablations]
        gates[choice] = 0.0
        m.set_gates(gates)


def kmeans_clustering(samples, num_clusters):
    n_pca_components = min([int(np.sqrt(samples.shape[1])), samples.shape[0], samples.shape[1]])
    pca = PCA(n_components=n_pca_components, whiten=True)
    alive_filters_pca = pca.fit_transform(samples)
    kmeans = KMeans(init='k-means++', n_clusters=num_clusters, n_init=2)
    distances = kmeans.fit_transform(alive_filters_pca)
    code = kmeans.predict(alive_filters_pca)
    centroids = pca.inverse_transform(kmeans.cluster_centers_)
    return ClusteringResult(distances=distances, code=code, centroids=centroids)


def vector_quantization(conv_modules, ablated_ratio, replace=True, clus=kmeans_clustering, graph=None):
    for i, m in enumerate(conv_modules):
        gates = m.get_gates().cpu().numpy()
        total_ablations = int(ablated_ratio * m.num_gates)
        cur_ablated = np.sum(gates == 0)
        remaining = m.num_gates - cur_ablated
        num_keep = m.num_gates - total_ablations
        if remaining <= num_keep:
            continue  # Skip this module as it already satisfies the sparsity target

        # Cluster filters
        alive_filters = m.weight.data.cpu().numpy()[gates != 0]
        alive_filters_2d = alive_filters.reshape(alive_filters.shape[0], -1)  # Filters are rows
        clus_result = clus(alive_filters_2d, num_keep)

        # Select filters
        cluster_representative = np.zeros(num_keep, dtype=np.int32)
        new_gates = np.zeros(remaining)
        for k in range(num_keep):
            cluster_mask = clus_result.code == k
            if np.sum(cluster_mask) > 0:
                repres = np.argmin(clus_result.distances[:, k] + new_gates * 1e38)
                cluster_representative[k] = repres
                new_gates[repres] = 1
        oldsrc_to_newsrc = [cluster_representative[c] for c in clus_result.code]
        alive_indices = gates.nonzero()[0]  # 'remaining idx' to 'full idx'

        # Set gates
        gates.fill(0)
        gates[alive_indices] = new_gates
        m.set_gates(torch.from_numpy(gates))

        # Replace filters
        if replace:
            for k in range(num_keep):
                new_filter = clus_result.centroids[k]
                shape = alive_filters.shape[1:]
                m.weight.data[cluster_representative[k]] = torch.from_numpy(new_filter.reshape(shape))

        # Re-route next layer connections
        if m in graph:
            for next_m in graph[m]:
                next_w = next_m.weight
                for oldsrc, newsrc in enumerate(oldsrc_to_newsrc):
                    oldsrc = alive_indices[oldsrc]
                    newsrc = alive_indices[newsrc]
                    if clus_result.flip_mask is None:
                        next_w.data[:, newsrc] += next_w[:, oldsrc]
                    else:
                        connections = next_w[:, oldsrc]
                        if clus_result.flip_mask[oldsrc]:
                            connections = -connections
                        next_w.data[:, newsrc] += connections


def vqs(conv_modules, lin_modules, ablated_ratio, graph=None):
    return vector_quantization(conv_modules, lin_modules, ablated_ratio, replace=False, graph=graph)


def interpolative_decomp(conv_modules, ablated_ratio, graph=None):
    for i, m in enumerate(conv_modules):
        gates = m.get_gates().cpu().numpy()
        total_ablations = int(ablated_ratio * m.num_gates)
        cur_ablated = int(np.sum(gates == 0))
        remaining = m.num_gates - cur_ablated
        if remaining == 1:
            continue
        num_keep = m.num_gates - total_ablations
        if remaining == num_keep:
            continue
        alive_idx = gates.nonzero()[0]

        # Do decomposition
        alive_filters = m.weight.data.cpu().numpy()[gates != 0]
        alive_filters_2d = alive_filters.reshape(alive_filters.shape[0], -1).T  # Filters are columns
        idx, proj = intdep.interp_decomp(alive_filters_2d.astype(np.float64), num_keep)
        kept_filters_idx = idx[:num_keep]
        # colonne de inter_matrix -> coefs qui approxime filtre
        P = np.hstack([np.eye(num_keep), proj])[:, np.argsort(idx)]  # see interp_decomp docs
        P_prime = np.zeros((remaining, remaining), dtype=np.float32)
        P_prime[kept_filters_idx] = P.astype(np.float32)
        # Create coef matrix Ppfull that is compatible with the full original feature width
        temp = np.zeros((m.num_gates, remaining), dtype=np.float32)
        temp[alive_idx] = P_prime
        Ppfull = np.zeros((m.num_gates, m.num_gates), dtype=np.float32)
        Ppfull[:, alive_idx] = temp

        # Set gates
        new_gates = np.zeros(remaining)
        new_gates[kept_filters_idx] = 1.0
        gates.fill(0)
        gates[alive_idx] = new_gates
        m.set_gates(torch.from_numpy(gates))

        # Re-route next layer connections
        P_prime_tensor = torch.from_numpy(Ppfull)
        if m in graph:
            for next_m in graph[m]:
                next_w = next_m.weight.data.cpu()
                if len(next_w.shape) == 4:
                    # Next module is Conv
                    new_next_w = torch.einsum("mn,onhw->omhw", [P_prime_tensor, next_w])
                else:
                    # Next module is Linear
                    new_next_w = torch.matmul(next_w, torch.from_numpy(Ppfull.T))
                next_w.data.copy_(new_next_w)


def weight_sum_pruning(conv_modules, ablated_ratio, graph=None):
    def prune_module(m, is_conv=True):
        gates = m.get_gates().cpu().numpy()
        total_ablations = int(ablated_ratio * m.num_gates)
        num_keep = m.num_gates - total_ablations
        dead_mask = torch.from_numpy((gates == 0).astype(np.uint8)).byte()

        if is_conv:
            m.weight.data[dead_mask].zero_()
            filter_importance = m.weight.data.view(m.num_gates, -1).abs().sum(dim=1)
            _, best_idx = torch.sort(filter_importance, descending=True)
        else:
            # Prune inbound connections
            m.weight.data[:, dead_mask].zero_()
            filter_importance = m.weight.data.abs().sum(dim=0)
            _, best_idx = torch.sort(filter_importance, descending=True)

        # Set gates
        best_idx = best_idx.cpu().numpy()
        gates.fill(0)
        gates[best_idx[:num_keep]] = 1.0
        m.set_gates(torch.from_numpy(gates))

    for m in conv_modules:
        prune_module(m, is_conv=True)
    #for m in lin_modules:
    #    prune_module(m, is_conv=False)


def _score_pruning(conv_modules, ablated_ratio, score_fn, max_prune_ratio=1.0):
    # Prune each layer by a given ratio, according to the given filter usefulness function

    cur_ablated_total = 0

    module_idx = []
    scores = []
    for i, m in enumerate(conv_modules):
        gates = m.get_gates().data.cpu().numpy()
        cur_ablated_total += int(np.sum(gates == 0))

        score = score_fn(i)  # get the score for all gates in this module
        score[gates == 0] = -float('inf')
        scores.append(score)
        module_idx += [(i, j) for j in range(m.num_gates)]
    scores = np.concatenate(scores)

    # Determine how many to prune
    total_num_filters = len(scores)
    total_ablations = int(ablated_ratio * total_num_filters)
    to_ablate = total_ablations - cur_ablated_total

    # Make pruning list
    sorted_filter_idx = np.argsort(scores)  # Ascending pruning score - prune last items
    prune_list = sorted_filter_idx[-to_ablate:]  # Select the last N, where N is to_ablate
    prune_list = [module_idx[i] for i in prune_list]

    _prune_list(conv_modules, prune_list, max_prune_ratio)


def _prune_list(conv_modules, indices_to_prune, max_prune_ratio=1.0):
    # indices_to_prune is a list of tuples. (module_idx, filter_idx)

    # Make new masks
    masks = {}
    n_before = {}
    n_pruned = {}
    for im, ig in indices_to_prune:
        if im not in masks:
            gates = conv_modules[im].get_gates().data.cpu()
            masks[im] = gates
            n_before[im] = int((gates == 1).sum())
            n_pruned[im] = 0
        else:
            gates = masks[im]
        if float(n_pruned[im] + 1) / float(n_before[im]) > max_prune_ratio:
            continue  # don't prune this filter
        gates[ig] = 0
        n_pruned[im] += 1

    # Ablate
    for im, gates in masks.items():
        conv_modules[im].set_gates(gates)


def infobot_pruning(conv_modules, ablated_ratio=None, graph=None, death_threshold=0.05):
    prune_list = []
    for i, m in enumerate(conv_modules):
        # Compute the pruning criterion
        usefulness = (m.mu.data.cpu() / torch.exp(m.log_sigma.data.cpu())) ** 2
        usefulness = torch.log(1 + usefulness)  # Separation is clearer after this transform

        # Find the threshold
        usefulness = usefulness.numpy()
        clustering = AgglomerativeClustering(n_clusters=2)
        cluster_labels = clustering.fit_predict(usefulness.reshape((usefulness.shape[0], 1)))

        # Find the filters to remove
        cluster0 = usefulness[cluster_labels == 0]
        cluster1 = usefulness[cluster_labels == 1]
        if np.mean(cluster0) < np.mean(cluster1):
            to_remove = cluster_labels == 0
        else:
            to_remove = cluster_labels == 1

        to_keep = 1 - to_remove
        to_keep *= usefulness > death_threshold
        to_remove = 1 - to_keep
        to_remove = np.nonzero(to_remove)[0]
        prune_list += [(i, j) for j in to_remove]

    print('To prune: ' + str(len(prune_list)))
    _prune_list(conv_modules, prune_list)


def bar_inflate(sparsenet_training, budget):
    """Grow the net to use the budget fully. (Part of BAR training/pruning)

    This method uses a binary search algorithm to find the right threshold for binarizing the gates.
    I heard you like binary..."""

    def binarize_tensor(w, thresh, on_value, off_value):
        keep = w >= thresh
        remove = w < thresh
        w[keep] = on_value
        w[remove] = off_value

    # Save orig params
    backup = {}
    for m in sparsenet_training.net.prunable_modules:
        backup[m] = m.log_alpha.data.clone()

    # Binary search
    min_alpha = -10
    max_alpha = 0
    on_value = 20
    off_value = -20
    for _ in range(40):
        mid = (min_alpha + max_alpha) / 2.0
        # Compute used neurons for alpha=mid
        for m in sparsenet_training.net.prunable_modules:
            binarize_tensor(m.log_alpha.data, mid, on_value, off_value)
        _, used, _ = sparsenet_training.get_sparsity_ratio(more_info=True)
        # Restore orig params
        for m in backup:
            m.log_alpha.data.copy_(backup[m])
        # Continue binary search
        if used <= budget:
            max_alpha = mid
        else:
            min_alpha = mid
    # Binarize with the threshold we have found (max_alpha is the best)
    for m in sparsenet_training.net.prunable_modules:
        binarize_tensor(m.log_alpha.data, max_alpha, on_value, off_value)


pruning_methods = {'rand': random_pruning,
                   'vq': vector_quantization,
                   'id': interpolative_decomp,
                   'wsum': weight_sum_pruning,
                   'infobo': infobot_pruning}
