# Structured Pruning of Neural Networks

This repo contains the official implementation of the publication "Structured Pruning of Neural Networks with Budget-Aware Regularization". It also contains implementations of 7 other pruning methods.

Structured pruning of CNNs, for acceleration purposes, consists in finding which filters (and associated feature maps) to remove. This program currently allows to prune two ResNet configurations: WideResNet-26 and ResNet-50. Their default width multipliers are 12 and 1, respectively, as in their original papers.

If you use this code, please [cite our paper using the bibtex code provided in the Reference section](#markdown-header-reference).

## Pruning methods

* `rand`: Random (worst case baseline)
* `vq`: Vector quantization
* `id`: Interpolative decomposition
* `wsum`: Weight magnitude
* `louiz`: L0 Regularization (Louizos et al. 2017)
* `infobo`: Information Bottleneck pruning (Dai et al. 2018)
* `morph`: MorphNet (Gordon et al. 2018)
* `bar`: Budget-Aware Regularization (ours)

To train the network without pruning it, use the method `full`.

# Setup

1. **Set the environment variable `CIFAR`** to a folder where the CIFAR-10/100 dataset will be (or was) downloaded.
    ```
    echo "export CIFAR=/path/to/existing/folder" >> ~/.bashrc
    ```
1. **Download the logits** for the datasets you want to use.  
    The logits files are available here: https://drive.google.com/open?id=14bMtRuktGLo5N9biKxC4BRxu4WibjpU4.  
    For CIFAR, put the file in the `$CIFAR` path defined above.
2. If you plan to use MIO-TCD or TinyImageNet, please refer to `datasets/miotcd.py` and `datasets/tinyimagenet.py` for instructions.
3. **Install the required packages**
    ```
    pip install -r requirements.txt
    ```

4. **Install PyTorch** and torchvision using the instructions on [pytorch.org](https://pytorch.org/get-started/locally/)

5. You may run into a `OSError: [Errno 24] Too many open files` when using certain PyTorch code on certain systems.
   This problem is not well understood by the community. The workaround is to increase the limit for open files on your
   system: <https://medium.com/@muhammadtriwibowo/set-permanently-ulimit-n-open-files-in-ubuntu-4d61064429a>


# Usage (summary)

The main script offers many options; here are the most important ones:

```
usage: main.py {c10,c100,tcd,tin} {wrn,r50}
               {louiz,bar,infobo,rand,vq,id,wsum,morph,full}
               [--budget BUDGET] [--flops] [--ckpt-dir DIR]

positional arguments:
  {c10,c100,tcd,tin}         Dataset choice
  {wrn,r50}                  Model choice
  {louiz,bar,infobo,rand,    Pruning method choice
   vq,id,wsum,morph,full}    
  
optional arguments:
  --ckpt-dir DIR     Directory in which to load/save checkpoints.
  --flops            Use the FLOPs as the budgeted metric (by default,
                     the budget is applied on the total volume of neurons
                     in all feature maps.
  --budget BUDGET    Alotted budget. Integer is interpreted as the total
                     alotted volume (or FLOPs). Float is interpreted as a
                     fraction of the initial volume (or FLOPs). Must be
                     set for methods "bar" and "morph".
```

# Example

The following command will train a WideResnet-26 on CIFAR-10, and prune it with Budget-Aware Regularization so that it uses only 50% of its original feature-map volume.

```
python main.py c10 wrn bar --budget 0.5 --ckpt-dir runs/001
```

Note that the checkpoints will be saved in the folder specified with `--ckpt-dir`. The process can be interrupted, and then restarted using the same command; the script will look for the last checkpoint in this folder.

# Reproducibility

A script is provided for reproducing the published results. Note that you need to follow the steps in the "Setup"
section above before attempting reproducibility checks.

## Step 1: Make and run the experiments

`python repro.py make` will output a bash script containing a line for each needed invocation of the training script.

```bash
python repro.py make repro/repro_c10.csv ~/results_cifar10/ > commands_cifar10.sh
```

In this example, we will check the results for CIFAR10. `repro_c10.csv` contains script parameters and corresponding
expected results (these csv files can be found in directory `repro` of this repo). `results_cifar10/` is the path where
all reproduction results will be gathered. 

You can run these invocations on your local machine or on a cluster (using multiple nodes in parallel). On a node with a
Nvidia Tesla V100 GPU and 8 CPUs,
experiments on CIFAR10/100 took less than 7 hours each; producing all the results for CIFAR10 took about 100 GPU-hours.

## Step 2: Check the results

`python repro.py check` will output a text report, and produce a html document with plots.

```bash
python repro.py check repro_c10.csv ~/results_cifar10/ > report_cifar10.txt
```

Give the same csv file as in step 1, and give the folder where the results have been gathered. In this example, the html
document will be created inside `results_cifar10/`.

# Visualizing the training process

This script will by default try import and use [visdom](https://github.com/facebookresearch/visdom), but work anyways
if it is not installed and running. See the [visdom README](https://github.com/facebookresearch/visdom) on
how to start visdom and open it in your browser. 

# Training schedule

The training schedule will contain different phases, depending on the chosen pruning method.
This is implemented in `schedule.py`.

## full

1. Train for `--ep-train` epochs
2. Reduce lr and train for `--ep-trainslow` epochs

## louiz, infobo, bar

1. Train for `--ep-train` epochs
2. Prune (`infobo` only)
3. Engage finetuning (freeze pruning parameters) and train for `--ep-finetune` epochs
4. Reduce lr and train for `--ep-trainslow` epochs

## morph

* Train for `--ep-train` epochs
* Prune
* Retrain for `--ep-retrain` epochs
* Reduce lr and train for `--ep-retrainslow` epochs

## rand, vq, id, wsum

1. Train for `--ep-train` epochs
2. Reduce lr and train for `--ep-trainslow` epochs
3. Repeat `--n-rounds` times:
    1. Prune
    2. Retrain for `--ep-retrain` epochs (with fast lr)
    3. Reduce lr and train for `--ep-retrainslow` epochs

# Reference

If you use code from this repository, please cite our work:

```
@inproceedings{lemaire2019structured,
  title={Structured Pruning of Neural Networks with Budget-Aware Regularization},
  author={Lemaire, Carl and Achkar, Andrew and Jodoin, Pierre-Marc},
  booktitle={Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition},
  pages={9108--9116},
  year={2019}
}
```