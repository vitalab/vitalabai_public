import timeit
from collections import OrderedDict, deque
import io

import numpy as np
import torch

VARIATIONAL_METHODS = ['louiz', 'bar', 'infobo']
DETERMINISTIC_METHODS = ['rand', 'vq', 'id', 'wsum', 'morph', 'full']
METHODS = VARIATIONAL_METHODS + DETERMINISTIC_METHODS


class LossHistory(object):
    """History of the loss during training. (Lighter version of MetricHistory)

    Usage:
        monitor = LossHistory()
        ...
        # Call update at each iteration
        monitor.update(2.3)
        ...
        monitor.avg  # returns the average loss
        ...
        monitor.end_epoch()  # call at epoch end
        ...
        monitor.epochs  # returns the loss curve as a list
    """

    def __init__(self):
        self.history = []
        self.epochs = []
        self.sum = 0.0
        self.count = 0
        self._avg = 0.0
        self.num_iter = 0
        self.num_epochs = 0

    def update(self, value):
        self.history.append(value)
        self.sum += value
        self.count += 1
        self._avg = self.sum / self.count
        self.num_iter += 1

    @property
    def avg(self):
        return self._avg

    def end_epoch(self, write=True):
        if write:
            self.epochs.append(self._avg)
            self.num_epochs += 1
        self.sum = 0.0
        self.count = 0
        self._avg = 0.0


MetricHistory = LossHistory


class TopXAccuracy:
    def __init__(self):
        self.sum_top1 = 0
        self.sum_top5 = 0
        self.total_samples = 0

    def update(self, logits_batch, target_batch):
        indices = torch.argsort(logits_batch, dim=1, descending=True)[:, :5]  # (B, 5) the top-5 class indices
        top5_match_mask = torch.sum(target_batch[:, None] == indices, dim=1) == 1

        self.sum_top1 += (indices[:, 0] == target_batch).long().sum().item()
        self.sum_top5 += top5_match_mask.long().sum().item()
        self.total_samples += len(target_batch)

    @property
    def top1(self):
        return self.sum_top1 / self.total_samples

    @property
    def top5(self):
        return self.sum_top5 / self.total_samples


class SimpleProfiler(object):
    """Measures the time taken by each step of a loop.

    Usage:
        prof = SimpleProfiler(['data_load', 'forward', 'backward'])
        for i in range(num_batches):
            x, y = load_batch(i)
            prof.step()  # data_load done
            pred_y = model(x)
            loss = criterion(y, pred_y)
            prof.step()  # forward done
            loss.backward()
            optimizer.step()
            prof.step()  # backward done
        profiling_result = prof.to_dict()
    """

    def __init__(self, step_names):
        self.step_names = step_names
        self.cur_idx = 0
        self.last_time = timeit.default_timer()
        self.avglen = 20
        self.times = [deque(maxlen=self.avglen) for _ in range(len(step_names))]

    def step(self):
        steptime = timeit.default_timer() - self.last_time
        self.times[self.cur_idx].append(steptime)
        self.last_time = timeit.default_timer()
        self.cur_idx = (self.cur_idx + 1) % len(self.step_names)

    def __getitem__(self, idx):
        """Get the average time of a step"""
        return sum(self.times[idx]) / self.avglen

    def to_dict(self):
        d = OrderedDict()
        for i, name in enumerate(self.step_names):
            d[name] = '{:.5f}'.format(self[i])
        return d

    def print_to_stdout(self):
        print('====================')
        print('Profiler dump:')
        for k, v in self.to_dict().items():
            print('{:>20} {}'.format(k[:20], v))
        print('====================')


def fig2data(fig):
    """
    @brief Convert a Matplotlib figure to a 4D numpy array with RGBA channels and return it
    @param fig a matplotlib figure
    @return a numpy 3D array of RGBA values
    """
    # draw the renderer
    fig.canvas.draw()

    # Get the RGBA buffer from the figure
    w, h = fig.canvas.get_width_height()
    buf = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8).reshape(h, w, 3)
    buf = np.moveaxis(buf, -1, 0)
    return buf


def plot_gate_histogram(conv_gates, linear_gates):
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.colors as colors

    bins = [0, 0.02, 0.05, 0.08, 0.12, 0.2, 0.5, 0.7, 1.0]

    def per_layer_histogram(gates):
        l_hist = []
        for l in gates:
            # hist, _ = np.histogram(l, bins=10, range=(0, 1))
            hist, _ = np.histogram(l, bins=bins)
            l_hist.append(hist)
        return np.array(l_hist)

    binleft = np.linspace(0.0, 0.9, num=10)
    xtickslabels = ['{:.2f}-'.format(b) for b in bins[:-2]] + ['{:.1f}-{:.1f}'.format(*bins[-2:])]

    def plot_it_to_the_limit(ax, img, name):
        ax.set_title(name)
        im = ax.imshow(img, norm=colors.LogNorm(), aspect='auto')
        ax.set_ylabel('Layer idx')
        if name == 'Conv layers' and not len(linear_gates) == 0:
            ax.get_xaxis().set_visible(False)
        else:
            ax.set_xlabel('Histogram of gate values')
            ax.set_xticklabels(xtickslabels)
        ax.set_yticks(np.arange(img.shape[0]))
        ax.set_xticks(np.arange(img.shape[1]))
        return im

    conv_hist = per_layer_histogram(conv_gates)
    if len(linear_gates) > 0:
        lin_hist = per_layer_histogram(linear_gates)
        fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(7, 5), gridspec_kw={'height_ratios': [10, 1]})
        for ax, img, title in zip(axes, [conv_hist, lin_hist], ['Conv layers', 'FC layers']):
            im = plot_it_to_the_limit(ax, img, title)
    else:
        fig = plt.figure(figsize=(7, 5))
        im = plot_it_to_the_limit(plt.gca(), conv_hist, 'Conv layers')

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax=cbar_ax)

    img = fig2data(fig)
    # plt.close()  Intermittent crashes if used
    return img


def plot_alphas(conv_modules, lin_modules):
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt

    alpha_conv = [m.log_alpha.data.cpu().numpy() for m in conv_modules]
    alpha_lin = [m.log_alpha.data.cpu().numpy() for m in lin_modules]

    # Make scatter
    def make_scatter(blocks):
        bob = [np.array([np.repeat(i, len(block)), block]) for i, block in enumerate(blocks)]
        return np.concatenate(bob, axis=1)

    fig = plt.figure(figsize=(7, 5))
    conv_scatter = make_scatter(alpha_conv)
    plt.scatter(conv_scatter[0], conv_scatter[1], label='conv')
    if len(lin_modules) > 0:
        lin_scatter = make_scatter(alpha_lin)
        plt.scatter(lin_scatter[0] + conv_scatter[0, -1] + 1, lin_scatter[1], label='linear')

    img = fig2data(fig)
    return img


def plot_gates(conv_gates, linear_gates):
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt

    # Make scatter
    def make_scatter(blocks):
        bob = [np.array([np.repeat(i, len(block)), block]) for i, block in enumerate(blocks)]
        return np.concatenate(bob, axis=1)

    fig = plt.figure(figsize=(7, 5))
    conv_scatter = make_scatter(conv_gates)
    plt.scatter(conv_scatter[0], conv_scatter[1], label='conv')
    if len(linear_gates) > 0:
        lin_scatter = make_scatter(linear_gates)
        plt.scatter(lin_scatter[0] + conv_scatter[0, -1] + 1, lin_scatter[1], label='linear')

    img = fig2data(fig)
    return img


def plot_num_alive(conv_gates):
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt

    all_gates = conv_gates
    alive = [np.count_nonzero(g) for g in all_gates]
    total = [len(g) for g in all_gates]
    values_2d = np.stack([alive, total], axis=0)
    sorted_idx = np.argsort(total)
    values_2d = values_2d[:, sorted_idx]
    plt.figure(figsize=(7, 5))
    width = 0.4
    left1 = np.arange(len(alive))
    left2 = left1 + width
    plt.bar(left2, values_2d[1], width, label='Total')
    plt.bar(left1, values_2d[0], width, label='Alive')
    plt.xlabel('Layer idx')
    plt.gca().set_yscale('log')
    plt.legend()
    plt.grid()
    plt.ylim((1, None))

    img = fig2data(plt.gcf())
    # plt.close()  Intermittent crashes if used
    return img


def plot_gaussian_layers(conv_modules):
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt

    to_np = lambda x: x.data.cpu().numpy()

    dead = []
    first_layer = []
    last_layer = []
    others = []
    for i, m in enumerate(conv_modules):
        mu = to_np(m.mu)
        sigma = np.exp(to_np(m.log_sigma))
        p = np.stack([mu, sigma], axis=0)
        gates = m.get_gates().cpu().numpy()
        dead.append(p[:, gates == 0])
        if i == 0:
            first_layer.append(p[:, gates != 0])
        elif i == len(conv_modules) - 1:
            last_layer.append(p[:, gates != 0])
        else:
            others.append(p[:, gates != 0])
    dead = np.concatenate(dead, axis=1)
    first_layer = np.concatenate(first_layer, axis=1)
    last_layer = np.concatenate(last_layer, axis=1)
    others = np.concatenate(others, axis=1)

    plt.figure(figsize=(6, 6))
    plt.scatter(dead[0], dead[1], label='Dead', marker='x', alpha=0.3, c='black')
    plt.scatter(others[0], others[1], label='Others', s=2, alpha=0.3, c='blue')
    plt.scatter(last_layer[0], last_layer[1], label='Last', s=8, c='orange')
    plt.scatter(first_layer[0], first_layer[1], label='First', s=32, c='red')
    plt.legend()
    plt.xlabel('Mu')
    plt.ylabel('Sigma')

    img = fig2data(plt.gcf())
    return img


class IterTimer(object):
    def __init__(self, history_len=4):
        self.history = deque(maxlen=history_len)
        self.iterable = None
        self.start_time = None

    def __call__(self, iterable):
        self.iterable = iter(iterable)
        return self

    def __iter__(self):
        return self

    def __next__(self):
        if self.start_time is not None:
            elapsed = timeit.default_timer() - self.start_time
            self.history.append(elapsed)
        self.start_time = timeit.default_timer()
        return next(self.iterable)

    @property
    def mean(self):
        return np.mean(self.history) if len(self.history) > 0 else 0


def str2bool(x):
    if x == 1 or x == 'Y' or x == 'T':
        return True
    elif x == 0 or x == 'N' or x == 'F':
        return False
    else:
        raise TypeError('Could not cast to bool.')


def parse_int_list(x):
    return [int(a) for a in x.split(',')]


def parse_float_list(x):
    return [float(a) for a in x.split(',')]


def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res


def per_class_stats(output, target):
    """Returns vectors containing per class stats. (n_correct, count)"""

    pred = output.argmax(1)
    correct = pred == target

    n_correct = torch.zeros(output.shape[1])
    count = torch.zeros(output.shape[1])

    for c in range(output.shape[1]):
        class_correct = correct[target == c]
        n_correct[c] = class_correct.sum()
        count[c] = class_correct.numel()

    return n_correct, count


def print_args(args_dict):
    print('=== ARGS ===================================')
    keys = set(args_dict.keys()) - {'cut', 'gates', 'novis', 'delete', 'onlyviz'}
    kcolwidth = max(len(k) for k in keys)
    for k in sorted(keys):
        print(('{:<' + str(kcolwidth) + '} {}').format(k, args_dict[k]))
    print('============================================')


def show_batch(batch):
    import matplotlib.pyplot as plt

    plt.figure()
    plt.imshow(np.moveaxis(batch.numpy(), 0, -1))
    plt.show()


def parse_directory(s):
    if s.endswith('/'):
        return s[:-1]
    else:
        return s


def partial_save(obj, f):
    """Pickle-style write that allows sequentially writing objects to a file

    Works with partial_load()"""
    start = f.tell()
    f.seek(start + 4)
    torch.save(obj, f)
    after = f.tell()
    f.seek(start)
    objsize = after - start - 4
    f.write(objsize.to_bytes(4, byteorder='big'))
    f.seek(after)


def partial_load(f, use_cpu=False):
    """Pickle-style read that allows sequentially reading objects to a file

    Works with partial_save()"""
    objsize = int.from_bytes(f.read(4), 'big')
    if use_cpu:
        return torch.load(io.BytesIO(f.read(objsize)), map_location=lambda storage, loc: storage)
    else:
        return torch.load(io.BytesIO(f.read(objsize)))


def save_objects_to_file(seq, filename):
    with open(filename, 'wb') as f:
        for obj in seq:
            partial_save(obj, f)


def load_objects_from_file(filename, how_many, use_cpu=False):
    objs = []
    with open(filename, 'rb') as f:
        for _ in range(how_many):
            objs.append(partial_load(f, use_cpu=use_cpu))
    return objs


class Checkpoint(object):
    """Class for lazy-loading a checkpoint. If you only need the metadata, the weights won't be loaded in memory."""

    def __init__(self, filename, old_format=False, use_cpu=False):
        self.filename = filename
        self._metadata = None
        self._state_dict = None
        self._optim_state_dict = None
        self.use_cpu = use_cpu

        if old_format:
            c = torch.load(filename)  # No lazy-loading for you
            self._state_dict = c.pop('state_dict')
            self._optim_state_dict = c.pop('optimizer')
            c['ep_idx'] = c['monitors']['loss_train'].num_epochs - 1
            self._metadata = c

    def load_all(self):
        self._metadata, self._state_dict, self._optim_state_dict = load_objects_from_file(self.filename, how_many=3,
                                                                                          use_cpu=self.use_cpu)

    @property
    def metadata(self):
        if self._metadata is None:
            self._metadata = load_objects_from_file(self.filename, how_many=1)[0]
        return self._metadata

    @property
    def state_dict(self):
        if self._state_dict is None:
            self.load_all()
        return self._state_dict

    @property
    def optim_state_dict(self):
        if self._state_dict is None:
            self.load_all()
        return self._optim_state_dict

    @staticmethod
    def save(metadata, state_dict, optim_state_dict, filename):
        save_objects_to_file([metadata, state_dict, optim_state_dict], filename)
