import argparse
import glob
import os
import sys

import numpy as np
import torch.backends.cudnn
import torch.nn.functional as F
from torch.optim import Adam
from tqdm import tqdm

from VITALabAI.project.bar.datasets.data import DataManager
from VITALabAI.project.bar.loss import get_loss, ce_criterion, get_l0_volume
from VITALabAI.project.bar.model import resnet_morphnet

import VITALabAI.project.bar.model.resnet as resnet
from VITALabAI.project.bar.model.layers import (SparseConvConfig, convert_state_dict, fix_state_dict,
                                                PrunableBatchNorm2d, SparseConv2d)
from VITALabAI.project.bar.pruning_methods import pruning_methods, bar_inflate
from VITALabAI.project.bar.schedule import TrainingSchedule
from VITALabAI.project.bar.utils import (METHODS, parse_directory, str2bool, MetricHistory, accuracy,
                                         per_class_stats, plot_gaussian_layers, Checkpoint, print_args,
                                         TopXAccuracy)
from VITALabAI.project.bar.visdom_helper import VisdomHelper


torch.backends.cudnn.benchmark = True  # Improves speed


ap = argparse.ArgumentParser(description='Train a ResNet CNN and/or prune its filters. '
                                         'Official implementation of the following paper: '
                                         'Lemaire, Achkar, Jodoin; Structured Pruning of Neural Networks '
                                         'with Budget-Aware Regularization; CVPR 2019.')
# Training setup and hyper params
ap.add_argument('dataset', choices=['c10', 'c100', 'tcd', 'tin'], type=str, help='Dataset choice')
ap.add_argument('model', choices=['wrn', 'r50'], type=str, help='Model choice')
ap.add_argument('method', choices=METHODS, type=str, help='Pruning method choice')
ap.add_argument('--load', '-l', type=str, default=None, help='Load a checkpoint file')
ap.add_argument('--ckpt-dir', type=parse_directory, default=None, help='Dir in which to load/save checkpoints')
ap.add_argument('--loadlast', type=parse_directory, default=None, help='Alias of --ckpt-dir')
ap.add_argument('--lr', default=1e-3, type=float, help='Learning rate')
ap.add_argument('--reg', default=5e-4, type=float, help='L2 regul')
ap.add_argument('--ep-train', '-e', default=40, type=int, help='Num epochs before lowering lr')
ap.add_argument('--ep-trainslow', default=10, type=int, help='Num epochs after lowering lr')
ap.add_argument('--batch_size', '-b', default=64, type=int)
# Sparsification params
ap.add_argument('--budget', type=float, default=None, help='Allowed budget. Can be a number of neurons or fraction.')
ap.add_argument('--flops', action='store_true', help='Use ops instead of volume for budget')
ap.add_argument('--n-rounds', default=4, type=int, help='Number of pruning rounds (deterministic methods)')
ap.add_argument('--ep-retrain', default=40, type=int, help='Num epochs after pruning (deterministic methods)')
ap.add_argument('--ep-retrainslow', default=10, type=int, help='Num epochs after pruning (slow lr)')
ap.add_argument('--ep-finetune', default=40, type=int, help='Num epochs with pruning parameters fixed')
ap.add_argument('--scratch', action='store_true', help='Retrain pruned architecture from scratch')
ap.add_argument('--lamb', type=float, default=1e-5, help='Tradeoff coeff for sparsity loss term')
ap.add_argument('--beta', type=float, default=None, help='Beta parameter of HardConcrete distribution')
ap.add_argument('--gamma', type=float, default=None, help='Gamma parameter of HardConcrete distribution')
ap.add_argument('--zeta', type=float, default=None, help='Zeta parameter of HardConcrete distribution')
# BAR params
ap.add_argument('--prfn', type=str, default='sig', help='Progress function for BAR objective')
ap.add_argument('--margin-a', type=float, default=0.0001, help='Parameter a in Eq. 5 of our paper')
ap.add_argument('--sig-a', type=float, default=10.0, help='Slope parameter of sigmoidal progress function')
ap.add_argument('--kdoff', action='store_true', help='Disable Knowledge Distillation')
ap.add_argument('--kdtemp', type=float, default=4.0, help='Knowledge Distillation temperature T')
ap.add_argument('--kdalpha', type=float, default=0.9, help='Knowledge Distillation alpha')
# Utilities
ap.add_argument('--logits', action='store_true', help='Produce teacher logits for Knowledge Distillation')
ap.add_argument('--checklogits', action='store_true', help='First compute the accuracy of the teacher logits')
ap.add_argument('--testonly', action='store_true', help='Evaluate on test set and exit')
ap.add_argument('--cpu', action='store_true', help='Use the cpu instead of CUDA')
ap.add_argument('--workers', '-w', type=int, default=8, help='Number of DataLoader workers')
ap.add_argument('--cut', action='store_true', help='End an epoch after 20 batches (for debugging)')
ap.add_argument('--delete-ckpt', type=str2bool, default=True, help='Delete last checkpoint')
ap.add_argument('--noprogress', action='store_true', help='Do not display progress bar in stdout')
ap.add_argument('--novis', action='store_true', help='Turn off visdom')
ap.add_argument('--parallel', '-p', action='store_true', help='Split batch to all GPUs')
args = ap.parse_args()

if args.ckpt_dir is not None and args.loadlast is None:
    args.loadlast = args.ckpt_dir  # Retrocompat

if args.method == 'igoo':
    print('Warning: Method "igoo" is now called "bar".')
    args.method = 'bar'

if args.method == 'bar' and args.budget is None:
    raise RuntimeError('Must give a budget for BAR')


class ClassificationTraining(object):
    last_checkpoint = None

    def __init__(self):
        # Utilities
        self.monitors = None
        self.parent = ''
        self.checkpoint = None
        self.checkpoint_filename_pattern = None
        self.vis = None  # for visdom

        # Training config
        self.epoch_idx = None
        self.fast_lr = args.lr
        self.slow_lr = 0.1 * self.fast_lr
        self.l2_reg = args.reg
        self.training_schedule = TrainingSchedule.from_args(args)
        self.training_schedule.set_handlers(self.on_event_slow, self.on_event_fast, self.on_event_prune,
                                            self.on_event_finetune, self.on_event_test, self.on_event_stop)

        # Model
        self.net = None
        self.parallel_net = None
        self.device = torch.device("cpu" if args.cpu else "cuda")
        self.optimizer = None
        self.criterion = None

        # Pruning
        self.total_sparse_conv, self.total_sparse_linear = 0, 0
        self.full_net_size = None
        self.pruning_targets = 2.0 ** -np.arange(1, 1 + args.n_rounds)
        self.cur_epoch_frac = None

        # Perform initialization
        self.read_checkpoint_metadata()
        self.data = DataManager(args)
        self.prepare_model()
        self.prepare_visdom()


    #################################################################################
    # MODEL & OPTIMIZER                                                             #
    #################################################################################

    def prepare_model(self):
        print('==> Preparing model...')

        ckpt = self.checkpoint
        
        # Build network
        if args.method == 'full':
            # Disable sparsification
            self.net = resnet.get_model(args.model, 'full', num_classes=self.data.num_classes,pretrained=(not ckpt),
                                        init_weights=(not ckpt))
        elif args.method == 'morph':
            # Morphnet is implemented separately
            
            if args.model == 'wrn':
                widths = resnet_morphnet.ResNetCifarMorphNet.get_widths_from_state_dict(
                    ckpt.state_dict) if ckpt else None
                self.morphnet_factory = resnet_morphnet.wide_resnet_morphnet
            else:  # args.model == 'r50'
                widths = resnet_morphnet.ResNetMorphNet.get_widths_from_state_dict(
                    ckpt.state_dict) if ckpt else None
                self.morphnet_factory = resnet_morphnet.resnet50_morphnet
            self.net = self.morphnet_factory(widths, num_classes=self.data.num_classes, pretrained=(not ckpt),
                                             init_weights=(not ckpt))
        else:  # bar, louiz, infobo, rand, wq, id, wsum
            # Enable sparsification
            
            # Configure the prunable modules
            for pname in ['beta', 'gamma', 'zeta']:
                val = getattr(args, pname)
                if val is not None:
                    setattr(SparseConvConfig, pname + '_init', val)
            SparseConvConfig.fine_tune_mode = self.training_schedule.is_finetuning
            
            self.net = resnet.get_model(args.model, args.method, num_classes=self.data.num_classes,
                                        pretrained=(not ckpt), init_weights=(not ckpt))
            self.total_sparse_conv = sum(m.num_gates for m in self.net.prunable_modules)
            assert (self.total_sparse_conv > 0)

            if ckpt and ckpt.metadata['args']['method'] == 'full':
                # Convert state dict for load (pruning from ordinary conv weights)
                convert_state_dict(ckpt.state_dict, self.net)

        if args.method != 'full':
            # Compute full net size
            if args.method == 'morph':
                _, self.full_net_size = self.net.get_default_widths(input_side=self.data.insize, more_info=True)
            else:
                self.net.forward(torch.zeros(1, 3, self.data.insize, self.data.insize))
                _, _, self.full_net_size = self.get_sparsity_ratio(more_info=True)

        self.net.to(self.device)
        self.parallel_net = torch.nn.DataParallel(self.net) if args.parallel else self.net

        if args.method in {'bar', 'morph'} and args.budget < 1.0:
            # Interpret as fraction of full net size
            fraction = args.budget
            args.budget = fraction * self.full_net_size
            print('Computed budget = {} * {} = {}'.format(fraction, self.full_net_size, args.budget))

        self.make_optimizer()
        self.make_monitors()

        # Load model state, optimizer state and monitors
        if ckpt:
            fix_state_dict(ckpt.state_dict, self.net)
            self.load(ckpt)

        self.criterion = ce_criterion if self.training_schedule.is_finetuning else get_loss(self, args)

    def get_sparsity_ratio(self, more_info=False):
        if args.method == 'full':
            assert not more_info
            return 1.0
        elif args.method == 'morph':
            assert not more_info
            return float(args.budget) / self.net.get_default_resources(self.data.insize)

        # Compute total metric (volume or ops)
        total_ops_full = 0
        for m in self.net.prunable_modules:
            if args.flops:
                k_area = 9 if m.kernel_size[0] == 3 else 1
                num_feat_prev = 3 if m.num_features == 3 else self.net.r_static_graph[m].num_gates
                total_ops_full += m.get_output_area() * m.num_gates * num_feat_prev * k_area
            else:
                total_ops_full += m.get_output_area() * m.num_gates
        total_ops_remaining = get_l0_volume(self.net.prunable_modules, self.net, args.flops)

        if more_info:
            return total_ops_remaining / total_ops_full, total_ops_remaining, total_ops_full
        else:
            return total_ops_remaining / total_ops_full

    def make_optimizer(self):
        # Build parameter groups. (Learnable gates must not be regularized)
        normal_params = []
        gate_params = []
        for k, v in self.net.named_parameters():
            if 'log_alpha' in k or 'mu' in k or 'sigma' in k:
                gate_params.append(v)
            else:
                normal_params.append(v)
        gate_lr = 0.0 if self.training_schedule.is_finetuning else self.fast_lr
        # noinspection PyTypeChecker
        self.optimizer = Adam([
            {'params': normal_params, 'lr': self.fast_lr, 'weight_decay': self.l2_reg},
            {'params': gate_params, 'lr': gate_lr, 'weight_decay': 0.0}
        ])

    #################################################################################
    # TRAINING AND VALIDATION                                                       #
    #################################################################################

    def train_epoch(self):
        self.net.train()
        self.data.use_train_transform()

        tot_per_class_correct = torch.zeros(self.data.num_classes)
        tot_per_class_count = torch.zeros(self.data.num_classes)

        for batch_idx, batch_data in enumerate(tqdm(self.data.train_loader,
                                                    desc=self.get_epoch_progress_str() + ' train',
                                                    disable=args.noprogress)):
            if args.cut and batch_idx == 20:
                break
            self.cur_epoch_frac = self.epoch_idx + batch_idx / len(self.data.train_loader)

            batch_data = tuple(map(lambda x: x.to(self.device), batch_data))  # images, targets [, logits]
            images, targets = batch_data[0], batch_data[1]

            output = self.parallel_net(images)
            loss, ce_loss, l0_loss = self.criterion(output, *batch_data[1:])

            self.optimizer.zero_grad()
            loss.backward()

            self.optimizer.step()

            loss_value = loss.item()

            prec1 = accuracy(output, targets, topk=(1,))
            self.monitors['accu_train'].update(prec1[0].item())
            self.monitors['loss_train'].update(loss_value)
            self.monitors['l0_loss'].update(l0_loss)
            if args.dataset in ['tcd', 'tcdu']:
                # Per-class metrics
                per_class_correct, per_class_count = per_class_stats(output, targets)
                tot_per_class_correct += per_class_correct
                tot_per_class_count += per_class_count
                accu_strings = ['{:.5f}'.format(acc.item()) for acc in tot_per_class_correct / tot_per_class_count]
                self.vis.show_dict(dict(zip(self.data.dataset.idx_to_label, accu_strings)), 'cwt')
                accu_strings = [int(acc.item()) for acc in tot_per_class_count]
                self.vis.show_dict(dict(zip(self.data.dataset.idx_to_label, accu_strings)), 'cwtc')

            self.vis.update_status(self.epoch_idx, 'Training', batch_idx, len(self.data.train_loader), ce_loss, l0_loss,
                                   loss_value, self.monitors['loss_train'].avg, self.monitors['accu_train'].avg)
            if batch_idx > 0:
                if self.epoch_idx == 0 and batch_idx == 200:
                    self.vis.loss(self.monitors['loss_train'].history, 'Loss Per Batch', 'Iterations', 'loss_batch')

            if not self.training_schedule.is_finetuning:
                self.vis.loss(self.monitors['l0_loss'].history, 'L0 loss', 'Iterations', 'l0_loss')

        self.monitors['loss_train'].end_epoch()
        self.monitors['accu_train'].end_epoch()
        self.monitors['l0_loss'].end_epoch()

    def val_epoch(self, write=True):
        self.net.eval()
        self.data.use_val_transform()

        with torch.no_grad():
            for batch_idx, batch_data in enumerate(tqdm(self.data.val_loader,
                                                        desc=self.get_epoch_progress_str() + ' val',
                                                        disable=args.noprogress)):
                if args.cut and batch_idx == 20:
                    break

                images, targets = batch_data[0], batch_data[1]
                images, targets = images.to(self.device), targets.to(self.device)

                # compute output
                output = self.parallel_net(images)
                loss, ce_loss, l0_loss = ce_criterion(output, targets)
                loss_value = loss.item()

                if batch_idx == 0:
                    if not args.method in ['full', 'morph']:
                        #self.show_sparsity_viz(write)
                        if args.method in ['infob', 'infobo'] and not args.novis:
                            self.vis.image(plot_gaussian_layers(self.net.prunable_modules), win='gaussians')

                prec1 = accuracy(output, targets, topk=(1,))
                self.monitors['accu_val'].update(prec1[0].item())
                self.monitors['loss_val'].update(loss_value)

                self.vis.update_status(self.epoch_idx, 'Test', batch_idx, len(self.data.val_loader), ce_loss, l0_loss,
                                       float(loss), self.monitors['loss_val'].avg, self.monitors['accu_val'].avg)

        self.monitors['loss_val'].end_epoch(write)
        self.monitors['accu_val'].end_epoch(write)

    def test_epoch(self):
        print('\nPredicting on test set...')
        self.net.eval()
        self.data.use_val_transform()

        true_positives = 0
        total_samples = 0

        SparseConvConfig.fine_tune_mode = True

        with torch.no_grad():
            for batch_idx, batch_data in enumerate(tqdm(self.data.test_loader, disable=args.noprogress)):
                images, targets = batch_data[0], batch_data[1]
                images, targets = images.to(self.device), targets.to(self.device)

                # compute output
                output = self.parallel_net(images)
                preds = torch.argmax(output, dim=1)
                true_positives += (preds == targets).long().sum().item()
                total_samples += len(images)

        SparseConvConfig.fine_tune_mode = self.training_schedule.is_finetuning  # Recover any state it was in

        test_accu = true_positives / total_samples
        print('Test accu:', test_accu)
        return test_accu

    def produce_logits(self):
        print('\nProducing logits...')

        logits_batches = []
        accu = TopXAccuracy()

        SparseConvConfig.fine_tune_mode = True
        self.net.eval()
        self.data.use_val_transform()
        with torch.no_grad():
            for batch_idx, batch_data in enumerate(tqdm(self.data.get_raw_dataloader())):
                images, targets = batch_data[0], batch_data[1]
                images, targets = images.to(self.device), targets.to(self.device)

                # compute output
                output = self.parallel_net(images)
                logits_batches.append(output)
                accu.update(output, targets)

        all_logits = torch.cat(logits_batches, dim=0)  # (n_samples, n_classes)
        torch.save(all_logits.cpu(), '{}_{}_logits.pt'.format(args.dataset, args.model))

        print('Top-1 Accu:', accu.top1)
        print('Top-5 Accu:', accu.top5)

    def check_logits(self):
        assert self.data.dataset.kd
        print('\nChecking logits')

        accu = TopXAccuracy()

        for batch_idx, batch_data in enumerate(self.data.get_raw_dataloader()):
            _, targets, logits = tuple(map(lambda x: x.to(self.device), batch_data))
            accu.update(logits, targets)

        print('Top-1 Accu:', accu.top1)
        print('Top-5 Accu:', accu.top5)

    def train(self):
        self.vis.plot_metrics()
        start_epoch = self.epoch_idx
        print('Training has started.')
        for epoch in range(start_epoch, self.training_schedule.end_epoch + 1):
            self.epoch_idx = epoch
            self.training_schedule.step()
            # Proceed with epoch
            self.train_epoch()
            self.val_epoch()
            sys.stdout.flush()
            self.save()
            self.vis.plot_metrics()

    def set_slow_lr(self):
        for param_group in self.optimizer.param_groups:
            if param_group['lr'] != 0.0:
                param_group['lr'] = self.slow_lr

    def set_fast_lr(self):
        for param_group in self.optimizer.param_groups:
            if param_group['lr'] != 0.0:
                param_group['lr'] = self.fast_lr

    ##############################################################################
    # CHECKPOINTS                                                                #
    ##############################################################################

    @staticmethod
    def get_checkpoint_file():
        if args.loadlast is not None:
            list_of_files = glob.glob(ClassificationTraining.full_path('*.pt'))
            if len(list_of_files) > 0:
                last_file = max(list_of_files, key=os.path.getctime)
                # Workaround needed because glob is not consistent about absolute/relative paths
                last_file = ClassificationTraining.full_path(os.path.basename(last_file))
                return last_file
            else:
                print('WARNING: Checkpoint dir is empty. Training from scratch.')
                return None
        elif args.load is not None:
            return args.load

    def read_checkpoint_metadata(self):
        ckpt_file = self.get_checkpoint_file()
        if ckpt_file:
            self.checkpoint = Checkpoint(ckpt_file, use_cpu=args.cpu)
            ckpt_ep = self.checkpoint.metadata['ep_idx']
            self.epoch_idx = ckpt_ep + 1
            self.training_schedule.catch_up(self.epoch_idx)
        else:
            self.epoch_idx = 0

    @staticmethod
    def full_path(filename):
        path = args.loadlast if args.loadlast is not None else '.'
        if not os.path.isdir(path):
            os.makedirs(path, exist_ok=True)
        return os.path.join(path, filename)

    def make_checkpoint_filename(self, ep=None):
        run_name = '{}_{}_{}'.format(args.method, args.model, args.dataset)
        pattern = '{}_ep{}.pt' if self.checkpoint_filename_pattern is None else self.checkpoint_filename_pattern
        filename = pattern.format(run_name, ep or self.epoch_idx)
        return self.full_path(filename)

    def load(self, c: Checkpoint):
        """Load a training checkpoint. See also save().

        Note: the Checkpoint object is instantatied in prepare_model() and some metadata is read there.
        """

        self.net.load_state_dict(c.state_dict)
        self.monitors = c.metadata['monitors']
        if 'l0_loss' not in self.monitors:
            self.monitors['l0_loss'] = MetricHistory()

        # If the method was changed (e.g. sparsify a full net), dont load optimizer state
        if c.metadata['args']['method'] == args.method:
            self.optimizer.load_state_dict(c.optim_state_dict)

        self.parent = c.filename
        self.last_checkpoint = c.filename

    def save(self):
        """Save a checkpoint of the training state."""

        full_filename = self.make_checkpoint_filename()
        metadata = {
            'monitors': self.monitors,
            'parent': self.parent,
            'ep_idx': self.epoch_idx,
            'args': vars(args)  # convert args to dict
        }
        Checkpoint.save(metadata, self.net.state_dict(), self.optimizer.state_dict(), full_filename)
        if args.delete_ckpt and self.last_checkpoint is not None:
            os.remove(self.last_checkpoint)
        self.last_checkpoint = full_filename

    ##############################################################################
    # MONITORING                                                                 #
    ##############################################################################

    def make_monitors(self):
        self.monitors = {name: MetricHistory() for name in ('loss_train', 'loss_val', 'accu_train', 'accu_val',
                                                            'alive_conv', 'alive_lin', 'l0_loss')}

    def prepare_visdom(self):
        is_debug = os.environ.get('DEBUG') == '1'
        env_name = ['spnet'] + ([args.loadlast.split('/')[-1].replace('_', '-')]
                                if args.loadlast is not None else []) + (['debug'] if is_debug else [])
        env_name = ' '.join(env_name)
        if args.novis:
            self.vis = VisdomHelper.make_dummy()
        else:
            self.vis = VisdomHelper(self.monitors, env_name)

    def get_epoch_progress_str(self):
        return 'Ep {}/{}'.format(self.epoch_idx, self.training_schedule.end_epoch)

    ##############################################################################
    # EVENTS (Training schedule)                                                 #
    ##############################################################################

    def on_event_slow(self, pruning_round):
        self.set_slow_lr()

    def on_event_fast(self, pruning_round):
        self.set_fast_lr()

    def on_event_prune(self, pruning_round):
        if args.method == 'infobo':
            pruning_methods['infobo'](self.net.prunable_modules)
        elif args.method == 'morph':
            new_widths = self.net.get_new_widths(self.data.insize, args.budget)
            ratio = float(args.budget) / self.net.get_default_resources(self.data.insize)
            # Rebuild net
            del self.net
            del self.parallel_net
            self.net = self.morphnet_factory(widths=new_widths, num_classes=self.data.num_classes, pretrained=False,
                                             init_weights=True)
            self.net.to(self.device)
            self.parallel_net = torch.nn.DataParallel(self.net) if args.parallel else self.net
            self.make_optimizer()
        else:
            ablated_ratio = 1.0 - self.pruning_targets[pruning_round]
            pruning_methods[args.method](self.net.prunable_modules, ablated_ratio, graph=self.net.static_graph)

    def on_event_finetune(self, pruning_round):
        if args.scratch:
            self.net.init_weights()
        if args.method == 'bar':
            self.net.remove_orphans()
            bar_inflate(self, args.budget)
        self.make_optimizer()  # Stop optimizing gates
        SparseConvConfig.fine_tune_mode = True
        self.criterion = ce_criterion  # Remove sparsity loss

    def on_event_test(self, pruning_round):
        filename = self.make_checkpoint_filename(self.epoch_idx - 1)
        sparsity = self.get_sparsity_ratio()
        test_accu = self.test_epoch()

        results_file = self.full_path('results.csv')
        print('Writing results in', results_file)
        with open(results_file, 'a') as f:
            f.write(filename + ',' + str(sparsity) + ',' + str(test_accu) + '\n')

    def on_event_stop(self, pruning_round):
        print('Caught Stop Event')
        exit(0)


##############################################################################
# ENTRY POINT                                                                #
##############################################################################

if __name__ == '__main__':
    print_args(vars(args))
    training = ClassificationTraining()

    if args.checklogits:
        training.check_logits()

    if args.testonly:
        training.test_epoch()
        print(training.get_sparsity_ratio())
    elif args.logits:
        assert args.method == 'full'
        training.produce_logits()
    else:
        training.train()
