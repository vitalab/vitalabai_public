from keras import Input, Model
from keras.engine.saving import load_model
from keras.optimizers import Adam
from tqdm import tqdm

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.dataset.semanticsegmentation.acdc.postprocess import (
    Post3dBigBlob,
)
from VITALabAI.dataset.semanticsegmentation.acdc.preprocess import (
    PreProcessSTD,
    PreQuantilePercent
)
from VITALabAI.project.acdc.acdc_base import ACDCBase
from VITALabAI.project.acdc.autoencoder_layer_utlis import vae_encoder, VAESampling, VAELatentMean, VAELatentLogVar
from VITALabAI.utils.dataset import centered_resize
from keras.utils import Sequence


class SegmentationEncoderSequenceWrapper(Sequence):
    """ Wrap the sequence to return the ground truth with the same scale as the predictions.
        Note, "sequence" is structure guarantees that the network will only train once on each sample per
        epoch, use for "generator" function from keras.
    """

    def __init__(self, seq):
        self.seq = seq
        self.batch_size = seq.batch_size

    def __len__(self):
        """This method returns the number of elements in the sequence."""
        return len(self.seq)

    def __getitem__(self, idx):
        """This method returns the image and the ground truth.

        Args:
            idx: int, Index of the sequence.

        Returns:
            list: input img
                  gt for the VAE encoder
            array: gt
        """
        img, gt = self.seq[idx]

        return [img, gt], gt


class ACDCSegmentationEncoder(ACDCBase):
    """ Class used to train an encoder of ACDC IRMs to the latent space of an autoencoder of ACDC IRM segmentations

       Here is a schematic view of this class when a vae is used as the autoencoder (most promising approach)

                          vae encoder
                          (frozen)
       gt segmentation ---------------> latent projection
                                               |
                                               + A loss can make the two projections close*
                                               |
                       irm encoder             |              vae decoder**
       imr image ---------------------> latent projection -------------------> predicted segmentation
                                                                                       |
                                                                                       + A loss can make the
                                                                                       | segmentations close*
                                                                                       |
                                                                                   gt segmentation

       * the specific losses used are subject to experimental hypothesis
       ** the vae decoder could be frozen or not according to experimental hypothesis
       The actual model is made of the imr encoder and the vae decoder.

    """

    def __init__(self, dataset: VITALabAiDatasetAbstract, nb_feature_maps=48, code_length=512,
                 pretrained_model=None, pretrained_vae=None, loss_fn=None, optimizer=None, metrics=None,
                 loss_weights=None, name=None, display=False):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            nb_feature_maps: int, number of feature maps in the network
            code_length: int, number of dimensions in the latent space
            pretrained_model: string, path to pretrained model
            pretrained_vae: string, path to VAE used for training
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            loss_weights: weights for the loss functions
            name: string, path to save the results.
            display: bool, if True results will be displayed after each epoch

        # Raises:
            Value error if there is no pretrained_model or pretrained_vae
        """

        self.code_length = code_length
        self.pretrained_model = pretrained_model
        self.pretrained_vae = pretrained_vae
        self.prior = False

        if not pretrained_vae and not pretrained_model:
            raise ValueError("You must provide a pretrained model or/and a pretrained vae model.\n" +
                             "If you provide a pretrained model and won't train it," +
                             "you don't need a pretrained vae model.")

        super().__init__(dataset, loss_fn, optimizer, metrics, loss_weights=loss_weights,
                         nb_feature_maps=nb_feature_maps,
                         pretrained_model=pretrained_model, name=name, display=display)

        self.postproc = (
            Post3dBigBlob(),
        )

    def build_model(self):
        """ Build a model to train a segmentation encoder

        Here the segmentation encoder is a submodel of the composed model used for training.
        The encoder and decoder of a pretrained convolutional variational autoencoder compose the rest of the model.

        Returns:
            keras.Model object *not* compiled.

        # Raises:
            Value error if there is no pretrained_model or pretrained_vae
        """

        if not self.pretrained_model and not self.pretrained_vae:
            raise ValueError("You need a pretrained_model or a pretrained_vae to use this model")

        self.model = None
        if self.pretrained_model:
            self.model = load_model(self.pretrained_model, {"VAESampling": VAESampling,
                                                            "VAELatentMean": VAELatentMean,
                                                            "VAELatentLogVar": VAELatentLogVar})

        if self.pretrained_vae:
            vae = load_model(self.pretrained_vae, custom_objects={"VAESampling": VAESampling}, compile=False)

            # Load a vae encoder used as a target for training
            # (this is not part of the model but is required for training)
            target_encoder = vae.get_layer("vae_encoder")

            input_gt = Input(shape=(self.dataset.get_input_shape()[0], self.dataset.get_input_shape()[1],
                                    self.dataset.nb_classes),
                             name='layer0')

            target_distribution = target_encoder(input_gt)

            if self.model is None:
                frozen_decoder = vae.get_layer("vae_decoder")

                # Build the image encoder model
                input_image = Input(self.dataset.get_input_shape(), name="input_image")
                encoder = vae_encoder(image_size=self.dataset.get_input_shape(), input_channels=1,
                                      nb_feature_maps=self.nb_feature_maps, code_length=self.code_length,
                                      name="seg_encoder")
                print("Model: %s built." % encoder.name)
                encoder.summary()

                # Get latent space distribution, sample, and decode the associated segmentation
                latent_distribution = encoder(input_image)
                latent_sample = VAESampling()(latent_distribution)
                segmentation = frozen_decoder(latent_sample)
                mean = VAELatentMean(name="latent_mean")(latent_distribution)
                log_var = VAELatentLogVar(name="latent_log_var")(latent_distribution)

                self.model = Model(inputs=[input_image, input_gt],
                                   outputs=[segmentation, latent_distribution, target_distribution, mean, log_var])

                self.model.add_loss(0.002 * self.acdclosses.KL_loss_two_normals(target_distribution,
                                    latent_distribution))

        self.model.get_layer("vae_decoder").trainable = False
        self.model.get_layer("vae_encoder").trainable = False

        self.model.summary()

        return self.model

    def losses(self):
        return {
            "vae_decoder": self.acdclosses.combined_loss
        }

    def losses_weights(self):
        return {"vae_decoder": 0.9998}

    def metrics(self):
        return {"latent_mean": self.acdclosses.tensor_similarity,
                "latent_log_var": self.acdclosses.tensor_similarity,
                "vae_decoder": [self.acdclosses.classes_dice,
                                self.acdclosses.dice_on_rv,
                                self.acdclosses.dice_on_myo,
                                self.acdclosses.dice_on_lv]}

    def predict_and_save(self, sequence, f5h):
        """ Function to predict and save results f

        Args:
            sequence: Keras.Sequence, sequence used to generate predictions
            f5h: h5py file to save predictions in
        """

        for i in tqdm(range(len(sequence))):
            data = sequence[i]
            if len(data) == 5:
                name, img, gt, prior, v_size = data
            else:
                name, img, gt, v_size = data

            if not name.endswith("_0"):
                continue  # Don't use augmented data

            pred, distribution, target_distribution, mean, log_var = self.model.predict_on_batch([img, gt])
            pred = centered_resize(pred, gt.shape[1:3])
            group = f5h.create_group(name)
            self.save_into_h5f(group, img, gt, pred, v_size, mean, log_var)

    def save_into_h5f(self, group, img, gt, pred, v_size, mean, log_var):
        """ Save the the results in the h5f output """
        super().save_into_h5f(group, img, gt, pred, v_size)
        group.create_dataset("latent_space_mean", data=mean)
        group.create_dataset("latent_space_log_var", data=log_var)

    def get_train_set(self):
        return SegmentationEncoderSequenceWrapper(self.dataset.get_train_set())

    def get_validation_set(self):
        return SegmentationEncoderSequenceWrapper(self.dataset.get_validation_set())

    def get_test_set(self):
        return SegmentationEncoderSequenceWrapper(self.dataset.get_test_set())

    @staticmethod
    def get_preprocessing():
        return PreProcessSTD(), PreQuantilePercent(),

if __name__ == '__main__':
    from VITALabAI.dataset.semanticsegmentation.acdc.acdc import VITALAcdc
    from VITALabAI.dataset.semanticsegmentation.acdc.augmentation_transformer import AugmentationTransformer
    import argparse

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the dataset", type=str, required=True)
    aparser.add_argument("--vae", help="Path to pretrained vae h5 file", type=str, default=None,
                         required=True)
    aparser.add_argument("--pretrained_model", type=str, help="Model h5 file", default=None)
    aparser.add_argument("--nb_epochs", type=int, help="Number of epoch to train on the dataset",
                         default=1000)
    aparser.add_argument("--batch_size", type=int, help="choose the size of the batch feeded to the network.",
                         default=40)
    aparser.add_argument("--nb_feature_maps", type=int, help="Number of features maps used on the first "
                                                             "layer", default=48)
    aparser.add_argument("--code_length", type=int, help="Code lenght", default=32)
    aparser.add_argument("--name", type=str, dest="name", help="Model path", default=None)
    aparser.add_argument("--display", dest="display", action="store_true",
                         help="Display results during training")
    aparser.add_argument("--testonly", action='store_true', dest="testonly",
                         help="Skip training and do test phase")
    aparser.add_argument("--lr", type=float, dest="lr", help="learning rate", default=3e-4)
    aparser.add_argument("--no_da", dest="use_da", action='store_false', help="Disable data augmentation")
    args = aparser.parse_args()

    augmentation_transformer = None
    if args.use_da:
        augmentation_transformer = AugmentationTransformer(rotation_range=60, width_shift_range=0,
                                                           height_shift_range=0, zoom_range=0.25,
                                                           dilation_range=0)

    ds = VITALAcdc(path=args.path, batch_size=args.batch_size,
                   augmentation_transformer=augmentation_transformer,
                   preproc_fn=ACDCSegmentationEncoder.get_preprocessing())

    Adam(lr=args.lr, amsgrad=True)
    model = ACDCSegmentationEncoder(ds, pretrained_vae=args.vae,
                                    pretrained_model=args.pretrained_model,
                                    nb_feature_maps=args.nb_feature_maps,
                                    code_length=args.code_lenght)

    if not args.testonly:
        callbacks = model.get_callbacks(learning_rate=args.lr)
        model.train(epochs=args.nb_epochs, callbacks=callbacks, workers=1)
        model.save()

    model.evaluate()
    model.export_results()
