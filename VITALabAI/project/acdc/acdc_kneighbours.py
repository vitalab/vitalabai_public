import numpy as np
from keras.engine.saving import load_model
from sklearn.neighbors import NearestNeighbors
from tqdm import tqdm

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.dataset.semanticsegmentation.acdc.augmentation_transformer import AugmentationTransformer
from VITALabAI.dataset.semanticsegmentation.acdc.postprocess import (
    Post3dBigBlob,
)
from VITALabAI.dataset.semanticsegmentation.acdc.preprocess import (
    PreProcessSTD,
    PreQuantilePercent
)
from VITALabAI.model.acdc.layers_utils import VAELatentMean
from VITALabAI.project.acdc.acdc_base import ACDCBase
from VITALabAI.project.acdc.autoencoder_layer_utlis import VAESampling, VAELatentLogVar
from VITALabAI.utils.dataset import centered_resize


class ACDCKNeighbours(ACDCBase):
    """KNN autoencoder model for the ACDC project. """

    def __init__(self, dataset: VITALabAiDatasetAbstract, latent_space_dataset=None, pretrained_model=None,
                 name=None, mode='vae', k=1, option='distance'):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.:
            latent_space_dataset: path to .npz containing all n-dim dataset encoded by the model
            pretrained_model: path of a valid model to encode input
            name: string, name of the model
            mode: {'vae', 'cae', 'seg_encoder'} : param that define how the model will be built
            k: number of k neighbours
            option: if k is greater than one, select how output will be generate form k nearest neighbours
                    distance : closer neighbours will have greater weight
                    uniform : result is average of k nearest neighbours

        # Raises:
            ValueError if the latent space dataset file is wrong format.")
        """

        self.latent_space_dataset = latent_space_dataset
        self.mode = mode
        self.k = k

        if self.k > 1 and option == 'distance':
            self.k_nearest_func = self.distance_k_nearest_neighbours
        elif self.k > 1 and option == 'uniform':
            self.k_nearest_func = self.uniform_k_nearest_neighbours
        elif self.k == 1:
            self.k_nearest_func = self.one_nearest_neighbour
        if not latent_space_dataset.endswith('.npz'):
            raise ValueError("Wrong format for latent space dataset file.")

        self.neigh = NearestNeighbors(n_neighbors=self.k)
        self.latent_space_dataset = np.load(latent_space_dataset)['latent_space']

        super().__init__(dataset, pretrained_model=pretrained_model, name=name)

        self.postproc = (Post3dBigBlob(),)

    def build_model(self):
        """ Build a model

        Encoder layer and decoder layer are built with regard to 'mode' param:
            *if vae is selected, encoder and decoder will be vae
            *if seg encoder is selected, encoder will be model from pretrained model path
            *if cae is selected, encoder and decoder will be cae

        KNN sklean object is fit to user input latent space dataset

        Returns:
            keras loaded model

        """
        if self.mode == 'seg_encoder':
            self.model = load_model(self.pretrained_model, {"VAESampling": VAESampling,
                                                            "VAELatentMean": VAELatentMean,
                                                            "VAELatentLogVar": VAELatentLogVar},
                                    compile=False)
            self.encoder = self.model.get_layer("seg_encoder")
            self.decoder = self.model.get_layer("vae_decoder")
        elif self.mode == 'cae':
            self.model = load_model(self.pretrained_model, compile=False)
            self.encoder = self.model.get_layer("cae_encoder")
            self.decoder = self.model.get_layer("cae_decoder")
        elif self.mode == 'vae':
            self.model = load_model(self.pretrained_model, {"VAESampling": VAESampling}, compile=False)
            self.encoder = self.model.get_layer("vae_encoder")
            self.decoder = self.model.get_layer("vae_decoder")

        self.neigh.fit(self.latent_space_dataset)

        return self.model

    def metrics(self):
        """ Metrics used during the training.

        Returns:
            dictionary of metrics associated with each output
        """
        return {
            "latent_mean": self.acdclosses.tensor_similarity,
            "latent_log_var": self.acdclosses.tensor_similarity,
            "vae_decoder": [self.acdclosses.classes_dice,
                            self.acdclosses.dice_on_rv,
                            self.acdclosses.dice_on_myo,
                            self.acdclosses.dice_on_lv]
        }

    def predict_and_save(self, sequence, f5h):
        """ Function to predict and save results f

        Args:
            sequence: Keras.Sequence, sequence used to generate predictions
            f5h: h5py file to save predictions in
        """

        for i in tqdm(range(len(sequence))):
            data = sequence[i]
            if len(data) == 5:
                name, img, gt, prior, v_size = data
            else:
                name, img, gt, v_size = data

            if not name.endswith("_0"):
                continue  # Don't use augmented data

            if self.mode == 'cae' or self.mode == 'vae':
                encoded = self.encoder.predict_on_batch(gt)[:, :, 0]
            else:
                encoded = self.encoder.predict_on_batch(img)[:, :, 0]

            mean = np.apply_along_axis(self.k_nearest_func, 1, encoded)
            pred = centered_resize(self.decoder.predict_on_batch(mean), gt.shape[1:3])

            group = f5h.create_group(name)
            self.save_into_h5f(group, img, gt, pred, v_size)

    @staticmethod
    def get_preprocessing():
        return PreProcessSTD(), PreQuantilePercent(),

    def one_nearest_neighbour(self, encoded):
        """Return nearest neighbours.

        Args:
            encoded: n-dimensional data living in latent space

        Returns:
            Nearest neighbour of input found in latent space dataset

        """
        _, idx = self.neigh.kneighbors([encoded])
        return self.latent_space_dataset[idx.squeeze()]

    def uniform_k_nearest_neighbours(self, encoded):
        """Return k nearest neighbours.

        Args:
            encoded: n-dimensional data living in latent space

        Returns:
            Average of k nearest neighbours of input data

        """
        _, idx = self.neigh.kneighbors([encoded])
        return np.sum((1 / self.k) * self.latent_space_dataset[idx.squeeze()], axis=0)

    def distance_k_nearest_neighbours(self, encoded):
        """Return k nearest neighbours.

        Args:
            encoded: n-dimensional data living in latent space

        Returns:
            Weighted average based on inverse of distance of k nearest neighbours of input data

        """
        dist, idx = self.neigh.kneighbors([encoded])
        dist = dist.squeeze()
        np.place(dist, dist == 0, 0.00001)
        w = 1 / dist
        return np.sum(w[:, None] * self.latent_space_dataset[idx.squeeze()], axis=0) / np.sum(w)


if __name__ == '__main__':
    from VITALabAI.dataset.semanticsegmentation.acdc.acdc import VITALAcdc
    import argparse

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the hdf5 dataset", type=str, required=True)
    aparser.add_argument("--latent_space_dataset", help="Path to the latent space dataset", type=str,
                         required=True)
    aparser.add_argument("--pretrained_model", type=str, help="Model h5 file", default=None, required=True)
    aparser.add_argument("--name", type=str, dest="name", help="Model path", default=None)
    aparser.add_argument("--no_da", dest="use_da", action='store_false', help="Disable data augmentation")
    aparser.add_argument("--mode", help="Which model to build", choices=['cae', 'vae', 'seg_encoder'],
                         default='vae')
    aparser.add_argument("--k", type=int, help="Number of k neighbours", default=1)
    aparser.add_argument("--option", type=str, choices=['distance', 'uniform'], default='distance',
                         help="If k is greater than one, select how output will "
                              "be generate form k nearest neighbours")

    args = aparser.parse_args()

    augmentation_transformer = None
    if args.use_da:
        augmentation_transformer = AugmentationTransformer(rotation_range=60, width_shift_range=0,
                                                           height_shift_range=0, zoom_range=0.1,
                                                           dilation_range=2)

    ds = VITALAcdc(path=args.path,
                   augmentation_transformer=augmentation_transformer,
                   preproc_fn=ACDCKNeighbours.get_preprocessing(), )

    model = ACDCKNeighbours(ds, name=args.name, pretrained_model=args.pretrained_model,
                            k=args.k, mode=args.mode, option=args.option,
                            latent_space_dataset=args.latent_space_dataset)

    model.evaluate()
    model.export_results()
