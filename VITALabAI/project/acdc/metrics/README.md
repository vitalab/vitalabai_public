# ACDC anatomical metrics

## Contributors
* Laurence Fournier (laurence.fournier@usherbrooke.ca)
* Nathan Painchaud (nathan.painchaud@usherbrooke.ca)

## Description
This is a utility program to compute anatomical metrics on results from the training of the ACDC models.
The purpose of the anatomical metrics is to provide means of measuring a model's performance aside from the traditional Hausdorff distance and dice.

The input results needed to compute the metrics are extracted from hdf5 files, and the output are csv files detailing the anatomical metrics for each image.
The metrics can be computed on either the predictions or the ground truth of the image, depending on the arguments passed to the program.

## Usage

Compute metrics on a model's predictions (do not forget the trailing slash in the path):
```bash
python anatomical_metrics_utils.py pred_m "path/to/directory/of/results/"
```

Compute metrics on the ground truth (do not forget the trailing slash in the path):
```bash
python anatomical_metrics_utils.py gt_m "path/to/directory/of/results/"
```

In both cases, the directory of results must contain the hdf5 files `TRAIN_PREDICTION`, `VALID_PREDICTION` and `TEST_PREDICTION` that where produced during the training of an ACDC model.
The output csv files are written to the current directory.