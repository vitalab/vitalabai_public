from abc import ABC

import numpy as np
from scipy import ndimage as ndi
from skimage import measure, morphology


class AnatomicalMetrics(ABC):
    """This class is used to compute the anatomical metrics for the ACDC dataset."""
    def __init__(self):
        self._background = 0
        self._rv = 1
        self._myo = 2
        self._lv = 3
        self._lv_concavity_threshold = 0
        self._rv_concavity_threshold = 22
        self._myo_concavity_threshold = 0

    def _count_holes_on_seg_class(self, img_2darray, seg_class):
        """ Counts the pixels that form holes in the supposedly contiguous area segmented as the given class.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the array is the
                        identifier of the segmentation class for the pixel
            seg_class: the identifier of the segmentation class

        Returns:
             the count of pixels that form holes in the supposedly contiguous are segmented as the given class
        """

        # Mark the class of interest as 0 and everything else as 1
        # Merge the regions of 1 that are open by a side using padding (these are not holes)
        seg_class_img = np.pad(1 * (img_2darray != seg_class), ((1, 1), (1, 1)), 'constant',
                               constant_values=1)

        # Extract properties of continuous regions of 1
        props = measure.regionprops(measure.label(seg_class_img, connectivity=2))

        hole_pixel_count = 0
        for prop in props:
            # Skip the region open by the side (the one that includes padding)
            if prop.bbox[0] != 0:
                hole_pixel_count += prop.area

        return hole_pixel_count

    def _count_disconnectivity_in_seg_class(self, img_2darray, seg_class):
        """ Counts the pixels that are disconnected from the main area segmented as the given class.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the array is
                        the identifier of the segmentation class for the pixel
            seg_class: the identifier of the segmentation class

        Returns:
             count of pixels that are disconnected from the main area segmented as the given class
        """
        # Merge segmented classes other than the one of interest with background
        seg_class_img = img_2darray == seg_class

        # Extract properties for each contiguous region segmented as the given class
        labels_props = measure.regionprops(measure.label(seg_class_img, connectivity=2))

        if len(labels_props) > 1:  # If there is disconnectivity in the segmentation of the given class
            # Sort by decreasing pixel count of the region
            labels_props_by_desc_size = sorted(labels_props, key=lambda k: k.area, reverse=True)

            total_minus_biggest = sum(label_props.area for label_props in labels_props_by_desc_size[1:])
            return total_minus_biggest

        else:  # No disconnectivity in the segmentation of the given class
            return 0

    def _count_holes_between_seg_classes(self, img_2darray, seg_class1, seg_class2):
        """ Counts the pixels in the gap between the two areas segmented as the given classes.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the
                        array is the identifier of the segmentation class for the pixel
            seg_class1: the identifier of a segmentation class
            seg_class2: the identifier of the other segmentation class

        Returns:
             count of pixels in the gap between the two areas segmented as the given classes
        """

        # Find holes inside class1
        class1 = np.pad(1 * (img_2darray == seg_class1), 1, 'constant', constant_values=0)
        not_class1 = 1 - class1
        holes_class1, last_hole_label_class1 = measure.label(not_class1, connectivity=2, return_num=True)
        outside_class1 = holes_class1[0, 0]
        holes_class1[holes_class1 == outside_class1] = 0

        # Find holes inside class2
        class2 = np.pad(1 * (img_2darray == seg_class2), 1, 'constant', constant_values=0)
        not_class2 = 1 - class2
        holes_class2, last_hole_label_class2 = measure.label(not_class2, connectivity=2, return_num=True)
        outside_class2 = holes_class2[0, 0]
        holes_class2[holes_class2 == outside_class2] = 0

        # Find the holes in class1 that don't contain class2 (and the opposite)
        hole_labels_class1_without_class2 = [h for h in range(1, last_hole_label_class1 + 1) if
                                             h not in np.unique(holes_class1 * class2)]
        hole_labels_class2_without_class1 = [h for h in range(1, last_hole_label_class2 + 1) if
                                             h not in np.unique(holes_class2 * class1)]
        holes_class1_without_class2 = 1 * np.isin(holes_class1, hole_labels_class1_without_class2,
                                                  assume_unique=True)
        holes_class2_without_class1 = 1 * np.isin(holes_class2, hole_labels_class2_without_class1,
                                                  assume_unique=True)

        not_both = 1 - ((class1 + holes_class1_without_class2 + class2 + holes_class2_without_class1) > 0)
        holes_between = measure.label(not_both, connectivity=2)
        outside = holes_between[0, 0]
        holes_between[holes_between == outside] = 0

        return np.sum(holes_between > 0)

    def _count_frontier_pixels(self, img_2darray, seg_class1, seg_class2):
        """ Counts the pixels of class1 that touch pixels of class2 in the segmented image.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the
                        array is the identifier of the segmentation class for the pixel
            seg_class1: the identifier of a segmentation class
            seg_class2: the identifier of the other segmentation class

        Returns:
            the count of pixels of class1 that touch pixels of class2
        """
        class1 = 1 * (img_2darray == seg_class1)
        class2 = 1 * (img_2darray == seg_class2)
        class2_dilated = morphology.dilation(class2, np.ones((3, 3)))
        frontier = class1 * class2_dilated
        return frontier.sum()

    def _measure_heavy_concavity_on_seg_class(self, img_2darray, voxel_size, threshold, seg_class):
        """ Measures the depth of a large concavity in the area segmented as the given class.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the
                        array is the identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)
            threshold: the minimum threshold for the diameter of the concavity to be considered
                        anatomically impossible
            seg_class: the identifier of the segmentation class

        Returns:
             the depth (in mm) of a large concavity in the area segmented as the given class
        """
        # Merge segmented classes other than the one of interest with background
        img_2darray = img_2darray == seg_class

        # Fill in the holes inside the segmented area (useful in the case of myo)
        img_2darray = ndi.morphology.binary_fill_holes(img_2darray).astype(int)

        if np.amax(img_2darray):  # If the given class is present in the image

            # Get convex hull of the segmented area
            convex_hull = morphology.convex_hull_image(img_2darray)

            # Compute a mask for the difference between the convex hull and the source image
            # The mask is eroded to account for the behavior of ```distance_transform_edt``` where a pixel
            # on the frontier with the background is at distance 1 from the background
            diff_convex_img = convex_hull - img_2darray
            diff_convex_img = morphology.erosion(diff_convex_img, np.ones((3, 3)))

            # Compute distance from convex hull pixels to nearest background pixel
            convex_dist_to_background = ndi.distance_transform_edt(convex_hull, voxel_size[0:2])

            # Apply the convex hull difference mask to the distances
            convex_dist_to_background *= diff_convex_img

            # Get the Hausdorff distance by finding the maximum distance in the remaining distances
            hausdroff_distance = np.max(convex_dist_to_background)

            # Only consider the diameter of the concavity to be anatomically impossible if it is above the empirical
            # threshold
            return hausdroff_distance if hausdroff_distance > threshold else 0

        else:  # If the given class in not in the image
            return 0
