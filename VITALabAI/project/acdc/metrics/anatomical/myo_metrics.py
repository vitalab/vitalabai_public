from skimage import measure

from .anatomical_metrics import AnatomicalMetrics


class MyoMetrics(AnatomicalMetrics):
    """Class used to compute the MYO metrics for the ACDC dataset."""

    def count_holes_on_myo(self, img_2darray, voxel_size):
        """ Counts the pixels that form holes in the MYO segmentation class.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the
                        array is the identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)

        Returns:
             the count of pixels that form holes in the MYO segmentation class
        """
        holes_on_myo = self._count_holes_on_seg_class(img_2darray, self._myo)

        if holes_on_myo > 0:  # If myo has holes, assume it is closed and account for lv amongst the holes

            # Count the number of pixels that make up the lv segmented area (that should form a hole inside myo)
            lv_img = img_2darray == self._lv
            lv_props = measure.regionprops(measure.label(lv_img, connectivity=2))
            lv_pixel_count = 0

            if lv_props:  # If the lv is present in the image
                lv_pixel_count = lv_props[0].area

            # Subtract the number of pixels making up the lv segmented area from the holes
            return holes_on_myo - lv_pixel_count

        else:  # If myo has no holes, it is open and other metrics should indicate an anatomical error
            return 0

    def count_disconnectivity_in_myo(self, img_2darray, voxel_size):
        """ Counts the pixels that are disconnected from the main area segmented as MYO.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the
                        array is the identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)

        Returns:
             the count of pixels that are disconnected from the main area segmented as MYO
        """
        return self._count_disconnectivity_in_seg_class(img_2darray, self._myo)

    def measure_heavy_concavity_on_myo(self, img_2darray, voxel_size):
        """ Measures the depth of a large concavity in the area segmented as MYO.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the array is
                        the identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)

        Returns:
             the depth (in mm) of a large concavity in the area segmented as MYO
        """
        return self._measure_heavy_concavity_on_seg_class(img_2darray, voxel_size,
                                                          self._myo_concavity_threshold,
                                                          self._myo)
