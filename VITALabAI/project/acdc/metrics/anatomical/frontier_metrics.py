import numpy as np
from scipy.ndimage import distance_transform_edt
from skimage import morphology

from .anatomical_metrics import AnatomicalMetrics


class FrontierMetrics(AnatomicalMetrics):
    """Class used to compute the frontier metrics for the ACDC dataset."""
    def count_holes_between_lv_and_myo(self, img_2darray, voxel_size):
        """ Counts the pixels in the gap between the two areas segmented as LV and MYO.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the
                        array is the identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)

        Returns:
             the count of pixels in the gap between the two areas segmented as LV and MYO
        """
        return self._count_holes_between_seg_classes(img_2darray, self._lv, self._myo)

    def count_holes_between_rv_and_myo(self, img_2darray, voxel_size):
        """ Counts the pixels in the gap between the two areas segmented as RV and MYO.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the
                        array is the identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)

        Returns:
             the count of pixels in the gap between the two areas segmented as RV and MYO
        """
        return self._count_holes_between_seg_classes(img_2darray, self._rv, self._myo)

    def compute_rv_disconnected_from_myo(self, img_2darray, voxel_size):
        """ Measures the gap (in mm) between the areas segmented as RV and MYO.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the array is the
                        identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)

        Returns:
             the width of the gap (in mm) between the areas segmented as RV and MYO
        """
        infinity = 1.5 * img_2darray.size * max(
            voxel_size[0:2])  # this should be bigger than any mm distance between 2 pixels!
        rv_as_1 = morphology.dilation(1 * (img_2darray == self._rv), np.ones((3, 3)))

        myo_as_0 = 1 * (img_2darray != self._myo)
        if rv_as_1.sum() > 0 and (1 - myo_as_0).sum() > 0:  # If both rv and myo are present in the image
            distance_to_myo = distance_transform_edt(myo_as_0, voxel_size[0:2])
            min_distance_rv_to_myo = (rv_as_1 * distance_to_myo + infinity * (1 - rv_as_1)).min()
            return min_distance_rv_to_myo
        else:  # If the image has no rv and myo
            return 0

    def count_lv_touches_rv(self, img_2darray, voxel_size):
        """ Counts the pixels that touch between the LV and RV segmentation classes.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the
                        array is the identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)

        Returns:
             the count of pixels that touch between the LV and RV segmentation classes
        """
        return self._count_frontier_pixels(img_2darray, self._lv, self._rv)

    def count_lv_touches_background(self, img_2darray, voxel_size):
        """ Counts the pixels that touch between the LV and background segmentation classes.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the
                        array is the identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)

        Returns:
             the count of pixels that touch between the LV and background segmentation classes
        """
        return self._count_frontier_pixels(img_2darray, self._lv, self._background)
