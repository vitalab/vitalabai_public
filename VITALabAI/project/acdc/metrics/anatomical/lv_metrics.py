from .anatomical_metrics import AnatomicalMetrics


class LvMetrics(AnatomicalMetrics):
    """Class used to compute the LV metrics for the ACDC dataset."""
    def count_holes_on_lv(self, img_2darray, voxel_size):
        """ Counts the pixels that form holes in the LV segmentation class.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the array is
                        the identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)

        Returns:
            the count of pixels that form holes in the LV segmentation class
        """
        return self._count_holes_on_seg_class(img_2darray, self._lv)

    def count_disconnectivity_in_lv(self, img_2darray, voxel_size):
        """ Counts the pixels that are disconnected from the main area segmented as LV.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the
                        array is the identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)

        Returns:
            the count of pixels that are disconnected from the main area segmented as LV
        """
        return self._count_disconnectivity_in_seg_class(img_2darray, self._lv)

    def measure_heavy_concavity_on_lv(self, img_2darray, voxel_size):
        """  Measures the depth of a large concavity in the area segmented as LV.

        Args:
            img_2darray: a 2D array of dimensions (height, width) where the value of each entry in the
                        array is the identifier of the segmentation class for the pixel
            voxel_size: the size of the voxels along each (x, y, z) dimensions (in mm)

        Returns:
             the depth (in mm) of a large concavity in the area segmented as LV
        """
        return self._measure_heavy_concavity_on_seg_class(img_2darray, voxel_size,
                                                          self._lv_concavity_threshold,
                                                          self._lv)
