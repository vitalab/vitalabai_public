import sys
from os.path import join as pjoin

import h5py
import pandas as pd
from tqdm import tqdm

from VITALabAI.project.acdc.metrics.anatomical import frontier_metrics
from VITALabAI.project.acdc.metrics.anatomical import lv_metrics
from VITALabAI.project.acdc.metrics.anatomical import myo_metrics
from VITALabAI.project.acdc.metrics.anatomical import rv_metrics


def extract_metrics(data_key, path_to_results_dir, metrics_folder=''):
    """ Computes and outputs to csv the anatomical metrics of the predictions made by a trained network over training,
    validation and testing datasets.

    NOTE: The files TRAIN_PREDICTION, VALID_PREDICTION and TEST_PREDICTION must be present in the current directory
    when calling this script.

    Args:
        data_key: key of the set of data on which to compute metrics (Possible values: 'pred_m', 'gt_m')
        path_to_results_dir: the path to directory containing the 'TRAIN_PREDICTION', 'VALID_PREDICTION' and
                            'TEST_PREDICTION' hdf5 results files
        metrics_folder: string, path to the directory in which to save the result csv files.

    # Raises:
        ValueError if the parameters are invalid
    """
    predictions_filenames = ['TRAIN_PREDICTION', 'VALID_PREDICTION', 'TEST_PREDICTION']
    if data_key == 'pred_m':
        metrics_filenames = ['TRAIN_PREDICTION_METRICS.csv',
                             'VALID_PREDICTION_METRICS.csv',
                             'TEST_PREDICTION_METRICS.csv']
    elif data_key == 'gt_m':
        metrics_filenames = ['TRAIN_GT_METRICS.csv',
                             'VALID_GT_METRICS.csv',
                             'TEST_GT_METRICS.csv']
    else:
        raise ValueError("Value %s for parameter 'data' is invalid." % data_key)

    for predictions_filename, metrics_filename in zip(predictions_filenames, metrics_filenames):
        print("Computing metrics for dataset: %s ..." % predictions_filename)
        _extract_metrics_by_dataset(path_to_results_dir + predictions_filename, pjoin(metrics_folder,
                                                                                      metrics_filename),
                                    data_key=data_key)
        print("Finished computing metrics for dataset: %s" % predictions_filename)


def _extract_metrics_by_dataset(predictions_filename, metrics_filename, data_key='pred_m'):
    """ Computes and outputs to csv the anatomical metrics of the predictions made by a trained network over a specific
    dataset.

    Args:
        predictions_filename: the name of the hd5 file containing the predictions dataset
        metrics_filename: the name of the csv file to be produced as output
        data_key: key of the set of data on which to compute metrics (Possible values: 'pred_m', 'gt_m')
    """
    lv = lv_metrics.LvMetrics()
    rv = rv_metrics.RvMetrics()
    myo = myo_metrics.MyoMetrics()
    frontier = frontier_metrics.FrontierMetrics()

    file = h5py.File(predictions_filename, 'r')

    # A list of identifiers for all the images in the dataset
    img_ids = ['%s_%s' % (group, slice_idx)
               for group in file.keys()
               for slice_idx in range(file[group][data_key].shape[0])]

    # A function that creates a generator that iterates over all the images (2d arrays) in the dataset
    # A function is used rather than a simple generator because the generator needs to be reused
    # (with a function, a new and identical generator is created each time)
    def dataset_generator():
        for group_key in file.keys():
            for slice_idx in range(file[group_key][data_key].shape[0]):
                yield file[group_key][data_key][slice_idx], file[group_key].attrs.get('voxel_size')

    # The detailed metrics for each image
    # Each dictionary entry is a list containing the values for that metric for all the images
    def compute_metric(metric_fn):
        return [metric_fn(img_2darray, voxel_size) for img_2darray, voxel_size in tqdm(dataset_generator())]

    metrics = {
        'patientName_frame_slice': img_ids,
        'holes_on_lv': compute_metric(lv.count_holes_on_lv),
        'holes_on_rv': compute_metric(rv.count_holes_on_rv),
        'holes_on_myo': compute_metric(myo.count_holes_on_myo),
        'disconnectivity_in_lv': compute_metric(lv.count_disconnectivity_in_lv),
        'disconnectivity_in_rv': compute_metric(rv.count_disconnectivity_in_rv),
        'disconnectivity_in_myo': compute_metric(myo.count_disconnectivity_in_myo),
        'holes_between_lv_and_myo': compute_metric(frontier.count_holes_between_lv_and_myo),
        'holes_between_rv_and_myo': compute_metric(frontier.count_holes_between_rv_and_myo),
        'rv_disconnected_from_myo': compute_metric(frontier.compute_rv_disconnected_from_myo),
        'lv_touches_rv': compute_metric(frontier.count_lv_touches_rv),
        'lv_touches_background': compute_metric(frontier.count_lv_touches_background),
        'heavy_concavity_on_lv': compute_metric(lv.measure_heavy_concavity_on_lv),
        'heavy_concavity_on_rv': compute_metric(rv.measure_heavy_concavity_on_rv),
        'heavy_concavity_on_myo': compute_metric(myo.measure_heavy_concavity_on_myo),
    }

    # The statistics computed over all the detailed results
    # Each entry is a list of only one element, because pandas behaves differently when writing scalar values
    def compute_results(metric):
        return [sum(i != 0 for i in metrics[metric])]

    # Computes the total number of images that featured at least one anatomically impossible result
    def images_with_mistakes(metrics):
        metrics_keys = list(metrics.keys())
        metrics_keys.remove('patientName_frame_slice')
        num_img = len(metrics['patientName_frame_slice'])
        return sum(sum(metrics[key][img_idx]
                       for key in metrics_keys) > 0
                   for img_idx in range(num_img))

    final_results = {
        'patientName_frame_slice': ['final_result'],
        'holes_on_lv': compute_results('holes_on_lv'),
        'holes_on_rv': compute_results('holes_on_rv'),
        'holes_on_myo': compute_results('holes_on_myo'),
        'disconnectivity_in_lv': compute_results('disconnectivity_in_lv'),
        'disconnectivity_in_rv': compute_results('disconnectivity_in_rv'),
        'disconnectivity_in_myo': compute_results('disconnectivity_in_myo'),
        'holes_between_lv_and_myo': compute_results('holes_between_lv_and_myo'),
        'holes_between_rv_and_myo': compute_results('holes_between_rv_and_myo'),
        'rv_disconnected_from_myo': compute_results('rv_disconnected_from_myo'),
        'lv_touches_rv': compute_results('lv_touches_rv'),
        'lv_touches_background': compute_results('lv_touches_background'),
        'heavy_concavity_on_lv': compute_results('heavy_concavity_on_lv'),
        'heavy_concavity_on_rv': compute_results('heavy_concavity_on_rv'),
        'heavy_concavity_on_myo': compute_results('heavy_concavity_on_myo'),
        'images_with_mistakes': images_with_mistakes(metrics),
    }

    _output_metrics_to_csv(final_results, metrics, metrics_filename)


def _output_metrics_to_csv(final_results, metrics, metrics_filename):
    """ Saves the computed metrics, with the final results at the top, to csv format.

    The final_results and metrics dictionaries must contain the following keys:
    - patientName_frame_slice
    - holes_on_lv
    - holes_on_rv
    - holes_on_myo
    - disconnectivity_in_lv
    - disconnectivity_in_rv
    - disconnectivity_in_myo
    - holes_between_lv_and_myo
    - holes_between_rv_and_myo
    - rv_disconnected_from_myo
    - lv_touches_rv
    - lv_touches_background
    - heavy_concavity_on_lv
    - heavy_concavity_on_rv
    - heavy_concavity_on_myo
    - images_with_mistakes    # Only in final_results

    Args:
        final_results: a dictionary containing the total results, where each entry is in the following format:
                        - metric_key: [final_result]    # The final_result must be a list, even though it contains
                                                        only one element,
                                                    # because pandas behaves differently when writing scalar values
        metrics: a dictionary containing the computes metrics, where each entry is in the following format:
                - metric_key: [value of metric for each image in the dataset]
        metrics_filename: the name of the csv file to be produced as output
    """
    data_columns = [
        'patientName_frame_slice',
        'holes_on_lv',
        'holes_on_rv',
        'holes_on_myo',
        'disconnectivity_in_lv',
        'disconnectivity_in_rv',
        'disconnectivity_in_myo',
        'holes_between_lv_and_myo',
        'holes_between_rv_and_myo',
        'rv_disconnected_from_myo',
        'lv_touches_rv',
        'lv_touches_background',
        'heavy_concavity_on_lv',
        'heavy_concavity_on_rv',
        'heavy_concavity_on_myo',
    ]
    result_columns = data_columns.copy()
    result_columns.append('images_with_mistakes')

    # Write final results at the top of the file
    pd.DataFrame(final_results).to_csv(metrics_filename, index=False, columns=result_columns)

    # Append the detailed metrics for each image after the final results
    with open(metrics_filename, 'a') as final_results_csv:
        pd.DataFrame(metrics).to_csv(final_results_csv, header=False, index=False, columns=data_columns)


if __name__ == "__main__":
    # Read the command line arguments from the command that should be called like follows:
    # python anatomical_metric_utils data_key path_to_results_dir
    extract_metrics(sys.argv[1], sys.argv[2])
