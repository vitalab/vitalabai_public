from glob import glob

import numpy as np
from skimage.io import imread

from VITALabAI.model.acdc.metrics.anatomical.anatomical_metrics import AnatomicalMetrics
from VITALabAI.model.acdc.metrics.anatomical.frontier_metrics import FrontierMetrics

black = (0, 0, 0)

red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)

dark_red = (127, 0, 0)
dark_green = (0, 127, 0)
dark_blue = (0, 0, 127)

light_red = (255, 127, 127)
light_green = (127, 255, 127)
light_blue = (127, 127, 255)

magenta = (127, 0, 127)


def segment_color(img, color):
    """ Segment a color from a picture.

    Args:
        img: numpy array of size (_, _, 3) containing the picture data
        color: tuple of size 3 with elements between 0 and 255

    Returns:
         numpy array of size (_, _, 1) with 1 where the color is present in the picture and 0 where it isn't
    """
    return 1 * np.all(img == np.array(color).reshape(1, 1, 3), axis=2)


def get_valid_tests(pattern, allowed_colors):
    """ Find the valid test pictures that match a file name pattern.

    Args:
        pattern: file name pattern that must be matched
        allowed_colors: list of tuples describing the allowed colors in the test pictures

    yield:
        (name, img): name and data of the valid test pictures (numpy.array of size (_, _, 3) for rgb)
    """
    for name in sorted(glob(pattern)):
        img = imread(name)

        # Verify that the transparency layer is not used.
        if np.any(img[:, :, 3] != 255):
            print(name, "contains transparency, will be skipped")
            continue
        img = img[:, :, 0:3]

        # Verify that only allowed colors are used.
        allowed_colors = set(allowed_colors)
        colors_in_test = set(map(tuple, np.unique(img.reshape(-1, 3), axis=0).tolist()))
        illegal_colors = colors_in_test.difference(allowed_colors)
        if illegal_colors:
            print(name, "contains illegal colors, will be skipped")
            print("illegal colors:", illegal_colors)
            continue

        yield name, img


def test_hole_count():
    """ Run a set of tests on AnatomicalMetrics()._count_holes_on_seg_class(...)

    Tests are defined by pictures with file names matching
    test_images/hole_count*.png

    Each picture use the following color convention to partition pixels
    black : class0 (background)
    red : class1
    blue : class2
    dark red: hole of class0 in class1
    dark blue: hole of class2 in class1
    """
    metrics = AnatomicalMetrics()
    for name, img in get_valid_tests("hole_count*.png", [black, red, dark_red, blue, dark_blue]):

        img_seg = segment_color(img, red) + 2 * segment_color(img, blue) + 2 * segment_color(img, dark_blue)
        expected_count = np.sum(segment_color(img, dark_red)) + np.sum(segment_color(img, dark_blue))
        count = metrics._count_holes_on_seg_class(img_seg, 1)

        if expected_count != count:
            print(name, "failed; expected", expected_count, "but found", count, "hole pixels")
        else:
            print(name, "Success")


def test_disconnectivity_count():
    """ Run a set of tests on AnatomicalMetrics()._count_disconnectivity_in_seg_class(...)

    Tests are defined by pictures with file names matching
    test_images/disconnectivity_count*.png

    Each picture use the following color convention to partition pixels
    black : class0 (background)
    red : class1 part of the biggest class1 blob
    dark red: class1 not part of the biggest class1 blob
    blue : class2
    """
    metrics = AnatomicalMetrics()
    for name, img in get_valid_tests("disconnectivity_count*.png", [black, red, dark_red, blue]):

        img_seg = segment_color(img, red) + segment_color(img, dark_red) + 2 * segment_color(img, blue)
        expected_count = np.sum(segment_color(img, dark_red))
        count = metrics._count_disconnectivity_in_seg_class(img_seg, 1)

        if expected_count != count:
            print(name, "failed; expected", expected_count, "but found", count, "hole pixels")
        else:
            print(name, "Success")


def test_hole_between_class_count():
    """ Run a set of tests on AnatomicalMetrics()._count_holes_between_seg_classes(...)

    Tests are defined by pictures with file names matching
    test_images/hole_between_class_count*.png

    Each picture use the following color convention to partition pixels
    black : class0 (background)
    red : class1
    blue : class2
    magenta : class0 between class1 and class2
    """
    metrics = AnatomicalMetrics()
    for name, img in get_valid_tests("hole_between_class_count*.png", [black, red, dark_red, blue, magenta]):

        img_seg = segment_color(img, red) + 2 * segment_color(img, blue)
        expected_count = np.sum(segment_color(img, magenta))
        count = metrics._count_holes_between_seg_classes(img_seg, 1, 2)

        if expected_count != count:
            print(name, "failed; expected", expected_count, "but found", count, "hole pixels")
        else:
            print(name, "Success")


def test_frontier_count():
    """ Run a set of tests on AnatomicalMetrics()._count_frontier_pixels(...)

    Tests are defined by pictures with file names matching
    test_images/hole_between_class_count*.png

    Each picture use the following color convention to partition pixels
    black : class0 (background)
    red : class1
    blue : class2
    light red : frontier of class1 that touch class2
    light blue : frontier of class2 that touch class1
    """
    metrics = AnatomicalMetrics()
    for name, img in get_valid_tests("frontier_count*.png", [black, red, blue, light_red, light_blue]):

        img_seg = segment_color(img, red) + segment_color(img, light_red) + \
                  2 * segment_color(img, blue) + 2 * segment_color(img, light_blue)

        expected_count = np.sum(segment_color(img, light_red))
        count = metrics._count_frontier_pixels(img_seg, 1, 2)

        if expected_count != count:
            print(name, "failed; expected", expected_count, "but found", count, "frontier pixels")
        else:
            print(name, "Success")


def test_distance_rv_myo():
    """ Run a set of tests on AnatomicalMetrics()._count_frontier_pixels(...)

    Tests are defined by pictures with file names matching
    test_images/distance_rv_myo*.png

    Each picture use the following color convention to partition pixels
    black : class0 (background)
    red : class1 (rv)
    blue : class2 (myo)
    light red : one of the closest pixel of class1 from class2
    light blue : the closest pixel of class2 from the first pixel
    (you need to consider axis scale (A = 5.3, B = 8.7) to find a pair of such pixels)
    """
    metrics = FrontierMetrics()
    for name, img in get_valid_tests("distance_rv_myo*.png", [black, red, blue, light_red, light_blue]):

        img_seg = AnatomicalMetrics()._rv * (segment_color(img, red) + segment_color(img, light_red)) + \
                  AnatomicalMetrics()._myo * (segment_color(img, blue) + segment_color(img, light_blue))

        closest1 = np.argwhere(segment_color(img, light_red))
        if closest1.size != 2:
            print(name, "must contains only one light red pixel, will be skip")
            continue
        x1, y1 = tuple(closest1[0])

        closest2 = np.argwhere(segment_color(img, light_blue))
        if closest2.size != 2:
            print(name, "must contains only one light blue pixel, will be skip")
            continue
        x2, y2 = tuple(closest2[0])

        A, B = 5.3, 8.7
        delta_x = A * max(0, abs(x1 - x2) - 1)
        delta_y = B * max(0, abs(y1 - y2) - 1)
        expected_distance = np.sqrt(delta_x ** 2 + delta_y ** 2)
        distance = metrics.compute_rv_disconnected_from_myo(img_seg, [A, B])

        if expected_distance != distance:
            print(name, "failed; expected", expected_distance, "but found", distance, "as distance")
            print("    (x1, y1) = ({}, {})\n    (x2, y2) = ({}, {})".format(x1, y1, x2, y2))
        else:
            print(name, "Success")


def test():
    print("Test hole count")
    test_hole_count()

    print("\nTest disconnectivity count")
    test_disconnectivity_count()

    print("\nTest hole between class count")
    test_hole_between_class_count()

    print("\nTest frontier count")
    test_frontier_count()

    print("\nTest distance between rv and myo")
    test_distance_rv_myo()


test()
