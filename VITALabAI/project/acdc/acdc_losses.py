"""
acdclosses.py
authot: Clement ZOTTI (clement.zotti@usherbrooke.ca)
"""
import keras.backend as K
import numpy as np
from keras import losses as kl


class ACDCLosses:
    """Losses for the ACDC project"""

    def __init__(self, nb_classes):
        self.nb_classes = nb_classes

    def l2_loss(self, y_true, y_pred):
        return K.sqrt(K.mean(K.pow(y_true - y_pred, 2), axis=-1))

    def _dice(self, y_true, y_pred):
        """ Dice score

        Args:
            y_true: keras_var, True value of the prediction.
            y_pred: keras_var, Model prediction.

        Returns:
            dice score for y_true and y_pred
        """
        flat_y_true = K.flatten(y_true)
        flat_y_pred = K.flatten(y_pred)

        # flat_y_true is a binary vector
        intersect = K.sum(flat_y_true * flat_y_pred)
        s_true = K.sum(flat_y_true)
        s_pred = K.sum(flat_y_pred)
        return (2. * intersect + 1.) / (s_true + s_pred + 1.)

    def classes_dice(self, y_true, y_pred):
        """ Inner function to compute dice on each classes.

        Args:
            y_true: keras_var, True value of the prediction.
            y_pred: keras_var, Model prediction.

        Returns:
            The sum of "dice" scores over all the classes.
        """

        weights = [0.1, 10, 10, 10]
        res = K.variable(0., name='dice_classes')

        if K.ndim(y_pred) <= 3:
            return res

        for i in range(self.nb_classes):
            res += self._dice(y_true[:, :, :, i], y_pred[:, :, :, i]) * weights[i]
        return res / np.sum(weights)

    def contour_dice(self, y_true, y_pred):
        """ Inner function to compute dice on each contour of classes.
        The contours are computed with a sobel filter.

        Args:
            y_true: keras_var, True value of the prediction.
            y_pred: keras_var, Model prediction.

        Returns:
            The sum of "dice" scores over all the classes.
        """
        sobelx = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
        sobely = np.copy(sobelx.T)

        sobelx = sobelx[:, :, np.newaxis, np.newaxis]
        sobely = sobely[:, :, np.newaxis, np.newaxis]

        k_x = K.constant(sobelx, name='sobel_x', shape=sobelx.shape,
                         dtype=np.float32)
        k_y = K.constant(sobely, name='sobel_y', shape=sobely.shape,
                         dtype=np.float32)

        # Contour loss
        loss_contour = K.variable(0., name='dice_contour')
        weights = [0, 1, 1, 1]
        for i in range(1, self.nb_classes):
            # Extract the channel i for the batch
            contour_true = y_true[:, :, :, i]
            contour_pred = y_pred[:, :, :, i]

            # Add the channel dim to process convolutions
            contour_pred = K.expand_dims(contour_pred, axis=1)
            contour_true = K.expand_dims(contour_true, axis=1)

            d_x = K.conv2d(contour_pred, k_x, padding='same')
            d_y = K.conv2d(contour_pred, k_y, padding='same')

            contour_pred = K.clip(K.abs(d_x) + K.abs(d_y), 0., 1.)
            loss_contour += self._dice(contour_true, contour_pred) * weights[i]
        return loss_contour / np.sum(weights)

    def classes_dice_loss(self, y_true, y_pred):
        """Compute the loss: 1 - dice(y_true, y_pred)"""
        return 1 - self.classes_dice(y_true, y_pred)

    def contour_dice_loss(self, y_true, y_pred):
        """Compute the loss: 1 - contour_dice(y_true, y_pred)"""
        return 1 - self.contour_dice(y_true, y_pred)

    def classes_crossentropy(self, y_true, y_pred):
        """ Inner function to compute the crossentropy on each classes.

        Args:
            y_true: keras_var, True value of the prediction.
            y_pred: keras_var, Model prediction.

        Returns:
            The crossentropy of each pixel weighted by a mask.
        """
        res = K.variable(0., name='dice_classes')

        if K.ndim(y_pred) <= 3:
            return res

        mask = K.zeros_like(y_pred[:, :, :, 0])

        weights = [0.1, 10, 10, 10]
        for i in range(self.nb_classes):
            mask += y_true[:, :, :, i] * weights[i]

        res = kl.categorical_crossentropy(y_true, y_pred)
        return res * mask

    def compute_contours(self, array):
        """ Compute the contour for each classes with a sobel filter.

        Args:
            array: array to calculate contours
        """
        sobelx = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
        sobely = np.copy(sobelx.T)

        sobelx = sobelx[:, :, np.newaxis, np.newaxis]
        sobely = sobely[:, :, np.newaxis, np.newaxis]

        k_x = K.constant(sobelx, name='sobel_x', shape=sobelx.shape,
                         dtype=np.float32)
        k_y = K.constant(sobely, name='sobel_y', shape=sobely.shape,
                         dtype=np.float32)

        contours = K.ones_like(array)
        for i in range(1, self.nb_classes):
            # Extract the channel i for the batch
            contour_pred = array[:, :, :, i]

            # Add the channel dim to process convolutions
            contour_pred = K.expand_dims(contour_pred, axis=3)
            ones = K.ones_like(contour_pred)

            d_x = K.conv2d(contour_pred, k_x, padding='same')
            d_y = K.conv2d(contour_pred, k_y, padding='same')

            contour_pred = K.clip(K.abs(d_x) + K.abs(d_y), 0., 1.)
            mults = [ones if ii != i else contour_pred for ii in range(4)]
            contour_mult = K.concatenate(mults, axis=3)
            contours *= contour_mult

        summed = contours[:, :, :, :1] - K.sum(contours[:, :, :, 1:],
                                               axis=-1,
                                               keepdims=True)
        ones = K.ones_like(summed)
        contours *= K.concatenate([summed, ones, ones, ones], axis=-1)
        contours = K.clip(contours, 0., 1, )
        return contours

    def contour_crossentropy(self, y_true, y_pred):
        """Inner function to compute the crossentropy on each contour classes.

        Args:
            y_true: keras_var, True value of the prediction.
            y_pred: keras_var, Model prediction.

        Returns:
            The crossentropy of each pixel weighted by a mask.
        """
        y_true_contours = self.compute_contours(y_true)
        y_pred_contours = self.compute_contours(y_pred)

        mask = K.zeros_like(y_pred[:, :, :, 0])

        weights = [0.1, 10, 10, 10]
        for i in range(self.nb_classes):
            mask += y_true[:, :, :, i] * weights[i]

        cross = kl.categorical_crossentropy(y_true_contours,
                                            y_pred_contours)

        return cross * mask

    def combined_loss(self, y_true, y_pred):
        c_loss = self.classes_crossentropy(y_true, y_pred)
        d_loss = 1 - self.classes_dice(y_true, y_pred)

        return c_loss + d_loss

    def dice_on_rv(self, y_true, y_pred):
        """ Inner function to compute dice on the RV class.

        Args:
            y_true: keras_var, True value of the prediction.
            y_pred: keras_var, Model prediction.

        Returns:
            The "dice" score over the RV class.
        """

        res = K.variable(0., name='dice_classes')
        if K.ndim(y_pred) <= 3:
            return res
        res = self._dice(y_true[:, :, :, 1], y_pred[:, :, :, 1])

        return res

    def dice_on_myo(self, y_true, y_pred):
        """ Inner function to compute dice on the MYO class.

        Args:
            y_true: keras_var, True value of the prediction.
            y_pred: keras_var, Model prediction.

        Returns:
            The "dice" score over the MYO class.
        """

        res = K.variable(0., name='dice_classes')
        if K.ndim(y_pred) <= 3:
            return res
        res = self._dice(y_true[:, :, :, 2], y_pred[:, :, :, 2])

        return res

    def dice_on_lv(self, y_true, y_pred):
        """ Inner function to compute dice on LV classes.

        Args:
            y_true: keras_var, True value of the prediction.
            y_pred: keras_var, Model prediction.

        Returns:
            The "dice" score over the LV class.
        """

        res = K.variable(0., name='dice_classes')
        if K.ndim(y_pred) <= 3:
            return res
        res = self._dice(y_true[:, :, :, 3], y_pred[:, :, :, 3])

        return res

    def vae_log_prob_expectation_loss(self, y_true, y_pred):
        """ Expectation term in the VAE loss

        Args:
            y_true: (keras_var), True value of the prediction.
            y_pred: (keras_var), Model prediction.

        Returns:
            VAE expectation term loss

        """
        return K.sum(kl.categorical_crossentropy(y_true, y_pred), axis=(1, 2))

    def KL_loss(self, dummy, latent_space_distribution):
        """ KL divergence term to train VAE

        Args:
            dummy: a dummy variable to work with Keras
            latent_space_distribution: a (n, nb_latent_dims, 2) tensor where
                latent_space_distribution[i,:,0] is the mean
                latent_space_distribution[i,:,1] is the log variance
                of the distribution associated to the sample i in the latent space

        Returns:
            VAE KL divergence term

        """

        mean, log_var = latent_space_distribution[:, :, 0], latent_space_distribution[:, :, 1]
        return 0.5 * K.sum(K.exp(log_var) + K.square(mean) - log_var - 1, axis=1)

    def KL_loss_two_normals(self, target_latent_distribution, latent_distribution):
        """ KL divergence between two multivariate normals with diagonal covariance matrices.

        The KL divergence is not symmetric and although confusing,
        this compute KL(latent_distribution || target_latent_distribution).
        For explanations look at
        https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence#Multivariate_normal_distributions

        Args:
            target_latent_distribution: a (n, nb_latent_dims, 2) tensor of target distribution
                target_latent_distribution[i,:,0] is the mean
                target_latent_distribution[i,:,1] is the log variance
            latent_distribution: a (n, nb_latent_dims, 2) tensor of prediction distribution
                latent_distribution[i,:,0] is the mean
                latent_distribution[i,:,1] is the log variance

        Returns:
            KL divergence between two normals
        """
        mean0 = latent_distribution[:, :, 0]
        log_var0 = latent_distribution[:, :, 1]
        mean1 = target_latent_distribution[:, :, 0]
        log_var1 = target_latent_distribution[:, :, 1]

        return K.mean(0.5 * K.sum(K.exp(log_var0 - log_var1) +
                                  K.exp(-log_var1) * K.square(mean1 - mean0) +
                                  log_var1 - log_var0 - 1, axis=1))

    def tensor_similarity(self, y_true, y_pred):
        return 1 - K.mean(K.abs(y_true - y_pred) / (K.abs(y_true) + K.abs(y_pred) + K.epsilon()))

    def dummy_loss(self, y_true, y_pred):
        """  Loss to use when the output is used only to compute metrics
        """
        return K.zeros_like(y_pred)
