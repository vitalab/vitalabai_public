import numpy as np

from keras import Model
from keras.engine.topology import Layer
from keras.layers import Conv2D, Dense, Flatten, Input, UpSampling2D, Reshape
from keras.regularizers import l2
from keras import backend as K


class VAESampling(Layer):
    """ Custom Keras layer use to sample for a latent distribution
    """
    def __init__(self, **kwargs):
        super(VAESampling, self).__init__(**kwargs)

    def build(self, input_shape):
        pass

    def call(self, latent_distribution):
        mean, log_var = latent_distribution[:, :, 0], latent_distribution[:, :, 1]
        noise = K.random_normal(shape=K.shape(mean))
        return mean + K.exp(0.5 * log_var) * noise

    def compute_output_shape(self, input_shape):
        return input_shape[0:2]


class VAELatentMean(Layer):
    """ Custom Keras layer use to retrieve the mean of a latent distribution
    """
    def __init__(self, **kwargs):
        super(VAELatentMean, self).__init__(**kwargs)

    def build(self, input_shape):
        pass

    def call(self, latent_distribution):
        return latent_distribution[:, :, 0]

    def compute_output_shape(self, input_shape):
        return input_shape[0:2]


class VAELatentLogVar(Layer):
    """ Custom Keras layer use to retrieve the log variance of a latent distribution
    """
    def __init__(self, **kwargs):
        super(VAELatentLogVar, self).__init__(**kwargs)

    def build(self, input_shape):
        pass

    def call(self, latent_distribution):
        return latent_distribution[:, :, 1]

    def compute_output_shape(self, input_shape):
        return input_shape[0:2]


def vae_encoder(image_size, input_channels, nb_feature_maps, code_length, name="vae_encoder"):
    """ Block making up the encoder half of a convolutional variational autoencoder.

    Args:
        image_size: (tuple), Tuple of the size of the input segmentation groundtruth on each axis.
        input_channels: (input_channels), The number of channels of the input.
        nb_feature_maps: (int), Factor used to compute the number of feature maps for the convolution layers.
        code_length: (int), The dimensionality of the latent space of the autoencoder.
        name: (str), The name to give to the encoder model.

    Returns:
        (Keras model) encoder model

    """

    input_image = Input(shape=(image_size[0], image_size[1], input_channels), name='input_image')

    conv1 = Conv2D(nb_feature_maps, (3, 3), activation='elu', strides=(2, 2), kernel_initializer='glorot_normal',
                   name='layer1')(input_image)
    conv2 = Conv2D(nb_feature_maps, (3, 3), activation='elu', kernel_initializer='glorot_normal', name='layer2')(conv1)
    conv3 = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', strides=(2, 2), kernel_initializer='glorot_normal',
                   name='layer3')(conv2)
    conv4 = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', kernel_initializer='glorot_normal', name='layer4')(
        conv3)
    conv5 = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu', strides=(2, 2), kernel_initializer='glorot_normal',
                   name='layer5')(conv4)
    conv6 = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu', kernel_initializer='glorot_normal', name='layer6')(
        conv5)
    conv7 = Conv2D(nb_feature_maps * 8, (3, 3), activation='elu', strides=(2, 2), kernel_initializer='glorot_normal',
                   name='layer7')(conv6)
    conv8 = Conv2D(nb_feature_maps * 8, (3, 3), activation='elu', kernel_initializer='glorot_normal', name='layer8')(
        conv7)
    conv9 = Conv2D(max(input_channels * 2, 8), (3, 3), activation='elu', strides=(2, 2),
                   kernel_initializer='glorot_normal', name='layer9')(conv8)

    flat = Flatten(name='flat')(conv9)
    dense = Dense(2 * code_length, name="dense")(flat)
    latent_distribution = Reshape((code_length, 2), name="latent_distribution")(dense)

    encoder = Model(name=name, inputs=input_image, outputs=latent_distribution)
    return encoder


def vae_decoder(image_size, output_channels, nb_feature_maps, code_length, name="vae_decoder"):
    """ Block making up the decoder half of a convolutional variational autoencoder.

    Args:
        image_size: (tuple), Tuple of the size of the output segmentation groundtruth on each axis.
        output_channels: (int), The number of channels to output.
        nb_feature_maps: (int), Factor used to compute the number of feature maps for the convolution layers.
        code_length: (int), The dimensionality of the latent space of the autoencoder.
        name: (str), The name to give to the decoder model.

    Returns:
        (Keras model) decoder model
    """

    input_encode = Input(shape=(code_length,), name='input_latent_space')

    size_before_up = np.int_(image_size[0] / 16)
    dense = Dense(size_before_up * size_before_up * output_channels, activation='elu', name='layer1')(input_encode)

    reshape = Reshape((size_before_up, size_before_up, output_channels), name='layer2')(dense)

    conv1 = Conv2D(nb_feature_maps * 8, (3, 3), activation='elu', padding='same', kernel_initializer='glorot_normal',
                   name='layer3')(reshape)
    conv2 = Conv2D(nb_feature_maps * 8, (3, 3), activation='elu', padding='same', kernel_initializer='glorot_normal',
                   name='layer4')(conv1)

    up1 = UpSampling2D(size=(2, 2), name='layer5')(conv2)

    conv3 = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu', padding='same', kernel_initializer='glorot_normal',
                   name='layer6')(up1)
    conv4 = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu', padding='same', kernel_initializer='glorot_normal',
                   name='layer7')(conv3)

    up3 = UpSampling2D(size=(2, 2), name='layer8')(conv4)

    conv5 = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', padding='same', kernel_initializer='glorot_normal',
                   name='layer9')(up3)
    conv6 = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', padding='same', kernel_initializer='glorot_normal',
                   name='layer10')(conv5)

    up4 = UpSampling2D(size=(2, 2), name='layer11')(conv6)

    conv7 = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', padding='same', kernel_initializer='glorot_normal',
                   name='layer12')(up4)
    conv8 = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', padding='same', kernel_initializer='glorot_normal',
                   name='layer13')(conv7)

    up5 = UpSampling2D(size=(2, 2), name='laye14')(conv8)

    decode = Conv2D(output_channels, (3, 3), activation='softmax', padding='same', name='output_image')(up5)

    decoder = Model(name=name, inputs=input_encode, outputs=decode)

    return decoder


def cae_encoder(image_size, input_channels, nb_feature_maps, code_length, name="cae_encoder"):
    """ Block making up the encoder half of a convolutional autoencoder.

    Args:
        image_size: (tuple), Tuple of the size of the input segmentation groundtruth on each axis.
        input_channels: (input_channels), The number of channels of the input.
        nb_feature_maps: (int), Factor used to compute the number of feature maps for the convolution layers.
        code_length: (int), The dimensionality of the latent space of the autoencoder.
        name: (str), The name to give to the encoder model.

    Returns:
        (Keras model) encoder model

    """

    input_image = Input(shape=(image_size[0], image_size[1], input_channels),
                        name='input_image')

    conv1 = Conv2D(nb_feature_maps, (3, 3), activation='elu', strides=(2, 2),
                   kernel_initializer='glorot_normal', name='layer1')(input_image)
    conv2 = Conv2D(nb_feature_maps, (3, 3), activation='elu',
                   kernel_initializer='glorot_normal', name='layer2')(conv1)
    conv3 = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', strides=(2, 2),
                   kernel_initializer='glorot_normal', name='layer3')(conv2)
    conv4 = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu',
                   kernel_initializer='glorot_normal', name='layer4')(conv3)
    conv5 = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu', strides=(2, 2),
                   kernel_initializer='glorot_normal', name='layer5')(conv4)
    conv6 = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu',
                   kernel_initializer='glorot_normal', name='layer6')(conv5)
    conv7 = Conv2D(max(input_channels, 4), (3, 3), activation='elu', strides=(2, 2),
                   kernel_initializer='glorot_normal', name='layer7')(conv6)
    flat = Flatten(name='flat')(conv7)
    latent_space = Dense(code_length, activation='elu', activity_regularizer=l2(0.0001),
                         name='output_latent_space')(flat)

    encoder = Model(name=name, inputs=input_image, outputs=latent_space)
    return encoder


def cae_decoder(image_size, output_channels, nb_feature_maps, code_length, name="cae_decoder"):
    """ Block making up the decoder half of a convolutional autoencoder.

    Args:
        image_size: (tuple), Tuple of the size of the output segmentation groundtruth on each axis.
        output_channels: (int), The number of channels to output.
        nb_feature_maps: (int), Factor used to compute the number of feature maps for the convolution layers.
        code_length: (int), The dimensionality of the latent space of the autoencoder.
        name: (str), The name to give to the decoder model.

    Returns:
        (Keras model) decoder model
    """

    input_encode = Input(shape=(code_length,), name='input_latent_space')

    size_before_up = np.int_(image_size[0] / 16)
    dense = Dense(size_before_up * size_before_up * output_channels, activation='elu',
                  name='layer1')(input_encode)
    reshape = Reshape((size_before_up, size_before_up, output_channels), name='layer2')(dense)

    conv1 = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu', padding='same',
                   kernel_initializer='glorot_normal', name='layer3')(reshape)
    conv2 = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu', padding='same',
                   kernel_initializer='glorot_normal', name='layer4')(conv1)
    up1 = UpSampling2D(size=(2, 2), name='layer5')(conv2)

    conv3 = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', padding='same',
                   kernel_initializer='glorot_normal', name='layer6')(up1)
    up2 = UpSampling2D(size=(2, 2), name='layer7')(conv3)

    conv4 = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', padding='same',
                   kernel_initializer='glorot_normal', name='layer8')(up2)
    up3 = UpSampling2D(size=(2, 2), name='layer9')(conv4)

    conv5 = Conv2D(nb_feature_maps, (3, 3), activation='elu', padding='same',
                   kernel_initializer='glorot_normal', name='layer10')(up3)
    conv6 = Conv2D(nb_feature_maps, (3, 3), activation='elu', padding='same',
                   kernel_initializer='glorot_normal', name='layer11')(conv5)
    up4 = UpSampling2D(size=(2, 2), name='layer12')(conv6)

    decode = Conv2D(output_channels, (3, 3), activation='softmax', padding='same', name='output_image')(up4)

    decoder = Model(name=name, inputs=input_encode, outputs=decode)
    return decoder
