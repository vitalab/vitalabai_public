from keras.engine.saving import load_model
from keras.optimizers import Adam
from tqdm import tqdm

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.dataset.semanticsegmentation.acdc.augmentation_transformer import AugmentationTransformer
from VITALabAI.dataset.semanticsegmentation.acdc.postprocess import (
    Post3dBigBlob,
)
from VITALabAI.dataset.semanticsegmentation.acdc.preprocess import (
    PreProcessSTD,
    PreQuantilePercent
)
from VITALabAI.model.semanticsegmentation.convdeconv import VITALConvDeconv
from VITALabAI.model.semanticsegmentation.losses import dice_crossentropy_loss
from VITALabAI.project.acdc.acdc_base import ACDCBase


class ACDCConvDeconv(ACDCBase):
    """Unet model for the ACDC project. """

    def __init__(self, dataset: VITALabAiDatasetAbstract, nb_feature_maps=32, l2reg=1e-4,
                 pretrained_model=None, loss_fn=None, optimizer=None, metrics=None, name=None, display=False):
        self.l2reg = l2reg
        super().__init__(dataset, loss_fn, optimizer, metrics, nb_feature_maps=nb_feature_maps,
                         pretrained_model=pretrained_model, name=name, display=display)
        self.postproc = (Post3dBigBlob(),)

    def build_model(self):
        if self.pretrained_model:
            return load_model(self.pretrained_model)
        else:
            return VITALConvDeconv(self.dataset, alpha=self.l2reg,
                                   nb_feature_maps=self.nb_feature_maps).get_model()

    def losses(self):
        return [
            self.acdclosses.classes_crossentropy,
        ]

    def metrics(self):
        """ Metrics used during the training. """
        return [
            self.acdclosses.classes_dice,
            self.acdclosses.dice_on_rv,
            self.acdclosses.dice_on_myo,
            self.acdclosses.dice_on_lv
        ]

    def predict_and_save(self, sequence, f5h):
        """ Function to predict and save results f

        Args:
            sequence: Keras.Sequence, sequence used to generate predictions
            f5h: h5py file to save predictions in
        """

        for i in tqdm(range(len(sequence))):
            data = sequence[i]
            if len(data) == 5:
                name, img, gt, prior, v_size = data
            else:
                name, img, gt, v_size = data

            if not name.endswith("_0"):
                continue  # Don't use augmented data

            pred = self.model.predict_on_batch(img)
            group = f5h.create_group(name)
            self.save_into_h5f(group, img, gt, pred, v_size)

    @staticmethod
    def get_preprocessing():
        return PreProcessSTD(), PreQuantilePercent(),


if __name__ == '__main__':
    from VITALabAI.dataset.semanticsegmentation.acdc.acdc import VITALAcdc
    import argparse

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the hdf5 dataset", type=str, required=True)
    aparser.add_argument("--pretrained_model", type=str, help="Model h5 file", default=None)
    aparser.add_argument("--nb_epochs", type=int, help="Number of epoch to train on the dataset",
                         default=100)
    aparser.add_argument("--batch_size", type=int, help="choose the size of the batch feeded to the network.",
                         default=32)
    aparser.add_argument("--nb_feature_maps", type=int, help="Number of features maps used on the first "
                                                             "layer", default=32)
    aparser.add_argument("--name", type=str, dest="name", help="Model path", default=None)
    aparser.add_argument("--display", dest="display", action="store_true",
                         help="Display results during training")
    aparser.add_argument("--testonly", action='store_true', dest="testonly",
                         help="Skip training and do test phase")
    aparser.add_argument("--lr", type=float, dest="lr", help="learning rate", default=3e-4)
    aparser.add_argument("--no_da", dest="use_da", action='store_false', help="Disable data augmentation")

    args = aparser.parse_args()

    augmentation_transformer = None
    if args.use_da:
        augmentation_transformer = AugmentationTransformer(rotation_range=60, width_shift_range=0,
                                                           height_shift_range=0, zoom_range=0.1,
                                                           dilation_range=2)
    ds = VITALAcdc(path=args.path, batch_size=args.batch_size,
                   augmentation_transformer=augmentation_transformer,
                   preproc_fn=ACDCConvDeconv.get_preprocessing())

    optim = Adam(lr=args.lr, beta_1=0.9, beta_2=0.99)
    loss = dice_crossentropy_loss
    model = ACDCConvDeconv(ds, optimizer=optim, name=args.name, display=args.display, loss_fn=loss,
                           pretrained_model=args.pretrained_model,
                           nb_feature_maps=args.nb_feature_maps)

    if not args.testonly:
        callbacks = model.get_callbacks(learning_rate=args.lr)
        model.train(epochs=args.nb_epochs, callbacks=callbacks, workers=1)
        model.save()

    model.evaluate()
    model.export_results()
