import argparse
import os.path
from math import inf

import h5py
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.manifold import TSNE

from VITALabAI.project.acdc.visualizations.utils import load_latent_points


def load_slice(predictions_path, patient_id, stage, slice):
    """ Load the a slice ground truth and prediction for a prediction file

    Args:
        predictions_path: (str) a path to a prediction file (TEST_PREDICTION, VALID_PREDICTION or TRAIN_PREDICTION)
        patient_id: (int) the id of the target patient
        stage: (str) the stage (ES, ED) in which the patient heart is
        slice: (int) the index of the slice

    Returns:
        gt (numpy array): the ground truth segmentation of the slice
        pred (numpy array): the predicted segmentation of the slice
    """
    with h5py.File(predictions_path, 'r') as file:
        patient = file["patient{0:0=3d}_{1:}_0".format(patient_id, stage)]
        gt = patient["gt_m"][slice]
        pred = np.squeeze(patient["pred_post"][slice])
        return gt, pred


def make_iteractive_tsne_plot(data, predictions_path):
    """ Produce an interactive tsne plot using Matplotlib

    Args:
        data: a panda dataframe containing the following variables for each slice:
            (patient_id, stage, slice, z0, z1, ... zn, t0, t1, dice_rv, dice_myo, dice_lv, dice_all)
            where   - t0 and t1 are the tsne variables
                    - the zi are the latent space variables

        predictions_path: a path to a prediction file (TEST_PREDICTION, VALID_PREDICTION or TRAIN_PREDICTION)

    Returns:
        None
    """
    print(data.keys())

    fig = plt.figure()

    # Split dataframe in two according to cardiac stage.
    # This is to use two markers type and still be able to hover points.
    data_dict = {stage: df for stage, df in data.groupby('stage')}
    data_ES = data_dict["ES"]
    data_ED = data_dict["ED"]

    # Find the min and max value of the variable used for color
    # This is to use the same color range for both plots (ES and ED).
    color_var = "dice_all"
    color = data[color_var]
    color_min, color_max = color.min(), color.max()

    ax = plt.subplot(121)
    plt.scatter(data_ES.t0, data_ES.t1, c=data_ES[color_var].values, s=25,
                cmap="Greens", edgecolors='black', marker="d")
    plt.clim(color_min, color_max)
    plt.scatter(data_ED.t0, data_ED.t1, c=data_ED[color_var].values, s=35,
                cmap="Greens", edgecolors='black', marker="o")
    arrow = plt.annotate("", (-10, -10), (10, 10), arrowprops={"arrowstyle": "->", "color": "red"})
    arrow.set_visible(False)
    plt.clim(color_min, color_max)
    plt.colorbar()

    gt, pred = load_slice(predictions_path, data.patient_id[15], data.stage[15], data.slice[15])
    plt.subplot(222)
    gt_title = plt.title("Ground truth")
    gt_display = plt.imshow(gt, vmin=0, vmax=3)
    plt.subplot(224)
    pred_title = plt.title("Prediction")
    pred_display = plt.imshow(pred, vmin=0, vmax=3)

    def hover(event):
        if event.inaxes == ax:
            xmin, xmax = ax.get_xlim()
            ymin, ymax = ax.get_ylim()
            points_in_lims = data.loc[(xmin < data["t0"]) & (data["t0"] < xmax) &
                                      (ymin < data["t1"]) & (data["t1"] < ymax)]

            if not points_in_lims.empty:
                closest_point_index = np.argmin((points_in_lims["t0"].values - event.xdata) ** 2 +
                                                (points_in_lims["t1"].values - event.ydata) ** 2)
                closest_point = points_in_lims.iloc[closest_point_index]

                arrow.xyann = (event.xdata, event.ydata)
                arrow.xy = (closest_point.t0, closest_point.t1)
                arrow.set_visible(True)

                patient_infos = (closest_point.patient_id, closest_point.stage, closest_point.slice)
                gt_title.set_text("Ground truth patient {} at {} on slice {}".format(*patient_infos))
                pred_title.set_text("Prediction   patient {} at {} on slice {}".format(*patient_infos))
                gt, pred = load_slice(predictions_path, *patient_infos)
                gt_display.set_data(gt)
                pred_display.set_data(pred)

                plt.draw()
            else:
                arrow.set_visible(False)

    fig.canvas.mpl_connect("motion_notify_event", hover)

    plt.show()


def tsne_with_parameter_selection(latent_points):
    """ An attempt to select the best tsne representation by minizing its KL divergence in a simple loop

    Args:
        latent_points: (numpy array) the latent points on which to perform tsne

    Returns:
        optimal_embedding: (numpy array) the latent points projection in the tsne space, of shape (n, 2)
        optimal_perplexity: (float):
        optimal_learning_rate: (float):
    """
    # selects best tsne visualization minimizing kl-divergence
    min_kl_divergence = inf
    optimal_embedding = np.zeros((latent_points.shape[0], 2))
    optimal_perplexity = 0
    optimal_learning_rate = 0
    for learning_rate in [10, 100]:
        for perplexity in range(5, 50, 5):
            tsne = TSNE(n_components=2, perplexity=perplexity, learning_rate=learning_rate,
                        n_iter_without_progress=600)
            embedded_data = tsne.fit_transform(latent_points)
            print("learning_rate", learning_rate, "perplexity", perplexity, "kl_divergence",
                  tsne.kl_divergence_)
            if tsne.kl_divergence_ < min_kl_divergence:
                min_kl_divergence = tsne.kl_divergence_
                optimal_embedding = embedded_data
                optimal_learning_rate = learning_rate
                optimal_perplexity = perplexity
    return optimal_embedding, optimal_perplexity, optimal_learning_rate


def main(predictions_path, tsne_data_cache=""):
    """ Perform a tsne projection and produce a interactive tsne plot on the predictions in a prediction file

    Args:
        predictions_path: (str) a path to a prediction file (TEST_PREDICTION, VALID_PREDICTION or TRAIN_PREDICTION)

        tsne_data_cache: (str, optional) a filename in which to save the tsne projection in the working directory.
            A full path name would probably work as well. This is useful to avoid recomputing tsne every time you
            rerun the script.

    Returns:
        None
    """
    if tsne_data_cache and os.path.isfile(tsne_data_cache):
        data = pd.read_pickle(tsne_data_cache)
    else:
        data, latent_labels = load_latent_points(predictions_path)
        latent_points = data[latent_labels].values
        embedded_data, _, _, = tsne_with_parameter_selection(latent_points)

        tsne_labels = ["t0", "t1"]
        tsne_variables = pd.DataFrame(
            data=embedded_data,
            columns=tsne_labels
        )
        data = pd.concat((data, tsne_variables), axis=1)

        if tsne_data_cache and not os.path.isfile(tsne_data_cache):
            data.to_pickle(tsne_data_cache)

    make_iteractive_tsne_plot(data, predictions_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Perform a tsne projection and produce a interactive tsne "
                                                 "plot")
    parser.add_argument("--prediction_path", required=True,
                        help="The target prediction file path ({TEST,VALID," "TRAIN}_PREDICTION)")

    parser.add_argument("--tsne_cache", nargs='?', default="",
                        help="A filename or full path to make a cache of the tsne projection. " +
                             "Useful to rerun the script.")
    args = parser.parse_args()
    main(args.prediction_path, args.tsne_cache)
