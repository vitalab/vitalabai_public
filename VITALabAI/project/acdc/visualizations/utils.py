import os

import h5py
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d

from VITALabAI.dataset.acdc.postprocess import (
    Post3dBigBlob,
)
from VITALabAI.dataset.acdc.utils import save_nii_file
from VITALabAI.utils.dataset import centered_resize


def to_array(dataset):
    """read array from hdf5 to np array

    Args:
        dataset: array data written in hdf5

    Returns:

    """
    array = np.zeros(dataset.shape)
    dataset.read_direct(array)
    return array


def format_output_name(name, path):
    """output file name formatting

    Args:
        name: name of output file
        path: directory of output file

    Returns:
        <path>/<name>

    """
    if name[0] == '/':
        name = name[1:]
    if not os.path.isdir(path):
        os.makedirs(path)
    return os.path.join(path, name)


def interpolate(latent_space, nb_samples):
    """Apply linear interpolation to input patient slices

    Args:
        latent_space: array of n-dimensional data in z-space
        nb_samples: number of sample

    Returns: array of all original points and additional interpolated points between original points

    """
    n = latent_space.shape[0]
    f = interp1d(np.arange(n), latent_space, axis=0)
    return np.apply_along_axis(f, 0, np.append(np.arange(0, n - 1, 1 / nb_samples), n - 1))


def save_nifti(output_name, predicted, gt_dim, voxel_size, output_folder='NIFTI'):
    """save autoencoder prediction as nifti for each slice

    Args:
        output_name: name of output file
        predicted: decoded image from interpolated data of latent space
        gt_dim: dimension of corresponding ground truth
        voxel_size: size of voxel of original acdc image
        output_folder: string, name of the output folder

    Returns:

    """
    pred = centered_resize(predicted, gt_dim).argmax(axis=-1)
    f = Post3dBigBlob()
    pred = f(pred)[:].squeeze()
    if len(pred.shape) < 3:
        pred = pred[np.newaxis]
    pred = pred.transpose((1, 2, 0))
    save_nii_file(pred,
                  "{}.nii.gz".format(format_output_name(output_name, output_folder)),
                  zoom=voxel_size)


def save_array(name, predicted, nb_samples):
    """ save interpolated data of latent space as .npz

        Args:
            name: name of patient
            predicted: decoded image from interpolated data of latent space
            nb_samples: number of sample

        Returns:

        """
    np.savez(format_output_name(name, 'INTERPOLATED_ARRAY'),
             array=predicted,
             nb_sample=nb_samples)


def compute_dices(gt, pred):
    """ Yet another definition of the dices...

    Strangely enough, neither the one from scores.py or acdclosses.py works here.
    The first compute the dices for the whole 3D volume and
    the second computes the dices using tensors (not numpy arrays).

    Args:
        gt: (numpy array) the ground truth segmentations, as shape (n, image_size[0], image_size[1])
        pred: (numpy array) the predicted segmentations, as shape (n, image_size[0], image_size[1])

    Returns:
        the dices for the different classes, a numpy array of shape (n, 4)
        the 4 columns are respectively rv, myo, lv, and global (mean) dice.
    """

    classes = np.arange(1, 4)[None, None, None, :]
    gt_classes = gt[:, :, :, None] == classes
    pred_classes = pred[:, :, :, None] == classes

    intersection_sizes = np.count_nonzero(1 * np.logical_and(gt_classes, pred_classes), axis=(1, 2))
    gt_sizes = np.count_nonzero(gt_classes, axis=(1, 2))
    pred_sizes = np.count_nonzero(pred_classes, axis=(1, 2))

    dices = (2. * intersection_sizes + 1.) / (gt_sizes + pred_sizes + 1.)
    dices = np.hstack((dices, dices.mean(axis=1, keepdims=True)))
    return dices


def load_latent_points(predictions_path):
    """ Load the latent points in a prediction file as a panda dataframe

    Args:
        predictions_path: (str) a path to a prediction file (TEST_PREDICTION, VALID_PREDICTION or TRAIN_PREDICTION)

    Returns:
        A panda dataframe containing the following variables for each slice:
        patient_id, stage, slice, z0, z1, ... zn, dice_rv, dice_myo, dice_lv, dice_all
        where the zi are the latent variables
    """
    with h5py.File(predictions_path, 'r') as file:
        patient_ids = []
        cardiac_cycle_stages = []
        imr_slices = []
        classes_dices = []
        latent_points = []
        for group in file.values():
            points = group["latent_space_mean"].value
            nb_slices = points.shape[0]
            slices = list(range(0, nb_slices))

            id, stage, _ = group.name.split('_')
            id = int(id.replace('/patient', ''))
            gts = group["gt_m"].value
            preds = np.squeeze(group["pred_post"].value)
            dices = compute_dices(gts, preds)

            patient_ids += [id] * nb_slices
            cardiac_cycle_stages += [stage] * nb_slices
            imr_slices += slices
            classes_dices.append(dices)
            latent_points.append(points)

        categories = pd.DataFrame({
            "patient_id": patient_ids,
            "stage": cardiac_cycle_stages,
            "slice": imr_slices,
        })

        classes_dices = np.vstack(classes_dices)
        dices_variables = pd.DataFrame(
            data=classes_dices,
            columns=["dice_rv", "dice_myo", "dice_lv", "dice_all"]
        )

        latent_points = np.vstack(latent_points)
        latent_variables_labels = ["z" + str(i) for i in range(latent_points.shape[1])]
        latent_variables = pd.DataFrame(
            data=latent_points,
            columns=latent_variables_labels
        )

        data = pd.concat((categories, dices_variables, latent_variables), axis=1)

        return data, latent_variables_labels
