import argparse
import random

import h5py
import numpy as np
from keras.models import load_model
from scipy.spatial.distance import pdist, squareform

from VITALabAI.model.acdc.layers_utils import VAESampling
from VITALabAI.project.acdc.visualizations.utils import to_array, \
    interpolate, \
    save_array, \
    save_nifti


class InterpolationSuperResolutionGenerator:
    """ Class used to generate super resolution for one heart
    """

    def __init__(self,
                 path,
                 model,
                 pretrained_model,
                 selection_mode,
                 nb_samples,
                 save_array):
        """ Creates Interpolation helper specific to ACDC database

        Args:
            path: path of dataset output from encoder/decoder NN
            model: type of autoencoder model {cae, vae}
            pretrained_model: path of vae or cae model with valid decoder layer
            selection_mode: slice selection for interpolation
            {farthest, nearest, random, None for interpolation between all slices}
            nb_samples: number of points between slices of a given patient data
            save_array: whether to save the interpolation as npz
        """
        self.path = path
        self.nb_samples = nb_samples
        if model == 'cae' and pretrained_model is not None:
            self.decoder = pretrained_model.get_layer("cae_decoder")
            self.latent_space_field_name = 'latent_space'
        elif model == 'vae' and pretrained_model is not None:
            self.decoder = pretrained_model.get_layer("vae_decoder")
            self.latent_space_field_name = 'latent_space_mean'

        if save_array:
            if selection_mode is None:
                self.generate_hyper_resolution = self.generate_interpolated_array_between_all_slices
            else:
                self.generate_hyper_resolution = self.generate_interpolated_array_between_2_slices
        else:
            if selection_mode is None:
                self.generate_hyper_resolution = self.generate_hyper_resolution_between_all_slices
            else:
                self.generate_hyper_resolution = self.generate_hyper_resolution_between_2_slices

        if selection_mode == 'farthest':
            self.select = self.find_farthest_slices
        elif selection_mode == 'nearest':
            self.select = self.find_nearest_slices
        elif selection_mode == 'random':
            self.select = self.select_random_slices

    def generate_hyper_resolution_between_2_slices(self, group):
        """apply prediction to given data of latent space and save as nifti

        Args:
            group: data of given patient
        """
        selected_slices = self.select(to_array(group[self.latent_space_field_name]))
        interp = interpolate(selected_slices, self.nb_samples)
        predicted = self.decoder.predict_on_batch(interp)
        print(group.get('gt_m').shape[1:3])
        save_nifti(group.name,
                   predicted,
                   group['gt_m'].shape[1:3],
                   np.multiply(group.attrs.get(['voxel_size'], [1, 1, 1]), [1, 1, 1 / self.nb_samples]),
                   output_folder='super_res_nifti')

    def generate_hyper_resolution_between_all_slices(self, group):
        """apply prediction to given data of latent space and save as nifti

        Args:
            group: the hdf5 group of the patient
        """
        interp = interpolate(to_array(group.get(self.latent_space_field_name)), self.nb_samples)
        predicted = self.decoder.predict_on_batch(interp)
        save_nifti(group.name,
                   predicted,
                   group['gt_m'].shape[1:3],
                   np.multiply(group.attrs.get('voxel_size', [1, 1, 1]), [1, 1, 1 / self.nb_samples]),
                   output_folder='super_res_nifti')

    def generate_interpolated_array_between_2_slices(self, group):
        """apply interpolation to get hyper resolution and save array.

        Args:
            group: the hdf5 group of the patient
        """
        selected_slices = self.select(to_array(group[self.latent_space_field_name]))
        interp = interpolate(selected_slices, self.nb_samples)
        save_array(group.name, interp, self.nb_samples)

    def generate_interpolated_array_between_all_slices(self, group):
        """apply interpolation to get hyper resolution and save array.

        Args:
            group: the hdf5 group of the patient
        """
        interp = interpolate(to_array(group[self.latent_space_field_name]), self.nb_samples)
        save_array(group.name, interp, self.nb_samples)

    def apply_to_all_patient(self):
        """apply interpolation to all patients in given hdf5 file.
        """
        [self.generate_hyper_resolution(group) for group in h5py.File(self.path, 'r').values()]

    def apply_to_patient(self, patient_id):
        """apply interpolation to a patient in given hdf5 file

        Args:
            patient_id: name of patient in hdf5 file

        # Raises:
            ValueError if patient_id is not a str
        """
        if isinstance(patient_id, str):
            self.generate_hyper_resolution(h5py.File(self.path, 'r')[patient_id])
        else:
            raise ValueError("Value %s for parameter 'data' is invalid." % patient_id)

    def find_farthest_slices(self, latent_space):
        """Find 2 nearest slices of patient

        Args:
            latent_space: array of n-dimensional data in z-space

        Returns: Slices with greatest euclidian distance as array

        """
        index_max = np.argmax(squareform(pdist(latent_space)))
        i = index_max % latent_space.shape[0]
        j = index_max // latent_space.shape[0]
        return np.vstack((latent_space[i, :], latent_space[j, :]))

    def find_nearest_slices(self, latent_space):
        """ Find 2 slices of patient with largest euclidian distance

        Args:
            latent_space: array of n-dimensional data in z-space

        Returns: Nearest slices as array

        """
        distance_matrix = squareform(pdist(latent_space))
        i, j = np.where(distance_matrix == np.min(distance_matrix[np.nonzero(distance_matrix)]))
        return np.vstack(latent_space[i[0], :], latent_space[i[1], :])

    def select_random_slices(self, latent_space):
        """Select 2 random slices of patient

        Args:
            latent_space: array of n-dimensional data in z-space

        Returns: 2 random slices as array
        """
        index = random.sample(range(latent_space.shape[0]), 2)
        return np.vstack((latent_space[index[0], :], latent_space[index[1], :]))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Util for interpolation of ACDC data living in latent space')
    parser.add_argument('--model', type=str, required=True,
                        help='model type', choices=['vae', 'cae'])
    parser.add_argument('--path', type=str, required=True,
                        help='The path of hdf5 file containing results of autoencoder on ACDC dataset')
    parser.add_argument('--pretrained', type=str, required=True,
                        help='cae/vae model path for decoder layer')
    parser.add_argument('--nb_samples', type=int, nargs='?', default=1,
                        help='number of points between slices of a given patient data')
    parser.add_argument('--selection', type=str, nargs='?', default=None,
                        help='type of interpolation',
                        choices=['farthest', 'nearest', 'random'])
    parser.add_argument('--name', type=str,
                        help='patient name if we want to apply interpolation to specific patient data')

    parser.add_argument('--array', action='store_true',
                        help='flag to save hyper resolution of latent space as array')
    args = parser.parse_args()
    interpolator = InterpolationSuperResolutionGenerator(model=args.model,
                                                         path=args.path,
                                                         selection_mode=args.selection,
                                                         nb_samples=args.nb_samples,
                                                         save_array=args.array,
                                                         pretrained_model=load_model(args.pretrained, {
                                                             "VAESampling": VAESampling}, compile=False)
                                                         )
    if len(args.name) > 0:
        interpolator.apply_to_patient(args.name)
    else:
        interpolator.apply_to_all_patient()
