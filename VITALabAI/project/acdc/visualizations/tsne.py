import argparse
import csv
import os
from math import inf
from re import findall

import h5py
import matplotlib.pyplot as plt
import pandas as pd
from numpy import max, min, linspace, tile, arange, vstack, load, zeros, append, asarray
from numpy.random import normal
from sklearn.manifold import TSNE

from VITALabAI.project.acdc.visualizations.utils import to_array


def display_from_csv(csv_filename):
    """ Method to display high-dimensional data, read from a csv file, in a 2D embedded space.

    Args:
        csv_filename: (str) The name of the csv file from which to extract the points in the high-dimensional space.
    """
    df = pd.read_csv(csv_filename, index_col=0)
    display(df.values)


def sample_latent_space(means, vars):
    """ Method to sample from multi-dimensional data from a distribution

    This method is useful in the case of variational autoencoders.

    Args:
        means: (ndarray) An array of the distribution's mean for each dimension.
        vars: (ndarray) An array of the distribution's variance for each dimension.
            (Equivalent to the diagonal of the covariance matrix)

    Returns:
        the sampled latent space projections
    """
    sample = normal(loc=means, scale=vars)
    return sample


def display(data):
    """ Method to display high-dimensional data in a 2D embedded space.

    Args:
        data: (ndarray) An array of dimensions N (samples in the data) by D (dimensions in the high-dimensional space).
    """
    data_embedded = _fit(data)
    x = data_embedded.T[0]
    y = data_embedded.T[1]
    plt.scatter(x, y)
    plt.show()


def _format_name(name):
    """ remove '/' char at begining of name

    Args:
        name: string

    Returns:

    """
    if name[0] == '/':
        return name[1:]
    return name


def _format_output_name(name, path, perplexity, learning_rate):
    """format output path and name of a tsne visualization

    Args:
        name: name of output file
        path: relative path of output file including output directory
        perplexity: value of perplexity parameter
        learning_rate: value of learning rate parameter

    Returns: output path

    """
    if name[0] == '/':
        name = name[1:]
    if not os.path.isdir(path):
        os.makedirs(path)
    return os.path.join(path, '{}_perplexity{}_learning_rate{}.png'.format(name, perplexity, learning_rate))


def _tsne_with_parameter_selection(data):
    """selects best tsne visualization minimizing kl-divergence

    Args:
        data: all point living in latent space we want to visualize

    Returns: tuple (embedded data, perplexity, learning rate)

    """
    min_kl_divergence = inf
    optimal_embedding = zeros((data.shape[0], 2))
    optimal_perplexity = 0
    optimal_learning_rate = 0
    for learning_rate in arange(10, 200, 100):
        for perplexity in arange(5, 50, 10):
            tsne = TSNE(n_components=2, perplexity=perplexity, learning_rate=learning_rate,
                        n_iter_without_progress=600)
            embedded_data = tsne.fit_transform(data)
            if tsne.kl_divergence_ < min_kl_divergence:
                min_kl_divergence = tsne.kl_divergence_
                optimal_embedding = embedded_data
                optimal_learning_rate = learning_rate
                optimal_perplexity = perplexity
    return optimal_embedding, optimal_perplexity, optimal_learning_rate


def _plot_tsne_dice(data, labels, array_sizes, scalar_array, min_scalar, max_scalar, output_name):
    """compute and plot tsne embedded data using blue to red color map

    Args:
        data: all point living in latent space we want to visualize
        use_parameter_selection: option for selection perplexity and learning rate
        labels: array of label representing patient name with same number of element as data
        array_sizes:  array of containing each patient's number of slice
        scalar_array: array of dice value representing patient name with same number of element as data
        min_scalar: min of dice
        max_scalar: max of dice
        output_name: name of output file

    Returns: Void

    """
    idx = 0
    plt.figure(dpi=1200)
    for i in arange(0, labels.shape[0]):
        dim = int(array_sizes[i])
        x = data.T[0][idx:int(idx + dim)]
        y = data.T[1][idx:int(idx + dim)]
        plt.scatter(x, y, c=tile(scalar_array[i], dim), cmap='coolwarm', marker=labels[i],
                    vmin=min_scalar, vmax=max_scalar, alpha=0.5)
        idx += dim
    plt.colorbar()
    plt.savefig(output_name, dpi=1200)
    plt.clf()
    plt.close()


def _plot_tsne_slice(data, labels, array_sizes, output_name):
    """ compute and plot tsne embedded data using blue to red color map

    Args:
        data: all point living in latent space we want to visualize
        labels: array of label representing patient name with same number of element as data
        array_sizes: array of containing each patient's number of slice
        output_name: the path to the file in which to save the plot

    Returns:

    """
    idx = 0
    plt.figure(dpi=1200)
    for i in arange(0, labels.shape[0]):
        dim = int(array_sizes[i])
        x = data.T[0][idx:int(idx + dim)]
        y = data.T[1][idx:int(idx + dim)]
        plt.scatter(x, y, c=linspace(0, 1, dim), cmap='autumn', marker=labels[i],
                    vmin=0, vmax=1, alpha=0.5)
        idx += dim
    plt.colorbar()
    plt.savefig(output_name, dpi=1200)
    plt.clf()
    plt.close()


def _tsne_interpolation(data, use_parameter_selection, labels, array_sizes, slice_index):
    """compute and plot tsne embedded data using blue to red color map

    Args:
        data: all point living in latent space we want to visualize
        use_parameter_selection: option for selection perplexity and learning rate
        labels: array of label representing patient name with same number of element as data
        array_sizes: array of containing each patient's number of slice
        slice_index: array of slice index with same number of element as data

    Returns: Void

    """
    embedded_data, perplexity, learning_rate = _tsne_with_parameter_selection(
        data) if use_parameter_selection else \
        (TSNE(n_components=2).fit_transform(data), 30, 200)
    idx = 0
    plt.figure(dpi=1200)
    for i in arange(0, labels.shape[0]):
        dim = int(array_sizes[i])
        x = embedded_data.T[0][idx:int(idx + dim)]
        y = embedded_data.T[1][idx:int(idx + dim)]
        plt.scatter(x, y, c=slice_index[i] / slice_index[i][-1], cmap='autumn', marker=labels[i],
                    vmin=0, vmax=1, alpha=0.5)
        idx += dim
    plt.colorbar()
    plt.savefig(
        _format_output_name('tsne_latent_space', 'TSNE_SLICE_INTERPOLATION', perplexity, learning_rate),
        dpi=1200)
    plt.clf()
    plt.close()


def iterate_on_folder_list(folder_list, use_parameter_selection, latent_space_field_name):
    """allow us to generate visu for a list of model

    Args:
        folder_list: list of absolute path of model
        use_parameter_selection: if we want to search best parameter or use default
        latent_space_field_name: name of latent space field in hdf5 folders

    Returns:

    """
    for folder_path in folder_list:
        hdf_path_train = os.path.join(folder_path, 'TRAIN_PREDICTION')
        dice_csv_train = os.path.join(os.path.join(folder_path, 'SCORES'), 'merge_score_train.csv')
        folder_path_array = folder_path.split('/')
        dir_name = folder_path_array[-1]
        if os.path.isfile(hdf_path_train) and os.path.isfile(dice_csv_train):
            display_tsne(hdf_path_train,
                         use_parameter_selection,
                         latent_space_field_name,
                         dice_csv_train,
                         os.path.join(dir_name, 'TSNE_TRAIN'))
        hdf_path_valid = os.path.join(folder_path, 'VALID_PREDICTION')
        dice_csv_valid = os.path.join(os.path.join(folder_path, 'SCORES'), 'merge_score_valid.csv')
        if os.path.isfile(hdf_path_valid) and os.path.isfile(dice_csv_valid):
            display_tsne(hdf_path_valid,
                         use_parameter_selection,
                         latent_space_field_name,
                         dice_csv_valid,
                         os.path.join(dir_name, 'TSNE_VALID'))


def display_tsne(hdf_path, use_parameter_selection, latent_space_field_name, csv_path, output_folder):
    """ extract data from file and compute generate tsne representation with dice colormap

    Args:
        hdf_path: path to hdf5 file containing results
        use_parameter_selection: option for selection perplexity and learning rate
        latent_space_field_name: name of hdf5 attribute we want to visualize
        csv_path: path to file containing dice data, must match hdf5 patient data
        output_folder: the folder in which to save the tsne plots

    Returns: Void

    """
    dices_rv, dices_lv, dices_myo = [], [], []
    labels, array_sizes, latent_space = [], [], []
    with h5py.File(hdf_path, 'r') as file:
        print('Open hdf5 result file')
        with open(csv_path) as csvfile:
            print('Open csv dice file')
            reader = csv.DictReader(csvfile)
            for row in reader:
                dices_rv.append(float(row['DiceRV']))
                dices_lv.append(float(row['DiceLV']))
                dices_myo.append(float(row['DiceMYO']))
                name = row['']
                name_array = name.split('_')
                labels.append(str('$' + findall('\d+', name_array[0])[0] + name_array[1] + '$'))
                array = to_array(file[name][latent_space_field_name])
                array_sizes.append(array.shape[0])
                latent_space.append(array)
    latent_space = vstack(latent_space)
    dices_lv = asarray(dices_lv)
    dices_rv = asarray(dices_rv)
    dices_myo = asarray(dices_myo)
    labels = asarray(labels)
    array_sizes = asarray(array_sizes)
    dices_average = (dices_lv + dices_rv + dices_myo) / 3
    min_dice = min([min(dices_lv), min(dices_rv), min(dices_myo), min(dices_average)])
    max_dice = max([min(dices_lv), max(dices_rv), max(dices_myo), max(dices_average)])
    print('Apply tsne')
    embedded_data, perplexity, learning_rate = _tsne_with_parameter_selection(latent_space) \
        if use_parameter_selection else (TSNE(n_components=2).fit_transform(latent_space), 30, 200)
    print('Generating tsne for right ventricule dice')
    _plot_tsne_dice(embedded_data,
                    labels,
                    array_sizes,
                    dices_rv,
                    min_dice,
                    max_dice,
                    _format_output_name('DiceRV', output_folder, perplexity, learning_rate))
    print('Generating tsne for left ventricule dice')
    _plot_tsne_dice(embedded_data,
                    labels,
                    array_sizes,
                    dices_lv,
                    min_dice,
                    max_dice,
                    _format_output_name('DiceLV', output_folder, perplexity, learning_rate))
    print('Generating tsne for myocard')
    _plot_tsne_dice(embedded_data,
                    labels,
                    array_sizes,
                    dices_myo,
                    min_dice,
                    max_dice,
                    _format_output_name('DiceMyo', output_folder, perplexity, learning_rate))
    print('Generating tsne for average dice')
    _plot_tsne_dice(embedded_data,
                    labels,
                    array_sizes,
                    dices_average,
                    min_dice,
                    max_dice,
                    _format_output_name('DiceAverage', output_folder, perplexity, learning_rate))
    print('Generating tsne for slice index')
    _plot_tsne_slice(embedded_data,
                     labels,
                     array_sizes,
                     _format_output_name('Slice_index', output_folder, perplexity, learning_rate))


def display_interpolated(hdf_path, use_parameter_selection, latent_space_field_name,
                         interpolated_latent_space_path):
    """display latent space with tsne dimensionality reduction with a given color for each slice of same index

    Args:
        hdf_path: path to hdf5 file containing results
        use_parameter_selection: select best tsne minimizing kl divergence
        latent_space_field_name: name of hdf5 attribute we want to visualize
        interpolated_latent_space_path: path to directory containing interpolated array (same dimension rendered data)

    Returns:

    """
    latent_spaces, labels, array_sizes, slice_index = [], [], [], []
    with h5py.File(hdf_path, 'r') as file:
        interpolated_latent_spaces = \
            [(os.path.splitext(file)[0], load(os.path.join(interpolated_latent_space_path, file)))
             for file in os.listdir(interpolated_latent_space_path)]
        for group in file.values():
            latent_space = to_array(group[latent_space_field_name])
            nb_samples, array = next(((latent_space[1]['nb_sample'], latent_space[1]['array'])
                                      for latent_space in interpolated_latent_spaces
                                      if _format_name(group.name) == latent_space[0]), (1, latent_space))
            array_sizes.append(array.shape[0])
            n = latent_space.shape[0]
            slice_index.append(append(arange(0, n - 1, 1 / nb_samples), n - 1))
            latent_spaces.append(array)
            name_array = group.name.split('_')
            labels.append(str('$' + findall('\d+', name_array[0])[0] + name_array[1] + '$'))
    _tsne_interpolation(vstack(latent_spaces),
                        use_parameter_selection,
                        asarray(labels),
                        asarray(array_sizes),
                        asarray(slice_index))


def _fit(data):
    """ Method to fit high-dimensional data into a 2D embedded space.

    Args:
        data: (ndarray) An array of dimensions N (samples in the data) by D (dimensions in the high-dimensional space).
    """
    data_embedded = TSNE(n_components=2).fit_transform(data)
    return data_embedded


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Util for tsne visualization of data living in latent space')
    parser.add_argument('--hdf5_path', type=str,
                        help='path to hdf5 containing multidimensional data living in latent space')
    parser.add_argument('--csv_path', type=str, required=False,
                        help='path to csv containing multidimensional dice metric')
    parser.add_argument('--use_parameter_selection', type=bool, nargs='?', default=False,
                        help='select best perplexity and learning rate minimizing kl divergence')
    parser.add_argument('--field_name', type=str, nargs='?', default='latent_space',
                        help='name of hdf5 attribute we want to visualize')
    parser.add_argument('--interpolated_data_path', type=str, nargs='?', default=None,
                        help='path to directory containing interpolated latent space data saved as .npz')
    parser.add_argument('--list', nargs='*', default='list of folder of model sub directory')
    args = parser.parse_args()

    if len(args.list) > 0:
        iterate_on_folder_list(args.list, args.use_parameter_selection, args.field_name)
    if args.interpolated_data_path is not None:
        display_interpolated(args.hdf5_path,
                             args.use_parameter_selection,
                             args.field_name,
                             args.interpolated_data_path)
    else:
        display_tsne(args.hdf5_path,
                     args.use_parameter_selection,
                     args.field_name,
                     args.csv_path,
                     'TSNE')
