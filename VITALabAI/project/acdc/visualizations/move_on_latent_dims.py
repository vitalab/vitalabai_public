import argparse

import matplotlib.pyplot as plt
import numpy as np
from keras.models import load_model
from matplotlib.widgets import Slider, RadioButtons

from VITALabAI.model.acdc.layers_utils import VAESampling
from VITALabAI.project.acdc.visualizations.utils import load_latent_points
from VITALabAI.utils.dataset import centered_resize


def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return array[idx]


def main(pretrained_model_path, predictions_path=""):
    """ Produce an interactive plot to move on the latent dimensions of a vae

    Args:
        pretrained_model_path: The path to a vae model
        predictions_path: The path to one of this vae prediction file

    Returns:
        None
    """
    image_size = (256, 256)
    pretrained_model = load_model(pretrained_model_path, {"VAESampling": VAESampling}, compile=False)
    decoder = pretrained_model.get_layer("vae_decoder")
    nb_latent_dims = decoder.input_shape[1]
    data, z_labels = load_latent_points(predictions_path)

    # Build the visualization
    fig = plt.figure(1)
    image = np.zeros(image_size)
    plt.axes((0.55, 0.05, 0.4, 1))
    plt.title("Prediction")
    display = plt.imshow(image, vmin=0, vmax=3)

    sliders_min = -20
    sliders_max = 20
    zlist = []
    dim_slider_height = 0.75 / nb_latent_dims
    dim_slider_buttom = 0.95
    for i in range(nb_latent_dims):
        dim_slider_buttom -= dim_slider_height
        ax = plt.axes((0.05, dim_slider_buttom, 0.40, dim_slider_height))
        zi = Slider(ax, 'z' + str(i), valmin=sliders_min, valmax=sliders_max, valinit=0, valstep=0.001)
        zlist.append(zi)

    patient_ids = data.patient_id.values
    ax = plt.axes((0.15, 0.10, 0.30, 0.05))
    patient_id = Slider(ax, "patient_id", valmin=patient_ids.min(), valmax=patient_ids.max(), valinit=0,
                        valstep=1,
                        valfmt="%i")
    plt.plot(data.patient_id.values, np.zeros_like(patient_ids), 'r.')

    slices = data.slice.values
    ax = plt.axes((0.15, 0.05, 0.30, 0.05))
    slice = Slider(ax, "slice_id", valmin=slices.min(), valmax=slices.max(), valinit=0, valstep=1,
                   valfmt="%i")
    slice_pos = plt.plot(slices, np.zeros_like(slices), 'r.')[0]

    ax = plt.axes((0.05, 0.05, 0.05, 0.1))
    stage = RadioButtons(ax, ("ES", "ED"))

    # Setup the interactive triggers
    def update_zis(val):
        z = np.array([zi.val for zi in zlist])[None, :]
        pred = decoder.predict_on_batch(z)
        image = centered_resize(pred, image_size).argmax(axis=-1).squeeze()
        display.set_data(image)
        plt.draw()

    def reset_zis(val):
        for zi in zlist:
            zi.set_val(0)
        update_zis("dummy")

    def update_patient_id(val):
        patient_data = data[data.patient_id == patient_id.val]
        if patient_data.empty:
            reset_zis("dummy")
        else:
            slices = patient_data.slice.values
            slice_pos.set_data(slices, np.zeros_like(slices))
            slice.set_val(find_nearest(slices, slice.val))
            update_slice("dummy")

    def update_slice(val):
        slice_data = data[(data.patient_id == patient_id.val) &
                          (data.slice == slice.val) &
                          (data.stage == stage.value_selected)]
        if slice_data.empty:
            reset_zis("dummy")
        else:
            z = np.squeeze(slice_data[z_labels].iloc[0].values)
            for i, zi in enumerate(zlist):
                zi.set_val(z[i])
            plt.draw()

    for zi in zlist:
        zi.on_changed(update_zis)
    patient_id.on_changed(update_patient_id)
    slice.on_changed(update_slice)
    stage.on_clicked(update_slice)

    update_zis("dummy")
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=(("Produce an interactive plot where one can move on the latent"
                      " dimensions of a vae model")))
    parser.add_argument("--pretrained_vae", help="The path to a vae model", required=True)
    parser.add_argument("--predictions", required=True,
                        help="The path to one of the vae model prediction file")
    args = parser.parse_args()
    main(args.pretrained_vae, args.predictions)
