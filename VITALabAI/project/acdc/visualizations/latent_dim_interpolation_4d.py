import argparse
import itertools

import h5py
import numpy as np
from keras.models import load_model
from scipy.interpolate import interp1d

from VITALabAI.model.acdc.layers_utils import VAESampling
from VITALabAI.project.acdc.visualizations.utils import to_array, save_nifti


class Interpolation4DGenerator:
    """ Class used to make interpolations between two hearts
    """

    def __init__(self,
                 path,
                 model,
                 pretrained_model,
                 nb_samples):
        """ Creates Interpolation helper specific to ACDC database

        Args:
            path: path of dataset output from encoder/decoder NN
            model: type of autoencoder model {cae, vae}
            pretrained_model: path of vae or cae model with valid decoder layer
            nb_samples: number of points between slices of a given patient data
        """
        self.path = path
        self.nb_samples = nb_samples

        if model == 'cae' and pretrained_model is not None:
            self.decoder = pretrained_model.get_layer("cae_decoder")
            self.latent_space = 'latent_space'
        elif model == 'vae' and pretrained_model is not None:
            self.decoder = pretrained_model.get_layer("vae_decoder")
            self.latent_space = 'latent_space_mean'

    def generate_4d_interpolation(self, group1, group2):
        """ apply prediction to sample between two patient data

        Args:
            group1: data of given patient1
            group2: data of given patient2

        Returns:

        """
        latent_space1 = to_array(group1[self.latent_space])
        latent_space2 = to_array(group2[self.latent_space])
        gt_shape = group1['gt_m'].shape[1:3]
        voxel_size = group1.attrs.get('voxel_size', [1, 1, 1])
        for coeff in np.linspace(1, 0, self.nb_samples + 2):
            interp = self.interpolate_binary(latent_space1, latent_space2, coeff)
            predicted = self.decoder.predict_on_batch(interp)
            save_nifti(group1.name[1:] + str(coeff) + group2.name[1:] + str(1 - coeff),
                       predicted,
                       gt_shape,
                       voxel_size,
                       output_folder='4d_interpolation_nifti')

    def interpolate_between_patient_in_list(self, patient_list):
        """Interpolate between list of user input patient

        Args:
            patient_list: list of patient we want to interpolate between

        Returns:

        """
        with h5py.File(self.path, 'r') as f:
            groups = [f[patient_id] for patient_id in patient_list]
            [self.generate_4d_interpolation(*pair) for pair in itertools.combinations(groups, r=2)]

    def interpolate_between_nearest_patient(self):
        """Find patient with smallest euclidian distance and interpolate

        Returns:

        """
        with h5py.File(self.path, 'r') as f:
            combinations = [pair for pair in itertools.combinations(f.values(), r=2)]
            idx = np.argmin([self.distance(*elem) for elem in combinations])
            self.generate_4d_interpolation(*combinations[idx])

    def interpolate_between_farthest_patient(self):
        """Find patient with greatest euclidian distance and interpolate

        Returns:

        """
        with h5py.File(self.path, 'r') as f:
            combinations = [pair for pair in itertools.combinations(f.values(), r=2)]
            idx = np.argmax([self.distance(*elem) for elem in combinations])
            self.generate_4d_interpolation(*combinations[idx])

    def interpolate_one_to_all(self, patient_id):
        """ Interpolate between 1 patient and all others

        Args:
            patient_id: input patient we want to interpolate to all other patient in hdf5

        Returns:

        """
        with h5py.File(self.path, 'r') as f:
            chosen_patient_data = f[patient_id]
            [self.generate_4d_interpolation(chosen_patient_data, group) for group in f.values()]

    def interpolate_binary(self, latent_space1, latent_space2, coeff):
        """Interpolate between 2 patient and save output as specify by user

        Args:
            latent_space1: slices of patient1 as n-dimensional data living in latent space
            latent_space2: slices of patient2 as n-dimensional data living in latent space
            coeff:

        Returns:

        """
        if latent_space1.shape != latent_space2.shape:
            latent_space2 = self.resize_to(latent_space2, latent_space1.shape[0])
        return coeff * latent_space1 + (1 - coeff) * latent_space2

    def distance(self, group1, group2):
        """get distance between 2 patient

        Args:
            group1: slices of patient1 as n-dimensional data living in latent space
            group2: slices of patient2 as n-dimensional data living in latent space

        Returns: euclidian distance between slices of each patient

        """
        latent_space1 = to_array(group1[self.latent_space])
        latent_space2 = to_array(group2[self.latent_space])
        if latent_space1.shape != latent_space2.shape:
            latent_space2 = self.resize_to(latent_space2, latent_space1.shape[0])
        return np.linalg.norm(latent_space1 - latent_space2)

    def resize_to(self, latent_space, new_dim):
        """ Resize array of array of latent space projection using interpolation

        Args:
            latent_space: array to resize
            new_dim: new array dimension

        Returns:

        """
        dim = latent_space.shape[0]
        f = interp1d(np.arange(dim), latent_space, axis=0)
        return np.apply_along_axis(f, 0, np.linspace(0, dim - 1, new_dim))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Util for interpolation of ACDC data living in latent space')
    parser.add_argument('--model', type=str, required=True,
                        help='model type', choices=['vae', 'cae'])
    parser.add_argument('--path', type=str, required=True,
                        help='The path of hdf5 file containing results of autoencoder on ACDC dataset')
    parser.add_argument('--pretrained', type=str, required=True,
                        help='cae/vae model path for decoder layer')
    parser.add_argument('--nb_samples', type=int, nargs='?', default=1,
                        help='number of points between slices of a given patient data')
    parser.add_argument('--selection', type=str, nargs='?', default=None,
                        help='type of interpolation',
                        choices=['farthest', 'nearest'])
    parser.add_argument('--name', nargs='*', default=[],
                        help='patient names if we want to apply interpolation to specific patient data')

    args = parser.parse_args()
    helper = Interpolation4DGenerator(model=args.model,
                                      path=args.path,
                                      pretrained_model=load_model(args.pretrained,
                                                                  {"VAESampling": VAESampling},
                                                                  compile=False),
                                      nb_samples=args.nb_samples)

    if args.name and len(args.name) == 1:
        helper.interpolate_one_to_all(args.name[0])
    if args.name and len(args.name) > 1:
        helper.interpolate_between_patient_in_list(args.name)
    else:
        if args.selection == 'nearest':
            helper.interpolate_between_nearest_patient()
        elif args.selection == 'farthest':
            helper.interpolate_between_farthest_patient()
