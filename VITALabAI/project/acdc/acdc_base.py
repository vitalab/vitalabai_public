import os
from glob import glob
from os.path import join as pjoin
from re import findall

import h5py
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from keras.engine.saving import load_model
from matplotlib import pyplot as plt

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.dataset.semanticsegmentation.acdc.scores import MergeScoreIntoCsv, SaveNifti, DiceScore, \
    HausdorffScore
from VITALabAI.project.acdc.acdc_losses import ACDCLosses
from VITALabAI.project.acdc.metrics.anatomical_metrics_utils import extract_metrics
from VITALabAI.project.acdc.autoencoder_layer_utlis import VAESampling
from VITALabAI.project.acdc.visualcallback import VisualCallback
from VITALabAI.utils.vitalutils import create_model_directories


class ACDCBase(VITALabAiKerasAbstract):
    """Base model to handle the ACDC project."""

    def __init__(self, dataset: VITALabAiDatasetAbstract, loss_fn=None, optimizer=None, metrics=None,
                 loss_weights=None, nb_feature_maps=32, pretrained_model=None, pretrained_vae=None,
                 name=None, display=False):

        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            loss_weights: list, losses weights used when computing the total loss.
            nb_feature_maps: int, Number of feature maps at beginning of the network, automatic scaling for
                            the following layers.
            pretrained_model: string, path to model
            pretrained_vae: string, path to a pretrained VAE used as post processing
            name: string, name of the model
            display: bool, if True a screen will display predictions at the end of every epoch.
        """
        self.nb_feature_maps = nb_feature_maps
        self.pretrained_model = pretrained_model

        self.acdclosses = ACDCLosses(dataset.get_num_classes())
        loss_fn = loss_fn or self.losses()
        metrics = metrics or self.metrics()
        loss_weights = loss_weights or self.losses_weights()

        super().__init__(dataset, loss_fn, optimizer, metrics, loss_weights)

        if pretrained_vae:
            print("Load Vae")
            self.postproc_vae = load_model(pretrained_vae, {"VAESampling": VAESampling}, compile=False)
        else:
            self.postproc_vae = None

        self.display = display
        self.name = self.__class__.__name__ if name is None else name

        self.name, self.checkpoint_dir, self.logs_dir, self.screen_dir, self.scores_dir, self.model_dir, \
            self.figures_dir, self.metrics_dir = \
            create_model_directories(self.name, ["ModelCheckpoint", "LOGS", "SCREEN", "SCORES", "MODEL",
                                                 "FIGURES", 'METRICS'])

    def build_model(self):
        raise NotImplementedError("Abstract class")

    def predict_and_save(self, sequence, f5h):
        raise NotImplementedError("Abstract class.")

    def losses(self):
        return None

    def losses_weights(self):
        return None

    def metrics(self):
        return None

    @staticmethod
    def get_preprocessing():
        return None

    def train(self, **kwargs):
        """Train the model.

        Args:
            **kwargs: dict with keywords to be used.
        """
        try:
            print(("Press ctrl-c to stop the training and continue "
                   "the pipeline."))
            # Fit the model
            print("Fitting model...")
            history = self.model.fit_generator(self.get_train_set(),
                                               validation_data=self.get_validation_set(),
                                               **kwargs)
            for key in history.history.keys():
                plt.figure()
                plt.plot(history.history[key])
                plt.title(key)
                plt.savefig(pjoin(self.figures_dir, "{}.png".format(key)))

        except KeyboardInterrupt:
            print("\nTraining cancelled!\n")

    def save(self):
        """Method to save the best weights. """
        # Extract the best weights
        list_of_files = glob(pjoin(self.checkpoint_dir, '*'))
        lower_loss = min(list_of_files, key=self.extract_losses)
        print('Best weights saving ... \n' + lower_loss)

        # Name the file
        h5_name = '{}_fm{}.h5'.format(self.__class__.__name__, self.nb_feature_maps)

        # Load weights and save in the right folder
        self.load_weights(lower_loss)
        self.model.save(pjoin(self.model_dir, h5_name))

    @staticmethod
    def extract_losses(path):
        """ Method to extract the loss values from the names files."""
        base = os.path.basename(path)
        base = os.path.splitext(base)[0]
        return float(findall("\d+\.\d+", base)[0])

    def evaluate(self, **kwargs):
        """Method used to generate predictions for the train, valid and test sets.

        Args:
            **kwargs: additional parameters
        """

        print("Testing on train set...")
        with h5py.File(pjoin(self.name, 'TRAIN_PREDICTION'), "w") as f5h:
            self.predict_and_save(self.dataset.get_set_for_prediction('train'), f5h)

        print("Testing on validation set...")
        with h5py.File(pjoin(self.name, 'VALID_PREDICTION'), "w") as f5h:
            self.predict_and_save(self.dataset.get_set_for_prediction('valid'), f5h)

        print("Testing on test set...")
        with h5py.File(pjoin(self.name, 'TEST_PREDICTION'), "w") as f5h:
            self.predict_and_save(self.dataset.get_set_for_prediction('test'), f5h)

        print("Computing metrics on the predictions...")
        extract_metrics('pred_m', pjoin(self.name, ''), metrics_folder=self.metrics_dir)

    def save_into_h5f(self, group, img, gt, pred, v_size, centers=None):
        """ Save an image, gt, and prediction in an HDF5 group.

        Args:
            group: hdf5 group
            img: array, input image
            gt: array, groundtruth for the input image
            pred: array, model prediction for the input image
            v_size: list, voxel dimensions
            centers: array, output from the gridnet model
        """
        group.create_dataset("image", data=img)
        group.create_dataset("gt_c", data=gt)
        group.create_dataset("pred_c", data=pred)

        gt = gt.argmax(axis=-1)
        group.create_dataset("gt_m", data=gt)
        group.create_dataset("pred_m", data=pred.argmax(axis=-1))

        if self.postproc_vae:
            pred = self.postproc_vae.predict(pred)[0]  # Vae outputs a list

        pred = pred.argmax(axis=-1)

        for post in self.postproc:
            pred = post(pred)

        group.create_dataset("pred_post", data=pred)
        if centers is not None:
            group.create_dataset('centers', data=centers)
        group.attrs['voxel_size'] = v_size

    def export_results(self):
        """ Generate the results. """

        print("Exporting results...")
        train_set = pjoin(self.name, "TRAIN_PREDICTION")
        valid_set = pjoin(self.name, "VALID_PREDICTION")
        test_set = pjoin(self.name, "TEST_PREDICTION")

        nii_images = pjoin(self.scores_dir, "NIFTI")
        if not os.path.isdir(nii_images):
            os.makedirs(nii_images)

        scores = [
            SaveNifti(nii_images),
            MergeScoreIntoCsv([DiceScore(), HausdorffScore()], self.scores_dir),
        ]

        for dataset, which_set in zip([train_set, valid_set, test_set], ["train", "valid", "test"]):
            with h5py.File(dataset, "r") as h5f:
                for score in scores:
                    score(h5f, which_set)

    def get_callbacks(self, learning_rate, early_stopping=False, reduce_lr=False, patience=10):
        """This method returns the callbacks used for training models from the uthercast project.

        Args:
            learning_rate: float, learning rate, used to specify reduce_lr min_lr
            early_stopping: bool, if True early stopping is used
            reduce_lr: bool, if True, reduce learning on plateau is used
            patience: int, number of epochs with no improvement used by early_stopping and reduce_lr

        Returns:
            list of callbacks
        """
        # Create callback for TensorBoard
        tensorboard = TensorBoard(log_dir=pjoin(self.name, "LOGS"), histogram_freq=0, write_graph=True,
                                  write_images=False)
        # Create model saving callback
        details = self.__class__.__name__
        details += '_{epoch:03d}' + '_{val_loss:.4f}.h5'

        model_saver = ModelCheckpoint(
            pjoin(self.name, "ModelCheckpoint", details),
            monitor='val_loss',
            save_best_only=True,
            save_weights_only=True,
        )
        # Create early stopping callback
        early_stopping_callback = EarlyStopping(monitor='val_loss', patience=patience, verbose=True)
        min_lr = learning_rate // 20
        reduce_lr_callback = ReduceLROnPlateau(monitor='val_loss', factor=0.5, verbose=1,
                                               patience=patience // 2, min_lr=min_lr)

        list_callback = [tensorboard, model_saver]
        if early_stopping:
            list_callback.append(early_stopping_callback)
        if reduce_lr:
            list_callback.append(reduce_lr_callback)

        if self.display:
            list_callback.append(
                VisualCallback(self.get_validation_set(), save_dir=pjoin(self.name, "SCREEN")))

        return list_callback
