import numpy as np
from keras import Input, Model
from keras.engine.saving import load_model
from keras.layers import (
    Conv2D, MaxPooling2D, Conv2DTranspose, Activation,
    regularizers)
from keras.layers.merge import Concatenate
from medpy.metric.binary import dc
from tqdm import tqdm

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.dataset.semanticsegmentation.acdc.acdcdatasetgenerator import _to_categorical
from VITALabAI.dataset.semanticsegmentation.acdc.augmentation_transformer import AugmentationTransformer
from VITALabAI.dataset.semanticsegmentation.acdc.postprocess import (
    Post3dBigBlob,
)
from VITALabAI.dataset.semanticsegmentation.acdc.preprocess import (
    PreProcessSTD,
    PreQuantilePercent
)
from VITALabAI.project.acdc.acdc_base import ACDCBase
from VITALabAI.utils.dataset import centered_resize


class ACDCUnet(ACDCBase):
    """Unet model for the ACDC project. """

    def __init__(self, dataset: VITALabAiDatasetAbstract, nb_feature_maps=32, l2reg=1e-4,
                 pretrained_model=None, pretrained_vae=None, loss_fn=None, optimizer=None, metrics=None,
                 name=None, display=False):
        self.l2reg = l2reg
        super().__init__(dataset, loss_fn, optimizer, metrics, nb_feature_maps=nb_feature_maps, name=name,
                         pretrained_model=pretrained_model, display=display, pretrained_vae=pretrained_vae)
        self.preproc = (PreProcessSTD(), PreQuantilePercent(),)
        self.postproc = (Post3dBigBlob(),)

    def build_model(self):
        """ Method to create the network architecture.

        Returns:
            keras.Model object *not* compiled.
        """

        if self.pretrained_model:
            model = load_model(self.pretrained_model)

        else:
            l2reg = self.l2reg

            input_image = Input((self.dataset.get_input_shape()), name="input_image")
            nb_feature_maps = self.nb_feature_maps
            conv0 = Conv2D(np.int_(nb_feature_maps / 2), (3, 3), activation='relu', padding='same',
                           name='layer0', kernel_regularizer=regularizers.l2(l2reg))(
                input_image)
            conv0 = Conv2D(np.int_(nb_feature_maps / 2), (3, 3), activation='relu', padding='same',
                           name='layer00', kernel_regularizer=regularizers.l2(l2reg))(conv0)
            pool0 = MaxPooling2D(pool_size=(2, 2))(conv0)

            conv1 = Conv2D(nb_feature_maps, (3, 3), activation='relu', padding='same', name='layer1',
                           kernel_regularizer=regularizers.l2(l2reg))(pool0)
            conv1 = Conv2D(nb_feature_maps, (3, 3), activation='relu', padding='same', name='layer2',
                           kernel_regularizer=regularizers.l2(l2reg))(conv1)
            pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
            conv2 = Conv2D(nb_feature_maps * 2, (3, 3), activation='relu', padding='same', name='layer3',
                           kernel_regularizer=regularizers.l2(l2reg))(pool1)
            conv2 = Conv2D(nb_feature_maps * 2, (3, 3), activation='relu', padding='same', name='layer4',
                           kernel_regularizer=regularizers.l2(l2reg))(conv2)
            pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
            conv3 = Conv2D(nb_feature_maps * 4, (3, 3), activation='relu', padding='same', name='layer5',
                           kernel_regularizer=regularizers.l2(l2reg))(pool2)
            conv3 = Conv2D(nb_feature_maps * 4, (3, 3), activation='relu', padding='same', name='layer6',
                           kernel_regularizer=regularizers.l2(l2reg))(conv3)
            pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
            conv4 = Conv2D(nb_feature_maps * 8, (3, 3), activation='relu', padding='same', name='layer7',
                           kernel_regularizer=regularizers.l2(l2reg))(pool3)
            conv4 = Conv2D(nb_feature_maps * 8, (3, 3), activation='relu', padding='same', name='layer8',
                           kernel_regularizer=regularizers.l2(l2reg))(conv4)
            pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)
            conv5 = Conv2D(nb_feature_maps * 16, (3, 3), activation='relu', padding='same', name='layer9',
                           kernel_regularizer=regularizers.l2(l2reg))(pool4)
            conv5 = Conv2D(nb_feature_maps * 16, (3, 3), activation='relu', padding='same', name='layer10',
                           kernel_regularizer=regularizers.l2(l2reg))(conv5)
            up6 = Concatenate(axis=3, name='layer11')(
                [Conv2DTranspose(nb_feature_maps * 8, (2, 2), strides=(2, 2), padding='same',
                                 kernel_regularizer=regularizers.l2(l2reg))(conv5), conv4])
            conv6 = Conv2D(nb_feature_maps * 8, (3, 3), activation='relu', padding='same', name='layer12',
                           kernel_regularizer=regularizers.l2(l2reg))(up6)
            conv6 = Conv2D(nb_feature_maps * 8, (3, 3), activation='relu', padding='same', name='layer13',
                           kernel_regularizer=regularizers.l2(l2reg))(conv6)
            up7 = Concatenate(axis=3, name='layer14')(
                [Conv2DTranspose(nb_feature_maps * 4, (2, 2), strides=(2, 2), padding='same',
                                 kernel_regularizer=regularizers.l2(l2reg))(conv6), conv3])
            conv7 = Conv2D(nb_feature_maps * 4, (3, 3), activation='relu', padding='same', name='layer15',
                           kernel_regularizer=regularizers.l2(l2reg))(up7)
            conv7 = Conv2D(nb_feature_maps * 4, (3, 3), activation='relu', padding='same', name='layer16',
                           kernel_regularizer=regularizers.l2(l2reg))(conv7)
            up8 = Concatenate(axis=3, name='layer17')(
                [Conv2DTranspose(nb_feature_maps * 2, (2, 2), strides=(2, 2), padding='same',
                                 kernel_regularizer=regularizers.l2(l2reg))(conv7), conv2])
            conv8 = Conv2D(nb_feature_maps * 2, (3, 3), activation='relu', padding='same', name='layer18',
                           kernel_regularizer=regularizers.l2(l2reg))(up8)
            conv8 = Conv2D(nb_feature_maps * 2, (3, 3), activation='relu', padding='same', name='layer19',
                           kernel_regularizer=regularizers.l2(l2reg))(conv8)
            up9 = Concatenate(axis=3, name='layer20')(
                [Conv2DTranspose(nb_feature_maps, (2, 2), strides=(2, 2), padding='same',
                                 kernel_regularizer=regularizers.l2(l2reg))(conv8), conv1])
            conv9 = Conv2D(nb_feature_maps, (3, 3), activation='relu', padding='same', name='layer21',
                           kernel_regularizer=regularizers.l2(l2reg))(up9)
            conv9 = Conv2D(nb_feature_maps, (3, 3), activation='relu', padding='same', name='layer22',
                           kernel_regularizer=regularizers.l2(l2reg))(conv9)
            up10 = Concatenate(axis=3, name='layer23')(
                [Conv2DTranspose(np.int_(nb_feature_maps / 2), (2, 2), strides=(2, 2), padding='same',
                                 kernel_regularizer=regularizers.l2(l2reg))(conv9), conv0])
            conv10 = Conv2D(np.int_(nb_feature_maps / 2), (3, 3), activation='relu', padding='same',
                            name='layer24', kernel_regularizer=regularizers.l2(l2reg))(up10)
            conv10 = Conv2D(np.int_(nb_feature_maps / 2), (3, 3), activation='relu', padding='same',
                            name='layer25', kernel_regularizer=regularizers.l2(l2reg))(conv10)
            conv11 = Conv2D(4, (1, 1), name='layer26', kernel_regularizer=regularizers.l2(l2reg))(conv10)
            out = Activation("softmax")(conv11)
            inputs = [input_image]
            outputs = [out]

            model = Model(inputs=inputs, outputs=outputs)

        return model

    @staticmethod
    def losses_names():
        """ Values to print during the training.

        Returns:
            list of values to print
        """

        return [
            ("cross_entrop", True),
            ("Metric_dice", True),
            ("dice_RV", True),
            ("dice_MYO", True),
            ("dice_LV", True),
        ]

    def losses(self):
        return [
            self.acdclosses.classes_crossentropy,
        ]

    def metrics(self):
        """ Metrics used during the training. """
        return [
            self.acdclosses.classes_dice,
            self.acdclosses.dice_on_rv,
            self.acdclosses.dice_on_myo,
            self.acdclosses.dice_on_lv
        ]

    def predict_and_save(self, sequence, f5h):
        """ Function to predict and save results f

        Args:
            sequence: Keras.Sequence, sequence used to generate predictions
            f5h: h5py file to save predictions in
        """
        final_losses = []
        data_print = {'dc_LVMetric': [], 'dc_MYOMetric': [], 'dc_RVMetric': [], 'ed_Lv': [], 'ed_My': [],
                      'ed_Rv': [], 'es_Lv': [], 'es_My': [], 'es_Rv': []}
        for i in tqdm(range(len(sequence))):
            data = sequence[i]
            if len(data) == 5:
                name, img, gt, prior, v_size = data
            else:
                name, img, gt, v_size = data

            if not name.endswith("_0"):
                continue  # Don't use augmented data

            pred = self.model.predict(img)
            pred = centered_resize(pred, gt.shape[1:3])

            group = f5h.create_group(name)
            self.save_into_h5f(group, img, gt, pred, v_size)

            # predict accuracy on each groups and ES,ED separately.
            one_hot_labels = pred.argmax(axis=-1)
            one_hot_labels = _to_categorical(one_hot_labels[:, :, :, np.newaxis], 4)
            dc_lv = dc(one_hot_labels[:, :, :, 3], gt[:, :, :, 3])
            dc_my = dc(one_hot_labels[:, :, :, 2], gt[:, :, :, 2])
            dc_rv = dc(one_hot_labels[:, :, :, 1], gt[:, :, :, 1])

            data_print['dc_LVMetric'].append(dc_lv)
            data_print['dc_MYOMetric'].append(dc_my)
            data_print['dc_RVMetric'].append(dc_rv)

            if name.endswith("_ED_0"):

                data_print['ed_Lv'].append(dc_lv)
                data_print['ed_My'].append(dc_my)
                data_print['ed_Rv'].append(dc_rv)

            else:
                data_print['es_Lv'].append(dc_lv)
                data_print['es_My'].append(dc_my)
                data_print['es_Rv'].append(dc_rv)

            final_losses.append(self.model.test_on_batch([img], [gt]))

        final_losses = np.array(final_losses)
        for key in data_print:
            data_print[key] = str(np.array(data_print[key]).mean())

        data_out = {}
        for (name, use), arr in zip(self.losses_names(),
                                    np.arange(final_losses.shape[1])):
            if not use:
                continue
            data_print.update({name: str(final_losses[:, arr].mean())})

            data_out.update({name: final_losses[:, arr].mean()})
            print(name, final_losses[:, arr].mean())
        print("dc_RVMetric", data_print['dc_RVMetric'])
        print("dc_MYOMetric", data_print['dc_MYOMetric'])
        print("dc_LVMetric", data_print['dc_LVMetric'])
        print("ed_RV", data_print['ed_Rv'])
        print("es_RV", data_print['es_Rv'])
        print("ed_MY", data_print['ed_My'])
        print("es_MY", data_print['es_My'])
        print("ed_LV", data_print['ed_Lv'])
        print("es_LV", data_print['es_Lv'])

    @staticmethod
    def get_preprocessing():
        return PreProcessSTD(), PreQuantilePercent(),

if __name__ == '__main__':
    from VITALabAI.dataset.semanticsegmentation.acdc.acdc import VITALAcdc
    import argparse
    from keras.optimizers import Adam

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the hdf5 dataset", type=str, required=True)
    aparser.add_argument("--pretrained_model", type=str, help="Model h5 file", default=None)
    aparser.add_argument("--nb_epochs", type=int, help="Number of epoch to train on the dataset",
                         default=100)
    aparser.add_argument("--batch_size", type=int, help="choose the size of the batch feeded to the network.",
                         default=32)
    aparser.add_argument("--nb_feature_maps", type=int, help="Number of features maps used on the first "
                                                             "layer", default=32)
    aparser.add_argument("--post-vae", type=str, help="Vae model h5 file", default=None)
    aparser.add_argument("--name", type=str, dest="name", help="Model path", default=None)
    aparser.add_argument("--display", dest="display", action="store_true",
                         help="Display results during training")
    aparser.add_argument("--testonly", action='store_true', dest="testonly",
                         help="Skip training and do test phase")
    aparser.add_argument("--lr", type=float, dest="lr", help="learning rate", default=3e-4)
    aparser.add_argument("--no_da", dest="use_da", action='store_false', help="Disable data augmentation")

    args = aparser.parse_args()

    augmentation_transformer = None
    if args.use_da:
        augmentation_transformer = AugmentationTransformer(rotation_range=60, width_shift_range=0,
                                                           height_shift_range=0, zoom_range=0.1,
                                                           dilation_range=2)
    ds = VITALAcdc(path=args.path, batch_size=args.batch_size,
                   augmentation_transformer=augmentation_transformer,
                   preproc_fn=ACDCUnet.get_preprocessing())

    optim = Adam(lr=args.lr, beta_1=0.9, beta_2=0.99)

    model = ACDCUnet(ds, optimizer=optim, name=args.name, display=args.display,
                     pretrained_model=args.pretrained_model,
                     nb_feature_maps=args.nb_feature_maps, pretrained_vae=args.post_vae)

    if not args.testonly:
        callbacks = model.get_callbacks(learning_rate=args.lr)
        model.train(epochs=args.nb_epochs, callbacks=callbacks, workers=1)
        model.save()

    model.evaluate()
    model.export_results()
