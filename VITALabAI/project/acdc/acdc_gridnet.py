"""
acdc_gridnet.py
author: Clement ZOTTI (clement.zotti@usherbrooke.ca)
Refactored: Thierry Judge (thierry.judge@usherbrooke.ca

C. Zotti et al. "GridNet with automatic shape prior registration
for automatic MRI cardiac segmentation.", https://arxiv.org/pdf/1705.08943.pdf

"""

import numpy as np
from keras.engine.saving import load_model
from keras.layers import (
    Input, Dense, Conv2D, MaxPooling2D, SpatialDropout2D, Conv2DTranspose,
    Activation, GlobalAveragePooling2D
)
from keras.layers.merge import Concatenate, Add
from keras.models import Model
from keras.utils import Sequence
from scipy import ndimage
from skimage.transform import rescale
from tqdm import tqdm

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.dataset.semanticsegmentation.acdc.acdc import ACDCSequence
from VITALabAI.dataset.semanticsegmentation.acdc.augmentation_transformer import AugmentationTransformer
from VITALabAI.dataset.semanticsegmentation.acdc.postprocess import (
    Post3dBigBlob,
)
from VITALabAI.dataset.semanticsegmentation.acdc.preprocess import (
    PreGammaCorrection,
    PreProcessSTD,
    PreQuantilePercent
)
from VITALabAI.project.acdc.acdc_base import ACDCBase
from VITALabAI.model.semanticsegmentation.layers_utils import conv_bn_relu, conv_relu
from VITALabAI.utils.dataset import centered_resize
from VITALabAI.utils.layers import MergeCenter


class GridNetSequenceWrapper(Sequence):
    """ Wrap the sequence to return the ground truth with extra modalities.
        Note, "sequence" is structure guarantees that the network will only train once on each sample per
        epoch, use for "generator" function from keras.
    """

    def __init__(self, seq: ACDCSequence):
        self.seq = seq

    def __len__(self):
        """This method returns the number of elements in the sequence."""
        return len(self.seq)

    def __getitem__(self, idx):
        """This method reads and rescales the ground truth.

        Args:
            idx: int, Index of the sequence.

        Returns:
            List containing img and prior
            List containing gt and centers
        """

        img, gt, prior = self.seq[idx]
        gt, centers = self.add_modalities(gt)
        return [img, prior], gt + [centers, ]

    @staticmethod
    def add_modalities(gt):
        """Add new modalities to the data array.

        Args:
            gt: ndarray, The Y_train examples

        Returns:
            A list with the ground truth, the computed contours, the gt and the centers.
        """
        center_mass = []
        for i in range(len(gt)):
            center_mass.append(ndimage.center_of_mass(gt[i, :, :, 3]))

        rescaled = [gt, gt, ]
        for scale in [1. / 2, 1. / 4, 1. / 8]:
            slices = []
            for s in range(len(gt)):
                classes = []
                for c in range(gt.shape[-1]):
                    classes.append(rescale(gt[s, :, :, c], scale, mode='constant'))

                slices.append(classes)
            slices = np.array(slices)
            slices = slices.transpose(0, 2, 3, 1)
            slices[slices > 0] = 1
            rescaled.append(slices.astype(np.uint8))
        center_mass = np.array(center_mass)
        # 128, 128 is the center of an image of 256x256
        center_mass[np.isnan(center_mass)] = 128
        return rescaled, np.around(center_mass).astype(np.float32)


class ACDCGridNet(ACDCBase):
    """ GridNet model for the ACDC project. """

    def __init__(self, dataset: VITALabAiDatasetAbstract, nb_feature_maps=20, pretrained_model=None,
                 pretrained_vae=None, loss_fn=None, optimizer=None, metrics=None, loss_weights=None,
                 contour_weights=0.01, dropout=0.25, name=None, display=False):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            nb_feature_maps: int, number of feature maps in the network
            pretrained_model: string, path to pretrained model
            pretrained_vae: string, path to a pretrained VAE used as post processing
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            loss_weights: list of weights for the loss functions
            contour_weights: float, weight for the contour loss.
            dropout: float, dropout probability
            name: string, path to save the results.
            display: bool, if True results will be displayed after each epoch
        """
        self.contour_weights = contour_weights
        self.dropout = dropout
        super().__init__(dataset, loss_fn, optimizer, metrics, nb_feature_maps=nb_feature_maps, name=name,
                         pretrained_model=pretrained_model, pretrained_vae=pretrained_vae,
                         loss_weights=loss_weights, display=display)
        self.preproc = (PreQuantilePercent(), PreGammaCorrection(), PreProcessSTD(),)
        self.postproc = (Post3dBigBlob(),)

    def losses(self):
        """ Losses function used for the gridnet.

        Returns:
            list of losses
        """
        return [
            self.acdclosses.combined_loss,
            self.acdclosses.contour_crossentropy,
            self.acdclosses.combined_loss,
            self.acdclosses.combined_loss,
            self.acdclosses.combined_loss,
            self.acdclosses.l2_loss
        ]

    def losses_weights(self):
        """ Losses weights used when computing the three loss term.

        Returns:
            list of loss weights
        """
        return [
            1.,
            self.contour_weights,
            0.5,
            0.25,
            0.125,
            0.000390625
        ]

    def metrics(self):
        """ Metrics used during the training. """
        return {
            "final_output": self.acdclosses.classes_dice
        }

    def build_model(self):
        """ Method to create the network architecture.

        Returns:
            keras.Model object *not* compiled.
        """

        if self.pretrained_model:
            model = load_model(self.pretrained_model)

        else:
            input_image = Input(self.dataset.get_input_shape(), name="input_image")
            nb_feature_maps = self.nb_feature_maps
            working_axis = 3
            prior = Input((100, 100, 3), name="prior_image")
            drop = self.dropout

            l2reg = 1e-4

            x = conv_relu(input_image, nb_feature_maps, l2reg=l2reg)
            x1 = conv_bn_relu(x, nb_feature_maps, working_axis, l2reg=l2reg)

            x = MaxPooling2D()(x1)
            i1 = MaxPooling2D()(input_image)
            x = Concatenate(axis=working_axis)([x, i1])
            x = conv_bn_relu(x, nb_feature_maps * 2, working_axis, l2reg=l2reg)
            x2 = conv_bn_relu(x, nb_feature_maps * 2, working_axis, l2reg=l2reg)

            x = MaxPooling2D()(x2)
            i2 = MaxPooling2D()(i1)
            x = Concatenate(axis=working_axis)([x, i2])
            x = conv_bn_relu(x, nb_feature_maps * 4, working_axis, l2reg=l2reg)
            x4 = conv_bn_relu(x, nb_feature_maps * 4, working_axis, l2reg=l2reg)

            x = MaxPooling2D()(x4)
            i3 = MaxPooling2D()(i2)
            x = Concatenate(axis=working_axis)([x, i3])
            x = conv_bn_relu(x, nb_feature_maps * 8, working_axis, l2reg=l2reg)
            x8 = conv_bn_relu(x, nb_feature_maps * 8, working_axis, l2reg=l2reg)

            x = MaxPooling2D()(x8)
            i4 = MaxPooling2D()(i3)
            x = Concatenate(axis=working_axis)([x, i4])
            x = conv_bn_relu(x, nb_feature_maps * 16, working_axis, l2reg=l2reg)
            x = conv_bn_relu(x, nb_feature_maps * 16, working_axis, l2reg=l2reg)
            x_enc = SpatialDropout2D(drop)(x)

            # Filtering path
            x1 = conv_relu(x1, nb_feature_maps)
            x2 = conv_relu(x2, nb_feature_maps * 2)
            x4 = conv_relu(x4, nb_feature_maps * 4)
            x8 = conv_relu(x8, nb_feature_maps * 8)

            x = Conv2DTranspose(nb_feature_maps * 8, (2, 2), strides=(2, 2))(x_enc)
            x = Activation('relu')(x)
            x = Concatenate(axis=working_axis)([x, x8])
            x = conv_bn_relu(x, nb_feature_maps * 8, working_axis, l2reg=l2reg)
            x = conv_bn_relu(x, nb_feature_maps * 8, working_axis, l2reg=l2reg)
            x = SpatialDropout2D(drop)(x)

            out8 = conv_bn_relu(x, nb_feature_maps, 3)
            out8 = Conv2D(4, 1)(out8)
            out8 = Activation("softmax", name='out_8')(out8)

            # 256
            x = Conv2DTranspose(nb_feature_maps * 4, (2, 2), strides=(2, 2))(x)
            x = Activation('relu')(x)
            x = Concatenate(axis=working_axis)([x, x4])
            x = conv_bn_relu(x, nb_feature_maps * 4, working_axis, l2reg=l2reg)
            x = conv_bn_relu(x, nb_feature_maps * 4, working_axis, l2reg=l2reg)
            x = SpatialDropout2D(drop)(x)

            out4 = conv_bn_relu(x, nb_feature_maps, 3)
            out4 = Conv2D(4, 1)(out4)
            out4 = Activation("softmax", name='out_4')(out4)

            x_add = Conv2DTranspose(nb_feature_maps * 2, (2, 2), strides=(2, 2))(x)
            x = Conv2DTranspose(nb_feature_maps * 2, (2, 2), strides=(2, 2))(x)
            x = Activation('relu')(x)
            x = Concatenate(axis=working_axis)([x, x2])
            x = conv_bn_relu(x, nb_feature_maps * 2, working_axis, l2reg=l2reg)
            x = conv_bn_relu(x, nb_feature_maps * 2, working_axis, l2reg=l2reg)
            x = SpatialDropout2D(drop)(x)

            out2 = conv_bn_relu(x, nb_feature_maps, 3)
            out2 = Conv2D(4, 1)(out2)
            out2 = Activation("softmax", name='out_2')(out2)

            x = Add()([x, x_add])
            x = conv_bn_relu(x, nb_feature_maps, working_axis, l2reg=l2reg)

            x_add = Conv2DTranspose(nb_feature_maps, (2, 2), strides=(2, 2))(x)
            x = Conv2DTranspose(nb_feature_maps, (2, 2), strides=(2, 2))(x)
            x = Activation('relu')(x)
            x = Concatenate(axis=working_axis)([x, x1])
            x = conv_bn_relu(x, nb_feature_maps, working_axis, l2reg=l2reg)
            x = conv_bn_relu(x, nb_feature_maps, working_axis, l2reg=l2reg)
            x = SpatialDropout2D(drop)(x)

            x = Add()([x, x_add])
            x = conv_bn_relu(x, nb_feature_maps, working_axis, l2reg=l2reg)
            # output
            x = Conv2D(4, (1, 1))(x)
            x = Activation("softmax", name="pre_output")(x)

            global_features = GlobalAveragePooling2D()(x_enc)
            reg = Dense(256)(global_features)
            reg = Activation("relu")(reg)
            reg = Dense(128)(reg)
            reg = Activation("relu")(reg)
            reg = Dense(2)(reg)
            reg = Activation("relu", name='reg')(reg)

            x = MergeCenter([256, 256, 4])([x, prior, reg])

            x = conv_bn_relu(x, 32, working_axis, l2reg=l2reg)
            x = conv_bn_relu(x, 32, working_axis, l2reg=l2reg)
            x = conv_bn_relu(x, 32, working_axis, l2reg=l2reg)

            x = Conv2D(4, (1, 1))(x)
            out = Activation("softmax", name="final_output")(x)

            inputs = [input_image, prior]
            outputs = [out, out, out2, out4, out8, reg]

            model = Model(inputs=inputs, outputs=outputs)

        return model

    def get_train_set(self):
        return GridNetSequenceWrapper(self.dataset.get_train_set())

    def get_validation_set(self):
        return GridNetSequenceWrapper(self.dataset.get_validation_set())

    def get_test_set(self):
        return GridNetSequenceWrapper(self.dataset.get_test_set())

    def predict_and_save(self, sequence, f5h):
        """ This method iterates through a sequence and saves predictions.

            Args:
                sequence: Keras Sequence, sequence to iterate through.
                f5h: h5py file, file in which predictions and other data is saved.
        """
        for i in tqdm(range(len(sequence))):
            data = sequence[i]
            name, img, gt, prior, v_size = data

            if not name.endswith("_0"):
                continue  # Don't use augmented data

            pred = self.model.predict_on_batch([img, prior])
            pred = pred[0]
            centers = pred[-1]
            pred = centered_resize(pred, gt.shape[1:3])
            group = f5h.create_group(name)
            self.save_into_h5f(group, img, gt, pred, v_size, centers)

    @staticmethod
    def get_preprocessing():
        return PreQuantilePercent(), PreGammaCorrection(), PreProcessSTD()

if __name__ == '__main__':
    from VITALabAI.dataset.semanticsegmentation.acdc.acdc import VITALAcdc
    import argparse
    from keras.optimizers import Adam

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the hdf5 dataset", type=str, required=True)
    aparser.add_argument("--pretrained_model", type=str, help="Model h5 file", default=None)
    aparser.add_argument("--nb_epochs", type=int, help="Number of epoch to train on the dataset",
                         default=100)
    aparser.add_argument("--batch_size", type=int, help="choose the size of the batch feeded to the network.",
                         default=10)
    aparser.add_argument("--nb_feature_maps", type=int, help="Number of features maps used on the first "
                                                             "layer", default=32)
    aparser.add_argument("--contour_weights", type=float, help="Weight to apply on the contour loss ",
                         default=0.01)
    aparser.add_argument("--name", type=str, dest="name", help="Model path", default=None)
    aparser.add_argument("--display", dest="display", action="store_true",
                         help="Display results during training")
    aparser.add_argument("--testonly", action='store_true', dest="testonly",
                         help="Skip training and do test phase")
    aparser.add_argument("--lr", type=float, dest="lr", help="learning rate", default=1e-4)
    aparser.add_argument("--no_da", dest="use_da", action='store_false', help="Disable data augmentation")

    args = aparser.parse_args()

    augmentation_transformer = None
    if args.use_da:
        augmentation_transformer = AugmentationTransformer(rotation_range=60, width_shift_range=0,
                                                           height_shift_range=0, zoom_range=0.1,
                                                           dilation_range=2)

    ds = VITALAcdc(path=args.path, batch_size=args.batch_size, use_prior=True,
                   augmentation_transformer=augmentation_transformer,
                   preproc_fn=ACDCGridNet.get_preprocessing())

    optim = Adam(lr=args.lr)
    model = ACDCGridNet(ds, optimizer=optim, name=args.name, display=args.display,
                        pretrained_model=args.pretrained_model, nb_feature_maps=args.nb_feature_maps,)

    if not args.testonly:
        callbacks = model.get_callbacks(learning_rate=args.lr)
        model.train(epochs=args.nb_epochs, callbacks=callbacks, workers=1)
        model.save()

    model.evaluate()
    model.export_results()
