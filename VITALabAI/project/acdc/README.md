# ACDC project

### Contributors

* Clément Zotti (clement.zotti@usherbrooke.ca)

### Description

This is a project for the cardiac segmentation of MRI images using various Convolutionnal Neural Networks. 
Each CNN is encapsulated inside a class and each class is derived from a parent class called ACDCBase. 
Each model has been developed, trained and tested in conjunction with the [ACDC](www.creatis.insa-lyon.fr/Challenge/acdc/) 2017 dataset (Automatic Cardiac Delineation Challenge).

### Requirements

* keras (with --no-deps)
* Tensorflow
* scikit-image
* pandas
* nibabel
* medpy (python3 version should be install with pip github link)
* tqdm
* natsort
* [pygpu](https://github.com/Theano/libgpuarray) (0.6.2)
* pygame (optional)


# Models 

## ACDC GridNet 

### How to use 

```bash
python acdc_gridnet.py --path='path/of/output.hdf5'
```

#### Parameters explanation

* `--path`, path of the h5 file that contains the ACDC dataset.
* `--batch_size`, is optional (default `10`), choose the size of the batch feeded to the network.
* `--nb_epochs`, is optional (default `100`), number of epoch to train on the dataset.
* `--nb_feature_maps`, is optional (default `32`), number of features maps used on the first layer (automaticly scaled for the following layers).
* `--contour_weights`, is optional (default `0.01`), choose the weight to apply on the contour loss.
* `--pretrained_model`, pretrained_model, is optional (default None), path to a Keras model that is a pretrained, saved ACDCGridNet.
* `--testonly`, is optional (default `False`). This options allows to skip the training.
* `--display`, is optional (defautl `False`), display the output segmentation map.
* `--name`, is optional (default is current folder), path where you want to save all the data.
* `--lr`, is optional (default `1e-4`), learning rate.
* `--no_da`, is optional (default `False`), disable data augmentation.


------------------------------------------------------------------------------

## ACDC UNET

### Contributors

* Nathan Painchaud

### Description

This is a project for the cardiac segmentation of MRI images using U_net Neural Networks.
Ref: https://arxiv.org/abs/1505.04597


### How to use `acdc_unet.py`

```bash
python acdc_unet.py --path='path/of/output.hdf5'
```

### Parameters explanation

* `--path`, path of the h5 file that contains the ACDC dataset.
* `--batch_size`, is optional (default `32`), choose the size of the batch feeded to the network.
* `--nb_epochs`, is optional (default `100`), number of epoch to train on the dataset.
* `--nb_feature_maps`, is optional (default `32`), number of features maps used on the first layer (automaticly scaled for the following layers).
* `--pretrained_model`, pretrained_model, is optional (default None), path to a Keras model that is a pretrained, saved ACDCGridNet.
* `--testonly`, is optional (default `False`). This options allows to skip the training.
* `--display`, is optional (defautl `False`), display the output segmentation map.
* `--name`, is optional (default is current folder), path where you want to save all the data.
* `--lr`, is optional (default `3e-4`), learning rate.
* `--no_da`, is optional (default `False`), disable data augmentation.

------------------------------------------------------------------------------

## ACDC ConvDeconv


### How to use `acdc_convdeconv.py`

```bash
python acdc_convdeconv.py --path='path/of/output.hdf5'
```

### Parameters explanation

* `--path`, path of the h5 file that contains the ACDC dataset.
* `--batch_size`, is optional (default `32`), choose the size of the batch feeded to the network.
* `--nb_epochs`, is optional (default `100`), number of epoch to train on the dataset.
* `--nb_feature_maps`, is optional (default `32`), number of features maps used on the first layer (automaticly scaled for the following layers).
* `--pretrained_model`, pretrained_model, is optional (default None), path to a Keras model that is a pretrained, saved ACDCGridNet.
* `--testonly`, is optional (default `False`). This options allows to skip the training.
* `--display`, is optional (defautl `False`), display the output segmentation map.
* `--name`, is optional (default is current folder), path where you want to save all the data.
* `--lr`, is optional (default `3e-4`), learning rate.
* `--no_da`, is optional (default `False`), disable data augmentation.

------------------------------------------------------------------------------

## ACDC Convolutional Autoencoder

## Contributors

* Laurence Fournier (laurence.fournier@usherbrooke.ca)
* Nathan Painchaud (nathan.painchaud@usherbrooke.ca)

## Description

This is a project for the encoding of cardiac segmentation of MRI images using convolutional autoencoders.

The models in this project are inspired by Keras’s autoencoder examples:
Ref: https://blog.keras.io/building-autoencoders-in-keras.html

### How to use `acdc_cae.py`

```bash
python acdc_cae.py --path='path/of/output.hdf5'
```

### Parameters explanation

* `--path`, path of the h5 file that contains the ACDC dataset.
* `--batch_size`, is optional (default `40`), choose the size of the batch feeded to the network.
* `--nb_epochs`, is optional (default `500`), number of epoch to train on the dataset.
* `--nb_feature_maps`, is optional (default `48`), number of features maps used on the first layer (automaticly scaled for the following layers).
* `--pretrained_model`, pretrained_model, is optional (default None), path to a Keras model that is a pretrained, saved ACDCGridNet.
* `--testonly`, is optional (default `False`). This options allows to skip the training.
* `--display`, is optional (defautl `False`), display the output segmentation map.
* `--name`, is optional (default is current folder), path where you want to save all the data.
* `--lr`, is optional (default `3e-4`), learning rate.
* `--no_da`, is optional (default `False`), disable data augmentation.
* `--code_length`, is optional (default `512`), the dimensionality of the latent space of the autoencoder.

------------------------------------------------------------------------------

## ACDC Variational Autoencoder

## Contributors

* Laurence Fournier (laurence.fournier@usherbrooke.ca)
* Nathan Painchaud (nathan.painchaud@usherbrooke.ca)

## Description

This is a project for the encoding of cardiac segmentation of MRI images using variational convolutional autoencoders.

It was inspired by the same type of neural network developped for the CAMUS dataset, and by Keras' own examples.
Ref: https://blog.keras.io/building-autoencoders-in-keras.html

### How to use `acdc_vae.py`

```bash
python acdc_vae.py --path='path/of/output.hdf5'
```

### Parameters explanation

* `--path`, path of the h5 file that contains the ACDC dataset.
* `--batch_size`, is optional (default `40`), choose the size of the batch feeded to the network.
* `--nb_epochs`, is optional (default `1000`), number of epoch to train on the dataset.
* `--nb_feature_maps`, is optional (default `48`), number of features maps used on the first layer (automaticly scaled for the following layers).
* `--pretrained_model`, pretrained_model, is optional (default None), path to a Keras model that is a pretrained, saved ACDCGridNet.
* `--testonly`, is optional (default `False`). This options allows to skip the training.
* `--display`, is optional (defautl `False`), display the output segmentation map.
* `--name`, is optional (default is current folder), path where you want to save all the data.
* `--lr`, is optional (default `3e-4`), learning rate.
* `--no_da`, is optional (default `False`), disable data augmentation.
* `--code_length`, is optional (default `32`), the dimensionality of the latent space of the autoencoder.

------------------------------------------------------------------------------

## ACDC Segmentation Encoder

## Contributors

* Laurence Fournier (laurence.fournier@usherbrooke.ca)
* Nathan Painchaud (nathan.painchaud@usherbrooke.ca)

## Description

This is a project for the segmentation of cardiac MRI images using variational convolutional autoencoders.

### Training an `acdc_seg_encoder.py`

The purpose of a segmentation encoder is to take a MRI image and to project it into a latent space the same way the encoder half of an autoencoder does.
Afterwards, the decoder half will take the projection into the latent space, as it would have the output of its encoder half, and will output a segmentation map.
For the projection made by the segmentation encoder to be of use, it must fall as close as possible to the projection, made by an autoencoder, of the groundtruth associated with the MRI image.
To achieve this, the training of a segmentation encoder requires a pretrained autoencoder, because the segmentation encoder must learn to output the same latent space projection as the encoder half of the autoencoder.


### How to use `acdc_seg_encoder.py`

```bash
python acdc_seg_encoder.py --path='path/of/output.hdf5' --pretrained_vae='path/of/pretrained/vae/model'
```

### Parameters explanation

* `--path`, path of the h5 file that contains the ACDC dataset.
* `--pretrained_vae`, is optional for testonly (default None), path to a Keras model that is a pretrained, saved ACDCVariationalAutoencoder
* `--batch_size`, is optional (default `40`), choose the size of the batch feeded to the network.
* `--nb_epochs`, is optional (default `1000`), number of epoch to train on the dataset.
* `--nb_feature_maps`, is optional (default `48`), number of features maps used on the first layer (automaticly scaled for the following layers).
* `--pretrained_model`, pretrained_model, is optional (default None), path to a Keras model that is a pretrained, saved ACDCGridNet.
* `--testonly`, is optional (default `False`). This options allows to skip the training.
* `--display`, is optional (defautl `False`), display the output segmentation map.
* `--name`, is optional (default is current folder), path where you want to save all the data.
* `--lr`, is optional (default `3e-4`), learning rate.
* `--no_da`, is optional (default `False`), disable data augmentation.
* `--code_length`, is optional (default `32`), the dimensionality of the latent space of the autoencoder.


------------------------------------------------------------------------------

## ACDC K Nearest Neighbours

## Contributors

* Laurence Fournier (laurence.fournier@usherbrooke.ca)
* Nathan Painchaud (nathan.painchaud@usherbrooke.ca)
* Lea Belisle Tourigny (lea.belisle.tourigny@usherbrooke.ca)



## How to use 'acdc_knearestneighbours': Generate dataset


We must first generate dataset of points in latent space:
	using all acdc dataset with ground truth, the chosen model's encoder layer takes input image and generate a point living in latent space for each slice
	This is done  by the ~/vitalabai_private/VITALabAI/dataset/acdc/kneighboursdataset.py
	This output a .npz file containing points of latent space and is needed by the knn

```bash
python kneighboursdataset.py --path='path/of/acdc/dataset.hdf5' --model='path/of/acdc/pretrained/model'
```

### Parameter explanation
* `--path` : Absolute path of ACDC dataset with hdf5 formatting
* `--model` : Absolute path of ACDC pretrained model (usually best model on valid)
* `--output` : optional parameter if we want to chose output file name 
* `--meanvar` : flag if loading pretrained model needs log, and mean (seg encoder dataset needs this)
* `--batch_size` : optional parameter for the batch size
* `--epochs` : optional number of epoch 
            The total number of points  in output dataset is batch-size * epochs

## How to use 'acdc_kneighbours.py'

```bash
python acdc_kneighbours.py --path='path/to/dataset'--latent_space_dataset='path/to/latent_space_dataset' --pretrained_model='path/to/model'
```

### Parameters explanation

* `--path`:path to hdf5 ACDC dataset file
* `--latent_space_dataset`: path to .npz containing all n-dim dataset encoded by the model
* `--pretrained_model`:path of a valid model to encode and decode input
* `--mode`: {'vae', 'cae', 'seg_encoder'} : param that define how the model will be built
* `--k`: number of k neighbours
* `--name`, is optional (default is current folder), path where you want to save all the data.
* `--no_da`, is optional (default `False`), disable data augmentation.
* `--option`: if k is greater than one, select how output will be generate form k nearest neighbours
		distance : closer neighbours will have greater weight
		uniform : result is average of k nearest neighbours

