from keras import Input, Model
from keras.engine.saving import load_model
from keras.optimizers import Adam
from keras.utils import Sequence
from tqdm import tqdm

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.dataset.semanticsegmentation.acdc.augmentation_transformer import AugmentationTransformer
from VITALabAI.dataset.semanticsegmentation.acdc.postprocess import (
    Post3dBigBlob,
)
from VITALabAI.dataset.semanticsegmentation.acdc.preprocess import (
    PreProcessSTD,
    PreQuantilePercent
)
from VITALabAI.project.acdc.acdc_base import ACDCBase
from VITALabAI.project.acdc.autoencoder_layer_utlis import cae_encoder, cae_decoder
from VITALabAI.utils.dataset import centered_resize


class AutoEncoderSequenceWrapper(Sequence):
    """ Wrap the sequence to return the ground truth as input and target.
        Note, "sequence" is structure guarantees that the network will only train once on each sample per
        epoch, use for "generator" function from keras.
    """

    def __init__(self, seq):
        self.seq = seq
        self.batch_size = seq.batch_size

    def __len__(self):
        """This method returns the number of elements in the sequence."""
        return len(self.seq)

    def __getitem__(self, idx):
        """This method the ground truth.

        Args:
            idx: int, Index of the sequence.

        Returns:
            array, Array with a size of (batch_size, height, width, channels).
            list, Out_gt from the scale of the three predictions, each scale is an array of size (
                batch_size, height, width, nb of class).
        """
        img, gt = self.seq[idx]
        return gt, gt


class ACDCConvolutionalAutoencoder(ACDCBase):
    """ Class that implements a convolutional autoencoder for the ACDC dataset.
    """

    def __init__(self, dataset: VITALabAiDatasetAbstract, nb_feature_maps=48, code_length=512,
                 pretrained_model=None, loss_fn=None, optimizer=None, metrics=None, name=None,
                 display=False):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            nb_feature_maps: int, number of feature maps in the network
            code_length: int, number of dimensions in the latent space
            pretrained_model: string, path to pretrained model
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            name: string, path to save the results.
            display: bool, if True results will be displayed after each epoch
        """

        self.code_length = code_length
        self.pretrained_model = pretrained_model

        super().__init__(dataset, loss_fn, optimizer, metrics, nb_feature_maps=nb_feature_maps,
                         pretrained_model=pretrained_model, name=name, display=display)

        self.postproc = (
            Post3dBigBlob(),
        )

    def build_model(self):
        """ Build the convolutional autoencoder model

        The model is compose of an encoder and decoder model to simplify further experiments.

        Returns:
            keras.Model object *not* compiled.
        """

        if self.pretrained_model:
            self.model = load_model(self.pretrained_model)
        else:
            # Build encoder
            self.encoder = cae_encoder(self.dataset.get_input_shape(), self.dataset.get_num_classes(),
                                       self.nb_feature_maps,
                                       self.code_length)
            print("Model: %s built." % self.encoder.name)
            self.encoder.summary()

            # Build decoder
            self.decoder = cae_decoder(self.dataset.get_input_shape(), self.dataset.get_num_classes(),
                                       self.nb_feature_maps,
                                       self.code_length)
            print("Model: %s built." % self.decoder.name)
            self.decoder.summary()

            # Build complete cae model
            input_image = Input(shape=(self.dataset.get_input_shape()[0], self.dataset.get_input_shape()[1],
                                       self.dataset.nb_classes),
                                name='layer0')
            latent_space = self.encoder(input_image)
            decoded_output = self.decoder(latent_space)
            self.model = Model(inputs=input_image, outputs=[decoded_output, latent_space])

        print("Convolutional autoencoder built")
        self.model.summary()

        return self.model

    def losses(self):
        return {"cae_decoder": self.acdclosses.combined_loss}

    def metrics(self):
        return {"car_decoder": [self.acdclosses.classes_dice,
                                self.acdclosses.dice_on_rv,
                                self.acdclosses.dice_on_myo,
                                self.acdclosses.dice_on_lv]
                }

    def predict_and_save(self, sequence, f5h):
        """ Function to predict and save results f

        Args:
            sequence: Keras.Sequence, sequence used to generate predictions
            f5h: h5py file to save predictions in
        """

        for i in tqdm(range(len(sequence))):
            data = sequence[i]
            if len(data) == 5:
                name, img, gt, prior, v_size = data
            else:
                name, img, gt, v_size = data

            if not name.endswith("_0"):
                continue  # Don't use augmented data

            pred, latent_space = self.model.predict_on_batch(gt)
            pred = centered_resize(pred, gt.shape[1:3])
            group = f5h.create_group(name)
            self.save_into_h5f(group, img, gt, pred, v_size, latent_space)

    def save_into_h5f(self, group, img, gt, pred, v_size, latent_space):
        """ Save the the results in the h5f output """
        super().save_into_h5f(group, img, gt, pred, v_size)
        group.create_dataset("latent_space", data=latent_space)

    def get_train_set(self):
        return AutoEncoderSequenceWrapper(self.dataset.get_train_set())

    def get_validation_set(self):
        return AutoEncoderSequenceWrapper(self.dataset.get_validation_set())

    def get_test_set(self):
        return AutoEncoderSequenceWrapper(self.dataset.get_test_set())

    @staticmethod
    def get_preprocessing():
        return PreProcessSTD(), PreQuantilePercent(),


if __name__ == '__main__':
    from VITALabAI.dataset.semanticsegmentation.acdc.acdc import VITALAcdc
    import argparse

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the dataset", type=str, required=True)
    aparser.add_argument("--pretrained_model", type=str, help="Model h5 file", default=None)
    aparser.add_argument("--nb_epochs", type=int, help="Number of epoch to train on the dataset",
                         default=500)
    aparser.add_argument("--batch_size", type=int, help="choose the size of the batch feeded to the network.",
                         default=40)
    aparser.add_argument("--nb_feature_maps", type=int, help="Number of features maps used on the first "
                                                             "layer", default=48)
    aparser.add_argument("--code_length", type=int, help="Code lenght", default=512)
    aparser.add_argument("--name", type=str, dest="name", help="Model path", default=None)
    aparser.add_argument("--display", dest="display", action="store_true",
                         help="Display results during training")
    aparser.add_argument("--testonly", action='store_true', dest="testonly",
                         help="Skip training and do test phase")
    aparser.add_argument("--lr", type=float, dest="lr", help="learning rate", default=3e-4)
    aparser.add_argument("--no_da", dest="use_da", action='store_false', help="Disable data augmentation")

    args = aparser.parse_args()

    augmentation_transformer = None
    if args.use_da:
        augmentation_transformer = AugmentationTransformer(rotation_range=60, width_shift_range=0.1,
                                                           height_shift_range=0.1, zoom_range=0.1,
                                                           dilation_range=2)

    ds = VITALAcdc(path=args.path, batch_size=args.batch_size,
                   augmentation_transformer=augmentation_transformer,
                   preproc_fn=ACDCConvolutionalAutoencoder.get_preprocessing())

    optimizer = Adam(lr=args.lr, amsgrad=True)
    model = ACDCConvolutionalAutoencoder(ds, name=args.name, display=args.display,
                                         pretrained_model=args.pretrained_model,
                                         nb_feature_maps=args.nb_feature_maps,
                                         code_length=args.code_length,
                                         optimizer=optimizer)

    if not args.testonly:
        callbacks = model.get_callbacks(learning_rate=args.lr)
        model.train(epochs=args.nb_epochs, callbacks=callbacks, workers=1)
        model.save()

    model.evaluate()
    model.export_results()
