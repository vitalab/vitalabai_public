import random

import numpy as np
from keras.callbacks import Callback

from VITALabAI.utils.display import Display


class VisualCallback(Callback):
    """Keras Callback to visualize segmentation results at each epoch end."""

    def __init__(self, sequence, save_dir=None):
        super().__init__()
        self.sequence = sequence
        self.img_per_row = min(sequence.batch_size, 5)
        self.display = Display(save_dir, img_per_row=self.img_per_row)

    def on_epoch_end(self, epoch, logs=None):
        """ Method called at the end of each epoch.
            Updates the visual display with new predictions.

            Args:
                epoch: int, current epoch
                logs: None
        """
        idx = random.randint(0, len(self.sequence) - 1)

        x, gt = self.sequence[idx]
        seg = self.model.predict(x)

        # The GridNet and seg_encoder model outputs a list
        if isinstance(gt, list):
            gt = gt[0]

        if isinstance(seg, list):
            seg = seg[0]

        seg = seg[:self.img_per_row, :, :, 1:]
        gt = gt[:self.img_per_row, :, :, 1:]

        data = np.concatenate((seg, gt))

        self.display.pydisplay(data, epoch)
