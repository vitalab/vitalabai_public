import argparse

from keras import optimizers
from keras.callbacks import EarlyStopping

from VITALabAI.dataset.classification.mnist import VITALMnist
from VITALabAI.model.classification.cnn import VITALCnn
from VITALabAI.model.classification.svm import VITALSvm

"""
Example usage of this script:

    python train_mnist.py --model=cnn
"""

if __name__ == '__main__':
    aparser = argparse.ArgumentParser()
    aparser.add_argument("--model", help="Model to train", choices=['cnn', 'svm'], type=str,
                         default='cnn')
    # CNN Parameters
    aparser.add_argument("--batch-size", help='Batch size used during training', type=int, default=128)
    aparser.add_argument("--nb-epoch", help="Number of epochs the model will train", type=int, default=12)
    aparser.add_argument("--optim", help="Keras optimizer", type=str, default='adam')
    aparser.add_argument("--lr", help="Learning rate for the optimizer", type=int, default=1e-3)

    # SVM Parameters
    aparser.add_argument("--features", help="Feature to give to SVM", type=str, default=None, choices=['HOG'])

    args = aparser.parse_args()

    ds = VITALMnist(batch_size=args.batch_size)

    if args.model == 'svm':
        mnist_model = VITALSvm(ds, args.features)
        mnist_model.train()
    else:  # CNN
        early_stop = EarlyStopping(monitor='val_loss', patience=5, verbose=1)
        optimizer = optimizers.get(args.optim)
        optimizer.lr = args.lr
        mnist_model = VITALCnn(ds, optimizer=optimizer, loss_fn='categorical_crossentropy',
                               metrics=['accuracy'])
        mnist_model.train(epochs=args.nb_epoch, callbacks=[early_stop])

    mnist_model.evaluate(save_errors=True)
