# Mnist models

What is cool about Mnist is that you can run almost any machine learning algorithms and they give almost always pretty good results (>95% accuracy).


**The train_mnist.py scrip allows the user to train on mnist with different models.**

## SVM

```bash
python train_mnist.py --model=svm
```

The following parameters are available, for more information, see [SGD](https://scikit-learn.org/stable/modules/sgd.html):

- `--features` select the required feature to give to SVM, only [HOG](https://en.wikipedia
.org/wiki/Histogram_of_oriented_gradients) features are implemented, default is None

## Random Forest

```bash
python randomforest.py
```

The following parameters are available, for more information, see 
[RandomForestClassifier](http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html):

- `--n-estimators` number of trees, default is 10
- `--criterion` split quality criterion, default is 'gini'
- `--max-features` number of features for a split, default is 'auto' 
- `--max-depth` depth of a tree, default is None
- `--n-jobs` number of CPU to train the forest, default is -1 (use all cores)
- `--features` see SVM

## CNN

```bash
python train_mnist.py --model=cnn
```

These other relevant parameters are available:

- `--batch-size` batch size used during training, default is 128
- `--nb-epoch` number of epochs the model will train, default is 12
- `--optim` select the optimizer, 'adam' or 'sgd', default is 'adam'
- `--lr` learning rate applied in the optimizer, default is 1e-3