# -*- coding: utf-8 -*-
#
# Author: Clement ZOTTI (clement.zotti@usherbrooke.ca)
# Refactored : Thierry Judge (thierry.judge@usherbrooke.ca), date: 31/10/2018

import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix
from sklearn.model_selection import GridSearchCV

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.model.classification.classification_utils import compute_features


class MnistRandomForest(VITALabAiKerasAbstract):
    """Create a standard random Forest Model.
        Mnist random forest classifier. All parameters are from scikit-learn.
        For more information on these parameters see:
        http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html#sklearn.ensemble.RandomForestClassifier

        We add the possibily to use the sklearn GridSearch function.
    """

    def __init__(self, dataset: VITALabAiDatasetAbstract, features=None, n_estimators=10,
                 criterion='gini', max_features='auto', max_depth=None, n_jobs=-1, gridsearch=False,
                 **kwargs):
        """

        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            features: string,  which feature to be computed before launching the random forest
            classification.
            n_estimators: int, number of trees in the forest.
            criterion: string, split criterion.
            max_features: int, float, string, Number of features for the best split.
            max_depth: int, None, Maximum depth of one tree.
            n_jobs: int, Number of jobs run in parallel.
            gridsearch: bool, if True, grid search is used
            **kwargs: additional parameters
        """

        self.features = features
        self.n_estimators = int(n_estimators)
        self.criterion = criterion
        self.max_features = max_features

        self.max_depth = max_depth
        if max_depth is not None:
            self.max_depth = int(max_depth)

        self.n_jobs = int(n_jobs)

        self.model = None

        self.enable_gs = gridsearch

        super().__init__(dataset)

        (self.X_train, self.y_train), (_, _), (self.X_test, self.y_test) = self.dataset.get_entire_dataset()

        self.X_train = self.X_train.squeeze()
        self.X_test = self.X_test.squeeze()

    def build_model(self):
        """Builds the model using the random forest for classification.

        Returns:
            RandomForestClassifier or GridSearchCV of RandomForestClassifier to br trained
        """
        rng = np.random.RandomState(7331)

        if self.enable_gs:
            estimators = np.arange(10, 200, 20)
            criterions = ['gini', 'entropy']
            max_depth = [None, 1, 2, 3, 4, 5, 6, 7]
            params = dict(n_estimators=estimators,
                          criterion=criterions,
                          max_depth=max_depth,
                          n_jobs=[self.n_jobs],
                          random_state=[rng])
            self.grid = GridSearchCV(RandomForestClassifier(), param_grid=params)
            return self.grid
        else:
            self.model = RandomForestClassifier(n_estimators=self.n_estimators,
                                                criterion=self.criterion,
                                                max_features=self.max_features,
                                                max_depth=self.max_depth,
                                                n_jobs=self.n_jobs,
                                                random_state=rng)
            return self.model

    def train(self):
        """ This method trains the model created by the build_model() function.
        """
        data = compute_features(self.X_train, self.features)

        if self.enable_gs:
            self.grid.fit(data.reshape(data.shape[0], -1), self.y_train.argmax(axis=1))
            self.model = self.grid.best_estimator_
            print(self.grid.best_params_)
        else:
            self.model.fit(data.reshape(data.shape[0], -1),
                           self.y_train.argmax(axis=1))

    def evaluate(self, **kwargs):
        """Predict on the train and test set, then print results

        Args:
            **kwargs: additional parameters
        """

        data = compute_features(self.X_test, self.features)
        data_train = compute_features(self.X_train, self.features)
        self.y_predicted = self.model.predict(data.reshape(data.shape[0], -1))
        self.y_training = self.model.predict(data_train.reshape(data_train.shape[0], -1))

        print(classification_report(self.y_test.argmax(axis=1), self.y_predicted))
        print(confusion_matrix(self.y_test.argmax(axis=1), self.y_predicted))
        print("Overall training accuracy = {}".format(
            accuracy_score(self.y_train.argmax(axis=1), self.y_training)))
        print("Overall testing accuracy = {}".format(
            accuracy_score(self.y_test.argmax(axis=1), self.y_predicted)))


if __name__ == '__main__':
    from VITALabAI.dataset.classification.mnist import VITALMnist
    import argparse

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--n-estimators", help="Number of trees", type=int, default=10)
    aparser.add_argument("--criterion", help="Split quality criterion", type=str, default='gini')
    aparser.add_argument("--max-features", help="Sumber of features for a split", type=str, default='auto')
    aparser.add_argument("--max-depth", help="Depth of a tree", type=int, default=None)
    aparser.add_argument("--n-jobs", help="Number of CPU to train the forest,", type=int, default=-1)
    aparser.add_argument("--features", help="Feature to give to RF", type=str, default=None, choices=['HOG'])

    args = aparser.parse_args()

    ds = VITALMnist(28)
    mnist_model = MnistRandomForest(ds, gridsearch=True, features=args.features,
                                    n_estimators=args.n_estimators, criterion=args.criterion,
                                    max_features=args.max_features, max_depth=args.max_depth,
                                    n_jobs=args.n_jobs)
    mnist_model.train()
    mnist_model.evaluate()
