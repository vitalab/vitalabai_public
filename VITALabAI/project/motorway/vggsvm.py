import argparse

import numpy as np
from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.dataset.classification.motorway import VITALMotorway
from VITALabAI.model.classification.classification_utils import *
from keras import backend as K
from keras import layers, models
from keras.applications import VGG16
from sklearn import svm


class VggSVM(VITALabAiKerasAbstract):
    """
    Motorway Vgg class. Classify highway images based on VGG Imagenet-pretrained
    features and SVM.
    """
    def __init__(self, dataset: VITALabAiDatasetAbstract, C=0.05, **kwargs):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            C: float, SVM slack parameter (optional)
            **kwargs: dict with keywords to be used
        """
        super().__init__(dataset)
        self.C = C

    def _get_dataset_preprocessed_with_cnn(self):
        """ Takes every image of the dataset, feeds it to the VGG model and keeps the
        output of the last layer before the softmax.  This is a 4096-D vector.

        Returns:
            Tuple of ndarray with all the data preprocessed with the cnn
        """
        print('Converting the Motorway images to 4096-D feature vectors with VGG')

        # Create a function which returns the features before the last layer
        layer = self.model.layers[-3]
        f = K.function([K.learning_phase(), ] + self.model.inputs,
                       [layer.output])

        return self.dataset.get_dataset_preprocessed_with_cnn(f)

    def build_model(self):
        """ Replicates the VGG model.

        Returns:
            keras.Model object *not* compiled.
        """
        print('making model')
        inp = layers.Input(self.dataset.get_input_shape())
        vgg = VGG16(include_top=False, input_tensor=inp, weights='imagenet')
        x = vgg.output
        x = layers.Flatten(name='flatten')(x)
        x = layers.Dense(4096, activation='relu', name='fc1')(x)
        x = layers.Dropout(0.5)(x)
        x = layers.Dense(4096, activation='relu', name='fc2')(x)
        x = layers.Dropout(0.5)(x)
        x = layers.Dense(self.dataset.get_num_classes(), activation='softmax',
                         name='predictions')(x)
        model = models.Model(inp, x)

        return model

    def train(self, **kwargs):
        """ This method trains an SVM model on the 4096D VGG feature vectors.

        Args:
            **kwargs: dict with keywords to be used.
        """

        (self.X_train, self.Y_train), (self.X_test, self.Y_test) = \
            self._get_dataset_preprocessed_with_cnn()

        self.lin_clf = svm.LinearSVC(C=self.C)
        self.lin_clf.fit(self.X_train, self.Y_train)

        Y_pred = self.lin_clf.predict(self.X_train)
        tp = np.sum(Y_pred == self.Y_train)
        dataset_size = float(self.dataset.TOTAL_NUM_IMG)

        accuracy = (tp / ((1 - self.dataset.test_split) * dataset_size) * 100.0)
        print("Training accuracy: %.1f%%" % accuracy)

    def evaluate(self, **kwargs):
        """ This method runs the model on test data once training is over.

        Args:
            **kwargs:
        """
        Y_pred = self.lin_clf.predict(self.X_test)
        tp = np.sum(Y_pred == self.Y_test)
        dataset_size = float(self.dataset.TOTAL_NUM_IMG)

        accuracy = (tp / (self.dataset.test_split * dataset_size) * 100.0)

        print("Testing accuracy: %.1f%%" % accuracy)


if __name__ == '__main__':
    """
       Example usage of this script:
           python vggsvm.py --path=$MOTORWAY_DATASET
    """
    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to motorway dataset", type=str)
    args = aparser.parse_args()

    ds = VITALMotorway(data_path=args.path)
    model = VggSVM(ds)
    model.train()
    model.evaluate()
