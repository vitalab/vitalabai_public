import argparse
import csv
import datetime

import keras
from keras.callbacks import EarlyStopping, TensorBoard, CSVLogger

from VITALabAI.dataset.classification.motorway import VITALMotorway
from VITALabAI.model.classification.cnn import VITALCnn
from VITALabAI.model.classification.resnet import VITALResnet
from VITALabAI.model.classification.vgg16 import VITALVGG16
from VITALabAI.project.motorway.vggsvm import VggSVM

"""
Example usage of this script:

    python train_motorway.py --path=$MOTORWAY_DATASET --model=cnn
"""

if __name__ == '__main__':
    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to motorway dataset", type=str)
    aparser.add_argument("--model", help="Model to train",
                         choices=['cnn', 'vgg16', 'resnet', 'vggsvm'],
                         type=str, default='cnn')
    aparser.add_argument("--no_da", dest="use_da", action='store_false', help="Disable data augmentation")
    aparser.add_argument("--weights", type=str, dest="weights", help="Weights h5 file", default=None)
    aparser.add_argument("--C", type=float, dest="c", help="SVM slack parameter ", default=0.05)

    args = aparser.parse_args()
    ds = VITALMotorway(batch_size=64, data_path=args.path, use_da=args.use_da)

    optim = keras.optimizers.Adam(lr=0.0001)
    if args.model == 'vgg16':
        model = VITALVGG16(ds, optimizer=optim, loss_fn='categorical_crossentropy',
                           metrics=['accuracy'], weights='imagenet')
    elif args.model == 'resnet':
        model = VITALResnet(ds, optimizer=optim, loss_fn='categorical_crossentropy',
                            metrics=['accuracy'], weights='imagenet')
    elif args.model == 'cnn':
        model = VITALCnn(ds, optimizer='adam', loss_fn='categorical_crossentropy',
                         metrics=['accuracy'])
    else:
        model = VggSVM(ds, C=args.c)
        model.train()
        model.evaluate()
        exit(0)

    if args.weights is not None:
        print("Loading weights: {}".format(args.weights))
        model.load_weights(args.weights)

    now = datetime.datetime.now()
    name = "{}_{}{}".format(args.model, now.strftime("%Y-%m-%d-%H:%M"), ("_da" if args.use_da else ""))
    tensorboard = TensorBoard('logs/{}/'.format(name), write_graph=True,
                              write_images=True)
    early_stop = EarlyStopping(monitor='val_loss', patience=10, verbose=1)

    model.train(epochs=200, callbacks=[early_stop, tensorboard, CSVLogger(name + '.csv')])
    train_value, val_value, test_value = model.evaluate(save_errors=True)

    with open('results.csv', 'a') as csv_file:
        file_writer = csv.writer(csv_file, delimiter=',')
        file_writer.writerow([now.strftime("%Y-%m-%d-%H:%M"), args.model, 'da=' + str(args.use_da)])
        file_writer.writerow(['Epochs:', str(early_stop.stopped_epoch)])
        file_writer.writerow(['Set', 'loss', 'accuracy'])
        file_writer.writerow(['Train', str(train_value[0]), str(train_value[1])])
        file_writer.writerow(['Val', str(val_value[0]), str(val_value[1])])
        file_writer.writerow(['Test', str(test_value[0]), str(test_value[1])])
