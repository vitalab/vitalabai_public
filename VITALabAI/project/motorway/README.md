# Motorway Dataset Classificaion

```bash
python train_motorway.py --path=$MOTORWAY_DATASET --model=cnn
```

The following parameters are required:
- `--path` is the path to the folder containing the motorway dataset
- `--model` model used to train, choices are : 'cnn', 'vgg16', 'resnet', 'vggsvm'.

You can get these files in the VITAlab shared folder.

The following parameters are optional:
- `--weights` is the path to the file containing the pretrained weights
- `--C` the SVM slack parameter (Only for VGG Svm)
- `--no_da` disable data augmentation



For model-specific parameters, see their constructor.