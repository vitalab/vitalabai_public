[![Universite de Sherbrooke](https://www.usherbrooke.ca/communications/fileadmin/_migrated/pics/UdeS_nouveau_coul.72dpi_05.jpg)](http://www.usherbrooke.ca/)

[![Autolog](http://www.autolog.com/gx/autolog_logo_en.jpg)](http://www.autolog.com/)

# Autolog project
***
### VITAL laboratory - Universite de Sherbrooke
Contributors:

* Pierre-Marc Jodoin    [Pierre-Marc.Jodoin@USherbrooke.ca]
* Karl-Etienne Perron   [Karl-Etienne.Perron@USherbrooke.ca]
* Clement Zotti         [Clement.Zotti@USherbrooke.ca]

February 2016 - July 2016
***
## Description
The Autolog project is a deep learning framework aiming to guide the development 
and computation of various deep learning models for the classification of 
Autolog wood plank images. This is the result of a first
[Engage Grants](http://www.nserc-crsng.gc.ca/Professors-Professeurs/RPP-PP/Engage-Engagement_eng.asp) 
collaboration between [Autolog](http://www.autolog.com/) and
[Universite de Sherbrooke's](http://www.usherbrooke.ca/)
[VITAL laboratory](http://vital.dinf.usherbrooke.ca/).
***
## Requirements
In order to be able to use this framework as provided, please make sure to have 
at least the following installed:

* For deep learning:
    * [Python3](https://www.python.org/) (tested with 3.4)
    * [Keras 1.0.5](http://keras.io/) and up (tested with 1.0.6)
        * Note that Keras' code has to be altered if you want to do 
          "full image prediction". See "Keras and full image prediction" below.
        * Whereas Keras optionally offers you to install [HDF5](https://www.hdfgroup.org/HDF5/), we ***require it***;
    * [Theano](http://deeplearning.net/software/theano/install.html#install) (tested with 0.8.2)
    * [H5py](http://www.h5py.org/) (tested with 2.6)
    * [Numpy](http://www.numpy.org/) (tested with 1.11)
    * [Ordered set](https://pypi.python.org/pypi/ordered-set) (tested with 2.0.1)
    * [Scikit-learn](http://scikit-learn.org/stable/) (tested with 0.17.1)
* For dataset generation:
    * [Python2.5](https://www.python.org/) and up (tested with 2.7)
    * [OpenEXR](http://excamera.com/articles/26/doc/) for Python (tested with 1.2)

**The Autolog project has mostly been created around the usage of Keras and 
comes with a fully working example. The rest of this README will provide 
indications on how to run this example as well as explaining the general 
structure and how to develop your own code.**

**Other python modules may be required.**
***
## Project structure and files

The Autolog project code is divided into three different sections:

* Interface (`VITALabAI/`)
* Dataset (`VITALabAI/dataset/autolog/`)
* Models (`VITALabAI/model/autolog/`)

### Interface
The "Interface" files consist of high level code acting as the project's 
backbone.

Main files:

* `VITALabAiAbstract.py`
* `VITALabAiDatasetAbstract.py`
* `main.py`

The `*Abstract.py` files contain simple python interfaces that must be 
implemented by your models and dataset classes accordingly. This makes it easier 
to run different models and to ensure consistency. The `VITALabAIAbstract` 
interface declares the main methods related to the execution of your deep 
learning models (i.e. training, testing, etc.). This interface will be used 
within the `main.py` code in order to run your model properly. The 
`VITALabAIDatasetAbstract` interface declares the main methods related to data 
management (i.e. getting data, preprocessing, etc.). This interface 
should be used by your model classes in order to, amongst other things, acquire 
training, validation and testing data.

`main.py` is the entry point to your code's execution (See 
**Running a model** below). When running a model, **you must run** `main.py` 
**using python3.** In `main.py`, a proper execution sequence is already in place
 which will instanciate your model (which implements `VITALabAiAbstract`) and 
call the interface's methods in the right order.


**You should not have to modify any of these files**, only implement their 
interface and/or run them.

### Dataset
The "Dataset" files consist of all the files related to data management.

Main files:

* `autologdataset.py` (provided example)
* `hdf5generation/generateautologdataset.py` (provided example)
* `hdf5generation/autologclasses.py` (provided example)

The Autolog project already comes with a fully working example. 
`autologdataset.py` implements the `VITALabAiDatasetAbstract` interface. Note 
however that since the Autolog data does not fit in memory, we must use an 
iterator to get our data at each forward pass. The iterator code is provided 
(`datasetloader.py`, `dataset2D.py` and others), but has been taken from an older 
project which does not always comply to the interface which explains why some 
methods are not implementd properly in `autologdataset.py` 
(i.e. `next()`, `preprocess_dataset()`).

**The dataset which holds all the data has to be generated prior to running 
your models. This must be done using python2, not python3** (See 
**Generating the dataset** below). The code for generating the dataset 
(HDF5 files) can be found in the `hdf5generation` directory. Running 
`generateautologdataset.py` will process raw EXR images and create PNG images 
for easier visualization and also **two HDF5 files** 
(here, {dataset} simply acts as a placeholder):

* `{dataset}.hdf5`
* `Pos_{dataset}.hdf5`

`{dataset}.hdf5` contains data for all the processed images and is properly 
divided into the three different deep learning sets (i.e. train, valid and test)
. Data found in one set cannot be found in other sets. `Pos_{dataset}.hdf5` 
contains all the positional information necessary in order to retreive the 
different 61x61 pixels patches possible to get from the images found in the 
`{dataset}.hdf5` file.

In order to stay consistent between the dataset during its creation and its use 
in our models, `autologclasses.py` contains a simple list of the EXR images 
categories. **The list's declaration order and names have to be respected in 
order for our test results to make sense**. This is why, `autologclasses.py` is 
used by both the dataset and model files.

### Models
The "Models" files consist of general files and implemented deep learning models 
used for training and testing with the dataset's data.

Main files:

* `autologcnn.py` (provided example)
* `autologcnn_0.py` (provided example)
* `autologcnn_incp.py` (provided example)

The Autolog project already comes with a fully working Keras example. 
`autologcnn.py` is the provided example of how a CNN model can be implemented 
following the project's structure. It fully implements the `VITALabAiAbstract` 
interface except for the `build_model()` method. `autologcnn.py`'s class is, in 
fact, abstract. Since the goal of this project is to be able to try and test 
different models and model iterations, **the** `build_model()` **method has to 
be implemented by a child class.** For example, if you would like to train a 
simple CNN model, but also a CNN with 
[inception modules](http://www.cs.unc.edu/~wliu/papers/GoogLeNet.pdf), you don't
 need to duplicate any code, only to create a new python file with a 
child class of `AutologCNN` which implements the `build_model()` method 
**(see** `autologcnn_0.py` **and** `autologcnn_incp.py` **for more details)**.

**Since** `autologcnn` **is abstract, we cannot provide it directly to** 
`main.py` **for execution, but rather, the child classes.**
***

## Usage
In order to get up and running with the Autolog project, simply follow these 
steps:
### Generating the dataset
**If the dataset has already been generated (you already have access to the HDF5
 files), you can skip directly to "Running a model"**.

The following instructions will result in the creation of the HDF5, PNG and EXR 
files pertaining to the processed images. During the processing, 
**we trim any black borders from the images and force their width to 61 pixels 
while leaving their height intact.**

First make sure that you have a copy of the original EXR images organized as 
follows (here, {autolog_images} simply acts as a placeholder):

* {autolog_images}/
    * BarkPocket/
        * 17561R1-craw_32F.exr
        * 17561R1-gdep_32F.exr
        * 17561R1-t_angle_32F.exr
        * 17561R1-eccentricity_32F.exr
        * 17561R2-craw_32F.exr
        * 17561R2-gdep_32F.exr
        * etc.
    * BlueStain/
        * etc.
    * etc.

Since the images are EXR files, we need the **OpenEXR** python module. Sadly, 
this module has not been adapted to work with python3. Therefore, when 
generating the dataset, you must run `generateautologdataset.py` using **python2.**


First, you need to install the VITALabAI package into python2:

```bash
python2 setup.py develop --user
```

From `vitalabai/VITALabAI`, where:

* {autolog_images} is the name of the directory containing the images;
* {dataset} is the name you want to give to your HDF5 file;

After you can run the following:

    cd dataset/autolog/hdf5generation/
    python2 generateautologdataset.py -d {autolog_images} -n {dataset} -s

This should result in the creation of:

* Within the same directory as `generateautologdataset.py`:
    * `{dataset}.hdf5`    (The image dataset divided in three sections: "train",
 "valid" and "test")
    * `Pos_{dataset}.hdf5`    (The 61x61 patches coordinates within the images)
* Within the {autolog_images} directory:
    * A `png` folder    (The processed images as PNG files)
    * An `exr` folder   (The processed images as EXR files)

The `png` and `exr` folders are simply there for visualization and informational
 purposes. They are not useful to our models.

### Running a model
Once you have access to the HDF5 dataset, you are ready to create your own 
dataset and model classes (see `autologdataset.py` and `autologcnn.py + 
autologcnn_0.py` should you need examples).

Once you have successfully created a runnable model (a model that implements 
`VITALabAiAbstract`), in order to run the model, you must call your model 
through the `main.py` script. When running a model, you must run `main.py` using
 **python3.**. This script requires you to provide the following: 

* The full path to your model's class;
* The different parameters you wish to forward to your model (`main.py` will 
instantiate your model by providing it with those parameters);

You provide the parameters and their value separated by an equal sign (`=`) and 
the different parameters separated by a comma (`,`):

Your full command should be similar to this:

    python3 main.py VITALabAI.model.autolog.autologcnn_0.AutologCNN_0 -o \
    dataset_filepath=~/data/autolog.hdf5,epochs=25

For example, the provided `autolog_cnn.py` example requires you to give a distribution percentage for
 each class (Notice how you may also provide `sgd_...` parameters for the child `autolog_cnn_0.py` class):

    python3 main.py VITALabAI.model.autolog.autologcnn_0.AutologCNN_0 -o \
    dataset_filepath=~/data/autolog.hdf5,BarkPocket=10.0,HoneyComb=10.0,SoundWood=10.0, \
    VisualSkip=10.0,BlueStain=10.0,HearthStain=10.0,Knot=10.0,Shake=10.0, \
    UnsoundWood=10.0,WhiteSpeck=10.0,batch_size=100,axes=('b', 'c', 0, 1),dropout=0.5, \
    input_shape=(61, 61),loss='categorical_crossentropy',nb_mini_batch=10000, \
    nb_channels=3,nb_epochs=30,nb_neurons=32,preprocessing=[PreProcessNonZeroSTD()], \
    strides=(1, 1),use_weights_flag=0,weights_file_dirname='.',sgd_decay=1e-6, \
    sgd_learning_rate=0.0001,sgd_momentum=0.9,sgd_nesterov=True
***

### Keras and full image prediction
As of July 2016, Keras does not allow users to use their model in order to do 
**full image prediction**. As a matter of fact, Keras' code first checks 
for the inputs' shape prior to computing the result, preventing any image with a
 shape different than the "input shape" declared upon building the 
model to be processed.

The Autolog project's code comes with Keras version-specific fixes to that 
problem:

* fix_conv_1.0.5.patch;
* fix_conv_1.0.6.patch;

In order to be able to do "full image prediction", it is important that you 
apply those patches before trying to run your models.

However, it is possible that even after those fixes, Keras will still not let 
you provide inputs with a different shape. If it is not already done, 
note that you might have to comment out the following lines in 
`keras/engine/training.py`:

    # check shapes compatibility
    if shapes:
        for i in range(len(names)):
            if shapes[i] is None:
                continue
            array = arrays[i]
            if len(array.shape) != len(shapes[i]):
                raise Exception('Error when checking ' + exception_prefix +
                                ': expected ' + names[i] +
                                ' to have ' + str(len(shapes[i])) +
                                ' dimensions, but got array with shape ' +
                                str(array.shape))
            for j, (dim, ref_dim) in enumerate(zip(array.shape, shapes[i])):
                if not j and not check_batch_dim:
                    # skip the first axis
                    continue
                if ref_dim:
                    if ref_dim != dim:
                        raise Exception('Error when checking ' + exception_prefix +
                                        ': expected ' + names[i] +
                                        ' to have shape ' + str(shapes[i]) +
                                        ' but got array with shape ' +
                                        str(array.shape))

***

