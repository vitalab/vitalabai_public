"""
VITAL Lab - Universite de Sherbrooke

Pierre-Marc Jodoin
Karl-Etienne Perron
Clement Zotti
"""

import os

from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.layers.core import Dropout, Flatten
from keras.models import Sequential
from keras.optimizers import SGD
from keras.regularizers import l2

from VITALabAI.utils import activation
from . import autologcnn


class AutologCNN_0(autologcnn.AutologCNN):
    """
    Child autolog project Keras CNN class. Implements the "build_model" method for a specific model.
    This class is to be called through "main.py"

    Conventional CNN
    """

    # ----------------------------------------------------------------------------------------------
    # PUBLIC METHODS

    def __init__(self, dataset_filepath, batch_size=100, sgd_decay=1e-6,
                 sgd_learning_rate=0.0001, sgd_momentum=0.9, sgd_nesterov=True, **kwargs):
        """
        Model specific constructor

        Parameters
        ----------
        dataset_filepath : str
            Full path to the hdf5 dataset file;
        batch_size : int
            Number of examples processed per forward pass;
        sgd_decay : float
            Learning rate decay factor;
        sgd_learning_rate : float
            Training learning rate;
        sgd_momentum : float
            Learning momentum rate;
        sgd_nesterov : bool
            If true, we apply Nesterov momentum during training;
        **kwargs : ... (REST OF PARAMETERS)
            Parameters to be forwarded to the mother class;

        Returns
        -------
        None
        """

        super().__init__(batch_size, dataset_filepath, **kwargs)
        self.sgd_decay = sgd_decay
        self.sgd_learning_rate = sgd_learning_rate
        self.sgd_momentum = sgd_momentum
        self.sgd_nesterov = sgd_nesterov

        self.name = os.path.basename(__file__)

    def build_model(self):
        """
        This method builds and compiles a model.
        (This method implements the mother class' abstract method of the same name).

        Returns
        -------
        None
        """

        print("Building model ...")

        # Model by Pierre-Marc Jodoin
        self.model = Sequential()

        self.model.add(
            Convolution2D(self.nb_neurons, 4, 4, activation='relu', W_regularizer=l2(0.02),
                          b_regularizer=l2(0.02),
                          input_shape=(self.dataset.nb_channels, self.dataset.input_shape[0],
                                       self.dataset.input_shape[1])))
        self.model.add(Dropout(self.dropout))
        self.model.add(MaxPooling2D())

        self.model.add(
            Convolution2D(self.nb_neurons * 2, 4, 4, activation='relu', W_regularizer=l2(0.02),
                          b_regularizer=l2(0.02)))
        self.model.add(Dropout(self.dropout))
        self.model.add(MaxPooling2D())

        self.model.add(
            Convolution2D(self.nb_neurons * 4, 4, 4, activation='relu', W_regularizer=l2(0.02),
                          b_regularizer=l2(0.02)))
        self.model.add(Dropout(self.dropout))

        self.model.add(
            Convolution2D(self.nb_neurons * 4, 9, 9, activation='relu', W_regularizer=l2(0.02),
                          b_regularizer=l2(0.02)))
        self.model.add(Dropout(self.dropout))

        self.model.add(
            Convolution2D(self.dataset.nb_classes, 2, 2,
                          activation=activation.across_channel_softmax,
                          W_regularizer=l2(0.02), b_regularizer=l2(0.02)))

        self.model.add(Flatten())
        print("Building model ... Done!")

        self.optimizer = SGD(lr=self.sgd_learning_rate, decay=self.sgd_decay,
                             momentum=self.sgd_momentum,
                             nesterov=self.sgd_nesterov)

        print("Compiling model ...")
        self.model.compile(loss=self.loss, optimizer=self.optimizer, metrics=['accuracy'])
        print("Compiling model ... Done!")

        self.name = "{0}_{1}_{2}".format(self.name, self.optimizer.lr.get_value(), self.nb_neurons)
