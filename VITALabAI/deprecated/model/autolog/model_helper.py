import numpy as np

from keras.utils import generic_utils
import keras.backend as K
import sklearn.metrics as skl


def shuffle(rng, x, y):
    """
    Method to suffle sample on the first axis.

    Parameters
    ----------
    rng: RandomState
    	RandomState object from numpy used to shuffle the data.

    x: numpy.ndarray
    	Numpy array containing the example to train on.

    y: numpy.ndarray
    	Numpy array containing the target for each x.

    Returns
    -------
    tuple of the shuffled and sync x and y.
    """
    idx = np.arange(x.shape[0])
    rng.shuffle(idx)
    return (x[idx], y[idx])


def model_test(model, dtest, rng, dirname, model_name, nb_classes, use_best_acc=True):
    """
    Function to test a model.

    Parameters
    ----------
    model: keras model
        Keras model object. We use it to call the test_on_batch() methods.

    dtest: dataset2D object
        Test dataset iterator.

    rng: numpy.random.RandomState object
        Random state object given to shuffler the data for the training.

    dirname: string
        Where to get the weights for the testing.

    model_name: string
        The model class' name. Usually the name of the class' file.

    nb_classes: int
        Number of classes the model must predict for.

    use_best_acc : bool
            If True, will load the weights with the best accuracy;
            Else, will load the weights with the best loss;

    Returns
    -------

    (test_loss, test_accuracy),
        tuple with list of loss and accuracy.
    """

    test_nll, test_acc = [], []

    model.load_weights("{0}/{1}_model_best_{2}.hdf5".format(dirname, model_name,
                                                            "acc" if use_best_acc else "nll"))

    # Create keras progress bar
    p_bar = generic_utils.Progbar(dtest.get_num_examples())

    # Create confusion matrix
    conf_matrix = np.zeros((nb_classes, nb_classes))

    # Test
    print("Test:")
    for xt, yt in dtest:
        xt, yt = shuffle(rng, xt, yt)
        nll, acc = model.test_on_batch(xt, yt)
        p_bar.add(xt.shape[0], values=[('loss', nll), ('acc', acc)])
        conf_matrix += skl.confusion_matrix([np.argmax(x) for x in model.predict(xt)],
                                            [np.argmax(y) for y in yt])
    test_nll.append(nll)
    test_acc.append(acc)

    return test_nll, test_acc, np.int_(conf_matrix)


def model_train(model, dtrain, dvalid, nb_epochs, rng, base_name, model_name):
    """
    Function to train a model by batch. This function uses a kind of early stopping
    to stop the training. In the end we save one model for the best accuracy
    and another one for the loss.

    Parameters
    ----------
    model: keras model
	Keras model object. We use it to call the train_on_batch() and test_on_batch()
	methods.

    dtrain: dataset2D object
	Train dataset iterator.

    dvalid: dataset2D object
	Valid dataset iterator.

    nb_epochs: int
	Number of epochs used to train.

    rng: numpy.random.RandomState object
	Random state object given to shuffler the data for the training.

    base_name: string
	Where to save all the weights for the training.

    Returns
    -------

    model, model trained.

    (train_loss, train_accuracy),
	tuple with list of loss and accuracy over epochs on train.

    (valid_loss, valid_accuracy),
	tuple with list of loss and accuracy over epochs on valid.
    """

    old_acc, old_nll, old_haus = (float("-inf"), float("inf"), float("inf"))
    train_nll, train_acc = [], []
    valid_nll, valid_acc = [], []
    lr_values = []
    count_epochs = 0

    # Training loop
    while(count_epochs < nb_epochs):
        # Create keras progress bar
        p_bar = generic_utils.Progbar(dtrain.get_num_examples())

        # Training
        print("Train:")
        for xt, yt in dtrain:
            xt, yt = shuffle(rng, xt, yt)
            nll, acc = model.train_on_batch(xt, yt)
            p_bar.add(xt.shape[0], values=[('loss', nll), ('acc', acc),
                                           ('epochs', count_epochs)])
        train_nll.append(nll)
        train_acc.append(acc)

        p_bar = generic_utils.Progbar(dvalid.get_num_examples())

        # Validation
        print("Valid:")
        for xv, yv in dvalid:
            nll, acc = model.test_on_batch(xv, yv)
            p_bar.add(xv.shape[0], values=[('loss', nll), ('acc', acc),
                                           ('epochs', count_epochs)])
        valid_nll.append(nll)
        valid_acc.append(acc)

        count_epochs += 1

        # early stopping accuracy
        if old_acc < acc:
            model.save_weights(
                "{0}/{1}_model_best_acc.hdf5".format(base_name, model_name), overwrite=True)
            old_acc = acc

        # early stopping loss
        if old_nll > nll:
            model.save_weights(
                "{0}/{1}_model_best_nll.hdf5".format(base_name, model_name), overwrite=True)
            old_nll = nll

    return (model, (train_nll, train_acc), (valid_nll, valid_acc), lr_values)


def model_predict(model, test, preprocessing, postprocessing, f5py, base_name, model_name, which_weights='acc'):
    """
    Function to make the prediction given an prediction iterator.

    Parameters
    ----------
    model: Keras model
	Model already trained.

    test: prediction iterator
    	Iterator that takes the model in input and makes the model predict patches.

    preprocessing: list of function or object 
    	The object has to implement the __call__() method. Each preprocessing is
    	applied on a whole image in order in the list.

    postprocessing: list of function or object
    	see preprocessing

    f5py: File
    	HDF5 file where the results of the prediction are saved.

    base_name: string
    	Where to load the weights of the model.

    which: string
	Select which weight to load, 'acc' is the best accuracy, 'nll' is the best loss.
    """
    # load weight and start prediction
    model.load_weights("{0}/{1}_model_best_{2}.hdf5".format(base_name, model_name, which_weights))

    last_layers = model.layers[-2]

    # Extract GPU function and remove the flattened layers
    pred_f = K.function([K.learning_phase(), ] + model.inputs,
                        last_layers.output)

    def predict_func(X_in):
        """
        Internal function used to set the model in prediction mode.
        """
        return pred_f([0, ] + X_in)

    # initialize the iterator with the GPU function
    test.initialize(predict_func, preprocessing)

    # Start generating the prediction
    for name, pred, gt, img, prob, voxel in test:
        f5py.create_group(name)
        
        # Save the voxel size 
        f5py[name].attrs.create('voxel', voxel)

        # Save each image inside the hdf5 file 
        f5py[name].create_dataset('IMG', data=img.astype(np.float32))
        f5py[name].create_dataset('GT', data=gt.astype(np.float32))
        f5py[name].create_dataset('PRED', data=pred.astype(np.float32))
        f5py[name].create_dataset('PROB', data=prob.astype(np.float32))
        for post in postprocessing:
            pred = post(pred, img)
        f5py[name].create_dataset(
            'PRED_Post', data=pred.astype(np.float32))
