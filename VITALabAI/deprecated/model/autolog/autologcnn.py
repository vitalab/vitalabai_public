"""
VITAL Lab - Universite de Sherbrooke

Pierre-Marc Jodoin
Karl-Etienne Perron
Clement Zotti
"""

import abc
import csv
import os
import re
import sys
from io import StringIO

import h5py
import numpy as np
import sklearn.metrics as skl

from VITALabAI.deprecated.dataset.autolog.autologdataset import AutologDataset
from .model_helper import model_train, model_test
from VITALabAI.VITALabAiAbstract import VITALabAiAbstract
from VITALabAI.deprecated.dataset.autolog.hdf5generation.autologclasses import CLASSES
from VITALabAI.deprecated.dataset.autolog.preprocess import PreProcessNonZeroSTD


class AutologCNN(VITALabAiAbstract, metaclass=abc.ABCMeta):
    """
    (ABSTRACT) Main autolog project Keras CNN class. It implements most of the methods common to
    our vision of autolog CNN training and testing. However, in order to be able to test different
    models and/or model iterations quickly, the "build_model" method has to be implemented in a
    child class. It is the child classes that should be called through the "main.py" script, never
    this one directly. This way, each child class is free to process parameters and/or override
    methods as necessary for its own model and goals. For example, a child class may require
    additionnal parameters for a specific model optimizer or layer while still leveraging the
    mother class' methods.
    """

    # ------------------------------------------------------------------------------------------------------------------
    # PUBLIC METHODS

    def __init__(self, dataset_filepath, batch_size=100, axes=('b', 'c', 0, 1), dropout=0.5,
                 input_shape=(61, 61), loss='categorical_crossentropy', nb_mini_batch=10000,
                 nb_channels=3, nb_epochs=30, nb_neurons=32, preprocessing=[PreProcessNonZeroSTD()],
                 strides=(1, 1), use_weights_flag=0, weights_file_dirname='.', **distribution):
        """
        Model constructor.

        Parameters
        ----------
        dataset_filepath : str
            Full path to the hdf5 dataset file;
        batch_size : int
            Number of examples processed per forward pass;
        axes : tuple
            Dataset axes (for example ('b', 'c', 0, 1))
                b: batch axes;
                c: channel axes;
                0: shape width;
                1: shape height;
        dropout : float
            Ratio of neurons that will not be updated during training;
        input_shape : (int,int)
            Input data shape
        loss : str
            Name of the Keras loss function to use in the model;
        nb_channels : int
            Number of channels to use during training and testing.
            3 = RGB
            4 = 3 + depth
            5 = 4 + angle
            6 = 5 + eccentricity
        nb_epochs : int
            Number of times we process all the gathered examples;
        nb_mini_batch : int
            Number of forward passes to do;
        nb_neurons : int
            Relative number of neurons to use in the model's layers;
        preprocessing : <preprocess> list
            List of preprocessing functions to apply on the data.
            See ".../dataset/autolog/preprocess.py for details;
        strides : (int,int)
            Filtering stride on each axis;
        use_weights_flag : int
            If 0, will not use pre-trained weights;
            If 1, will load the pre-trained weights with the best accuracy;
            If 2, will load the pre-trained weights with the best loss;
        weights_file_dirname : str
            Weights file parent directory's name;
        distribution : ... (REST OF PARAMETERS)
            The rest of the parameters should be used to provide the desired distribution percentage
            for each category of planks. To do so, provide "class name=distribution percentage"
            parameters. For example, when providing parameters to main.py:
            python3 main.py ...,SoundWood=10.0,Knot=10.0,BlueStain=10.0,...

            Any other parameter that is not a plank category will simply be ignored;

        Returns
        -------
        None
        """

        self.dropout = dropout
        self.loss = loss
        self.nb_epochs = nb_epochs
        self.nb_neurons = nb_neurons
        self.use_weights_flag = use_weights_flag
        self.weights_file_dirname = weights_file_dirname

        self.dataset = AutologDataset(dataset_filepath, batch_size, axes, input_shape,
                                      nb_mini_batch, nb_channels, preprocessing, strides,
                                      **distribution)

        self.name = os.path.basename(__file__)

        self.rng = np.random.RandomState(1337)

        self.full_image_predictions = None
        self.model = None
        self.optimizer = None
        self.test_results = None
        self.train_results = None

    def load_dataset(self):
        """
        This method obtains training, validation and test patches from the dataset.

        Returns
        -------
        None
        """

        print("Loading dataset ...")
        self.dataset.generate_dataset()
        print("Loading dataset ... Done!")

    def load_model(self):
        """
        This method loads pre-trained weights for our model.

        Returns
        -------
        None
        """

        if self.use_weights_flag in [1, 2]:
            print("Loading model ...")

            model_weights_filename = "{0}/{1}_model_best_{2}.hdf5".format(
                self.weights_file_dirname, self.name,
                "acc" if self.use_weights_flag else "nll")

            print("Using weights from file: {}".format(model_weights_filename))

            self.model.load_weights(model_weights_filename)

            print("Loading model ... Done!")

    @abc.abstractmethod
    def build_model(self):
        """
        (ABSTRACT) This method should build and compile a model. Should be implemented by child
        classes.

        Returns
        -------
        None
        """

        pass

    def model_summarize(self):
        """
        This method produces a full model summary (parameters, structure, etc).

        Returns
        -------
        : str
            Model summary string;
        """

        # Since model.summary() prints to the console, we capture the console's output in a string
        sys.stdout = my_output = StringIO()
        self.model.summary()
        summary_string = my_output.getvalue()
        sys.stdout = sys.__stdout__

        return summary_string

    def train(self):
        """
        This method trains the model using data loaded from the dataset.

        Returns
        -------
        None
        """

        print("Training model ...")
        self.load_model()
        if self.use_weights_flag not in [1, 2]:
            self.train_results = model_train(self.model, self.dataset.train_data,
                                             self.dataset.valid_data, self.nb_epochs, self.rng, '.',
                                             self.name)
        print("Training model ... Done!")

    def test(self):
        """
        This method applies the (trained) model onto test data.

        Returns
        -------
        None
        """

        print("Testing model ...")

        self.test_results = model_test(self.model, self.dataset.test_data, self.rng, '.', self.name,
                                       len(self.dataset.data_classes[1]),
                                       self.use_weights_flag == 1)
        print("Testing model ... Done!")

        print("Full image prediction ...")
        self.full_image_predictions = self._full_image_prediction()
        print("Full image prediction ... Done!")

    def save_model(self):
        """
        This method saves the model's weights.
        (Not applicable in our case since the weights are saved automatically during training)

        Returns
        -------
        None
        """

        pass

    def export_results(self):
        """
        This method produces files containing the various data we consider useful after training
        and testing.

        Returns
        -------
        None
        """
        print("Exporting results ...")
        res_file = csv.writer(open("{0}.csv".format(self.name), "w"))
        res_file.writerow([self.name])
        res_file.writerow([""])

        self._save_dataset_info(res_file)
        self._save_hyperprameters_info(res_file)
        self._save_test_info(res_file)
        self._save_training_info(res_file)
        self._save_model_info(res_file)
        self._save_full_image_prediction_info(res_file)
        print("Results exportation ... Done!")

    # ----------------------------------------------------------------------------------------------
    # PRIVATE METHODS

    def _compute_recalls(self, confusion_matrix):
        """
        This private method computes the recall for each class based on a confusion matrix;

        Parameters
        ----------
        confusion_matrix : <int> 2D array
            Confusion matrix;

        Returns
        -------
        : <float> array
            Class recall vector;
        """
        recall_vector = []
        for i in range(len(confusion_matrix)):
            recall_val = (confusion_matrix[i][i]*100.0)/np.sum(confusion_matrix[i])
            recall_vector.append(recall_val if not np.isnan(recall_val) else 0.0)
        return recall_vector

    def _full_image_prediction(self, set_name='test'):
        """
        This private method uses the model to make full image prediction on its dataset's data.

        Parameters
        ----------
        set_name : str
            Name of the dataset set from which we get images for the full image prediction
            ('test','train','valid');

        Returns
        -------
        : tuple
            Full image prediction results:
            : str
                Predicted image's name;
            : str
                Ground truth class name;
            : str
                Prediction class name;
            : <float> list
                Prediction histogram;
        """

        dataset = h5py.File(self.dataset.dataset_filepath, 'r')
        dataset_set = dataset[set_name]

        reg = re.compile("{}|{}".format(*self.dataset.data_classes[1]))
        keys = [k for k in dataset_set.keys() if len(reg.findall(k)) > 0]

        self.load_model()

        pred_res = []
        for key in keys:
            plank = dataset_set[key]
            img = plank['img'][0][:self.dataset.nb_channels]
            gt = plank['gt']

            for i in range(img.shape[0]):
                for pre in self.dataset.preprocessing:
                    img[i] = pre(img[i])

            raw_pred = self.model.predict(img[np.newaxis, ...])

            nb_patches = raw_pred.shape[1]/self.dataset.nb_classes

            pred_histogram = np.sum(raw_pred.reshape(
                self.dataset.nb_classes, -1), axis=1)/nb_patches

            image_pred = self.dataset.data_classes[1][np.argmax(pred_histogram)]

            cur_res = [plank.name, CLASSES[np.argmax(gt.value)], image_pred, pred_histogram]

            pred_res.append(cur_res)

        return pred_res

    def _save_dataset_info(self, res_file):
        """
        This private method exports dataset information

        Parameters
        ----------
        res_file : file
            Results export file;

        Returns
        -------
        None
        """

        res_file.writerow([""])
        res_file.writerow(["DATASET INFO:"])
        info = [self.dataset.axes, self.dataset.batch_size, self.dataset.dataset_filepath,
                self.dataset.input_shape, self.dataset.nb_mini_batch, self.dataset.nb_channels,
                self.dataset.strides]
        for pre in self.dataset.preprocessing:
            info.append(pre.__class__.__name__)
        res_file.writerow(["axes", "batch_size", "dataset file path", "input shape",
                           "nb mini batch", "nb channels", "strides", "preprocessing"])
        res_file.writerow(info)

    def _save_hyperprameters_info(self, res_file):
        """
        This private method exports model parameters information

        Parameters
        ----------
        res_file : file
            Results export file;

        Returns
        -------
        None
        """

        res_file.writerow([""])
        res_file.writerow(["HYPERPARAMETERS:"])
        res_file.writerow(["dropout", "loss", "name", "nb epochs", "nb neurons"])
        res_file.writerow([self.dropout, self.loss, self.name, self.nb_epochs, self.nb_neurons])

    def _save_test_info(self, res_file):
        """
        This private method exports our test results

        Parameters
        ----------
        res_file : file
            Results export file;

        Returns
        -------
        None
        """

        res_file.writerow([""])
        res_file.writerow(["TEST RESULTS:"])
        res_file.writerow(["test loss", "test accuracy"])
        res_file.writerow([self.test_results[0][0], self.test_results[1][0]*100])

        res_file.writerow([""])
        res_file.writerow(["[[confusion matrix]]"])
        classes_row = [" GT↓ \ PRED→ "]
        classes_row.extend(self.dataset.data_classes[1])
        res_file.writerow(classes_row)

        confusion_matrix = self.test_results[2]

        recall_vector = self._compute_recalls(confusion_matrix)

        for i in range(len(self.dataset.data_classes[1])):
            b = [self.dataset.data_classes[1][i]]
            b.extend(confusion_matrix[i])
            res_file.writerow(b)
        res_file.writerow([""])
        recall_info = ["recall (%): "]
        recall_info.extend(recall_vector)
        res_file.writerow(recall_info)

    def _save_training_info(self, res_file):
        """
        This private method exports our training results

        Parameters
        ----------
        res_file : file
            Results export file;

        Returns
        -------
        None
        """

        if not (self.use_weights_flag):
            res_file.writerow([""])
            res_file.writerow(["VALIDATION RESULTS:"])
            res_file.writerow(["validation loss", "validation accuracy"])
            res_file.writerow([np.min(self.train_results[2][0]),
                               np.max(self.train_results[2][1])*100])

            res_file.writerow([""])
            res_file.writerow(["TRAINING HISTORY:",
                               "{0} epochs".format(len(self.train_results[1][0]))])
            res_file.writerow(["training loss", "training accuracy", "validation loss",
                               "validation accuracy"])

            for i in range(len(self.train_results[1][0])):
                res_file.writerow([self.train_results[1][0][i], self.train_results[1][1][i]*100,
                                   self.train_results[2][0][i],
                                   self.train_results[2][1][i]*100])

        else:
            res_file.writerow([""])
            res_file.writerow(["VALIDATION RESULTS:"])
            res_file.writerow(["USED WEIGHTS: NO VALIDATION RESULTS"])

            res_file.writerow([""])
            res_file.writerow(["TRAINING HISTORY:"])
            res_file.writerow(["USED WEIGHTS: NO TRAINING HISTORY"])

    def _save_model_info(self, res_file):
        """
        This private method exports our training results

        Parameters
        ----------
        res_file : file
            Results export file;

        Returns
        -------
        None
        """

        summary_string = self.model_summarize()

        res_file.writerow([""])
        res_file.writerow(["MODEL DETAILS"])
        for r in summary_string.splitlines():
            res_file.writerow([r])

    def _save_full_image_prediction_info(self, res_file):
        """
        This private method exports results related to the full image prediction

        Parameters
        ----------
        res_file : file
            Results export file;

        Returns
        -------
        None
        """

        res_file.writerow([""])
        res_file.writerow(["FULL IMAGE PREDICTION RESULTS:"])

        res_file.writerow([""])
        res_file.writerow(["[[confusion matrix]]"])
        classes_row = [" GT↓ \ PRED→ "]
        classes_row.extend(self.dataset.data_classes[1])
        res_file.writerow(classes_row)

        confusion_matrix = skl.confusion_matrix([CLASSES.index(i[1]) for i in
                                                 self.full_image_predictions],
                                                [CLASSES.index(i[2]) for i in
                                                 self.full_image_predictions])
        recall_vector = self._compute_recalls(confusion_matrix)

        for i in range(len(self.dataset.data_classes[1])):
            b = [self.dataset.data_classes[1][i]]
            b.extend(confusion_matrix[i])
            res_file.writerow(b)
        res_file.writerow([""])
        recall_info = ["recall (%): "]
        recall_info.extend(recall_vector)
        res_file.writerow(recall_info)

        res_file.writerow([""])
        res_file.writerow(["global accuracy (%)"])
        res_file.writerow([[self.full_image_predictions[i][1] ==
                            self.full_image_predictions[i][2] for i in
                            range(len(self.full_image_predictions))].count(True)*100/(len(
                             self.full_image_predictions))])
        res_file.writerow(["", "", "", "[[histogram]]"])
        fip_header = ["image", "gt", "pred"]
        for dc in self.dataset.data_classes[1]:
            fip_header.extend([dc])
        res_file.writerow(fip_header)
        for fip in self.full_image_predictions:
            fip_row = [fip[0], fip[1], fip[2]]
            for c in fip[3]:
                fip_row.extend([c])
            res_file.writerow(fip_row)


if not __name__ == '__main__':
    print('Imported {} ...'.format(os.path.basename(__file__)))
