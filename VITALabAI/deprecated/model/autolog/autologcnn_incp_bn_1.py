"""
VITAL Lab - Universite de Sherbrooke

Pierre-Marc Jodoin
Karl-Etienne Perron
Clement Zotti
"""

import os

from keras.layers import Input, merge, Flatten, Dropout
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.optimizers import SGD
from keras.regularizers import l2

from VITALabAI.utils import activation
from . import autologcnn


class AutologCNN_INCP_BN_1(autologcnn.AutologCNN):
    """
    Child autolog project Keras CNN class. Implements the "build_model" method for a specific model.
    This class is to be called through "main.py"

    Inception modules w/ Batch normalization + FC Layers
    """

    # ----------------------------------------------------------------------------------------------
    # PUBLIC METHODS

    def __init__(self, dataset_filepath, batch_size=100, sgd_decay=1e-6,
                 sgd_learning_rate=0.00001, sgd_momentum=0.9, sgd_nesterov=True, **kwargs):
        """
        Model specific constructor

        Parameters
        ----------
        dataset_filepath : str
            Full path to the hdf5 dataset file;
        batch_size : int
            Number of examples processed per forward pass;
        sgd_decay : float
            Learning rate decay factor;
        sgd_learning_rate : float
            Training learning rate;
        sgd_momentum : float
            Learning momentum rate;
        sgd_nesterov : bool
            If true, we apply Nesterov momentum during training;
        **kwargs : ... (REST OF PARAMETERS)
            Parameters to be forwarded to the mother class;

        Returns
        -------
        None
        """

        super().__init__(batch_size, dataset_filepath, **kwargs)
        self.sgd_decay = sgd_decay
        self.sgd_learning_rate = sgd_learning_rate
        self.sgd_momentum = sgd_momentum
        self.sgd_nesterov = sgd_nesterov

        self.name = os.path.basename(__file__)

    def build_model(self):
        """
        This method builds and compiles a model.
        (This method implements the mother class's abstract method of the same name).

        Returns
        -------
        None
        """

        print("Building model ...")

        # Inception module #1 (61x61 -->)

        input0 = Input(shape=(self.dataset.nb_channels, self.dataset.input_shape[0],
                              self.dataset.input_shape[1]))

        tower_1 = Convolution2D(self.nb_neurons, 1, 1, border_mode='same',
                                activation='relu')(input0)
        tower_1 = Convolution2D(self.nb_neurons, 3, 3, border_mode='same',
                                activation='relu')(tower_1)

        tower_2 = Convolution2D(self.nb_neurons, 1, 1, border_mode='same',
                                activation='relu')(input0)
        tower_2 = Convolution2D(self.nb_neurons, 5, 5, border_mode='same',
                                activation='relu')(tower_2)

        tower_3 = MaxPooling2D((3, 3), strides=(1, 1), border_mode='same')(input0)
        tower_3 = Convolution2D(self.nb_neurons, 1, 1, border_mode='same',
                                activation='relu')(tower_3)

        output = merge([tower_1, tower_2, tower_3], mode='concat', concat_axis=1)

        output = BatchNormalization()(output)

        # Inception module #2 (30x30 -->)

        input = MaxPooling2D()(output)

        tower_1 = Convolution2D(self.nb_neurons, 1, 1, border_mode='same',
                                activation='relu')(input)
        tower_1 = Convolution2D(self.nb_neurons, 3, 3, border_mode='same',
                                activation='relu')(tower_1)

        tower_2 = Convolution2D(self.nb_neurons, 1, 1, border_mode='same',
                                activation='relu')(input)
        tower_2 = Convolution2D(self.nb_neurons, 5, 5, border_mode='same',
                                activation='relu')(tower_2)

        tower_3 = MaxPooling2D((3, 3), strides=(1, 1), border_mode='same')(input)
        tower_3 = Convolution2D(self.nb_neurons, 1, 1, border_mode='same',
                                activation='relu')(tower_3)

        output = merge([tower_1, tower_2, tower_3], mode='concat', concat_axis=1)

        output = BatchNormalization()(output)

        # Inception module #3 (15x15 -->)

        input = MaxPooling2D()(output)

        tower_1 = Convolution2D(self.nb_neurons, 1, 1, border_mode='same',
                                activation='relu')(input)
        tower_1 = Convolution2D(self.nb_neurons, 3, 3, border_mode='same',
                                activation='relu')(tower_1)

        tower_2 = Convolution2D(self.nb_neurons, 1, 1, border_mode='same',
                                activation='relu')(input)
        tower_2 = Convolution2D(self.nb_neurons, 5, 5, border_mode='same',
                                activation='relu')(tower_2)

        tower_3 = MaxPooling2D((3, 3), strides=(1, 1), border_mode='same')(input)
        tower_3 = Convolution2D(self.nb_neurons, 1, 1, border_mode='same',
                                activation='relu')(tower_3)

        output = merge([tower_1, tower_2, tower_3], mode='concat', concat_axis=1)

        output = BatchNormalization()(output)

        # FC Layers

        fc_shape_x = output._keras_shape[2]
        fc_shape_y = output._keras_shape[3]

        layer = Convolution2D(fc_shape_x*fc_shape_y, fc_shape_x, fc_shape_y, activation='relu',
                              W_regularizer=l2(0.02), b_regularizer=l2(0.02))(output)
        layer = Dropout(self.dropout)(layer)

        layer = Convolution2D(fc_shape_x*fc_shape_y, 1, 1, activation='relu', W_regularizer=l2(0.02)
                              , b_regularizer=l2(0.02))(layer)
        layer = Dropout(self.dropout)(layer)

        layer = Convolution2D(self.dataset.nb_classes, 1, 1,
                              activation=activation.across_channel_softmax, W_regularizer=l2(0.02),
                              b_regularizer=l2(0.02))(layer)

        layer = Flatten()(layer)

        self.model = Model(input=input0, output=layer)

        print("Building model ... Done!")

        self.optimizer = SGD(lr=self.sgd_learning_rate, decay=self.sgd_decay,
                             momentum=self.sgd_momentum,
                             nesterov=self.sgd_nesterov)

        print("Compiling model ...")
        self.model.compile(loss=self.loss, optimizer=self.optimizer, metrics=['accuracy'])
        print("Compiling model ... Done!")

        self.name = "{0}_{1}_{2}".format(self.name, self.optimizer.lr.get_value(), self.nb_neurons)
