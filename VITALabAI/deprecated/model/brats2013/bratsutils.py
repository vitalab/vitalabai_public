from __future__ import absolute_import

from os import listdir
from os.path import join

import matplotlib
import numpy as np

from VITALabAI.deprecated.dataset.brats2013 import BRATS_rate_results

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import os


def save_log(log, filename):
    """
    saves the training logs

    Parameters:
    ----------
    log : dictionary
        contains train loss, valid loss, learning rate and epoch
    filename : string
        path to save the log file
    """
    elements = list(log.keys())
    with open(filename, 'a') as f:
        string = str(log[elements[3]][-1]) + ',' + str(log[elements[2]][-1]) + \
            ',' + str(log[elements[0]][-1]) + ',' + \
            str(log[elements[1]][-1]) + '\n'
        f.write(string)


def fix_dataset_axes(A):
    """
    Changes the axis of the data matrix to be combatible with the expected convolutional space imposed by keras.
    """

    return A.transpose((3, 0, 1, 2))


def prepare_padded_brain(slices, patch_shrink_size, depth, height, width):
    """
    Padds the original brain according to the shrink size of the model.

    Parameters:
    ----------
    slices : ndarray
        containing the original brain
    patch_shrink_size : int
            The number of pixels that the input image to the model is shrinked in the output layer.
    depth , height , width : int

    Returns:
    -------
        ndarray

    """
    first_half = patch_shrink_size // 2
    second_half = patch_shrink_size - first_half

    padded_brain = np.zeros(
        (depth, height + patch_shrink_size, width + patch_shrink_size))
    padded_brain_height, padded_brain_width = padded_brain.shape[1:3]

    padded_brain[:, first_half:padded_brain_height - second_half, first_half:padded_brain_width -
                 second_half] = slices   # shape = (depth, height, wideth, channels)
    return padded_brain


def write_dice(output_folder):
    """
    Reads dice values from individual csv files and unifies them in one file.

    Parameters:
    ----------
    output_folder : string
    """
    csv_list = [f for f in listdir(
        output_folder) if '.csv' in f and 'Dice_score.csv' not in f]
    csv_text = ''
    csv_text += 'item,Complete,Core,Enhanced\n'
    for csv_item in csv_list:
        with open(join(output_folder, csv_item)) as f:
            csv_text += csv_item.replace('_Dice.csv', '') + ','
            f_lines = f.readlines()
            for line in f_lines:
                dice = line.replace('\n', '').split(',')[1]
                csv_text += dice + ','
        csv_text += '\n'
    csv_text = csv_text.replace(',\n', '\n')
    with open(join(output_folder, 'Dice_score.csv'), 'w') as w:
        w.write(csv_text)


def plot_box_no_param(output_folder):
    """
    Creats a plot box figure of average prediction dice scores.

    Parameters:
    ----------
    output_folder : string
    """

    ## Read the csv table ##
    plt.figure()
    k = pd.read_csv(join(output_folder, 'Dice_score.csv'), sep=',')
    k.boxplot(showmeans=True)
    plt.ylim([0, 1])
    plt.savefig(join(output_folder, 'Dice_boxplot.png'))


def plot_figure_loss_dice(log_values, jpeg_file=None):
    """
    Creates a figure of training loss and dice validation score for every training epoch.

    Parameters:
    ----------
    log_values : python dictionary
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    par1 = ax.twinx()

    ax.set_ylabel("Loss")
    par1.set_ylabel("Dice")
    par1.set_ylim(0, 1)

    valid_dice = np.array(log_values['valid_dice'])
    train_loss, = ax.plot(log_values['epoch'], log_values[
                          'train_loss'], 'b', marker='o')
    valid_dice_complete, = par1.plot(
        log_values['epoch'], valid_dice[:, 0], 'k', marker='o')
    valid_dice_core, = par1.plot(
        log_values['epoch'], valid_dice[:, 1], 'g', marker='o')
    valid_dice_enhancing, = par1.plot(
        log_values['epoch'], valid_dice[:, 2], 'r', marker='o')

    lgd = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), handles=[train_loss, valid_dice_complete, valid_dice_core, valid_dice_enhancing], labels=[
        'train_loss', 'valid_dice_complete', 'valid_dice_core', 'valid_dice_enhancing'])
    plt.grid()
    if os.path.isfile(jpeg_file):
        os.remove(jpeg_file)
    fig.savefig(jpeg_file, bbox_extra_artists=(lgd,), bbox_inches='tight')


def save_png(flair, t1c, prediction_array, truth, output_filepath):
    """
    Creates a subplots of flair, prediciton, ground truth of all slices in png format

    Parameters:
    ----------
    flair : ndarray
    prediction_array : ndarray
    truth : ndarray
    output_filepath : string
    """

    f, axarr = plt.subplots(1, 4)
    f.subplots_adjust(hspace=0.025, wspace=0.01)

    axarr[0].imshow(flair, cmap="Greys_r", origin='lower')
    axarr[0].set_xticks([])
    axarr[0].set_yticks([])
    axarr[1].imshow(t1c, cmap="Greys_r", origin='lower')
    axarr[1].set_xticks([])
    axarr[1].set_yticks([])

    axarr[2].imshow(flair, cmap="Greys_r", origin='lower')
    axarr[2].hold(True)
    prediction_array = np.ma.masked_where(
        prediction_array < 0.5, prediction_array)
    axarr[2].imshow(prediction_array, alpha=0.5, origin='lower', vmin=0, vmax=5)
    axarr[2].set_xticks([])
    axarr[2].set_yticks([])
    axarr[3].imshow(flair, cmap="Greys_r", origin='lower')
    axarr[3].hold(True)
    truth = np.ma.masked_where(truth < 0.5, truth)
    axarr[3].imshow(truth, alpha=0.5, origin='lower', vmin=0, vmax=5)
    axarr[3].set_xticks([])
    axarr[3].set_yticks([])

    axarr[0].set_xlabel('FLAIR')
    axarr[1].set_xlabel('T1C')
    axarr[2].set_xlabel('Prediction')
    axarr[3].set_xlabel('Truth')

    plt.savefig(output_filepath, bbox_inches='tight')
    plt.clf()
    plt.close()


def print_evaluation(prediction, labels, output_dir, name):
    """
    Saves a file containing the dice scroes.

    Parameters:
    ----------
    prediction : ndarray
    labels : ndarray
    output_dir : string
    name : string
    """
    evaluation = BRATS_rate_results([prediction], [labels])

    lineFormat = ' + {0:-^10} + {0:-^10}'
    titleFormat = ' | {:^10} | {:^10} |'
    numbersFormat = ' | {:<10} | {:>10.3f} |'

    print (lineFormat.format('-'))
    print (titleFormat.format('region', 'mean'))
    print (lineFormat.format('-'))
    print (numbersFormat.format('complete', evaluation[0][0]))
    print (numbersFormat.format('core', evaluation[1][0]))
    print (numbersFormat.format('enhanced', evaluation[2][0]))
    print (lineFormat.format('-'))
    print ('')
    with open(join(output_dir, name + '_Dice.csv'), 'w') as f:
        f.write('complete, ')
        f.write(str(evaluation[0][0]))
        f.write('\n')
        f.write('core, ')
        f.write(str(evaluation[1][0]))
        f.write('\n')
        f.write('enhanced, ')
        f.write(str(evaluation[2][0]))
        f.write('\n')
