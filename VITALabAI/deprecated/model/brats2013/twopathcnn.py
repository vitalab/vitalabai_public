import os
import timeit
from os.path import join as join

import numpy as np
from VITALabAI.dataset.brats.bratsdataset import BRATS
from VITALabAI.dataset.brats.lisa_brats.misc.BRATS_rate_scores import BRATS_rate_results
from VITALabAI.dataset.brats.lisa_brats.misc.utils import postprocess_label_image
from VITALabAI.dataset.brats.lisa_brats.misc.utils import prediction_get_labels_happy
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Dropout, Activation, Flatten, merge, Input
from keras.models import Model
from keras.optimizers import SGD
from keras.utils import generic_utils
from model.brats.bratsutils import print_evaluation, write_dice, plot_box_no_param, fix_dataset_axes, \
    prepare_padded_brain

from VITALabAI.VITALabAiAbstract import VITALabAiAbstract
from VITALabAI.deprecated.dataset.brats2013.lisa_brats.brains import BrainSet
from VITALabAI.utils.activation import across_channel_softmax

np.random.seed(1337)  # for reproducibility
import keras.backend as K
import yaml
import nibabel as nib
from keras.regularizers import l2


class TwoPathCNN(VITALabAiAbstract):
    """
    Model for brain tumor segmentation.
    """

    def __init__(self, parameters_path, batch_size=128, nb_classes=5, nb_epoch=100, input_dim=33, img_channels=4, **kwargs):
        """
        Parameters:
        ----------
        batch_size : int
            number of examples in a minibatch
        nb_classes : int
            number of classes in the dataset
        nb_epoch : int
            number of training epochs
        input_dim : int
            size of the input patch to the model
        img_channels : int
            number of input channels (number of modalities)
        """
        self.nb_slices = 40
        self.num_minibatches_train = 200
        self.batch_size = int(batch_size)
        self.nb_epoch = int(nb_epoch)
        self.patch_shrink_size = 32
        self.parameters = dict()
        self.nb_features = 64
        self.nb_classes = int(nb_classes)
        self.batch_size = int(batch_size)
        self.input_dim = int(input_dim)
        self.img_channels = int(img_channels)
        self.parameters['learning_rate'] = 0.001
        self.parameters['decay'] = 0.001
        #K.set_image_dim_ordering('th')
        self.parameters.update(self.set_paths_from_file(parameters_path))
        self.save_path = self.parameters['save_path']
        if not os.path.isdir(self.save_path):
            os.mkdir(self.save_path)
        self.p = 0.8  # dropout probability of drop
        self.L2 = 0.01   # L2 regularizer coeffcient

    def set_paths_from_file(self, path_to_file):
        """
        Reads the required path files from a file.

        Parameters:
        ----------
        path_to_file : string

        Returns:
        -------
            dictionary

        """
        with open(path_to_file, 'r') as f:
            data = yaml.load(f)

        return data

    def load_dataset(self):
        """
        Loads the dataset from HDF5 file.
        """

        input_shape = [self.input_dim, self.input_dim]
        self.dataset = dict()
        self.dataset['train'] = BRATS(path_brains=self.parameters['trainset_file'],
                                      path_analysis=self.parameters[
                                          'trainset_file'] + '_ANALYSIS',
                                      patch_shape=input_shape,
                                      label_patch_shape=[1, 1],
                                      num_minibatches_train=self.num_minibatches_train,
                                      axes=('b', 0, 1, 'c'),
                                      nb_classes=self.nb_classes,
                                      batch_size=self.batch_size,
                                      distribution=None,
                                      nb_modalities = self.img_channels)

    def _build_model(self):
        """
        The TwoPathCNN model from http://www.medicalimageanalysisjournal.com/article/S1361-8415(16)30033-0/abstract
        """

        input_l = Input(shape=(self.img_channels, self.input_dim,
                               self.input_dim), name='local_input')

        model_local = Convolution2D(
            self.nb_features, 5, 5, border_mode='valid', W_regularizer=l2(self.L2))(input_l)
        model_local = MaxPooling2D(pool_size=(2, 2), strides=(
            1, 1), border_mode='valid')(model_local)
        model_local = Activation('relu')(model_local)

        model_local = Convolution2D(
            self.nb_features, 5, 5, border_mode='valid', W_regularizer=l2(self.L2))(model_local)
        model_local = Activation('relu')(model_local)
        model_local = Dropout(self.p)(model_local)

        input_g = Input(shape=(self.img_channels, self.input_dim,
                               self.input_dim), name='global_input')

        model_global = Convolution2D(
            100, 10, 10, border_mode='valid', W_regularizer=l2(self.L2))(input_g)
        model_global = Activation('relu')(model_global)
        model_global = Dropout(self.p)(model_global)

        model_merge = merge([model_local, model_global],
                            mode='concat', concat_axis=1)
        model_merge = Convolution2D(
            self.nb_classes, 24, 24, border_mode='valid', W_regularizer=l2(self.L2))(model_merge)
        prob_y_given_x = Activation(across_channel_softmax)(model_merge)

        prob_y_given_x = Flatten()(prob_y_given_x)

        model = Model(input=[input_l, input_g], output=[prob_y_given_x])
        sgd = SGD(lr=self.parameters['learning_rate'], decay=self.parameters[
                  'decay'], momentum=0.9, nesterov=True)

        print("Build model.")
        model.compile(loss='categorical_crossentropy',
                      optimizer=sgd, metrics=['accuracy'])

        print("Model built.")
        model.summary()
        return model

    def build_model(self):
        """
        Builds keras model. 
        """
        model = self._build_model()
        self.model = model

    def build_secondphase(self):
        """
        Builds secondphsae model where the weights from feature layers are frozen and the classifer is trained from scratch with a minibatch distribution of class labels closer to the true distribution.

        Returns:
        -------
            keras model
        """

        input_shape = [self.input_dim, self.input_dim]
        self.dataset['train'] = BRATS(path_brains=self.parameters['trainset_file'],
                                      path_analysis=self.parameters[
                                          'trainset_file'] + '_ANALYSIS',
                                      patch_shape=input_shape,
                                      label_patch_shape=[1, 1],
                                      num_minibatches_train=self.num_minibatches_train,
                                      axes=('b', 0, 1, 'c'),
                                      nb_classes=self.nb_classes,
                                      batch_size=self.batch_size,
                                      distribution=[103, 4, 12, 3, 6],
                                      nb_modalities=self.img_channels)

        secondphase = self._build_model()
        for i in range(12):
            layer_weights = self.model.layers[i].get_weights()
            secondphase.layers[i].set_weights(layer_weights)
            secondphase.layers[i].trainable = False
        return secondphase

    def load_model(self, path):
        # Not applicable.
        pass

    def model_summarize(self):
        """
        This method is used to show a summarized version of the model,
        like a list of most important parameters, neural network structure, ...
        """
        pass

    def prepare_train_dictionary(self, X_batch):
        """
        Prepares the training data for frontend and backend input layers.
        """
        return [X_batch, X_batch]

    def _train(self):
        """
        This method trains the model created by the build_model() function.
        """

        log = {'train_loss': [], 'valid_dice': [],
               'learning_rate': [], 'epoch': []}
        log_filename = join(self.save_path, 'out.log')
        tic = timeit.default_timer()
        best_train_loss = 1000000000.0
        for e in range(self.nb_epoch):
            print('-' * 40)
            print('Epoch', e + 1)
            print('-' * 40)
            print("Training...")

            dataset_iterator = self.dataset['train'].iterator()
            progbar = generic_utils.Progbar(self.dataset['train'].num_examples)
            cumulative_train_loss = 0
            for X_batch, Y_batch in dataset_iterator:
                X_batch = fix_dataset_axes(X_batch)
                data = self.prepare_train_dictionary(X_batch)
                loss_and_metrics = self.model.train_on_batch(
                    x=[data[0], data[1]], y=Y_batch)
                loss = loss_and_metrics[0]
                progbar.add(X_batch.shape[0], values=[("train loss", loss)])
                cumulative_train_loss += np.array(loss)

            cumulative_train_loss = cumulative_train_loss / self.dataset['train'].num_batches
            log['train_loss'].append(cumulative_train_loss)

            # Average over dice measure is used for validation
            '''
            mean_dice = self.generate_prediction(
                brain_set_path=self.parameters['validset_file'],
                prediction_folder=self.save_path,
                nb_slices=self.nb_slices,
                generate_prediction_image=False
            )

            print('dice measure is', mean_dice)
            log['valid_dice'].append(mean_dice)

            # save other information in the log dictionary
            log['learning_rate'].append(self.parameters['learning_rate'])
            log['epoch'].append(e + 1)
            save_log(log, log_filename)
            '''
            if best_train_loss > cumulative_train_loss:
                best_train_loss = cumulative_train_loss
                print('saving best model with loss = ' + repr(best_train_loss))
                self.save_model()
        toc = timeit.default_timer()

        # Print the final training time
        print('Training time: ', round(toc - tic, 2), 'sec')
        #plot_figure_loss_dice(log, log_filename.replace('.log', '.png'))

    def train(self):
        """
        Trains the fristphase and secondphase sequentially. 
        """
        self._train()
        self.test()
        # SECONDPHASE TRAINING
        self.num_minibatches_train = 2000
        secondphase = self.build_secondphase()
        self.save_path = join(self.save_path, 'secondphase')
        if not os.path.isdir(self.save_path):
            os.mkdir(self.save_path)
        self.model = secondphase
        self.nb_epoch = 1
        self._train()

    def test(self):
        """
        This method runs the model on test data once training is over.
        """
        if not os.path.exists(self.save_path):
            os.makedirs(self.save_path)
        # Test the model on valid set
        valid_save_path = join(self.save_path, 'validset')
        if not os.path.exists(valid_save_path):
            os.makedirs(valid_save_path)
        self.generate_prediction(
            brain_set_path=self.parameters['validset_file'],
            prediction_folder=valid_save_path,
            nb_slices=self.nb_slices,
            generate_prediction_image=True
        )
        # Test the model on trainset if requested.
        try:
            print ('Making predictions on trainset: ',
                   self.parameters['trainset_test_file'])
            train_save_path = join(self.save_path, 'trainset')
            if not os.path.exists(train_save_path):
                os.makedirs(train_save_path)
            self.generate_prediction(
                brain_set_path=self.parameters['trainset_test_file'],
                prediction_folder=train_save_path,
                nb_slices=self.nb_slices,
                generate_prediction_image=True
            )
        except:
            print('trainset_test_file not found. Trainset was not selected for prediction')
            pass

    def export_results(self):
        """
        This method is called after testing to save the current model
        as well as its current parameters so it can be loaded afterwards.
        """
        pass

    def save_model(self):
        """
        This method is called at the end to save the best parameters for the model.
        """
        self.model.save_weights(
            join(self.save_path, 'model_weights.h5'), overwrite=True)

    def generate_segmentation(self, slices, nb_slices):
        """
        Generates segmentation for one brain

        Parameters:
        ----------
        slices : ndarray
            contains the brain.
        nb_slices : int
            size of the minibatch at test time. Number of slices loaded into the gpu at test time.

        Returns:
        -------
            ndarray
        """
        image_modalities = slices.transpose(3, 0, 1, 2)

        depth, height, width = image_modalities.shape[1:]

        data_list = []
        for modality in image_modalities:
            padded_brain = prepare_padded_brain(
                modality, self.patch_shrink_size, depth, height, width)
            data_list.append(padded_brain)

        data_list = np.array(data_list, dtype=np.float32).swapaxes(0, 1)

        prediction = []
        for i in range(0, len(padded_brain), nb_slices):
            prediction.extend(self.get_activations(
                data=data_list[i:i + nb_slices]))
        prediction = np.array(prediction, dtype=np.float32).swapaxes(0, 1)
        return prediction

    def get_activations(self, data):
        """
        Forward passes data through the model.

        Parameters:
        ----------
        data: list of inputs to the model.

        Returns:
        -------
            ndarray containing class probabilities.
        """
        # Build the Keras function used to make the prediction
        keras_fct = K.function([K.learning_phase(), ] + self.model.inputs,
                               self.model.layers[-2].output)
        output = keras_fct([0, data, data])
        return output

    def generate_prediction(self, brain_set_path, prediction_folder, generate_prob=False, print_eval=True, nb_slices=2, generate_prediction_image=True):
        """
        Generates prediction for a BrainSet archive.

        Parameters:
        ----------
        brain_set_path : string
            filename of the BrainSet archive
        prediction_folder : string
            save path
        generate_prob : bool
            if True saves the prediction probability matrix in .npy format and skips the segmentation
        print_eval : bool
            if True prints the dice scores for the predictions
        nb_slices : int
            number of slices given to the model (loads in gpu) each forward pass during prediciton.
        generate_prediction_image : bool
            if True saves the predictions in .png format.

        Returns:
        -------
            list containing the average dice scores of complete, core, enhancing categories.
        """
        brain_set = BrainSet.from_path(brain_set_path)
        brains = brain_set.get_brains()
        brain_names = []
        evaluation_results = []
        for name in brains:
            brain_names.append(name)
        brain_names.sort()

        for i, name in enumerate(brain_names):

            tic = timeit.default_timer()

            b = brains[name]
            slices = brains[name].images
            print('brain {} ({})'.format(name, i + 1))

            fname = name + str('_predictions.npy')
            fname = os.path.join(prediction_folder, fname)
            if os.path.exists(fname.replace('.npy', '.mha')):
                print(fname + ' exists already. skipping')
                continue

            prediction = self.generate_segmentation(
                slices, nb_slices=self.nb_slices)

            if generate_prob:
                fhandle = open(fname, 'wb+')
                np.save(fhandle, prediction)
                fhandle.close()
            else:

                labels = prediction_get_labels_happy(prediction)
                post_processed_labels = postprocess_label_image(
                    labels, min_size=2000)

                MASK = (slices[:, :, :, 0] > 0.0)
                post_processed_labels = MASK * post_processed_labels
                if b.labels is not None and print_eval:
                    print('-' * 40)
                    print('Evaluation for ', name)
                    print('-' * 40)
                    print_evaluation(post_processed_labels.flatten(
                    ), b.labels.flatten(), prediction_folder, name)

                evaluation_results_perbrain = BRATS_rate_results(
                    [post_processed_labels.flatten()], [b.labels.flatten()])
                mean_perbrain = np.asarray(evaluation_results_perbrain)[:, 0]
                evaluation_results.append(mean_perbrain)

            toc = timeit.default_timer()
            print('Prediction time: ', round(toc - tic, 2), 'sec')

            # Saving visual results
            if generate_prediction_image is True:
                save_brain_dir = join(prediction_folder, name)
                if not os.path.isdir(save_brain_dir):
                    os.mkdir(save_brain_dir)

                flair = slices[..., 0]
                t1c = slices[..., 2]
                t2 = slices[..., 3]
                truth = brains[name].labels
                flair = flair[:, ::-1, ::-1]
                t1c = t1c[:, ::-1, ::-1]
                t2 = t2[:, ::-1, ::-1]
                truth = truth[:, ::-1, ::-1]
                post_processed_labels = post_processed_labels[:, ::-1, ::-1]
                print('Generating images ...')
                affine = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]
                nimg = nib.Nifti1Image(post_processed_labels, affine=affine, header=nib.Nifti1Header())
                nimg.to_filename(save_brain_dir + '_result.nii.gz')
                nimg = nib.Nifti1Image(flair, affine=affine, header=nib.Nifti1Header())
                nimg.to_filename(save_brain_dir + '_Flair.nii.gz')
                nimg = nib.Nifti1Image(t1c, affine=affine, header=nib.Nifti1Header())
                nimg.to_filename(save_brain_dir + '_T1C.nii.gz')
                nimg = nib.Nifti1Image(t2, affine=affine, header=nib.Nifti1Header())
                nimg.to_filename(save_brain_dir + '_T2.nii.gz')

        write_dice(prediction_folder)
        plot_box_no_param(prediction_folder)
        return np.mean(np.asarray(evaluation_results), axis=0)
