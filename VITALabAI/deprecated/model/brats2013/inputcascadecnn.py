import numpy as np
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Dropout, Activation, Flatten, merge, Input
from keras.models import Model
from keras.optimizers import SGD
from model.brats.bratsutils import prepare_padded_brain

from VITALabAI.deprecated.dataset.brats2013 import BRATS
from VITALabAI.deprecated.model.brats2013.twopathcnn import TwoPathCNN
from VITALabAI.utils.activation import across_channel_softmax

np.random.seed(1337)  # for reproducibility
import keras.backend as K
from keras.regularizers import l2
import os


class InputCascadeCNN(TwoPathCNN):

    def __init__(self, parameters_path, batch_size=128, nb_classes=5, nb_epoch=100, input_dim_backend=65, input_dim_frontend=33, img_channels=4, **kwargs):
        """
        Model for brain tumor segmentation.

        Parameters:
        ----------
        batch_size : int
            number of examples in a minibatch
        nb_classes : int
            number of classes in the dataset
        nb_epoch : int
            number of training epochs
        input_dim_backend : int
            size of the input patch to the backend model
        input_dim_frontend : int
            size of the input patch to the frontend model
        img_channels : int
            number of input channels (number of modalities)
        """
        # Add all arguments of this constructor as properties of this object.
        self.__dict__.update(locals())

        # Since self is also an argument, remove it to avoid infinite loop.
        del self.self
        self.nb_slices = 40
        self.num_minibatches_train = 200
        self.nb_features = 64
        self.batch_size = int(batch_size)
        self.nb_epoch = int(nb_epoch)
        self.patch_shrink_size = 64
        self.parameters = dict()
        self.parameters['learning_rate'] = 0.001
        self.parameters['decay'] = 0.001
        self.parameters_path = parameters_path
        self.parameters.update(self.set_paths_from_file(parameters_path))
        self.save_path = self.parameters['save_path']
        self.input_dim = self.input_dim_backend
        self.p = 0.8  # dropout probability of drop
        self.L2 = 0.01  # L2 regularization coefficient
        K.set_image_dim_ordering('th')

        if not os.path.isdir(self.save_path):
            os.mkdir(self.save_path)

    def _build_model(self):
        """
        The InputCascadeCNN model from http://www.medicalimageanalysisjournal.com/article/S1361-8415(16)30033-0/abstract

        """
        def build_backend(input_l_backend):
            """
            Builds the backend section of the inputcascade mdoel

            Parameters:
            ----------
            input_l_backend: thenao symbolic tensor

            Returns:
            -------
                Keras model.
            """

            model_local_backend = Convolution2D(
                self.nb_features, 5, 5, border_mode='valid', W_regularizer=l2(self.L2))(input_l_backend)
            model_local_backend = MaxPooling2D(pool_size=(2, 2), strides=(
                1, 1), border_mode='valid')(model_local_backend)
            model_local_backend = Activation('relu')(model_local_backend)
            model_local_backend = Dropout(self.p)(model_local_backend)
            model_local_backend = Convolution2D(
                self.nb_features, 5, 5, border_mode='valid', W_regularizer=l2(self.L2))(model_local_backend)
            model_local_backend = Activation('relu')(model_local_backend)
            model_local_backend = Dropout(self.p)(model_local_backend)
            model_global_backend = Convolution2D(
                100, 10, 10, border_mode='valid', W_regularizer=l2(self.L2))(input_l_backend)
            model_global_backend = Activation('relu')(model_global_backend)
            model_global_backend = Dropout(self.p)(model_global_backend)
            model_merge_backend = merge(
                [model_local_backend, model_global_backend], mode='concat', concat_axis=1)
            model_merge_backend = Convolution2D(
                self.nb_classes, 24, 24, border_mode='valid', W_regularizer=l2(self.L2))(model_merge_backend)
            prob_y_given_x_backend = Activation(
                across_channel_softmax)(model_merge_backend)
            model = Model(input=[input_l_backend], output=[
                          prob_y_given_x_backend])
            return model

        # Add cascade
        def add_cascade(prob_y_given_x_backend, input_l_frontend, input_l_backend):
            """
            Takes as input the class probabilities and builds up a cascaded model on top of that.

            Parameters:
            ----------
            prob_y_given_x_backend: theano symbolic tensor
            input_l_frontend: theano symbolic tensor
            input_l_backend: theano symbolic tensor

            Returns:
            -------
                Keras model
            """
            model_merge_backend_frontend = merge(
                [input_l_frontend, prob_y_given_x_backend], mode='concat', concat_axis=1)

            model_local_frontend = Convolution2D(
                self.nb_features, 5, 5, border_mode='valid', W_regularizer=l2(self.L2))(model_merge_backend_frontend)
            model_local_frontend = MaxPooling2D(pool_size=(2, 2), strides=(
                1, 1), border_mode='valid')(model_local_frontend)
            model_local_frontend = Activation('relu')(model_local_frontend)
            model_local_frontend = Dropout(self.p)(model_local_frontend)
            model_local_frontend = Convolution2D(
                self.nb_features, 5, 5, border_mode='valid', W_regularizer=l2(self.L2))(model_local_frontend)
            model_local_frontend = Activation('relu')(model_local_frontend)
            model_local_frontend = Dropout(self.p)(model_local_frontend)
            model_merge_frontend = Convolution2D(
                self.nb_classes, 24, 24, border_mode='valid', W_regularizer=l2(self.L2))(model_local_frontend)
            prob_y_given_x_frontend = Activation(
                across_channel_softmax)(model_merge_frontend)
            prob_y_given_x_frontend = Flatten()(prob_y_given_x_frontend)
            model = Model(input=[input_l_backend, input_l_frontend], output=[
                          prob_y_given_x_frontend])
            return model

        input_l_backend = Input(shape=(self.img_channels, self.input_dim_backend,
                                       self.input_dim_backend), name='local_input_backend')
        input_l_frontend = Input(shape=(self.img_channels, self.input_dim_frontend,
                                        self.input_dim_frontend), name='local_input_frontend')

        model_backend = build_backend(input_l_backend)
        try:
            model_backend = self.load_basic_weights(
                model_backend, self.parameters['basic_model_weights'])
        except:
            print('No pre-trained backend model selected!')
            pass
        model = add_cascade(model_backend.output,
                            input_l_frontend, input_l_backend)
        sgd = SGD(lr=self.parameters['learning_rate'], decay=self.parameters[
                  'decay'], momentum=0.9, nesterov=True)

        try:
            model.load_weights(self.parameters['basic_model_weights'])
        except:
            print('No pre-trained weights for the entire model.  Training entire model from scratch')
            pass

        print("Build model.")
        model.compile(loss='categorical_crossentropy',
                      optimizer=sgd, metrics=['accuracy'])
        print("Model built.")
        model.summary()
        return model

    def load_basic_weights(self, backend_model, f):
        """
        Initializes the weights of the backend_model to that of a pre-trained model.

        Parameters:
        ----------
        backend_model: Keras model
        f: string
            path to the weights of the pretrained model.

        Returns:
        -------
            Keras model
        """
        basic_model = TwoPathCNN(parameters_path=self.parameters_path)
        basic_model.build_model()
        basic_model.model.load_weights(f)
        for (basic_layer, backend_layer) in zip(backend_model.layers, basic_model.model.layers):
            backend_layer.set_weights(basic_layer.get_weights())
        return backend_model

    def build_model(self):
        """
        Builds the keras model. 
        """
        model = self._build_model()
        self.model = model

    def build_secondphase(self):
        """
        Prepares the model for sechond phase training where the feature layers are frozen and the classifiaction layer is trained from scratch with a class distribution close to the ture distribution of class labels.

        Returns:
        -------
            Keras model 
        """

        input_shape = [self.input_dim, self.input_dim]
        self.dataset['train'] = BRATS(path_brains=self.parameters['trainset_file'],
                                      path_analysis=self.parameters[
                                          'trainset_file'] + '_ANALYSIS',
                                      patch_shape=input_shape,
                                      label_patch_shape=[1, 1],
                                      num_minibatches_train=self.num_minibatches_train,
                                      axes=('b', 0, 1, 'c'),
                                      nb_classes=self.nb_classes,
                                      batch_size=self.batch_size,
                                      distribution=[103, 4, 12, 3, 6],
                                      nb_modalities=self.img_channels)
        secondphase = self._build_model()
        for i in range(24):
            layer_weights = self.model.layers[i].get_weights()
            secondphase.layers[i].set_weights(layer_weights)
            secondphase.layers[i].trainable = False
        return secondphase

    def prepare_train_dictionary(self, X_batch_backend):
        """
        Prepares the training data for frontend and backend input layers.
        """
        shrink_size = 32
        X_batch_frontend = X_batch_backend[
            :, :, shrink_size // 2:-shrink_size // 2, shrink_size // 2:-shrink_size // 2]

        return [X_batch_backend, X_batch_frontend]

    def generate_segmentation(self, slices, nb_slices):
        """
        Generates segmentation for one brain

        Parameters:
        ----------
        slices : ndarray
            contains the brain.
        nb_slices : int
            size of the minibatch at test time. Number of slices loaded into the gpu at test time.

        Returns:
        -------
            ndarray
        """
        image_modalities = slices.swapaxes(0, 3).swapaxes(1, 3).swapaxes(2, 3)

        if self.img_channels==3: #get rid of t1 modality
            image_modalities = image_modalities[[0,2,3]]

        depth, height, width = image_modalities.shape[1:]

        data_list = []
        for modality in image_modalities:
            padded_brain = prepare_padded_brain(
                modality, self.patch_shrink_size, depth, height, width)
            data_list.append(padded_brain)

        data_list = np.array(data_list, dtype=np.float32).swapaxes(0, 1)

        prediction = []
        for i in range(0, len(padded_brain), nb_slices):
            data = self.prepare_train_dictionary(
                data_list[i:i + nb_slices])

            prediction.extend(self.get_activations(data))
        prediction = np.array(prediction, dtype=np.float32).swapaxes(0, 1)
        return prediction

    def get_activations(self, data):
        """
        Forward passes data through the model.

        Parameters:
        ----------
        data: list of inputs to the model.

        Returns:
        -------
            ndarray containing class probabilities.
        """
        # Build the Keras function used to make the prediction
        keras_fct = K.function([K.learning_phase(), ] + self.model.inputs,
                               self.model.layers[-2].output)
        output = keras_fct([0, data[0], data[1]])
        return output
