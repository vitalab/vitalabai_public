# BRATS dataset

BRATS 2013 contains 20 subjects with high grade glioma and 10 subjects with low grade glioma. If you have access to VITALab repository you can locate this dataset in `vitalab/datasets/brats/BRATS2013` if not you can download the dataset from virtualskeleton.sh. The subjects in the repository are already pre-processed. 


### Required packages
- Cython `pip3 install Cython`
- SimpleITK `pip3 install SimpleITK`
- pyYAML `pip3 install pyyaml`
- pandas `pip3 instal pandas`
- tables `pip3 install tables`
- keras 1.2 `pip3 install keras=1.2.0`
- tensorflow 1.0 `pip3 install tensorflow-gpu=1.0.0`

WARNING! if you are using a version of python other than 3.4 or using a package like anaconda you need to replace -I/usr/include/python3.4 by -I/path/to/your/includePython/folder. Not doing so could lead to an obscure "PyFPE_jbuf" error.

## Pre-processing
If you need to pre-process your data (i.e. if you are outside of vital lab), you need to copy the files inside `dataset/brats/lisa_lab/pre-processing` to where your brain folders are located. To run the pre-processing scripts you need to following requirements:

### Required packages
- itk Install [ITK-4.5](http://www.itk.org/ITK/resources/legacy_releases.html). (You can follow this [howto](http://pythonhosted.org/MedPy/installation/itkwrapper4.7.html) . In case of python error after using this Howto (Usually there can be an error with  libitkvnl_algo-4.7.so.1 library), do `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/your_ITK4.7_path/build/lib`
- ANTs `https://github.com/stnava/ANTs`

Run the following scripts sequentially inside the directory where the brain folders are located
```
python create_mask.py
sh preprocessing.sh
```

## Creating the dataset
 Compile `_brains_analysis.pyx` in `lisa_brats/misc/` using Cython. This script contains a function to compute the entropy over the label image. 

``
cython -a _brains_analysis.pyx
``

``
gcc -shared -pthread -fPIC -fwrapv -O2 -Wall -fno-strict-aliasing -I Path/to/your/python/headers (e.g./usr/include/python3.4) -o _brains_analysis.so _brains_analysis.c
``

Creating the HDF5 dataset for BRATS is handled by `datasets/brats/lisa_brats/brains.py`. This script will create the following files: 

- trainset files which consist of 2 HDF5 files; one containing the brains and the other is the coordinates of the training patches.
- validset consisting of a single HDF5 file containing validation brains. Validation is done by making predictions on validset brains and computing the dice score.
- testset consisting of a single HDF5 file containing test brain. There exists no ground truth for these brains.

A yaml file should be created which contains the paths to the raw data as well as the HDF5 file paths for train, valid and test sets. This yaml file is used both for creating the datasets as well as for running the model.
The yaml file should contain the keys in the following example below.

`paths.yaml` :

```
training_data: '/path/to/train/data/folder'
validation_data: 'path/to/validation/data/folder'
test_data: 'path/to/test/data/folder'
save_path: 'path/to/save'
trainset_file: 'path/to/brats/hdf5/train/file'
validset_file: 'path/to/brats/hdf5/valid/file'
testset_file: 'path/to/brats/hdf5/test/file'
```
If you choose to make predictions on the trainset, you need to add `trainset_test_file` with the path pointing to it in the yaml file. 


Having defined the yaml file, we can execute `brains.py` as follows:


```
python3 brains.py -D /path/to/the/YAML/file.yaml
```

# BRATS models


## TwoPathCNN 


The model can be executed using the following command line

```
python3 main.py model.brats.twopathcnn.TwoPathCNN -o "parameters_path='/path/to/the/YAML/file.yaml'"
```
## InputCascadeCNN

```
python3 main.py model.brats.inputcascadecnn.InputCascadeCNN -o parameters_path=/path/to/the/YAML/file.yaml
```
To initialize the backend of the InputCascadeCNN model to a pretrained TwoPathCNN model add the following key to the yaml file. If the key is not included, the backend is trained from scratch.
```
basic_model_weights: 'path/to/pretrained/TwoPathCNN(i.e.backend_model)weights.h5'
```



