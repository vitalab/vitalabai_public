# Using pipeline for PPMI

```bash
python3 main.py model.ppmi.ppmisvm.PpmiSvm -o data_file=path/to/meanstdperpoint.npy, \
labels_file=path/to/groups.npy
```

The following parameters are required:

- `data_file` is the path to the meanstdperpoint.npy file.
- `labels_file` is the path to the groups_pd_con.npy file.

You can get these files here: https://bitbucket.org/vitalab/ppmi_tractprofiles20

The following options are available:

- `sets_proportion` sets the proportion for training and testing sets.
- `seed` sets the seed for the random number generator.

For model-specific options, see their constructor.
