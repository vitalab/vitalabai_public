#
# Author: Martin COUSINEAU (martin.p.cousineau@usherbrooke.ca)
#


"""
PPMI SVM Class
"""

from sklearn.svm import SVC

from VITALabAI.VITALabAiAbstract import VITALabAiAbstract
from VITALabAI.deprecated.dataset.ppmi.ppmidataset import PpmiDataset


class PpmiSvm(VITALabAiAbstract):

    def __init__(self, data_file, labels_file, c=1.0, kernel="linear",
                 gamma="auto", **kwargs):
        """
        Parameters
        ----------
        data_file: string
            Path to the data file.
            Note: For example, see meanstdperpoint_mean.npy of
                  https://bitbucket.org/vitalab/ppmi_tractprofiles20

        labels_file: string
            Path to the labels file.
            Note: For example, see groups_pd_con.npy of
                  https://bitbucket.org/vitalab/ppmi_tractprofiles20

        c: float
            Penalty parameter C of the error term of the SVM (optional).

        kernel: string
            Kernel type to be used (optional).
            See scikit-learn documentation for more information:
            http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html

        gamma: float or "auto"
            Kernel coefficient for non-linear kernels (optional).
            See scikit-learn documentation for more information.
        """

        self.c = c
        self.kernel = kernel
        self.gamma = gamma

        self.model = None
        self.y_predicted = None
        self.training_error = None
        self.testing_error = None

        # Give the remaining arguments to the dataset class.
        # See its description for more info.
        self.dataset = PpmiDataset(data_file, labels_file, **kwargs)

    def load_dataset(self):
        """
        We load the PPMI dataset and split it into training and testing sets.
        """

        print("Loading dataset...")
        self.dataset.generate_dataset()
        self.dataset.preprocess_dataset()

    def load_model(self, path):
        # Not applicable.
        pass

    def build_model(self):
        """
        Builds the model using the C-Support Vector Machine for classification
        of scikit-learn:
        http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html
        """

        print("Building model...")
        self.model = SVC(C=self.c, kernel=self.kernel, gamma=self.gamma)

    def model_summarize(self):
        """
        This method is used to show a summarized version of the model,
        like a list of the most important parameters, the neural network structure, etc.
        """

        if not self.model:
            self.build_model()

        print(self.model)

    def train(self):
        """
        This method trains the model created by the build_model() function.
        """

        print("Training...")
        (x_train, y_train), _ = self.dataset.get_entire_dataset()
        self.model.fit(x_train, y_train)
        self.training_error = 1. - self.model.score(x_train, y_train)

    def test(self):
        """
        This method runs the model on test data once training is over.
        """

        print("Testing...")
        _, (x_test, y_test) = self.dataset.get_entire_dataset()
        self.y_predicted = self.model.predict(x_test)
        self.testing_error = 1. - self.model.score(x_test, y_test)

    def export_results(self):
        """
        This method is called after testing to save the current model
        as well as its current parameters so it can be loaded afterwards.
        """

        print("Training error of {0:0.2f}%.".format(100. * self.training_error))
        print("Testing error of {0:0.2f}%.".format(100. * self.testing_error))

    def save_model(self):
        """
        This method is called at the end to save the best parameters for the model.
        """

        # Not applicable.
        pass
