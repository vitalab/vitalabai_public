# CNN video ground truthing project



## Contributors

* Yi Wang (yi.wang@usherbrooke.ca)



## Description
This is the project for the CNN video ground truthing. 
In this project we proposed a multi-scale cascaded convolutional neural network used to segment foreground objects in videos. The model achieves human-level accuracies while being 40 times faster than manual labeling.

The paper about this project can be found at:
* Wang, Y., Luo, Z. M., and Jodoin, P. M. "[Interactive Deep Learning Method for Segmenting Moving Objects](http://www.sciencedirect.com/science/article/pii/S0167865516302471)", Pattern Recognition Letters, 2016.



## Dependensies
* numpy		1.12.1
* scipy		0.19.0
* keras 	2.0.3
* tqdm		4.11.2
* sklearn	0.18.1
* h5py		2.7.0
* theano	0.9.0
* natsort	5.0.3



## Dataset
The dataset is visible in the lab shared folder with name 'CNN_VideoGroundTruthing', which stores the hdf5 file for each video. The hdf5 video files can be generated from the 'CDnet 2014 dataset'.



## Model type
As mentioned in the paper, we provide three different models:
1. Basic model;
2. Multi-scale CNN (MSCNN)
3. Multi-scale CNN with cascade

## How to execute the code

### To gererate the dataset

In order to execute the models, each video has to be stored in a hdf5 file. (It's much faster than reading the video frame by frame. For more details about the hdf5 file, please check the description of the `../../dataset/cnnvideogroundtruthing/cnnvideogroundtruthingdatasetgenerator.py`.)
The dataset's hdf5 files have been generated on the lab shared folder so you can directly download it. However, if you want to re-generate the hdf5 files by yourself, this pipeline may help you:

1. Download the CDnet 2014 dataset and save it at: `root_dataset_path/CDnet2014`.
2. run the 'cnnvideogroundtruthingdatasetgenerator.py'
	1. For generating the testing data: 
		'python3 dataset/cnnvideogroundtruthing/cnnvideogroundtruthingdatasetgenerator.py [root_dataset_path] test'

		This will create a folder `root_dataset_path/CNN_VideoGroundTruthing/category/video` for each video to store its h5py file. In which a h5py file which contains a 'testing' group which store all the frames of the entire video will be generated.

	2. For generating the training data: 
		'python3 dataset/cnnvideogroundtruthing/cnnvideogroundtruthingdatasetgenerator.py [root_dataset_path] train SELECT_METHOD NUMBER_OF_TRAINING_FRAMES'

	#### Variables
	* `SELECT_METHOD`: The strategy to select the training frames.
		1. 'manual'; 
		2. 'random'; 
		3. 'uniform'.
	* `NUMBER_OF_TRAINING_FRAMES`: The number of frames to train the model.
		1. 50; 
		2. 100; 
		3. 150; 
		4. 200. 

	This will add the training data with a specific select_method and number_of_training_frames to the h5py file that is generated in step 1.

### To run the model
You can run each model on one video or on the entire dataset.
* For a single video: 
	* For the basic model: `python3.4 main.py model.cnnvideogroundtruthing.cnnvideogroundtruthing.CNNVideoGroundTruthing \
	-o dataset_root_path='ROOT_DATASET_PATH', nb_epochs=50, batch_size=5, video_id=VIDEO_ID, model_type='basic', \
	select_method=SELECT_METHOD, training_frame_number=TRAINING_FRAME_NUMBER`

	* For the mscnn model: `python3.4 main.py model.cnnvideogroundtruthing.cnnvideogroundtruthing.CNNVideoGroundTruthing \
	-o dataset_root_path='ROOT_DATASET_PATH', nb_epochs=50, batch_size=5, video_id=VIDEO_ID, model_type='mscnn', \
	select_method=SELECT_METHOD, training_frame_number=TRAINING_FRAME_NUMBER`

	* For the mscnn+cascade model: `python3.4 main.py model.cnnvideogroundtruthing.cnnvideogroundtruthing.CNNVideoGroundTruthing \
	-o dataset_root_path='ROOT_DATASET_PATH', nb_epochs=50, batch_size=5, video_id=VIDEO_ID, model_type='cascade', \
	select_method=SELECT_METHOD, training_frame_number=TRAINING_FRAME_NUMBER`

* For the entire dataset: 
	Edit the `model/cnnvideogroundtruthing/bash.sh` file and then run `sh model/cnnvideogroundtruthing/bash.sh`.

#### Variables
* `dataset_root_path`: the path of the root dataset.
* `nb_epochs`: number of epochs.
* `batch_size`: batch size.
* `video_id`: the id of the video.
* `model_type`: the type of the cnn model:
	1. 'basic': The basic CNN model.
	2. 'mscnn': The multi-scale CNN model.
	3. 'cascade': The multi-scale CNN with cascade structure.

#### Output
For each video, the results are saved at `/datasets/CNN_VideoGroundTruthing/CATEGORY/VIDEO`. The structure looks like:
```
datasets
    ├───CDnet2014
    └───CNN_VideoGroundTruthing
                    └───CATEGORY
                              └───VIDEO
                                      ├───models
                                      ├───results
                                      |        └───MODEL_TYPE
                                      |                  ├───compare
                                      |                  └───prob_map
                                      └───evaluation.txt
```

* `\models\VIDEO_MODEL-TYPE_SELECTED-METHOD_TRAINING-FRAME_model.hdf5`: The trained model.
* `\results\MODEL_TYPE\prob_map`: The folder to save the probability map estimated with MODEL_TYPE for each frame in the video.
* `\results\MODEL_TYPE\compare`: The folder to save the image that merges the input, the ground truth, the probability map, and the thresholded binary result.
* `evaluation.txt`: file that saves the Precision, Recall, and F-measure.