"""
Author: Yi Wang (yi.wang@usherbrooke.ca)

Video ground truthing project.

To run the code: 
python3.4 main.py model.cnnvideogroundtruthing.cnnvideogroundtruthing.CNNVideoGroundTruthing \
-o dataset_root_path=DATASET_ROOT_PATH, nb_epochs=50, batch_size=3, video_id=VIDEO_ID, model_type=MODEL_TYPE, \
select_method=SELECT_METHOD, training_frame_number=TRAINING_FRAME_NUMBER

Notice: 
* DATASET_ROOT_PATH is the the parent directory of the video_ground_truthing dataset. 
  e.g: dataset_root_path/CNN_VideoGroundTruthing/
* SELECT_METHOD: 'random', 'uniform', 'manual'
* TRAINING_FRAME_NUMBER: 50, 100, 150, 200
* The training data with SELECT_METHOD and TRAINING_FRAME_NUMBER must be generated with cnnvideogroundthingdatsetgenerator.py first.
"""

import math
import timeit
from os import makedirs
from os.path import join, isfile, isdir

import h5py
import keras.backend as K
import numpy as np
from keras.layers import (
    Input,
    Conv2D, MaxPooling2D,
    Permute, Activation,
    ZeroPadding2D
)
from keras.models import Model
from keras.optimizers import Adam
from keras.utils import generic_utils
from natsort import natsorted
from scipy.misc import imsave, imresize
from sklearn.metrics import precision_recall_fscore_support

from VITALabAI.VITALabAiAbstract import VITALabAiAbstract
from VITALabAI.deprecated.dataset.cnnvideogroundtruthing.cnnvideogroundtruthingdataset \
    import CNNVideoGroundTruthingDataset

K.set_image_data_format('channels_first')


class CNNVideoGroundTruthing(VITALabAiAbstract):

    def __init__(self, dataset_root_path, video_id, model_type='basic', 
                 select_method='manual', training_frame_number=200, 
                 batch_size=5, nb_epochs=20, binary_thr=0.5, 
                 learning_rate=1e-4):

        """
        Parameters
        ----------
            dataset_root_path: string
                Path to the dataset.

            video_id: int
                The id of the video.

            model_type: string
                The type of the model.
                    1. 'basic'; (default)
                    2. 'mscnn';
                    3. 'cascade';

            select_method: string
                The strategy to select the training frames:
                    1. uniform: select the training frames uniformly;
                    2. random : select the training frames randomly;
                    3. manual : select the training frames manually. (default)

            training_frame_number: int
                Number of frames for to train the model:
                    1. 50;
                    2. 100;
                    3. 150;
                    4. 200. (default)

            batch_size: int
                Number of frames in each batch.

            nb_epochs: int
                Number of epochs to train.

            binary_thr: float
                The threshold to binarize the estimated probability map.

            learning_rate: float
                The learning rate.
        """
        self.dataset_root_path = dataset_root_path
        self.video_id = video_id
        self.model_type = model_type
        self.video = None
        self.select_method = select_method
        if self.model_type == 'cascade':
            self.input_channel = 4		# rgb + prob_map
        else:
            self.input_channel = 3		# rgb

        self.training_frame_number = int(training_frame_number)
        self.batch_size = int(batch_size)
        self.nb_epochs = int(nb_epochs)
        self.model = None
        self.binary_thr = binary_thr
        self.learning_rate = learning_rate

    def build_model(self):
        """
        Build the model.
        """
        
        input_image = Input(shape=(self.input_channel, None, None))
        # Layer 1
        x = Conv2D(32, (7, 7), padding='valid')(input_image)

        # Layer 2
        x = Activation('relu')(x)

        # Layer 3
        x = MaxPooling2D(pool_size=(2, 2), strides=1)(x)
        x = ZeroPadding2D(padding=((0, 1), (0, 1)))(x)

        # Layer 4
        x = Conv2D(32, (7, 7), padding='valid')(x)

        # Layer 5
        x = Activation('relu')(x)

        # Layer 6
        x = MaxPooling2D(pool_size=(2, 2), strides=1)(x)
        x = ZeroPadding2D(padding=((0, 1), (0, 1)))(x)

        # Layer 7
        x = Conv2D(32, (7, 7), padding='valid')(x)

        # Layer 8
        x = Activation('relu')(x)

        # Layer 9
        x = Conv2D(32, (7, 7), padding='valid')(x)

        # Layer 10
        x = Activation('relu')(x)

        # Layer 11
        x = Conv2D(64, (7, 7), padding='valid')(x)

        # Layer 12
        x = Conv2D(2, (1, 1), padding='valid')(x)

        # Layer 13
        x = Permute((2, 3, 1))(x)
        output = Activation('softmax')(x)
        self.model = Model(inputs=[input_image, ], outputs=[output, ])

        print("Building the model, with model type {}...".format(self.model_type))
        self.model.compile(loss=[self._crossentropy_loss],
                           optimizer=Adam(lr=self.learning_rate),
                           metrics=[self._dice])
        print("Model building complete.")

        self.model_summarize()

    def load_dataset(self):
        """
        Load one video in the CNN-VideoGroundTruthing dataset.
        """
        
        # prepare the training data with ratio 1.
        self.train_it_ratio_01 = CNNVideoGroundTruthingDataset(
            dataset_root_path=self.dataset_root_path,
            video_id=self.video_id,
            mode='train',
            video_ratio=1.0,
            input_channel=self.input_channel,
            select_method=self.select_method,
            training_frame_number=self.training_frame_number,
            batch_size=self.batch_size,
            shuffle=True
        )

        if self.model_type != 'basic':
            # prepare the training data with ratio 2.
            self.train_it_ratio_02 = CNNVideoGroundTruthingDataset(
                dataset_root_path=self.dataset_root_path,
                video_id=self.video_id,
                mode='train',
                video_ratio=0.75,
                input_channel=self.input_channel,
                select_method=self.select_method,
                training_frame_number=self.training_frame_number,
                batch_size=self.batch_size,
                shuffle=True
            )

            # prepare the training data with ratio 3.
            self.train_it_ratio_03 = CNNVideoGroundTruthingDataset(
                dataset_root_path=self.dataset_root_path,
                video_id=self.video_id,
                mode='train',
                video_ratio=0.5,
                input_channel=self.input_channel,
                select_method=self.select_method,
                training_frame_number=self.training_frame_number,
                batch_size=self.batch_size,
                shuffle=True
            )

        # prepare the data for the testing.
        self.test_it = CNNVideoGroundTruthingDataset(
            dataset_root_path=self.dataset_root_path,
            video_id=self.video_id,
            mode='test',
            input_channel=self.input_channel,
            select_method=self.select_method,
            training_frame_number=self.training_frame_number,
            batch_size=self.batch_size*3,
            shuffle=False
        )

    def _preload_weights(self):
        """
        Function to load the pre-trained weight of the model for the cascade model.
        """
        path = 'model/cnnvideogroundtruthing/pretrained_network'
        with h5py.File(path, "r") as h5f:
            keys = natsorted([key for key in h5f.keys() if key.startswith("conv2d")])
            for key in keys:
                if key == "conv2d_1":
                    continue

                layer = h5f["{0}/{0}".format(key)]
                self.model.get_layer(key).set_weights(
                    (
                        layer["kernel"][:],
                        layer["bias"][:],
                    )
                )

    def train(self):
        """
        Method to train the model.
        """
        print('Training video: {}_{}'.format(self.test_it.video.category_name, 
                                             self.test_it.video.video_name))

        model_name = '{}_{}_{}_{}_model.hdf5'.format(self.train_it_ratio_01.video.video_name, 
                                                  self.model_type,
                                                  self.select_method,
                                                  self.training_frame_number)
        model_full_path = join(self.train_it_ratio_01.video.model_path, model_name)
        
        if not isfile(model_full_path):
            # if no model saved, load the pre-trained model and re-train it.
            if self.model_type != 'cascade':
                self.model.load_weights(
                   'model/cnnvideogroundtruthing/pretrained_network')
            else:
                self._preload_weights()

            # train the 'basic' model
            if self.model_type == 'basic':
                self.model.fit_generator(self.train_it_ratio_01.flow(),
                                         steps_per_epoch=math.ceil(
                                             self.training_frame_number / self.batch_size),
                                         epochs=self.nb_epochs,
                                         max_q_size=10,
                                         workers=1, pickle_safe=True)

            # train the 'mscnn' or the 'mscnn+cascade' model
            else:
                tic = timeit.default_timer()

                for e in range(self.nb_epochs):
                    print('Training video: {}_{}'.format(self.test_it.video.category_name, 
                                                         self.test_it.video.video_name))
                    print('-' * 40)
                    print('Epoch {}'.format(e + 1))

                    progbar = generic_utils.Progbar(self.training_frame_number)

                    for iter_id in range(math.ceil(self.training_frame_number / self.batch_size)):
                        loss_total = 0
                        metric_total = 0

                        # train with the first ratio.
                        train_x_ratio_01, train_y_ratio_01 = next(self.train_it_ratio_01.flow())
                        loss, metric = self.model.train_on_batch(train_x_ratio_01, train_y_ratio_01)
                        loss_total += loss
                        metric_total += metric

                        # train with the second ratio.
                        train_x_ratio_02, train_y_ratio_02 = next(self.train_it_ratio_02.flow())
                        loss, metric = self.model.train_on_batch(train_x_ratio_02, train_y_ratio_02)
                        loss_total += loss
                        metric_total += metric

                        # train with the third ratio.
                        train_x_ratio_03, train_y_ratio_03 = next(self.train_it_ratio_03.flow())
                        loss, metric = self.model.train_on_batch(train_x_ratio_03, train_y_ratio_03)
                        loss_total += loss
                        metric_total += metric

                        progbar.add(self.batch_size, values=[("train loss", loss_total / 3), 
                                                             ("dice", metric_total / 3)]) 
                        
                toc = timeit.default_timer()

                # print the final training time.
                print('Training time: {} sec'.format(round(toc - tic, 2)))
                
            if not isdir(self.test_it.video.model_path):
                makedirs(self.test_it.video.model_path)

            self.model.save_weights(model_full_path)
        else:
            # else load the trained model.
            print('Trained model found, loading the existing model...')
            self.model.load_weights(model_full_path)

    @staticmethod
    def _dice(y_true, y_pred):
        """
        Function to compute dice.

        Parameters
        ----------
            y_true: keras_var
                Ground truth.

            y_pred: keras_var
                Model prediction.

        Returns
        -------
            res: float
                The sum of "dice" scores.
        """
        res = K.variable(0., name='dice_classes')

        flat_y_true = K.flatten(y_true[:, :, :, 1])
        flat_y_pred = K.flatten(y_pred[:, :, :, 1])

        # flat_y_true is a binary vector
        intersect = K.sum(flat_y_true * flat_y_pred)
        s_true = K.sum(flat_y_true)
        s_pred = K.sum(flat_y_pred)
        res += (2. * intersect + 1.) / (s_true + s_pred + 1.)

        return res

    def _crossentropy_loss(self, y_true, y_pred):
        """
        Function to compute binary crossentroty loss.

        Parameters
        ----------
            y_true: keras_var
                Ground truth.

            y_pred: keras_var
                Model prediction.

        Returns
        -------
            loss: float
                The binary crossentroty loss.
        """
        
        eps = 1e-4

        # pass the roi as the third channel of the ground truth
        # and then separate it from the ground truth.
        roi = y_true[0,:,:,2]
        roi = np.repeat(roi[:, :, np.newaxis], 2, axis=2)
        # roi = K.repeat_elements(roi, 2, axis=2)
        y_true = y_true[:,:,:,0:-1]

        loss = -(y_true * K.log(y_pred + eps) + (1 - y_true) * K.log(
           1 - y_pred + eps)) * roi

        loss = K.sum(loss)

        return loss

    def test(self):
        """
        Method to test the model.
        """

        # prediction
        print('Testing video: {}_{}'.format(self.test_it.video.category_name, 
                                            self.test_it.video.video_name))

        prob = self.model.predict_generator(self.test_it.flow(),
                                            steps=math.ceil(self.test_it.video.video_len / (self.batch_size*3)),
                                            max_q_size=10, workers=1,
                                            pickle_safe=True,
                                            verbose=1)

        # # save the result images
        # print('Saving the results of video...')

        # compare_path = join(self.test_it.video.result_path, self.model_type, 'compare')
                                    
        # if not isdir(compare_path):
        #     makedirs(compare_path)

        # probmap_path = join(self.test_it.video.result_path, self.model_type, 'prob_map')
                                    
        # if not isdir(probmap_path):
        #     makedirs(probmap_path)

        # for frame_id, frame in tqdm(
        #         enumerate(self.test_it.video.h5file['test']),
        #         total=self.test_it.video.video_len):

        #     # input image
        #     input_img = self.test_it.video.h5file['test'][frame]['input'][:]
        #     input_img = imresize(input_img, tuple(self.test_it.video.new_frame_size),
        #                          interp='nearest')

        #     # ground truth image
        #     gt_img = (self.test_it.video.h5file['test'][frame]['gt'][:])
        #     gt_img = imresize(gt_img, tuple(self.test_it.video.new_frame_size),
        #                       interp='nearest')
        #     gt_img = gt_img[:, :, None] * np.ones(3, dtype=int)[None, None, :]

        #     # estimated probability map
        #     est_img = prob[frame_id, :, :, 1]
        #     est_img = est_img[:, :, None] * np.ones(3, dtype=int)[None, None, :]

        #     # thresholded minary result
        #     bi_img = (est_img > self.binary_thr)

        #     result_img_name = '{:06}.jpg'.format(frame_id + 1)

        #     # save the prob_map
        #     probmap_img_full_path = join(probmap_path, result_img_name)
        #     imsave(probmap_img_full_path, est_img)

        #     # save the input, gt, prob_map, and binarize_map in one image
        #     compare_img_full_path = join(compare_path, result_img_name)
        #     self.save_compare_image(input_img, gt_img, est_img, bi_img,
        #                             compare_img_full_path)

        # evaluation
        print('Evaluating video...')

        precision = 0; recall = 0; f_measure = 0; legal_clip_num = 0
        for clip_id in range(math.ceil(self.test_it.video.video_len / 1000)):
            # import ipdb; ipdb.set_trace()
            start_id = clip_id * 1000
            end_id = min((clip_id + 1) * 1000, self.test_it.video.video_len)

            gt = self.test_it.video.load_test_y(start_id, end_id)
            prob_clip = prob[start_id:end_id, :, :, :]
            precision_tmp, recall_tmp, f_measure_tmp = self._evaluation(gt, prob_clip,
                                                               self.binary_thr)
            precision += precision_tmp
            recall += recall_tmp
            f_measure += f_measure_tmp
            if f_measure_tmp > 0:
                legal_clip_num += 1

        precision /= legal_clip_num
        recall /= legal_clip_num
        f_measure /= legal_clip_num

        # save the evaluation result into a txt file
        text_full_path = join(self.test_it.video.video_path,
                                      'evaluation.txt')
        with open(text_full_path, 'a+') as text_file:
            text_file.write('The %s model: '
                            'precision: %f '
                            'recall: %f '
                            'f-measure: %f\r\n'
                            % (self.model_type, precision, recall, f_measure))

    def save_compare_image(self, input_img, gt_img, est_img, bi_img,
                           compare_img_full_path):
        """
        This method saves the estimated results. For each frame, a compared image 
        includes: (1) input, (2) ground truth, (3) probability map, and (4) a binary 
        result.

        Parameters
        ----------
            input_img: np.array
                Input image

            gt_img: np.array
                Ground truth image

            est_img: np.array
                Estimated probability image

            bi_img: np.array
                Binarized estimated image

            compare_img_full_path: string
                The path to save the compared image
        """

        single_height = self.test_it.video.new_frame_size[0]
        single_width = self.test_it.video.new_frame_size[1]

        compare_img = np.zeros((single_height * 2, single_width * 2, 3))

        compare_img[0:single_height, 0:single_width, :] = input_img

        compare_img[0:single_height, single_width:, :] = gt_img

        compare_img[single_height:, 0:single_width, :] = est_img * 255

        compare_img[single_height:, single_width:, :] = bi_img * 255

        compare_img = imresize(compare_img, 0.5, interp='nearest')

        imsave(compare_img_full_path, compare_img)

    @staticmethod
    def _evaluation(gt, prob, binary_thr):
        """
        This method is used to compute the precision, recall, and f_measure.

        Parameters
        ----------
            gt: np.array
                Ground truth.

            prob: np.array
                The estimated probability map.

            binary_thr: float
                The estimated probability map.

        Returns
        -------
            precision: float
                The precision.
            recall: float
                The recall.
            f_measure: float
                The f-measure.
        """

        gt_unknow = (gt == 170)
        gt_outroi = (gt == 85)

        gt = (gt * (~ gt_outroi) * (~ gt_unknow) == 255).flatten()
        prob = (prob[:, :, :, 1] * (~ gt_outroi) * (~ gt_unknow))
        prob = (prob > binary_thr).flatten()

        precision, recall, f_measure, _ = \
            precision_recall_fscore_support(gt, prob, average='binary')

        return precision, recall, f_measure

    def model_summarize(self):
        """
        This method is used to show a summarized version of the model, like a
        list of the most important parameters, the neural network structure,
        etc.
        Not needed for this dataset.
        """
        self.model.summary()

    def load_model(self, path):
        """
        This method should load the model from the path given in parameters.
        Not needed for this dataset.
        """
        pass

    def export_results(self):
        """
        This method is called after testing to save the current model
        as well as its current parameters so it can be loaded afterwards.
        Not needed for this dataset.
        """
        pass

    def save_model(self):
        """
        This method is called at the end to save the best parameters for the
        model.
        Not needed for this dataset.
        """
        pass
