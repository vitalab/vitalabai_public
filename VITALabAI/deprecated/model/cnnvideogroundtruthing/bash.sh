#!/bin/bash

for video_id in $(seq 2 52)
do
    python3.4 main.py model.cnnvideogroundtruthing.cnnvideogroundtruthing.CNNVideoGroundTruthing -o dataset_root_path='/media/wany1601/57412FAF5495BFC3/Projects/datasets', video_id=$video_id, nb_epochs=50, batch_size=5, model_type='cascade', select_method='manual', training_frame_number=200
    # echo $video_id
done