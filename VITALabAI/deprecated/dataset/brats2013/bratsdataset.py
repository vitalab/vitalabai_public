from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract
import warnings
try:
    import tables
except ImportError:
    warnings.warn("Couldn't import tables, so far SVHN is "
                  "only supported with PyTables")
import numpy

try:
    from itertools import izip
except ImportError:  # python3.x
    izip = zip
    xrange = range

from .lisa_brats.brains import Brain


def save_im(im_patch, label_patch, z, x, y):
    """
    A script which saves patches in the current mini-batch
    to the "figures" directory in the current folder.
    This method is meant to be used for debugging and data visualization.

    Parameters:
    ----------
    im_patch : ndarray
    label_patch : ndarray
    z : scalar
    x : scalar
    y : scalar
    """
    from matplotlib.image import imsave
    from os.path import join

    path = './figures'

    imsave(join(path, 'image_' + 'x_' + str(x) + 'y_' +
                str(y) + 'z_' + str(z) + '.png'), im_patch[:, :, 0])
    imsave(join(path, 'labels_' + 'x_' + str(x) + 'y_' + str(y) +
                'z_' + str(z) + '.png'), label_patch, vmin=0, vmax=1)
    print('saved_' + 'x_' + str(x) + 'y_' + str(y) + 'z_')
    print(label_patch.sum())


def move_to_next_brain(label_end, nb_classes):
    """
    This method serves as an indicator for BRATS dataset class to iterate over the next
    brain in the dataset.
    Parameters
    ----------
    label_end: ndarray
        contains as many rows as number of classes (i.e 5) and 2 columns for 'all' and 
        '32' training patches categories. If the training goes through all the patches
        of a class in a categoriy, that entry in label_end should be 1, else 0.  
    nb_classes: scalar

    Returns
    -------
        Boolean
    """
    if nb_classes == 5:
        non_enh = label_end[3, :]
        enh = label_end[4, :]
        if non_enh.sum() + enh.sum() == 4:  # means of all are True
            return True
        else:
            return False
    elif nb_classes == 2:
        if label_end[1, :].sum() == 1:
            return True
        else:
            return False
    else:
        raise ValueError('Unsupported labels (must be 2 or 5 for now)')


def get_brain_from_h5file(h5file, brain_name):
    """
    Creats a Brain class instance of images and labels given a brain name.
    Parameters
    ----------
    h5file: HDF5 file
    brain_name: string

    Returns:
    -------
        Brain class instance
    """
    images = h5file.get_node('/BRAINS/' + brain_name + '/images').read()
    labels = h5file.get_node('/BRAINS/' + brain_name + '/labels').read()
    brain = Brain(brain_name, images.transpose(),
                  labels.transpose())
    return brain


class Table(object):
    """
    The class Table saves the patch indices for every class.
    """

    def __init__(self, h5file, brain_name, nb_classes):
        """
        Parameters:
        ----------
        h5file : table
        brain_name : string
        nb_classes : scalar
        """
        self.labels_tables = []
        self.lengths = numpy.zeros((nb_classes, 2), dtype=numpy.int)
        self.current_index_training = numpy.zeros(
            (nb_classes, 2), dtype=numpy.int)
        self.current_index_testing = numpy.zeros(
            (nb_classes, 2), dtype=numpy.int)
        self.end_of_labels = numpy.zeros((nb_classes, 2), dtype=numpy.bool)
        print('Loading brain ' + brain_name)
        # For every class create two category of patches. One randomly selected (i.e. 'all') and the 
        # other patcehs with entropy higher than one in label image (i.e. '32')
        for i in range(nb_classes):
            self.labels_tables.append([])
            for j, name in izip(xrange(2), ['all', '32']):
                table = h5file.get_node(
                    '/' + brain_name + '/' + str(i) + '_' + name)
                self.labels_tables[i].append(table)
                self.lengths[i, j] = table.shape[0]


class BRATS(VITALabAiDatasetAbstract):

    """
    The BRATS dataset contains brain MRIs from real patients. The raw mha files were downloaded from virtualskeleton.ch
    BRATS is a dataset class that extracts patches of an arbitrary size from an HDF5 file containing the dataset.
    This dataset can handle both BRATS 2013 and 2015. 
    """

    def __init__(self, path_brains, path_analysis, patch_shape, label_patch_shape, nb_classes, batch_size, num_minibatches_train=None,  axes=('b', 0, 1, 'c'),
                 distribution=None, current_brain_index=None, nb_modalities=4):
        """
        Parameters:
        ----------
        path_brains : string
            path to the brain archive HDF5 file which contains the input images and ground truths.
        path_analysis : string
            path to the brain analysis HDF5 file which contains patch positions.
        patch_shape : tuple of scalars
            size of the input pathc to the model during the train pahse. e.g. (33, 33)
        label_patch_shape : tuple of scalars
            size of output of the model during the train phase. e.g. (1,1)
        nb_classes : scalar
        batch_size : scalar
        num_minibatches_train : scalar
        num_minibatches_test : scalar
        axes : tuple
             default value to ('b', 0, 1, 'c'), which means (batch, hight, weight, classes)
        distribution : list
            Controls the distribution of patch labels in every mini_batch. Default value is None which correstponds to a uniform distribution.
            If not None, the sum of values should equal the batch size. e.g. for batch_size = 128 it can be [100, 6, 10, 6, 6]
        current_brain_index : scalar
        nb_modalities : scalar
            number of modalities in the dataset.
        """

        self.__dict__.update(locals())
        del self.self
        # load data
        self.patch_shape = patch_shape
        self.label_patch_shape = label_patch_shape
        self.sums = numpy.zeros(
            (self.label_patch_shape[0] * self.label_patch_shape[1], nb_classes))
        self.nb_samples = 0
        self.nb_classes = nb_classes
        # BRAINS
        self.h5file = tables.open_file(path_analysis, mode='r')
        self.brain_h5file = tables.open_file(path_brains, mode='r')
        self.nb_modalities = nb_modalities

        # If distribution is not defined then make the minibatch with uniform distribution of class labels
        if distribution is not None:
            assert len(distribution) == self.nb_classes
            distribution = numpy.asarray(distribution, dtype=numpy.int)
            assert distribution.sum() == self.batch_size
        self.distribution = distribution
        self.num_minibatches_train = num_minibatches_train
        if current_brain_index is None:
            self.current_brain_index = 0
        else:
            self.current_brain_index = current_brain_index
        self.end_of_brain = False
        self.brain_list = []
        for d_brain in self.h5file.root:
            self.brain_list.append(d_brain._v_name)

        self.current_brain_name = self.get_brain_name_from_h5file(
            self.current_brain_index)
        self.brain = get_brain_from_h5file(
            self.brain_h5file, self.current_brain_name)
        self.table_c = Table(
            self.h5file, self.current_brain_name, self.nb_classes)

        self.h5file.flush()

    def get_brain_name_from_h5file(self, index):
        """
        Returns the name of the brain associated with the index in the HDF5 file.

        Parameters:
        ----------
        index: int

        Returns:
        -------
            string containing the name of the brain.
        """
        return self.h5file.list_nodes(self.h5file.root)[index]._v_name

    def get_patch(self, z, x, y):
        """
        Extracts a patch given the brain name and the spatial coordinates
        Parameters:
        ----------
        x, y, z : scalar

        Returns:
            tuple of ndarrays
            ndarray of the image data and ndarray of the corresponding one hot vector of the label
        """
        labels = self.brain.labels
        images = self.brain.images
        patch_shape = self.patch_shape
        output_shape = self.label_patch_shape

        # handle the input
        pad = False
        pad_xl = 0
        pad_xr = 0
        pad_yl = 0
        pad_yr = 0

        x_left = x - patch_shape[0] // 2
        x_right = x_left + patch_shape[0]
        y_left = y - patch_shape[1] // 2
        y_right = y_left + patch_shape[1]

        # that means if the patch_shape is even, we don't look at the center, but slightly more at the bottom right.
        # if the patch_shape is odd, we look at the real center

        if x_left < 0:
            pad = True
            pad_xl = -x_left
            x_left = 0
        if x_right > images.shape[1]:
            pad = True
            pad_xr = x_right - images.shape[1]
            x_right = images.shape[1]
        if y_left < 0:
            pad = True
            pad_yl = -y_left
            y_left = 0
        if y_right > images.shape[2]:
            pad = True
            pad_yr = y_right - images.shape[2]
            y_right = images.shape[2]

        slice_x = slice(x_left, x_right)
        slice_y = slice(y_left, y_right)

        im_patch = images[z, slice_x, slice_y, ...]

        if pad:
            im_patch = numpy.pad(
                im_patch, ((pad_xl, pad_xr), (pad_yl, pad_yr), (0, 0)), mode='constant')

        # handle the output
        pad = False
        pad_xl = 0
        pad_xr = 0
        pad_yl = 0
        pad_yr = 0

        x_left = x - output_shape[0] // 2
        x_right = x_left + output_shape[0]
        y_left = y - output_shape[1] // 2
        y_right = y_left + output_shape[1]

        if x_left < 0:
            pad = True
            pad_xl = -x_left
            x_left = 0
        if x_right > labels.shape[1]:
            pad = True
            pad_xr = x_right - labels.shape[1]
            x_right = labels.shape[1]
        if y_left < 0:
            pad = True
            pad_yl = -y_left
            y_left = 0
        if y_right > labels.shape[2]:
            pad = True
            pad_yr = y_right - labels.shape[2]
            y_right = labels.shape[2]

        slice_x = slice(x_left, x_right)
        slice_y = slice(y_left, y_right)

        labels_patch = labels[z, slice_x, slice_y]

        if pad:
            labels_patch = numpy.pad(
                labels_patch, ((pad_xl, pad_xr), (pad_yl, pad_yr)), mode='constant')

        label_onehot_patch = numpy.zeros(
            (self.label_patch_shape[0] * self.label_patch_shape[1], self.nb_classes))
        for i, l in enumerate(labels_patch.flatten()):
            label_onehot_patch[i, l] = 1

        return im_patch.swapaxes(0, 2).swapaxes(1, 2), label_onehot_patch.flatten()

    def iterator(self, num_batches=None, mode=None):
        """
        Iterator over the dataset. Used with next() method.
        Parameters:
        ----------
        num_batches : scalar
        """
        self.count = 0
        # Given a distribution (uniform if None), number of examples from every class in a minibatch is calculated and assigned 
        if self.distribution is None:
            self.num_examples_labels = numpy.ones((self.nb_classes, 2), dtype=numpy.int)
            uniform_base = self.batch_size // (3 * self.nb_classes)
            self.num_examples_labels[:, 0] = uniform_base
            self.num_examples_labels[:, 1] = 2 * uniform_base
            self.num_examples_labels[0, 0] = self.batch_size - \
                self.num_examples_labels.flatten().sum() + \
                self.num_examples_labels[0, 0]
        else:
            self.num_examples_labels = numpy.zeros(
                (self.nb_classes, 2), dtype=numpy.int)
            for i in xrange(self.nb_classes):
                num_for_label = self.distribution[i]
                dec = num_for_label // 2
                self.num_examples_labels[i, 0] = dec
                self.num_examples_labels[i, 1] = num_for_label - dec

            assert self.num_examples_labels.sum() == self.batch_size

        
        if self.num_minibatches_train is None:
            raise ValueError(
                "not able to be used for training: num_minibatches_train not set")

        return self

    def __iter__(self):
        return self

    def next(self):
        """
        Retrieve the next item from the iterator.

        Returns:
        -------
            tuple
        """
        # Load a new brain if end of a brain is reached.
        if self.end_of_brain:
            if self.current_brain_index == len(self.brain_list) - 1:
                self.current_brain_index = -1
            self.current_brain_index += 1
            self.current_brain_name = self.get_brain_name_from_h5file(
                self.current_brain_index)
            self.brain = get_brain_from_h5file(
                self.brain_h5file, self.current_brain_name)
            self.table_c = Table(
                self.h5file, self.current_brain_name, self.nb_classes)
            self.end_of_brain = False

        if ( self.count >= self.num_minibatches_train):
            raise StopIteration()
        X0 = numpy.zeros((self.nb_modalities, self.patch_shape[0], self.patch_shape[
                         1], self.batch_size), dtype=numpy.float32)
        y = numpy.zeros((self.batch_size, self.label_patch_shape[
                        0] * self.label_patch_shape[1] * self.nb_classes), dtype=numpy.float32)
        index = 0
        
        current_index_array = self.table_c.current_index_training.copy()

        # For every class and every category and according to the given distribution 
        # load a patch into X0 and y which correspond to the minibatch data and label
        for i in xrange(self.nb_classes):
            for j in xrange(2):
                num_examples = self.num_examples_labels[i, j]
                current_index = current_index_array[i, j]
                data = self.table_c.labels_tables[i][j].read(
                    start=current_index, stop=current_index + num_examples)
                current_index_array[i, j] += num_examples
                assert len(data) <= num_examples
                if len(data) < num_examples:
                    current_index_array[i, j] = num_examples - len(data)
                    data = numpy.concatenate((data, self.table_c.labels_tables[i][j].read(
                        start=0, stop=num_examples - len(data))), axis=0)
                    self.table_c.end_of_labels[i, j] = True
                    if move_to_next_brain(self.table_c.end_of_labels, self.nb_classes):
                        self.end_of_brain = True
                assert len(data) == num_examples
                for d in data:
                    i0 = self.get_patch( d[1], d[2], d[3])
                    if self.nb_modalities==4:
                        X0[..., index] = i0[0]
                    else:
                        a = i0[0]
                        b = a[[0, 2, 3]] #only select T1c,T2 and Flair.  When having 3 modalities, remove T1
                        X0[..., index] = b
                    y[index, ...] = i0[1]
                    index += 1

        mini_batch = (X0, y)
        assert index == self.batch_size
        self.count += 1
        self.table_c.current_index_training = current_index_array

        return mini_batch

    @property
    def num_batches(self):
        """
        Returns number of mini-batches in train and valid datasets
        """
        
        return self.num_minibatches_train


    @property
    def num_examples(self):
        """
        Returns the number of examples in train and valid datasets
        """
        
        return self.num_minibatches_train * self.batch_size


    def get_entire_dataset(self):
        pass

    def preprocess_dataset(self):
        """
        Preprocessing of the dataset is handled in lisa_brats folder.
        """
        pass

    def generate_dataset(self):
        """
        Generation of the brats dataset is handled by brains.py script, located in datasets/brats/lisa_brats folder.
        """
        pass
