import numpy as np
from scipy.ndimage.morphology import generate_binary_structure
from scipy.ndimage.measurements import label


def find_brain_borders(brain):
    """
    Truncates a given brain to non-zero values

    Parameters:
    ----------
    brain : ndarray

    Returns:
    -------
    ndarray
        returns the coordinates of the brain
    """
    if brain.ndim != 3:
        raise ValueError("brain does not have correct number of dimensions")

    axis0_nonzero = np.where(np.abs(brain).sum(axis=2).sum(axis=1) > 0)
    axis0_nonzero = axis0_nonzero[0]
    min_0 = min(axis0_nonzero)
    max_0 = max(axis0_nonzero)

    axis1_nonzero = np.where(np.abs(brain).sum(axis=2).sum(axis=0) > 0)
    axis1_nonzero = axis1_nonzero[0]
    min_1 = min(axis1_nonzero)
    max_1 = max(axis1_nonzero)

    axis2_nonzero = np.where(np.abs(brain).sum(axis=1).sum(axis=0) > 0)
    axis2_nonzero = axis2_nonzero[0]
    min_2 = min(axis2_nonzero)
    max_2 = max(axis2_nonzero)

    return min_0, max_0, min_1, max_1, min_2, max_2


def truncate_brain(images, labels):
    """
    truncate_brain is a python method that takes as input a whole brain and outputs
    a truncated version with zero value rows and columns removed.

    Parameters:
    ----------
    images : ndarray
        image data of the brain to be truncated
    labels : ndarray
        labels data of the brain to be truncated

    Returns:
    -------
    ndarray
    """
    if images.ndim != 4:
        raise ValueError(
            "image tensor does not have corrent number of dimensions")
    if labels.ndim != 3:
        raise ValueError(
            "label tensor does not have corrent number of dimensions")

    min_0, max_0, min_1, max_1, min_2, max_2 = find_brain_borders(images[
                                                                  :, :, :, 0])

    images = images[min_0:max_0, min_1:max_1, min_2:max_2, :]
    labels = labels[min_0:max_0, min_1:max_1, min_2:max_2]
    return images, labels


def shuffle_slices(train, nb_slices=None):
    """
    The shuffle_slices method takes as input a BrainSet class instance
    and outputs a tuple containing the np arrays of shuffled slices
    and their corresponding labels.

    Parameters:
    ----------
    train : A BrainSet class instance
    nb_slices : int
        number of slices which the user wants each hybrid brain contain. 

    Returns:
    -------
    A tuple containing brain images and corresponding labels
    """
    brain_names = train.get_brains_names()
    h_max = 0
    w_max = 0
    nb_brain_slices = 0
    for name in brain_names:
        brain_shape = train.get_brain_by_name(name).images.shape
        nb_brain_slices += brain_shape[0]
        if brain_shape[1] > h_max:
            h_max = brain_shape[1]
        if brain_shape[2] > w_max:
            w_max = brain_shape[2]

    if nb_slices is None:
        nb_slices = nb_brain_slices

    brains = np.zeros(
        (nb_slices, h_max, w_max, brain_shape[3]), dtype=np.float32)
    labels = np.zeros((nb_slices, h_max, w_max), dtype=np.int)
    current_slice = 0
    for name in brain_names:
        subject = train.get_brain_by_name(name).images
        subject_label = train.get_brain_by_name(name).labels

        brains[current_slice:current_slice + subject.shape[0],
               0:subject.shape[1], 0:subject.shape[2], :] = subject
        labels[current_slice:current_slice + subject.shape[0],
               0:subject.shape[1], 0:subject.shape[2]] = subject_label

        current_slice = current_slice + subject.shape[0]

    rng = np.random.RandomState(seed=1234)
    combined = list(zip(brains, labels))
    rng.shuffle(combined)
    brains[:], labels[:] = zip(*combined)
    return (brains, labels)


def prediction_get_labels_happy(prediction, th=0.5):
    '''
    Makes the segmentation from probabilities.

    Parameters:
    ----------
    prediction : ndarray
        class probabilities
    th : float
        segmentaiton threshold

    Returns:
    -------
        ndarray
    '''
    if len(prediction.shape) == 4:
        num_classes = prediction.shape[0]
        # needs an argmax
        if num_classes == 5:
            is_tumor = (prediction[1, ...] + prediction[2, ...] +
                        prediction[3, ...] + prediction[4, ...]) >= 0.5
            is_core = (prediction[1, ...] +
                       prediction[3, ...] + prediction[4, ...]) >= 0.5
            is_enhancing = (prediction[4, ...]) >= 0.5
            prediction_ret = prediction.argmax(axis=0)
            prediction_ret[is_tumor] = 2
            prediction_ret[is_core] = 1
            prediction_ret[
                is_core * (prediction[3, ...] >= prediction[1, ...])] = 3
            prediction_ret[is_enhancing] = 4
            prediction = prediction_ret
        elif num_classes == 2:
            prediction = (prediction[1]) >= float(th)
    return prediction


def postprocess_label_image(label_image, min_size, connectivity=3, min_height=15, min_diam=15, diam_height_prop=3):
    """
    Postprocess the segmetation map using connected components.

    Parameters:
    ----------
    label_image : ndarray
    min_size : int
    connectivity : 1, 2, 3
    min_height : int
    min_diam : int
    diam_height_prop : int

    Returns:
    -------
        ndarray

    """
    connectivity = 3

    min_height = 15
    min_diam = 15
    diam_height_prop = 3
    original_mask = (label_image != 0)
    # create a structure that defines the neighborhood
    s = generate_binary_structure(3, connectivity)
    # create an image where each blob as its own label in range(1, num_lable +
    # 1) 0 is for background
    labeled_image, num_label = label(original_mask, structure=s)
    # a mask of the size of the image that is going to be an accumulator
    new_mask = np.zeros(original_mask.shape)

    # loop on the different blobs
    for current_label in range(1, num_label + 1):
        # take the current_label blob
        current_labeled_image = (labeled_image == current_label)
        # count the number of pixel in it
        current_count = current_labeled_image.sum()
        # take the indexes of the pixels that are in the blob
        indexes = np.argwhere(current_labeled_image)
        # compute the min over the rows of indexes with has the shape (z, x, y)
        current_min = indexes.min(axis=0)
        current_max = indexes.max(axis=0)
        # compute the height of the blob -1
        height = current_max[0] - current_min[0]
        diam = max(current_max[1:] - current_min[1:])
        # some criteria to keep the blob
        if (current_count >= min_size) and (height >= min_height or diam >= min_diam) and (diam <= height * diam_height_prop):
            new_mask += (labeled_image == current_label)
    to_return = (label_image * new_mask)
    return to_return
