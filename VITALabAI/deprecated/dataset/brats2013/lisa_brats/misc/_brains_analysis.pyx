cdef extern from "math.h":
    float log2f(float x) nogil

from cython cimport view, boundscheck, wraparound, cdivision
from cython.parallel cimport prange

DEF BOUNDS_CHECK = False  # Enable this if debugging a segfault.


@wraparound(False)
@boundscheck(BOUNDS_CHECK)
cdef inline void fill_2d_integral_table(char[:, :] slice_labels,
                                        int[:, :, :] out) nogil:
    cdef int i, j, k
    cdef int above, beside, diag
    cdef char cur
    for i in range(0, slice_labels.shape[0]):
        for j in range(0, slice_labels.shape[1]):
            for k in range(out.shape[2]):
                above = out[i - 1, j, k] if i > 0 else 0
                beside = out[i, j - 1, k] if j > 0 else 0
                diag = out[i - 1, j - 1, k] if i > 0 and j > 0 else 0
                out[i, j, k] = above + beside - diag
            out[i, j, slice_labels[i, j]] += 1


@wraparound(False)
@boundscheck(BOUNDS_CHECK)
cdef inline void _integrate(int[:, :, :] image, int start_row,
                            int start_col, int end_row,
                            int end_col, int[:] ret) nogil:
    cdef int k
    for k in range(image.shape[2]):
        ret[k] = image[end_row, end_col, k]
        if start_row > 0 and start_col > 0:
            ret[k] += image[start_row - 1, start_col - 1, k]
        if start_row > 0:
            ret[k] -= image[start_row - 1, end_col, k]
        if start_col > 0:
            ret[k] -= image[end_row, start_col - 1, k]


@cdivision(True)
@wraparound(False)
@boundscheck(BOUNDS_CHECK)
cdef inline void compute_slice_patch_entropies(int[:, :, :] integral,
                                               long neighbourhood,
                                               int[:] label_counts,
                                               float[:, :] result) nogil:
    cdef int row, col, label
    cdef float num_pixels = <float>(neighbourhood * neighbourhood)
    # TODO: check for off-by-one errors
    cdef int nover2 = neighbourhood // 2
    # We generate the center pixel by patch_width // 2, floor division.
    # So the center pixel of a 4x4 OR a 5x5 patch with corner (0,0) is (2,2).
    # To recover the boundaries of the patch we thus need to subtract (2,2)
    # and add (2 - (1 - patch_width % 2), 2 - (1 - patch_width % 2)), so that
    # in the 4x4 case we do center - 2:center + 1 (because patch_width % 2
    # is 0 so 1 - patch_width - 2 is 1), and the opposite in the odd case.
    cdef int minus = nover2
    cdef int plus = nover2 - (1 - neighbourhood % 2)
    cdef int p_lo, p_hi, p_l, p_r, count
    cdef int rows = result.shape[0]
    cdef int cols = result.shape[1]
    cdef float z, logz, rr
    for row in range(integral.shape[0]):
        for col in range(integral.shape[1]):
            p_lo = row - minus
            p_hi = row + plus
            p_l = col - minus
            p_r = col + plus
            if p_lo < 0 or p_l < 0 or p_hi >= rows or p_r >= cols:
                result[row, col] = 0
            else:
                _integrate(integral, p_lo, p_l, p_hi, p_r, label_counts)
                rr = 0
                for label in range(integral.shape[2]):
                    count = label_counts[label]
                    z = (<float>count) / num_pixels
                    logz = log2f(z) if z > 0 else 0.
                    rr -= z * logz
                result[row, col] = rr


@wraparound(False)
@boundscheck(BOUNDS_CHECK)
cpdef float[:, :, :, :] compute_entropies(char[:, :, :] labels,
                                          long[:] neighbourhoods,
                                          int num_classes):
    """
    Compute a map of patch label entropies.

    Parameters
    ----------
    labels : 3-tensor, char
        The labels of a brain volume as (slices, rows, cols).
        Must be char (int8), must contain no values equal to
        or greater than `num_classes`.
    neighbourhoods : vector, int64
        The square patch sizes to compute entropies for.
        An array containing [32, 16, 4] will compute entropies
        of 32x32 regions, 16x16 regions, 4x4 regions.
    num_classes : int
        The total number of classes (for BRATS, 5).

    Returns
    -------
    result : memoryview of a Cython array
        Size (neighbourhoods.shape[0], slices, rows, cols).
        Contains a map, for each neighbourhood size, of the entropies
        at each patch location. Border regions that do not have a nxn
        patch surrounding them are marked with 0.
        Coerceable to an ndarray with `np.asarray`.
    """
    cdef int num_slices = labels.shape[0]
    cdef int num_rows = labels.shape[1]
    cdef int num_cols = labels.shape[2]
    cdef int slice, row, col, nhood
    cdef int[:, :, :, :] integral = view.array(shape=(labels.shape[0],
                                                      labels.shape[1],
                                                      labels.shape[2],
                                                      num_classes),
                                               itemsize=sizeof(int),
                                               format='i')
    cdef float[:, :, :, :] result = view.array(shape=(neighbourhoods.shape[0],
                                                      labels.shape[0],
                                                      labels.shape[1],
                                                      labels.shape[2]),
                                               itemsize=sizeof(float),
                                               format='f')

    cdef int[:] label_counts = view.array(shape=(integral.shape[2],),
                                          itemsize=sizeof(int),
                                          format='i')
    for slice in range(num_slices):
        fill_2d_integral_table(labels[slice], integral[slice])

    for nhood in range(neighbourhoods.shape[0]):
        for slice in range(labels.shape[0]):
            compute_slice_patch_entropies(integral[slice],
                                          neighbourhoods[nhood],
                                          label_counts,
                                          result[nhood, slice])
    return result
