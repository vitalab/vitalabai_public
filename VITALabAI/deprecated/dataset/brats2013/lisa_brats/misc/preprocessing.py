try:
    from itertools import izip
except ImportError:  # python3.x
    izip = zip
import numpy as np

_EPS = 1e-16


def standardize_nonzeros(tensor, dtype='float32'):
    """
    Normalizing tensor to zero mean and standard deviation one

    Parameters
    ----------
    tensor : ndarray
        The thing to normalize.
    dtype : str or dtype
        The dtype to cast to before normalizing.

    Returns
    -------
    preprocessed : ndarray
        Ndarray with same dimensions but possibly different
        dtype as `tensor`.
    """
    tensor = tensor.astype(dtype)
    axis = 3  # The third axis is the channel axis
    swapped = tensor.swapaxes(0, axis)
    means = np.array([t[t != 0].mean() for t in swapped])
    stds = np.array([t[t != 0].std() for t in swapped])
    for slice, mean, std in izip(swapped, means, stds):
        # Modifies swapped in-place.
        mask = slice != 0
        slice[mask] -= mean
        slice[mask] /= std + _EPS
    tensor = swapped.swapaxes(0, axis)
    return tensor
