import numpy

"""
Calculating the Dice score, etc of the data.

based on
http://hal.inria.fr/docs/00/93/56/40/PDF/brats_preprint.pdf

labels:
0: not a tumor
1: necrosis
2: Edema
3: non-enhancing tumor
4: enhancing tumor

Complete Tumor : 1,2,3,4
Tumor Core     : 1,3,4
Enhance Tumor  : 4
"""


"""
dice_score:
parameters:
    P1: boolean 1D numpy array
    T1: boolean 1D numpy array
    P1 and T1 have same length
returns:
    The DICE score (see http://hal.inria.fr/docs/00/93/56/40/PDF/brats_preprint.pdf)
    
"""
def dice_score(P1,T1):
    numerator = 2.*float(numpy.sum(P1*T1))
    denominator = (float(numpy.sum(P1)+numpy.sum(T1)))
    if denominator == 0:
        return 1 # expected and predicted are same
    else:
        return numerator / denominator

"""
rate_result:
parameters: 
    predicted: 1D numpy array
    truth: 1D numpy array
returns:
    DICE score for the Complete Tumor,
    DICE score for the Tumor Core,
    DICE score for the Enhance Tumor
"""

def rate_result(predicted, truth):
    assert predicted.ndim == 1
    assert truth.ndim == 1
    ct_score = dice_score(predicted >= 1, truth >= 1)
    tc_score = dice_score((predicted >= 3) + (predicted == 1), (truth >= 3) + (truth == 1))
    et_score = dice_score(predicted == 4, truth == 4)
    return ct_score, tc_score, et_score

"""
BRATS_rate_results:
parameters:
    predictions: list (or array) of 1D arrays corresponding to predicted labels
    truth: list (or array) of 1D arrays corresponding to expected labels
returns:
    For each part of the tumor (Complete Tumor, Tumor Core, Enhance Tumor),
    the mean DICE score, the std deviation and the median DICE score.
"""

def BRATS_rate_results(predictions, truth_values):
    ct_scores = numpy.zeros(len(predictions))
    tc_scores = numpy.zeros(len(predictions))
    et_scores = numpy.zeros(len(predictions))
    for i in range(len(predictions)):
        ct_scores[i], tc_scores[i], et_scores[i] = rate_result(predictions[i],truth_values[i])
    return [[numpy.mean(ct_scores), numpy.std(ct_scores), numpy.median(ct_scores)],
            [numpy.mean(tc_scores), numpy.std(tc_scores), numpy.median(tc_scores)],
            [numpy.mean(et_scores), numpy.std(et_scores), numpy.median(et_scores)]]
