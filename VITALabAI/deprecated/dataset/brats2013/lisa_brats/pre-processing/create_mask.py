import os
import os.path
import numpy as np
import itk


def save_file_mask(entry_filepath, output_filepath):
    """
    Creates a brain mask which is used by pre-processing scripts. 

    Parameters:
    ----------
    entry_filepath: string
        path the brain image file

    output_filepath: string 
        path to the masked brain image file

    """
    image_type = itk.Image[itk.SS, 3]
    itk_py_converter = itk.PyBuffer[image_type]
    reader = itk.ImageFileReader[image_type].New()
    reader.SetFileName(entry_filepath)
    reader.Update()
    out = reader.GetOutput()
    print ('get content')
    content = itk_py_converter.GetArrayFromImage(out)
    content = np.array(content, dtype=np.short)
    print ('get mask')
    mask = (content != 0)
    print ("mask :", mask.sum(), "on", mask.size, "pixels.")
    writer = itk.ImageFileWriter[image_type].New()
    writer.SetFileName(output_filepath)
    itk_image = itk_py_converter.GetImageFromArray(mask.tolist())
    itk_image.SetSpacing(out.GetSpacing())
    itk_image.SetOrigin(out.GetOrigin())
    writer.SetInput(itk_image.GetPointer())
    writer.UseCompressionOn()
    writer.Update()

path = '.'
for dir in os.listdir(path):
    newpath = os.path.join('.', dir)
    if not os.path.isdir(newpath):
        continue
    for dir2 in os.listdir(newpath):
        newpath2 = os.path.join(newpath, dir2)
        if not os.path.isdir(newpath2):
            continue
        for f in os.listdir(newpath2):
            if f.count(dir2) > 0:
                filepath = os.path.join(newpath2, f)
                print (filepath)
                (filepath_no_ext, ext) = os.path.splitext(filepath)
                assert ext == '.mha'
                save_file_mask(filepath, filepath_no_ext + 'mask.mha')
