for f in *mask.mha; do
    f2=`echo $f | sed -e 's/mask//'`;
    ImageMath 3 ${f2}_N4ITK_temp1.mha TruncateImageIntensity ${f2} 0.01 0.99 200;
    if echo "$f2" | egrep -q T1c;
    then
        N4BiasFieldCorrection -d 3 -i ${f2}_N4ITK_temp1.mha -x $f -o ${f2}_N4ITK_temp2.mha -b [200] -s 2 -c[50x50x50x10,0];
        ImageMath 3 ${f2}_N4ITK_temp1.mha m $f ${f2}_N4ITK_temp2.mha;
        rm ${f2}_N4ITK_temp2.mha
    fi
    ImageMath 3 ${f2}_N4ITK.mha m $f ${f2}_N4ITK_temp1.mha;
    rm ${f2}_N4ITK_temp1.mha
done