import argparse
import os

import numpy as np
import tables
import yaml

from VITALabAI.deprecated.dataset.brats2013 import shuffle_slices
from VITALabAI.deprecated.dataset.brats2013 import standardize_nonzeros
from VITALabAI.deprecated.dataset.brats2013 import truncate_brain

try:
    from itertools import izip
except ImportError:  # python3.x
    izip = zip
try:
    import SimpleITK as itk
except ImportError:
    print('Warning: SimpleITK is not installed. The model will run anyways!')

from itertools import product
from VITALabAI.deprecated.dataset.brats2013 import compute_entropies
_DEFAULT_SEED = (2014, 5, 29)
seed = 1337
rng = np.random.RandomState(seed)

"""
python brain.py in the data directory will load
the n4itk preprocessed data in mha format (float),
and save into train and test sets. It also makes the analysis file which contains the coordinate of training patches.
"""

_DEFAULT_SEED = (2014, 3, 28)


class Unit(tables.IsDescription):
    """
    Subclass to cell categories for the table
    """
    brain_name = tables.StringCol(32, pos=0)
    z = tables.IntCol(pos=1)
    x = tables.IntCol(pos=2)
    y = tables.IntCol(pos=3)


def make_analysis_file(brains_archive):
    # make the Analysis file. The Analysis file contains the index of training patches for every
    # class and for every brain in the DATATSET hdf5 file.

    def create_patches_info(brain, nb_classes):
        """
        This method computes the entropies on the label image for the given brain and saves the coordinates to the already open
        HDF5 analysis file.
        Parameters:
        ----------
        brain : A Brain class object
        nb_classes : scalar
        """

        brain_name = brain.name
        print(brain_name)
        shape = brain.labels.shape
        num_pending = 0
        n = 5
        labels_pendingne = [[] for i in range(n)]
        labels_pendingnne32 = [[] for i in range(n)]
        labels_pendingnne8 = [[] for i in range(n)]
        assert shape == brain.images.shape[0:3]
        labels = np.asarray(brain.labels, dtype=np.int8)

        entropies = compute_entropies(labels, np.array([32, 8]), nb_classes)
        zlast = -1
        # z, x, y correspond to depth, hight and width respectively
        for z, x, y in product(range(shape[0]), range(shape[1]), range(shape[2])):
            if np.all(brain.images[z, x, y, :] == 0):
                continue

            label = labels[z, x, y]
            if z != zlast:
                print('slice', z)
                zlast = z

            labels_pendingne[label].append((
                brain_name, z, x, y
            ))
            if entropies[0, z, x, y] > 0.:
                labels_pendingnne32[label].append((
                    brain_name, z, x, y
                ))
            if entropies[1, z, x, y] > 0.:
                labels_pendingnne8[label].append((
                    brain_name, z, x, y
                ))
            num_pending += 1

        for l in range(nb_classes):
            rng.shuffle(labels_pendingne[l])
            rng.shuffle(labels_pendingnne32[l])
            rng.shuffle(labels_pendingnne8[l])

        for l in range(nb_classes):
            if len(labels_pendingne[l]) > 0:
                ltablesnne[l].append(labels_pendingne[l])
                labels_pendingne[l] = []
            if len(labels_pendingnne32[l]) > 0:
                ltablesnne32[l].append(labels_pendingnne32[l])
                labels_pendingnne32[l] = []
            if len(labels_pendingnne8[l]) > 0:
                ltablesnne8[l].append(labels_pendingnne8[l])
                labels_pendingnne8[l] = []

        h5file.flush()

    f = tables.open_file(brains_archive, mode="r")
    brains_temp = f.root.BRAINS
    length = 0
    analysis_file_path = brains_archive + '_ANALYSIS'
    rng = np.random.RandomState(_DEFAULT_SEED)

    for bb in brains_temp:
        length += 1
    brain_set_length = length
    current_path = os.path.join(analysis_file_path)
    h5file = tables.open_file(
        current_path, mode="w", title="BRATS_DATASET")
    filters = tables.Filters(complib='blosc')

    for current_brain in brains_temp:
        name = current_brain._v_name
        brain = Brain(name, current_brain.images.read().transpose(),
                      current_brain.labels.read().transpose())

        ltablesnne = []
        ltablesnne32 = []
        ltablesnne8 = []
        node = h5file.create_group(
            h5file.root, brain.name, filters=filters)

        for label in range(nb_classes):
            mode = str(label) + "_" + "all"
            table = h5file.create_table(
                node, mode, description=Unit, filters=filters, expectedrows=50 * 200 * 200 * brain_set_length)
            ltablesnne.append(table)
            mode = str(label) + "_" + "32"
            table = h5file.create_table(
                node, mode, description=Unit, filters=filters, expectedrows=10 * 200 * 200 * brain_set_length)
            ltablesnne32.append(table)
            mode = str(label) + "_" + "8"
            table = h5file.create_table(
                node, mode, description=Unit, filters=filters, expectedrows=5 * 200 * 200 * brain_set_length)
            ltablesnne8.append(table)

        create_patches_info(brain, nb_classes)

    f.close()
    h5file.close()


class Brain(object):
    """
    Contains the information (i.e. image data, label data, name, etc) of an individual brain.
    """

    def _itk_load_file(self, directory, n4itk_fname, original_fname):
        """
        Finds the path for T1c, T1, Flair, T2 modalities in a directory

        Parameters:
        ----------
        directory : string
        n4itk_fname : string
        original_fname : string
        """
        # Isolated itk import and itk-related functionality so that
        # this class/module can still function without ITK installed.

        if directory.count('T1c') > 0:
            assert n4itk_fname
            image = itk.ReadImage(n4itk_fname)
            T1c = itk.GetArrayFromImage(image)
            if (np.sum(image.GetDirection())<0):
                T1c = np.flip(np.flip(T1c, axis=1), axis=2)
            self._modalities['T1c'] = T1c

        elif directory.count('T1') > 0 or directory.count('40452') or directory.count('40520'):
            assert n4itk_fname
            image = itk.ReadImage(n4itk_fname)
            T1 = itk.GetArrayFromImage(image)
            if (np.sum(image.GetDirection())<0):
                T1 = np.flip(np.flip(T1, axis=1), axis=2)
            self._modalities['T1'] = T1

        elif directory.count('T2') > 0:
            assert n4itk_fname
            image = itk.ReadImage(n4itk_fname)
            T2 = itk.GetArrayFromImage(image)
            if (np.sum(image.GetDirection())<0):
                T2 = np.flip(np.flip(T2, axis=1), axis=2)
            self._modalities['T2'] = T2

        elif directory.count('Flair') > 0:
            assert n4itk_fname
            image = itk.ReadImage(n4itk_fname)
            Flair = itk.GetArrayFromImage(image)
            if (np.sum(image.GetDirection())<0):
                Flair = np.flip(np.flip(Flair, axis=1), axis=2)
            self._modalities['Flair'] = Flair
        # truth values but not gray matter

        elif directory.count('OT') > 0 and directory.count('matter') <= 0:
            print(original_fname)
            assert original_fname
            image = itk.ReadImage(original_fname)
            Truth = itk.GetArrayFromImage(image)
            self._modalities['Truth'] = Truth

    def __init__(self, name, images=None, labels=None):
        """
        Parameters:
        ----------
        name : string
            name of the brain
        images : ndarray
        labels : ndarray
        """
        self.name = name
        self.images = images
        self.labels = labels

    @classmethod
    def load_itk(cls, name, base_dir):
        """
        Initializes a Brain class object and loads the modalities into self.images and the ground truth to self.labels

        Parameters:
        ----------
        name : string
            name of the brain
        base_dir : string
            base folder containing all modality folders

        Returns:
        -------
        Class instance Brain
        """
        brain = cls(name=name)
        assert name
        assert base_dir is not None
        path = os.path.join(base_dir, name[-1:2], name[0:])
        print('processing new folder')
        print(path)
        try:
            brain._modalities = {}

            for dir in os.listdir(path):
                newpath = os.path.join(path, dir)
                print(newpath)

                if os.path.isfile(newpath):
                    if dir.count('.mha') > 0:
                        n4itk_fname = None
                        original_fname = None
                        brain._itk_load_file(dir, n4itk_fname, original_fname)
                        continue
                    else:
                        continue

                n4itk_fname = None
                original_fname = None
                for f in os.listdir(newpath):
                    if f.count(dir) > 0:
                        if f.count('N4ITK') > 0:
                            n4itk_fname = os.path.join(newpath, f)
                    if f.count('mask') == 0 and f.count('processing') == 0 and f.count('.txt') == 0 and f.count('N4ITK') == 0:
                        original_fname = os.path.join(newpath, f)

                print(n4itk_fname)
                print(original_fname)
                brain._itk_load_file(dir, n4itk_fname, original_fname)
            Flair, T1, T1c, T2, Truth = map(brain._modalities.get,
                                            ['Flair', 'T1', 'T1c',
                                             'T2', 'Truth'])
            print([type(current) for current in map(brain._modalities.get,
                                                    ['Flair', 'T1', 'T1c', 'T2', 'Truth'])])
            brain.images = np.concatenate([Flair[..., np.newaxis],
                                           T1[..., np.newaxis],
                                           T1c[..., np.newaxis],
                                           T2[..., np.newaxis],
                                           ],
                                          axis=3)

            if Truth is not None:
                brain.labels = Truth
            else:  # test data
                brain.labels = - np.ones(Flair.shape, dtype=np.int)
        finally:
            del brain._modalities
        assert len(brain.images) == len(brain.labels)
        return brain


class BrainSet(object):
    """
    A class containing the entire dataset.
    """

    def __init__(self, brains=None):
        """
        brains : A dictionary of Brain class instances.
        """
        self.brains = brains if brains is not None else {}

    @classmethod
    def load_itk(cls, names, base_dir, trainset_flag):
        """
        Initializes a BrainSet class object and loads the modalities in self.brains

        Parameters:
        ----------
        names : list
            list of strings containing name of brains
        base_dir : string
        trainset_flag : bool

        Returns:
        -------
        BrainSet class object
        """
        brain_set = cls()
        for name in names:
            assert name not in brain_set.brains
            brain = Brain.load_itk(name, base_dir)
            brain.images = standardize_nonzeros(brain.images)
            if trainset_flag:
                brain.images, brain.labels = truncate_brain(
                    brain.images, brain.labels)
            brain_set.brains[name] = brain
        return brain_set

    def get_brains(self):
        """
        Returns:
        -------
        A dictionary containing all the Brain class instances.
        """
        return dict(self.brains)

    def get_brains_names(self):
        """
        Returns:
        -------
        A list containing brain names
        """
        return self.brains.keys()

    def get_brain_by_name(self, name):
        """
        Returns:
        -------
        A Brain class instance
        """
        return self.brains[name]

    def remove_brain_from_set(self, name):
        """
        Removes a Brain class instance from self.brains

        Parameters:
        ----------
        name : string
        """
        del self.brains[name]

    def add_brain_to_set(self, brain):
        """
        Adds a Brain to self.brains

        Parameters:
        ----------
        Brain class instance
        """
        self.brains[brain.name] = brain

    def save(self, path_or_file_obj):
        """
        Saves the BrainSet archive into an HDF5 file.

        Parameters:
        ----------
        path_or_file_obj : string
            path to save the HDF5 file.
        """
        self._save(path_or_file_obj)

    def _save(self, f):
        """
        Saves the BrainSet archive into an HDF5 file.

        Parameters:
        ----------
        f : string
            path to save the HDF5 file.
        """
        names = list(sorted(iter(self.brains.keys())))

        self.h5file = tables.open_file(f, mode="w", title="BRATS Dataset")
        filters = tables.Filters(complib='blosc')
        brain_group = self.h5file.create_group(self.h5file.root, "BRAINS")
        atom_images = tables.Float32Atom()
        atom_labels = tables.UInt8Atom()

        for bname in names:
            print(bname)
            brain = self.brains[bname]
            brain_node = self.h5file.create_group(brain_group, bname)
            brain_node_images_array = self.h5file.create_carray(brain_node, 'images', atom=atom_images,
                                                                shape=reversed(
                                                                    brain.images.shape),
                                                                title="images", filters=filters)
            brain_node_labels_array = self.h5file.create_carray(brain_node, 'labels', atom=atom_labels,
                                                                shape=reversed(
                                                                    brain.labels.shape),
                                                                title="labels", filters=filters)
            for current_canal in range(brain.images.shape[3]):
                for current_slice in range(brain.images.shape[2]):
                    brain_node_images_array[current_canal, current_slice, ...] = brain.images[
                        ..., current_slice, current_canal].transpose()
            for current_slice in range(brain.labels.shape[2]):
                brain_node_labels_array[
                    current_slice, ...] = brain.labels[..., current_slice].transpose()
            self.h5file.flush()
        self.h5file.close()

    def __len__(self):
        return len(self.brains)

    @classmethod
    def from_path(cls, path, mmap_mode=None):
        """
        Initializes a BrainSet class from an already saved HDF5 file.

        Parameters:
        ----------
        path : string
            path to the HDF5 file.

        Returns:
        -------
        BrainSet class instance
        """
        try:
            f = np.load(path, mmap_mode=mmap_mode)
            names = f['names']
            images = f['images']
            labels = f['labels']
            brains = [(name, Brain(name, images=im, labels=lbl))
                      for name, im, lbl in zip(names, images, labels)]
            obj = cls.__new__(cls)
            obj.brains = dict(brains)
            return obj
        except IOError:
            f = tables.open_file(path, mode="r")
            brains_temp = f.root.BRAINS
            brains = dict()
            for current_brain in brains_temp:
                name = current_brain._v_name
                brains[name] = Brain(name, current_brain.images.read().transpose(),
                                     current_brain.labels.read().transpose())
            f.close()
            obj = cls.__new__(cls)
            obj.brains = dict(brains)
            return obj


def make_argument_parser():
    parser = argparse.ArgumentParser(description='Load brain data and dump it '
                                                 'to a np zip archive, '
                                                 'loadable by the BrainSet '
                                                 'class.')
    parser.add_argument('-D', '--paths', type=str, required=True,
                        help='yaml file containing paths to train, valid and test folders')

    return parser


def make_hybrid_Brainset(train):
    """
    Shuffles the slices of all brains containing in train and returns a new BrainSet instance

    Parameters:
    ----------
    train : BrainSet class instance

    Returns:
    -------
    A BrainSet class instance
    """
    stacked_brains, stacked_labels = shuffle_slices(train)
    hybrid_set = BrainSet()
    # number of slices for each hybridBrain
    num_hybridBrains = len(stacked_brains) // 400
    for i in range(num_hybridBrains + 1):
        name_hybridBrain = 'hybridBrain_' + str(i)
        images = stacked_brains[i * 400:(i + 1) * 400, ...]
        labels = stacked_labels[i * 400:(i + 1) * 400, ...]

        brain = Brain(name=name_hybridBrain, images=images, labels=labels)
        hybrid_set.brains[name_hybridBrain] = brain
    return hybrid_set


if __name__ == "__main__":
    parser = make_argument_parser()
    args = parser.parse_args()
    nb_classes = 5
    f = open(args.paths)
    paths = yaml.load(f)

    # Make the train set hdf5 file and the analysis file
    brain_names = [name for name in os.listdir(paths['training_data']) if os.path.isdir(
        os.path.join(paths['training_data'], name))]
    train = BrainSet.load_itk(brain_names, paths['training_data'], True)
    train = make_hybrid_Brainset(train)
    train_out = paths['trainset_file']
    train.save(train_out)
    make_analysis_file(brains_archive=train_out)

    # Make the train test hdf5 file if the key exists
    try:
        train_test_out = paths['trainset_test_file']
        brain_names = [name for name in os.listdir(paths['training_data']) if os.path.isdir(
            os.path.join(paths['training_data'], name))]
        train_test = BrainSet.load_itk(
            brain_names, paths['training_data'], False)
        train_test.save(train_test_out)
    except:
        pass

    # Make the valid set hdf5 file
    brain_names = [name for name in os.listdir(paths['validation_data']) if os.path.isdir(
        os.path.join(paths['validation_data'], name))]
    valid = BrainSet.load_itk(brain_names, paths['validation_data'], False)
    valid_out = paths['validset_file']
    valid.save(valid_out)

    # Make the test set hdf5 file
    brain_names = [name for name in os.listdir(
        paths['test_data']) if os.path.isdir(os.path.join(paths['test_data'], name))]
    test = BrainSet.load_itk(brain_names, paths['test_data'], False)
    test_out = paths['testset_file']
    test.save(test_out)
