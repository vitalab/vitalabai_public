"""
Author: Yi Wang

This file is used to generate the video ground truthing dataset hdf5 file
for each video. The testing data includes all the frames in a video. The 
training data is indicated by  

To generate the test file:
python3 dataset/cnnvideogroundtruthing/cnnvideogroundtruthingdatasetgenerator.py /path/of/the/dataset test

To generate the train file:
python3 dataset/cnnvideogroundtruthing/cnnvideogroundtruthingdatasetgenerator.py /path/of/the/dataset train manual 200

Parameters
----------
    root_dataset_path: string
        The path of the root dataset, in which should contains the 'CDnet' dataset

    mode: string
        1. test: generate the test data, should be generated first.
        2. train: generate the train data, should be generated after the test data.

    select_method: string (only for generating the training data)
            The strategy to select the training frames:
                1. uniform: select the training frames uniformly;
                2. random : select the training frames randomly;
                3. manual : select the training frames manually.

    training_frame_number: int (only for generating the training data)
        Number of frames for to train the model:
            1. 50;
            2. 100;
            3. 150;
            4. 200.
"""


import sys
from os import listdir, makedirs
from os.path import join, exists

import h5py
from scipy.misc import imread
from tqdm import tqdm

from VITALabAI.deprecated.dataset.cnnvideogroundtruthing.utils import video_names


def generate_test_dataset(cdnet_dataset_path, gting_dataset_path):
    """
    This method generates an hdf5 files to store both the input and the gt
    of all frames for a video. The data are stored in the 'test' group.

    The roi of the video is stored in the 'roi' group.

    Each hdf5 file also calculates the two attributes:
        frame_size: The size of the frame, [h, w, c].
        len: The length of the video.

    Parameters
    ----------
        cdnet_dataset_path: string
            The path of the cdnet dataset.

        gting_dataset_path: string
            The path of the ground truthing dataset.
    """

    for video_id in range(len(video_names)):

        category_name, video_name = video_names[video_id].split('/')

        # the input and the gt frames are stored at CDnet2014 dataset
        cdnet_video_path = join(cdnet_dataset_path, category_name, video_name)
        cdnet_video_input_path = join(cdnet_video_path, 'input')
        cdnet_video_gt_path = join(cdnet_video_path, 'groundtruth')

        gting_video_path = join(gting_dataset_path, category_name, video_name)
        if not exists(gting_video_path):
            makedirs(gting_video_path)

        file_name = join(gting_video_path, video_name + '.hdf5')
                         
        # create an hdf5 file
        with h5py.File(file_name, 'w') as f:

            # load the ROI of the video and save it into the hdf5 file
            roi_group = f.create_group('roi')
            roi = imread(join(cdnet_video_path, 'ROI.bmp'))
            roi = (roi == 255).astype(dtype=int)
            if len(roi.shape) == 3:
                roi = roi[:, :, 0]
            roi_group.create_dataset('roi', data=roi, compression="gzip")

            # load the entire video as the testing set
            test_group = f.create_group('test')

            print(
                'generating the hdf5 file for video ' + str(video_id) + ': '
                + category_name + '-' + video_name)

            video_input_paths = sorted(listdir(cdnet_video_input_path))
            for frame_id, frame in tqdm(enumerate(video_input_paths),
                                        total=len(video_input_paths)):

                frame_id += 1

                # create an hdf5 group for each testing frame
                test_frame_group = test_group.create_group('%06d' % frame_id)

                # read the image
                frame_input_name = 'in' + "%06d" % frame_id + '.jpg'
                frame_input_image = imread(
                    join(cdnet_video_input_path, frame_input_name))

                # read the gt
                frame_gt_name = 'gt' + "%06d" % frame_id + '.png'
                frame_gt_image = imread(
                    join(cdnet_video_gt_path, frame_gt_name))

                # save to the hdf5 training file
                test_frame_group.create_dataset('input',
                                                data=frame_input_image,
                                                compression="gzip")
                if len(frame_gt_image.shape) == 3:
                    gt_data = frame_gt_image[:, :, 0]
                else:
                    gt_data = frame_gt_image

                test_frame_group.create_dataset('gt', data=gt_data,
                                                compression="gzip")

            # save the attributes
            video_len = len(f['test'])
            f.attrs['len'] = video_len
            frame_size = list(f['test']['000001']['input'].shape)
            f.attrs['frame_size'] = frame_size


def generate_train_dataset(cdnet_dataset_path, gting_dataset_path,
                           select_method, training_frame_number):
    """
    This method add training data to the hdf5. The data are stored in the
    'train' group.

    Parameters
    ----------
        cdnet_dataset_path: string
            The path of the cdnet dataset.

        gting_dataset_path: string
            The path of the ground truthing dataset.

        select_method: string
            The strategy to select the training frames:
                1. uniform: select the training frames uniformly;
                2. random : select the training frames randomly;
                3. manual : select the training frames manually. (default)

        training_frame_number: int
            Number of frames for to train the model:
                1. 50;
                2. 100;
                3. 150;
                4. 200. (default)
    """

    for video_id in range(len(video_names)):

        category_name, video_name = video_names[video_id].split('/')

        cdnet_video_path = join(cdnet_dataset_path, category_name, video_name)
        cdnet_video_input_path = join(cdnet_video_path, 'input')
        cdnet_video_gt_path = join(cdnet_video_path, 'groundtruth')

        cnndataset_video_path = join(gting_dataset_path,
                                     category_name, video_name)

        txt_name = 'FG_list_' + str(training_frame_number) + '.txt'
        video_traininglist_file = join(cdnet_video_path, 'training_list',
                                       select_method, txt_name)

        # add the data to the hdf5 file
        file_name = join(cnndataset_video_path, video_name + '.hdf5')

        # the training hdf5 file contains a 'train' groups
        h5py_file = h5py.File(file_name, 'a')

        if "/train" not in h5py_file:
            train_group = h5py_file.create_group('train')
        else:
            train_group = h5py_file['/train']

        # a sub group for a specific training data
        if "/train/" + select_method + str(training_frame_number) not in h5py_file:
            sub_train_group = train_group.create_group(
                select_method + str(training_frame_number))
        else:
            print ('the ' + select_method + str(training_frame_number) + ' training data is already generate for video' + video_names[video_id])
            continue

        # read the training frames according to the training list and save
        # them into an hdf5 file
        with open(video_traininglist_file, 'r') as f:
            for frame_id in f:
                # create an hdf5 group for each training frame
                frame_id = int(frame_id.strip())
                train_frame_group = sub_train_group.create_group(
                    '%06d' % frame_id)

                # read the image frame
                frame_input_name = 'in' + "%06d" % frame_id + '.jpg'
                frame_input_image = imread(
                    join(cdnet_video_input_path, frame_input_name))

                # read the gt frame
                frame_gt_name = 'gt' + "%06d" % frame_id + '.png'
                frame_gt_image = imread(
                    join(cdnet_video_gt_path, frame_gt_name))

                # save to the hdf5 training file
                train_frame_group.create_dataset('input',
                                                 data=frame_input_image,
                                                 compression="gzip")

                if len(frame_gt_image.shape) == 3:
                    train_frame_group.create_dataset('gt',
                                                     data=frame_gt_image[:, :, 0],
                                                     compression="gzip")
                else:
                    train_frame_group.create_dataset('gt',
                                                     data=frame_gt_image,
                                                     compression="gzip")
        h5py_file.close()


def main():
    root_dataset_path = sys.argv[1]
    mode = sys.argv[2]
    if mode == 'train':
        try:
            select_method = sys.argv[3]
            training_frame_number = sys.argv[4]
        except IndexError:
            print('Need to give the select_method and the training_frame_number to generate the training dataset.')
            raise

    cdnet_dataset_path = join(root_dataset_path, 'CDnet2014')
    gting_dataset_path = join(root_dataset_path, 'CNN_VideoGroundTruthing')
    
    if mode == 'test':
        generate_test_dataset(cdnet_dataset_path, gting_dataset_path)
    elif mode == 'train':
        generate_train_dataset(cdnet_dataset_path, gting_dataset_path,
                               select_method, training_frame_number)

if __name__ == "__main__":
    main()
