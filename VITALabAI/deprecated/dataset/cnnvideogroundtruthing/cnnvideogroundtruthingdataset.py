"""
Authors: Yi Wang (yi.wang@usherbrooke.ca)

The 'CNN_VideoGroundTruthing' dataset contains 53 videos in 11 categories.
A readme file with more details about the dataset can be found in the dataset
folder with name 'CNN_VideoGroundTruthing'.
"""

from os.path import join, isdir
from os import listdir
from scipy.misc import imread, imresize
import numpy as np
import h5py
from tqdm import tqdm
import threading
from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract
from .utils import video_names

class CNNVideoGroundTruthingDataset(VITALabAiDatasetAbstract):
    """
    The 'CNN-CNN_VideoGroundTruthing' dataset is located in the VITAlab shared
    folder.
    """

    def __init__(self, dataset_root_path, video_id, mode, input_channel, video_ratio=1.0,
                 select_method='manual', training_frame_number=200,
                 batch_size=50, shuffle=False):
        """
        Parameters
        ----------
            dataset_root_path: string
                Path to the shared folder dataset,
                i.e. 'vitalLab/lab/vitalab/datasets'

            video_id: int
                The id of the video.

            mode: string
                1. 'train'
                2. 'test'

            input_channel: int
                The number of channels of the input.
                    1. basic and mscnn: 3
                    2. cascade: 4  (rgb+prob_map)

            select_method: string
                The strategy to select the training frames
                    1. uniform: select the training frames uniformly;
                    2. random : select the training frames randomly;
                    3. manual : select the training frames manually. (default)

            training_frame_number: int
                Number of frames for to train the model.
                    1. 50;
                    2. 100;
                    3. 150;
                    4. 200. (default)

            batch_size: int
                Number of frames in each batch.

            shuffle: bool
                Shuffle the data or not. (training: True, testing: False)
        """

        self.gting_dataset_path = join(dataset_root_path,
                                               'CNN_VideoGroundTruthing')
        self.video_id = int(video_id)
        self.mode = mode
        self.shuffle = shuffle

        self.select_method = select_method
        self.training_frame_number = training_frame_number
        self.batch_size = batch_size
        self.input_channel = input_channel

        # the list of the videos in the dataset, 'category_name/video_name'
        self.video_names = video_names
        self.number_of_videos = len(self.video_names)
        self.video = VideoIterator(self.gting_dataset_path,
                                   self.video_id,
                                   self.video_names[self.video_id],
                                   self.select_method,
                                   self.training_frame_number,
                                   self.mode,
                                   input_channel=self.input_channel,
                                   video_ratio=video_ratio,
                                   batch_size=self.batch_size,
                                   shuffle=self.shuffle)

    def __iter__(self):
        """
        This method is used to get the iterator for the dataset.
        """
        return self

    def flow(self):
        """
        Iterator
        """
        while True:
            yield next(self.video)

    def __next__(self):
        """
        This method is called by python3 loop iteration process.
        """
        return self.next()

    def next(self):
        """
        This method is called by the current interface to load a video frame by
        frame.
        """
        pass

    def preprocess_dataset(self):
        """
        This method is used when you want to preprocess all the data from the
        dataset.
        """
        pass

    def get_entire_dataset(self):
        """
        This method returns all the data from the dataset into a numpy array.
        Not needed for this dataset.
        """
        pass

    def generate_dataset(self):
        """
        This method is used to generate the dataset from the data, like taking all the images
        inside one big hdf5 file, numpy file or something else.
        """
        pass


class Iterator(object):
    """
    Abstract class for video data iterators.
    """

    def __init__(self, test_n,  train_n, mode, batch_size, shuffle, seed):
        """
        Parameters
        ----------
            test_n: int
                Total number of samples in the testing data.

            train_n: int
                Total number of samples in the training data.

            mode: string
                'train' or 'test'.

            batch_size: int
                Size of a batch.

            shuffle: bool
                Whether to shuffle the data between epochs.

            seed: int
                Random seeding for data shuffling.
        """
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.batch_index = 0
        self.total_batches_seen = 0
        self.lock = threading.Lock()
        self.index_generator = self._flow_index(train_n, test_n, mode,
                                                batch_size, shuffle, seed)

    def reset(self):
        self.batch_index = 0

    def _flow_index(self, test_n,  train_n, mode, batch_size=32, shuffle=False,
                    seed=None):
        """
        Function to generate the index of the frames for the training.
        Parameters
        ----------
            test_n: int
                Total number of samples in the testing data.

            train_n: int
                Total number of samples in the training data.

            mode: string
                'train' or 'test'.

            batch_size: int
                Size of a batch.

            shuffle: bool
                Whether to shuffle the data between epochs.

            seed: int
                Random seeding for data shuffling.
        """
        # Ensure self.batch_index is 0.
        if mode == 'train':
            n = train_n
        elif mode == 'test':
            n = test_n
        self.reset()
        while 1:
            if seed is not None:
                np.random.seed(seed + self.total_batches_seen)
            if self.batch_index == 0:
                index_array = np.arange(n)
                if shuffle:
                    index_array = np.random.permutation(n)

            current_index = (self.batch_index * batch_size) % n
            if n > current_index + batch_size:
                current_batch_size = batch_size
                self.batch_index += 1
            else:
                current_batch_size = n - current_index
                self.batch_index = 0
            self.total_batches_seen += 1
            yield (index_array[current_index: current_index + current_batch_size],
                   current_index, current_batch_size)


class VideoIterator(Iterator):
    """
    This class will get the details of a video, including the name, the number
    of frames and the frames.
    """

    def __init__(self, gting_dataset_path, video_id, video, select_method,
                 training_frame_number, mode, input_channel, video_ratio, batch_size, shuffle):
        """
        Parameters
        ----------
        gting_dataset_path: string
            The path of the video ground truthing dataset.

        video_id: int
            The video id.

        video: string
            Name of the video: category/video.

        select_method: string
            The strategy to select the training frames
                1. uniform: select the training frames uniformly;
                2. random : select the training frames randomly;
                3. manual : select the training frames manually.

        training_frame_number: int
            Number of frames for to train the model.
                1. 50;
                2. 100;
                3. 150;
                4. 200.

        mode: string
            1. 'train'
            2. 'test'

        input_channel: int
            The number of channels of the input.
                1. basic and mscnn: 3
                2. cascade: 4  (rgb+prob_map)

        video_ratio: float
            Video resize ratio for mscnn model.
                1. 1.0
                2. 0.75
                3. 0.5

        batch_size: int
            Number of frames in each batch.

        shuffle: bool
            Shuffle the data or not. (training: True, testing: False)
        """

        self.gting_dataset_path = gting_dataset_path
        self.video_id = video_id
        self.category_name = video.split('/')[0]
        self.video_name = video.split('/')[1]
        self.video_path = join(gting_dataset_path, self.category_name,
                               self.video_name)
        self.result_path = join(self.video_path, 'results')
        # the cascade model load the mscnn prob_map, 
        # if it does not exist, load the prob_amp of the basic model
        if isdir(join(self.result_path, 'mscnn', 'prob_map')):
            self.prob_map_path = join(self.result_path, 'mscnn', 'prob_map')
        else:
            self.prob_map_path = join(self.result_path, 'basic', 'prob_map')
        self.model_path = join(self.video_path, 'models')
        self.hdf5_path = join(self.video_path, self.video_name + '.hdf5')
        self.batch_size = batch_size
        self.select_method = select_method
        self.training_frame_number = training_frame_number
        self.h5file = h5py.File(self.hdf5_path, 'r')
        self.mean = [112.8629, 115.7700, 115.3712]
        self.video_len = len(self.h5file['test'])
        self.frame_size = list(self.h5file['test']['000001']['input'].shape)
        self.resize_flag = self.frame_size[0] > 400 or self.frame_size[1] > 400
        self.resize_ratio = 0.5
        self.video_ratio = video_ratio
        self.half_filter_width = 15        # filter size = 31
        self.mode = mode
        self.input_channel = input_channel
        self.shuffle = shuffle    

        # resize the frames if they are too big
        if self.resize_flag:
            self.new_frame_size = [int(self.frame_size[0] / 2),
                                   int(self.frame_size[1] / 2),
                                   self.frame_size[2]]
        else:
            self.new_frame_size = self.frame_size

        # resize frames with different ratios for the mscnn model
        self.new_frame_size = [int(self.new_frame_size[0] * video_ratio),
                               int(self.new_frame_size[1] * video_ratio),
                               self.new_frame_size[2]]

        self.roi = imresize(self.h5file['roi/roi'][:].astype(np.uint8),
                            tuple(self.new_frame_size),
                            interp='nearest')


        seed = 123
        super(VideoIterator, self).__init__(self.training_frame_number,
                                            self.video_len,
                                            self.mode,
                                            self.batch_size,
                                            self.shuffle, seed)

    def __next__(self):
        """
        This method loads and returns the data of a video.
        """
        method = "{}{}".format(self.select_method, self.training_frame_number)

        # Keeps under lock only the mechanism which advances
        # the indexing of each batch.
        with self.lock:
            index_array, current_index, current_batch_size = next(
                self.index_generator)

        # add 2 * self.half_filter_width to the width and height for the 
        # mirrow padding of the input frame.
        batch_x = np.zeros((current_batch_size,
                            self.new_frame_size[0] + 2 * self.half_filter_width,
                            self.new_frame_size[1] + 2 * self.half_filter_width,
                            self.input_channel),
                           dtype='float32')
        batch_y = np.zeros((current_batch_size,
                            self.new_frame_size[0],
                            self.new_frame_size[1],
                            3),     # two channels for the ground truth, one channel for roi
                           dtype='float32')

        if self.mode == 'train':
            keys = list(self.h5file['train'][method].keys())
            h5group = self.h5file['train'][method]
        elif self.mode == 'test':
            keys = list(self.h5file['test'].keys())
            h5group = self.h5file['test']

        index_array = index_array[index_array < len(keys)]
        for j, i in enumerate(index_array):
            # load the input frame
            x = h5group[keys[i]]['input'][:].astype('float32')
            x -= self.mean
            x = imresize(x, tuple(self.new_frame_size), interp='nearest')
            x = self.input_padding(x)

            if self.input_channel == 3:
                batch_x[j] = x / 255

            else:
                batch_x[j,:,:,0:3] = x / 255
                prob_map = imread(join(self.prob_map_path, '{}.jpg'.format(keys[i])))[:,:,0]
                prob_map = imresize(prob_map, tuple(self.new_frame_size[0:2]), interp='nearest')
                prob_map = self.input_padding(prob_map)
                batch_x[j,:,:,3] = prob_map

            # load the ground truth
            y = h5group[keys[i]]['gt'][:]
            y = imresize(y, tuple(self.new_frame_size), interp='nearest')
            # 50: shadow; 170: foreground edge; 255: foreground
            y[y == 50] = 0.25
            y[y == 170] = 0.75
            y[y == 255] = 1
            batch_y[j, :, :, 0] = (1 - y)
            batch_y[j, :, :, 1] = y
            batch_y[j, :, :, 2] = self.roi

        # (f, c, h, w)
        batch_x = np.transpose(batch_x, (0, 3, 1, 2))

        return batch_x, batch_y

    def input_padding(self, input_frame):
        """
        This method do mirror padding of the input image.
        Parameters
        ----------
            input_frame: nparray
                A frame before padding, with size [w, h, c]

        Returns
        -------
            padded_frame: nparray
                A frame after padding, with size [w + 2 * self.half_filter_width, 
                                                  h + 2 * self.half_filter_width, 
                                                  c]
        """
        if len(input_frame.shape) == 3:
            padded_frame = np.zeros((self.new_frame_size[0] + 2 * self.half_filter_width,
                                     self.new_frame_size[1] + 2 * self.half_filter_width,
                                     self.new_frame_size[2]))
            for channel_id in range(self.new_frame_size[2]):
                padded_frame[:, :, channel_id] = np.pad(
                    input_frame[:, :, channel_id],
                    self.half_filter_width,
                    'symmetric')

        elif len(input_frame.shape) == 2:
            padded_frame = np.zeros((self.new_frame_size[0] + 2 * self.half_filter_width,
                                     self.new_frame_size[1] + 2 * self.half_filter_width,
                                     ))
            padded_frame = np.pad(
                    input_frame,
                    self.half_filter_width,
                    'symmetric')

        return padded_frame

    def load_test_y(self, start_id, end_id):
        """
        This method loads and returns the ground truth of a specific range of
        frames in a video. This is used for the evaluation.

        Parameters
        ----------
            start_id: int
                The id of the start frame.

            end_id: int
                The id of the end frame.

        Returns
        -------
            the ground truth of the testing video from frame 'start_id' to frame
            'end_id'
        """
        test_y = []
        with h5py.File(self.hdf5_path, 'r') as f:

            # load the training frame and its gt
            for frame_id, frame in enumerate(f['test']):

                if frame_id < start_id:
                    continue
                if frame_id >= end_id:
                    break
                test_y_frame = f['test'][frame]['gt'][:]
                test_y_frame = imresize(test_y_frame,
                                        tuple(self.new_frame_size),
                                        interp='nearest')
                test_y.append(test_y_frame)

        test_y = np.array(test_y, dtype=int)

        return test_y
