#
# Author: Martin COUSINEAU (martin.p.cousineau@usherbrooke.ca)
#


"""
PPMI 20-points Dataset Class
"""

import numpy as np

from VITALabAI.utils.dataset import extract_balanced_sets, standardize_data, vectorize_data
from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract


class PpmiDataset(VITALabAiDatasetAbstract):
    """
    PPMI or Parkinson's Progression Markers Initiative is a dataset of hundreds
    of diffusion images of patients with Parkinson's disease or older controls.

    This dataset has tract profiles extracted for every possible combination of
    a bunch of fiber bundles and diffusion metrics with n points per profile.

    PPMI: http://www.ppmi-info.org/
    n=20 dataset: https://bitbucket.org/vitalab/ppmi_tractprofiles20
    """

    def __init__(self, data_file, labels_file, sets_proportion=(70, 30),
                 seed=1234, **kwargs):
        """
        Parameters
        ----------
        data_file: string
            Path to the data file.
            Note: For example, see meanstdperpoint_mean.npy of
                  https://bitbucket.org/vitalab/ppmi_tractprofiles20

        labels_file: string
            Path to the labels file.
            Note: For example, see groups_pd_con.npy of
                  https://bitbucket.org/vitalab/ppmi_tractprofiles20

        sets_proportion: list of int
            Percent of subjects to be contained in each set (optional).

        seed: int
            Seed for the random number generator (optional).
        """

        self.data_file = data_file
        self.labels_file = labels_file
        self.sets_proportion = sets_proportion
        self.seed = seed

        self.X_train = None
        self.Y_train = None
        self.X_test = None
        self.Y_test = None

    def generate_dataset(self):
        """
        Loads the dataset and splits it into training and testing sets.

        For info, the data (X_train, X_test) has the following shape:
            subjects x bundles x metrics x points per tract profile
        """

        data = np.load(self.data_file)
        labels = np.load(self.labels_file)

        if data.shape[0] != len(labels):
            raise ValueError("Data and labels files do not have the same " +
                             "number of subjects.")

        sets = extract_balanced_sets(labels, self.sets_proportion, self.seed)
        training_set = sets == 1
        testing_set = sets == 2

        self.X_train, self.Y_train = data[training_set], labels[training_set]
        self.X_test, self.Y_test = data[testing_set], labels[testing_set]

    def preprocess_dataset(self):
        """
        This method is used when you want to preprocess all the data from the dataset.
        """

        if not hasattr(self, 'X_train'):
            self.generate_dataset()

        # Normalize the data.
        self.X_train = standardize_data(self.X_train)
        self.X_test = standardize_data(self.X_test)

        # Vectorize the data
        self.X_train = vectorize_data(self.X_train)
        self.X_test = vectorize_data(self.X_test)

    def next(self):
        """
        This method is called by python3 loop iteration process.
        """

        # No need to implement it because the dataset fits in memory.
        pass

    def get_entire_dataset(self):
        """
        This method returns all the data from the dataset into a numpy array.

        Returns
        -------
        Tuple of ndarray with all the data.
        """

        if not hasattr(self, 'X_train'):
            self.generate_dataset()

        return (self.X_train, self.Y_train), (self.X_test, self.Y_test)
