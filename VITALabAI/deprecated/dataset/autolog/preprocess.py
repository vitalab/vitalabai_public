"""
preprocess.py
author: clement zotti (clement.zotti@usherbrooke.ca)
"""

import numpy as np


class PreProcessing(object):

    """
    This is the base class for all preprocessing classes.
    """
    name = None

    def __call__(self, data):
        """
        All PreProcessing subclasses must override this method.
        This is called by the prediction part of this pipeline.

        Parameters
        ----------
        data: ndarray
                Data to preprocess.

        Returns
        -------
        ndarray data where preprocessing is applied
        """
        raise NotImplementedError("Abstract class.")


class PreQuantilePercent(PreProcessing):

    """
    Quantile normalization
    """
    name = 'quantile'

    def __init__(self, percent=96):
        """
        Initializer.

        Parameters
        ----------
        percent: int
                Above this percent the data are considered outlier.
        """
        self.percent = percent

    def __call__(self, data):
        """
        This class provides a percentile normalization.
        Sets all outlier values higher than a given percentage to the highest
        acceptable value.

        Parameters
        ----------
        data: ndarray
                numpy array data to apply the preprocessing.

        Returns
        -------
        ndarray data without outlier
        """
        tresh = np.percentile(data, self.percent)
        idx = data > tresh
        data[idx] = data.min()
        data[idx] = data.max()
        return data


class PreProcessSTD(PreProcessing):

    """
    Standard deviation normalization
    """
    name = 'std'

    def __call__(self, data):
        """
        This class provides a standard deviation normalization.

        Parameters
        ----------
        data: ndarray
                Data to preprocess.

        Returns
        -------
        ndarray data preprocessed
        """

        # If data are all zeros
        if np.abs(data).max() == 0:
            return data

        data = data.astype(np.float32)
        data = data - data.mean()

        # Avoid division by zero
        if data.std() == 0:
            return data

        return data / data.std()


class PreProcessNonZeroSTD(PreProcessing):

    """
    Non zero standard deviation normalization
    """
    name = 'nonZeroSTD'

    def __call__(self, data):
        """
        This class provides a standard deviation normalization, ignoring the 0s.

        Parameters
        ----------
        data: ndarray
                Data to preprocess.

        Returns
        -------
        ndarray data preprocessed
        """

        non_zeros = data[...] != 0

        data[non_zeros] -= data[non_zeros].mean()

        # Avoid division by zero
        if data[non_zeros].std() == 0:
            return data

        data[non_zeros] /= data[non_zeros].std()

        return data
