"""
dataset2D.py
author: clement zotti (clement.zotti@usherbrooke.ca)
"""
import numpy as np
from VITALabAI.utils.utils import get_logger
import VITALabAI.utils.dataset as utils

from .datasetloader import GenericDataset

LOGGER = get_logger(__file__)


class Dataset2D(GenericDataset):

    """
    This class is used to iterate over a hdf5 file with a well defined structure.
    The structure is the one created by the DatasetSaver class inside
    the basedataset.py file.

    This class is an iterator.
    """
    _axis = ['b', 'c', 0, 1]

    def __init__(self, which_set, data, patch_size, strides,
                 batch_size, num_channel, axis, cache_limit=1500,
                 **kwargs):
        """
        Python initializer for the iteration process.

        Parameters
        ----------
        which_set: string
                This string defines which dataset ('train', 'valid') will be used
                during the iteration.

        data: string
                Path to the hdf5 data file.

        patch_size: tuple or list
                2 elements array with the dimension of the patch to extract
                at each iteration.

        strides: tuple or list
                2 elements array used as displacement during patch generation.

        batch_size: int
                Number of elements to return at each iteration.

        num_channel: int
                Number of channels to use for a 2D image.

        axis: tuple or list
                Dataset axes (for example ('b', 'c', 0, 1))
                    b: batch axes;
                    c: channel axes;
                    0: shape width;
                    1: shape height;

        cache_limit: int (optional)
                Number of items we can store. Used to store the preprocessed images inside a
                FIFO cache.
        """

        # Used to automatically add all the local variables to the current
        # object
        self.__dict__.update(locals())
        del self.self

        # define the cache system
        self.cache = {'names': [], 'data': {}}

        # Call the parent class to initialize the iteration system
        super(Dataset2D, self).__init__(**kwargs)

    def get_distribution_for_batchsize(self):
        """
        This method computes the number of elements for each class given a batch_size.

        Returns
        -------
        dict, where the key is one class and the value is the number of elements
        of this class to be extracted from the dataset.
        """
        disp = {k: int(self.batch_size * (v / 100.))
                for k, v in self.distribution.items()}

        missing = self.batch_size - sum(disp.values())
        # We cannot have more missing cases than the number of categories
        assert missing <= len(disp)

        # We distribute the missing element among the categories
        for i, key in zip(range(missing), disp.keys()):
            disp[key] += 1

        return disp

    def n_next(self):
        """
        This method is called during the iteration by __next__().

        Returns
        -------
        tuple of ndarray, the first element is the training examples,
        the second is the target of the training examples.
        """

        # Stop the iteration if we reach the end of an epoch
        if self.get_num_examples() == self._total_view:
            raise StopIteration()

        pos = []
        # Get all positions given the distribution
        for k, v in self.get_distribution_for_batchsize().items():
            # Format the path for the hdf5 file
            path = '{0}/{1}'.format(self.which_set, k)
     
            # Extract the number of needed position
            val = self._pdata[path][self._cur_index[k]:self._cur_index[k] + v]
            self._cur_index[k] += v

            # Duplicates data if one sample does not have enough data for a batch_size
            if len(val) < v:
                self._cur_index[k] = 0
                step = v - len(val)
                nstep = self._cur_index[k] + step
                val = np.hstack(
                    (val, self._pdata[path][self._cur_index[k]:nstep]))
                self._cur_index[k] = step

            pos += list(val)

        # Extract all positions from data
        dres, yres, nres = [], [], []
        for name, x, y, s in pos:
            nres.append(name)
            # Apply preprocessing on each channel (modalities)
            val = self._extract_data(name, s, x, y)

            # Append patch to result (b,c,0,1)
            dres.append(val)

            # Extract Y value for training
            val = self._extract_ground_truth(name, s, x, y)
            yres.append(val)

        yres = np.array(yres)

        # Batch size sent to the model
        self._total_view += self.batch_size

        # Put the training sample in the right axes
        dres = np.array(dres).transpose(
            list(map(self.__class__._axis.index, self.axis)))

        # Use only the classes needed so each column of 0 will be
        # removed from the y vector
        yres = self.trim_classes(yres)

        # Return the X_(train|valid) and Y_(train|valid)
        return (dres.astype(np.float32)[:, :self.num_channel],
                yres.astype(np.float32))

    def _extract_data(self, name, s, x, y):
        """
        This method gets the data of one image from the dataset
        and extracts the patch at a given position.

        Parameters
        ----------
        name: string
                Name of the image to extract from the dataset.

        s: int
                Slice number to extract from the image.

        x, y: int
                Position of the patch to extract from the image.

        Return
        ------
        ndarray, the extracted patch of the image.
        """

        img = self._get_data(name, 'img')
        return utils.get_patch_at_x_y(img[..., s], self.patch_size, x, y)

    def _extract_ground_truth(self, name, s, x, y):
        """
        This method extracts the ground truth from the cache.

        Parameters
        ----------
        name: string
                Name of the image to extract the ground truth

        s: int
                Slice number to extract from the image.

        x, y: int
                Position of the patch to extract from the image.

        Return
        ------
        ndarray, this is the extracted patch of the ground truth.
        """
        gt = self._get_data(name, 'gt')
        return gt.flatten()

    def _get_data(self, name, dtype):
        """
        This method extracts the data from the dataset if needed.

        Parameters
        ----------
        name: string
                Name of the image to extract from the dataset.
        dtype: string
                Which value you want to extract:
        		'img': the raw image
        		'gt': the ground truth of this image

        Returns
        -------
        ndarray of the image preprocessed.
        """

        if dtype not in ['img', 'gt']:
            raise ValueError(
                'The component to extract should either be "img" or "gt", not {}'.format(dtype))

        # If we don't have the data and our cache is full
        if name not in self.cache['names'] and \
           len(self.cache['data']) >= self.cache_limit:
            # We delete the oldest image
            to_delete = self.cache['names'].pop(0)
            del self.cache['data'][to_delete]

        # If we don't have the data in memory, we go get it
        if name not in self.cache['names']:
            # We add the image to the cache
            self.cache['names'].append(name)
            self.cache['data'][name] = {}

            # Extract the image from the dataset file
            img = self._data['{0}/img'.format(name.decode('UTF-8'))]

            # Preprocess the image by channel
            img = self._preprocess_channel(img)

            # Convert the image in float32 (GPU limitation) and store it inside the cache
            img = img.astype(np.float32)
            self.cache['data'][name]['img'] = img

            # Get the groundtruth from the dataset
            gt = self._data['{0}/gt'.format(name.decode('UTF-8'))]
            gt = np.array([gt])
            gt = gt.astype(np.float32)

            # Cache the groundtruth inside the FIFO
            self.cache['data'][name]['gt'] = gt

        return self.cache['data'][name][dtype]

    def _preprocess_channel(self, img):
        """
        This method preprocesses an image by channel

        Parameters
        ----------
        img: ndarray
		img in 3D like this ('c', 0, 1, 'b').

        Returns
        -------
        ndarray of the image with preprocesses applied on each channel.
        """
        img = np.array(img).astype(np.float32)

        # Need to transpose the value like this ('c', 0, 1, 's')
        img = img.transpose((1,2,3,0))

        # Preprocessing each channel
        for i in range(img.shape[0]):
            for pre in self.preprocessing:
                img[i] = pre(img[i])
        return img
