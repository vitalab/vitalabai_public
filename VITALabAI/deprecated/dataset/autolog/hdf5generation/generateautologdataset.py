"""
autologdataset.py
author: clement zotti (clement.zotti@usherbrooke.ca)
"""

from collections import OrderedDict
import os
import itertools as it
import array
import glob
import shutil
import argparse

import numpy as np
from scipy.misc import imsave
from skimage.transform import resize

import OpenEXR as exr
import Imath

from utils import DATA_POS_TYPE
from VITALabAI.utils.utils import get_logger, natural_order
from VITALabAI.utils.dataset import get_patch_list

from basedataset import DatasetSaver, DatasetIterator
from autologclasses import CLASSES

LOGGER = get_logger(__file__)


class IterationWrongDimension(Exception):

    """
    This exception is used when an image does not have all its
    modalities.
    """

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def generate_dataset(name, dataset_iterator, shuffle_dataset):
    """
    Function to generate the dataset if does not already exist.

    Parameters
    ----------
    name: string
        Name of the hdf5 file.

    dataset_iterator: AutologDataset
        Object used to iterate over the dataset files and save
        them into an hdf5 file.

    shuffle_dataset: boolean
        If we want to shuffle the dataset when we save it into the hdf5.
    """
    dbsaver = DatasetSaver(name)
    # Create the file with the name given
    if dbsaver.db_exists():
        print('Dataset already exists, skipping generation.')
        return None
    dataset_iterator.prepare()
    dbsaver.open_hdf5_file()
    for dbname in ['train', 'valid', 'test']:
        # Select which group to start inserting data
        dbsaver.select_hdf5_group(dbname)
        dataset_iterator.initialize(dbname)
        for iname, data, pos in dataset_iterator:
            # Save each part of the image, ground truth and
            # position inside the db.numpy
            dbsaver.create_hdf5_db(iname, data)

            dbsaver.create_hdf5_position(pos)
    dbsaver.done(shuffle_dataset)


class AutologDataset(DatasetIterator):
    """
    This class is intended to convert the autolog dataset
    into the hdf5 file format.
    """

    def __init__(self, data_path, max_width=61, train_split=70, valid_split=20):
        self.data_path = data_path
        self.max_width = max_width
        self.data = {}
        self.rng = np.random.RandomState(1)
        self.cur_data = None
        self.cur_class = None
        self.train_split = train_split
        self.valid_split = valid_split

    def prepare(self):
        """
        Method that fetches every file inside the dataset directory and generates a
        dictionary with [train, valid, test] as keys and a list of files, each file
        being represented as a tuple containing the multiple modalities.
        This dictionary is used in the next() method.
        """
        # generate tuple (path, wood_class) from the data_path directory
        lst_dir = [(os.path.join(self.data_path, d), d) for d in CLASSES]

        # generate path for each image sorted by name
        lst_dir = {d: sorted(it.imap(os.path.join, it.repeat(path), os.listdir(path)), key=natural_order)
                   for path, d in lst_dir}

        # pack image path by 4 (number modalities per image)
        lst_files = {k: [tuple(d[i:i + 4]) for i in range(0, len(d), 4)]
                     for k, d in lst_dir.items()}

        # Split in percent
        train_split = self.train_split
        test_split = self.train_split + self.valid_split
        train_dict = {k: iter(d[:len(d) * train_split / 100])
                      for k, d in lst_files.items()}
        valid_dict = {k: iter(d[len(d) * train_split / 100: len(d) * test_split / 100])
                      for k, d in lst_files.items()}
        test_dict = {k: iter(d[len(d) * test_split / 100:])
                     for k, d in lst_files.items()}

        # Setup the dictionary
        self.data['train'] = OrderedDict(
            sorted(train_dict.items(), key=lambda t: t[0]))
        self.data['valid'] = OrderedDict(
            sorted(valid_dict.items(), key=lambda t: t[0]))
        self.data['test'] = OrderedDict(
            sorted(test_dict.items(), key=lambda t: t[0]))

    def initialize(self, which_set):
        """
        See: DatasetIterator
        """
        self.cur_data = self.data[which_set]
        self.cur_class = self.cur_data.keys()[0]

    def _extract_image(self):
        """
        Extract the image for the current class.

        Return
        ------
        (string): name of the image
        (ndarray): image extracted
        (ndarray): ground truth of the image
        """
        def swap_rbg_rgb(rbgimg):
            """
            Swap 'b' and 'g' channel, specific to openEXR format.
            """
            temp = np.copy(rbgimg[0][1])
            rbgimg[0][1] = rbgimg[0][2]
            rbgimg[0][2] = temp
            return rbgimg

        try:
            # Get the next image to extract. If the current category (Sawcut, SoundWood,...)
            # is empty the iterator from train_dict, valid_dict, test_dict inside the prepare()
            # function will throw a StopIteration exception
            paths = next(self.cur_data[self.cur_class])
        except StopIteration:
            # We don't have any samples to extract from this category.

            # If we reached the last category of a set ('train', 'valid', or 'test'),
            # we have finished the iteration.
            if self.cur_class == self.cur_data.keys()[-1]:
                raise StopIteration()

            # We are not at the last category, let's get the current index and
            # go to the next category
            idx = self.cur_data.keys().index(self.cur_class)
            self.cur_class = self.cur_data.keys()[idx + 1]
            paths = next(self.cur_data[self.cur_class])

        # Get the name of the image (for example: 17561R1, 17561R2, etc.)
        name = "{}_{}".format(self.cur_class,
                              os.path.basename(paths[0]).split('-')[0])
        LOGGER.warning("Process, {}".format(name))

        # Open the file with openEXR package
        f_in = [exr.InputFile(path) for path in paths]

        # Get the image size from header and compute the real size
        img_size = f_in[0].header()["dataWindow"]
        img_size = (img_size.max.y - img_size.min.y + 1,
                    img_size.max.x - img_size.min.x + 1)
        # Set the extraction dtype
        dtype = Imath.PixelType(Imath.PixelType.FLOAT)

        # Extract the image and stack it inside a numpy array
        img = [
            np.array(
                [
                    array.array('f', f.channel(c, dtype))  # Extract the image channel
                    for c in f.header()['channels'].keys()  # Iterate over each channel
                ]).reshape((-1,) + img_size)  # Reshape the resulting image (modalities, row, col)
            for f in f_in                     # Iterate over all the modalities of the image
        ]

        img = swap_rbg_rgb(img)
        # dimension there should be (6, row, col)
        # color raw is (R, G, B) + 3 other modalities
        img = np.vstack(img)

        # remove the black border
        img = self.remove_black_border(img)
        # check if one of the dimension is not 0, resize and save to png
        if not np.all(img.shape):
            # raise exception to go to the next image without crash
            raise IterationWrongDimension(name)

        img = self.resize_to_fit(img)
        self.save_exr(img, paths)
        self.save_png(img, paths)

        # Compute the classification label
        gt = np.zeros(len(CLASSES))
        gt[CLASSES.index(self.cur_class)] = 1

        return (name, img, gt)

    def save_png(self, img, paths):
        """
        Method used to save the first 3 channels into png, used only for
        visualization purposes.

        Parameters
        ----------
        img: ndarray
                Image to save into a png.

        paths: list
                List of EXR images paths to be saved into png.
        """

        paths = [self._insert_ext(p.replace(".exr", ".png"), 'png')
                 for p in paths]
        base = os.path.dirname(paths[0])
        if not os.path.isdir(base):
            os.makedirs(base)
        imsave(
            paths[0], (img[:3].transpose((1, 2, 0)) * 255.0).astype('uint8'))
        imsave(paths[1], (img[3:4].squeeze() * 255.0).astype('uint8'))
        imsave(paths[2], (img[4:5].squeeze() * 255.0).astype('uint8'))
        imsave(paths[3], (img[5:].squeeze() * 255.0).astype('uint8'))

    def _insert_ext(self, path, ext):
        """
        Change the extension of a file.

        Parameters
        ----------
        path: string
                File path. ex: /mnt/data/martin.exr

        ext: string
                Extension to use in place of the old one.

        Returns
        -------
        string, a path with a new extension, ex: /mnt/data/martin.png
        """
        split_path = path.split(os.sep)
        split_path.insert(-2, ext)
        return os.path.join(*split_path)

    def save_exr(self, img, paths):
        """
        Method used to save (processed) images back into EXR

        Parameters
        ----------
        img: ndarray
                Image to save into EXR format.

        paths: string
                Path to save all the preprocessed modalities into EXR.
        """

        paths = [self._insert_ext(p, 'exr') for p in paths]
        base = os.path.dirname(paths[0])
        if not os.path.isdir(base):
            os.makedirs(base)

        # RGB
        r = array.array('f', img[0].flatten()).tostring()
        g = array.array('f', img[1].flatten()).tostring()
        b = array.array('f', img[2].flatten()).tostring()
        exr_file = exr.OutputFile(paths[0], exr.Header(
            img[:3].transpose((0, 2, 1)).shape[1], img[:3].transpose((0, 2, 1)).shape[2]))
        exr_file.writePixels({'R': r, 'B': b, 'G': g})
        exr_file.close()

        # Other modalities
        for i in range(3, 6):
            modal = img[i:i + 1]
            y = array.array('f', modal.flatten()).tostring()
            exr_header = exr.Header(
                modal.transpose((0, 2, 1)).shape[1], modal.transpose((0, 2, 1)).shape[2])
            exr_header['channels'] = {
                'Y': Imath.Channel(Imath.PixelType(Imath.PixelType.FLOAT), 1, 1)}
            exr_file = exr.OutputFile(paths[i - 2], exr_header)
            exr_file.writePixels({'Y': y})
            exr_file.close()

    def remove_black_border(self, img, treshold=20):
        """
        This method removes all the black borders of an image.
        This is a crop along the column axis.

        Parameters
        ----------
        img: ndarray
                Image that needs to be cropped along the columns.

        Returns
        -------
        ndarray, the cropped image.

        """
        # Select the RGB channels and sum over column
        mask = img[:3].sum(axis=(0, 1))

        # Create mask index to extract only the needed column
        mask_idx = np.arange(len(mask))

        # Filter all the zero columns
        mask_idx = mask_idx[np.where(mask > treshold)]
        LOGGER.warning("Removed {} column.".format(len(mask) - len(mask_idx)))

        # Apply the filtered mask_index to the image to get only the needed
        # column
        n_img = img[:, :, mask_idx]
        return n_img

    def resize_to_fit(self, img):
        """
        Parameters
        ----------
        img: ndarray
                Image to resize.

        Returns
        ------
        ndarray, the image cropped at a specific size defined in the initializer.
        """
        shape = img.shape[:-1]
        n_img = resize(img, shape + (self.max_width,))
        LOGGER.warning(
            "Resized image from {} to {}".format(img.shape, n_img.shape))
        return n_img

    def _select_position(self, name, img):
        """
        Extracts all the positions needed for one image
        in this case it's only a vector.

        Parameters
        ----------
        name: string
                Name of the image to extract position.

        img:ndarray
                Image where we extract the patch position.
        Returns
        -------
        dict of position to extract.
        """
        res = []
        # Tranpose image to have the channels as the last dimensions
        img_t = img.transpose((1, 2, 0))

        # the minimum width size of an image inside the dataset
        img_shape = (61, 61, 6)

        # extract all the patches from one image with stride of 1
        patches = get_patch_list(img_t, img_shape, (1, 1))[0]
        row, col, z = patches.shape[:3]

        # generate position
        res = it.product(range(row), range(col), range(z))

        # setup into a numpy array with custom datatype to save into hdf5
        res = np.array([(name, x, y, z) for x, y, z in res],
                       dtype=DATA_POS_TYPE)

        return {self.cur_class: res}

    def move_image(self, name):
        """
        Method to move the image into a 'rejected' folder and skip it.

        Parameters
        ----------
        name: string
                Name of the image to move.
        """
        name_class, name_img = name.split("_")
        rejected = os.path.join(self.data_path, "rejected", name_class)
        all_files = glob.glob(
            os.path.join(self.data_path, name_class, "{}*".format(name_img)))
        if not os.path.isdir(rejected):
            os.makedirs(rejected)
        for f in all_files:
            out_file = os.path.join(rejected, os.path.basename(f))
            shutil.move(f, out_file)

    def next(self):
        """
        Method used for iteration.

        Returns
        -------
        tuple of 3 elements:
                1. string, name of the current image
                2. list, image and ground truth concatenated together
                3. dict, dictionary of positions
        """
        try:
            name, deg, dgt = self._extract_image()
            dps = self._select_position(name, deg)
            deg = np.expand_dims(deg, axis=0)
        except IterationWrongDimension as e:
            self.move_image(e.value)
            return next(self)

        return (name, [deg, dgt], dps)


if __name__ == "__main__":
    """
    Main function used to generate the dataset
    """
    # parse the needed arguments
    parser = argparse.ArgumentParser(
        description="Dataset generator for pipeline")
    parser.add_argument(
        "-d", "--dataset", type=str, help="Path to the dataset")
    parser.add_argument("-n", "--name", type=str,
                        help="Name of the hdf5 output file")
    parser.add_argument(
        "-s", "--shuffle", action="store_true", help="Whether to shuffle the dataset")
    args = parser.parse_args()

    # genrate the dataset
    generate_dataset(args.name, AutologDataset(args.dataset), args.shuffle)
