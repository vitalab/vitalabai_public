"""
The base dataset class is used to provide a way for saving the dataset
in a format that can be read by the generic dataset loader.

The generic dataset loader is used for loading a dataset for training
"""

import os
import numpy as np
import h5py
import itertools as it

from VITALabAI.utils.utils import get_logger
from utils import DATA_POS_TYPE

LOGGER = get_logger(__file__)


class DatasetIterator(object):

    """
    This class is used to iterate over the dataset.
    You need to use it as parent class in order to save your dataset
    in a way that is readable by the training step.
    """

    def prepare(self):
        """
        This method is called to prepare the dataset for iteration before
        choosing which_set to iterate on.
        """
        raise NotImplementedError("Abstract class.")

    def initialize(self, which_set):
        """
        Method used to initialize your dataset before the iteration start.
        You should override it.

        Parameters
        ----------
        which_set: string
                which set we want to save, one of ('train','valid','test').
        """
        raise NotImplementedError("Abstract class.")

    def __iter__(self):
        return self

    def next(self):
        """
        The next should return a tuple containing the name and the data:
        ('my_img', numpy.ndarray), the ndarray must have a shape like this
        (2,t,c,0,1)

        2 for ('img','gt')

        t is the number of slices

        c is the number of channels

        0,1 is axis x,y

        pos must be a dictionary

        Returns:
                tuple: (name, data, pos)
        """
        raise NotImplementedError("Abstract class.")


class DatasetSaver(object):

    """
    This class saves a dataset into two hdf5 files, one for the data and the other
    for the positions.
    """

    def __init__(self, name):
        """
        Initializer

        Parameters
        ----------
        name: string
                name of the hdf5 containing the data, the position file
                will be inferred from it.
        """
        self.name = name

        self._cur_g = None
        self._cur_p = None
        self._fdb = None
        self._pos = None
        self.rng = np.random.RandomState(1)
        self.kdt = DATA_POS_TYPE

    @staticmethod
    def validate_set_name(set_name):
        """
        Validate the database name.

        Parameters
        ----------
        set_name: string
                Name of the set to get, one of ('train', 'valid', 'test').
        """
        allowed_names = ('train', 'valid', 'test')
        if set_name not in allowed_names:
            raise ValueError('the group name must be one of these {0}'.format(
                allowed_names))

    def db_exists(self):
        """
        Simple method to check if the database already exists
        """
        return os.path.exists(self.name)

    def open_hdf5_file(self):
        """
        Create the hdf5 file with the name given when building the object.
        """
        self._fdb = h5py.File(self.name, "w")
        self._pos = h5py.File(self.pos_name, "w")
        self._fdb.create_group('train')
        self._fdb.create_group('valid')
        self._fdb.create_group('test')
        self._pos.create_group('train')
        self._pos.create_group('valid')
        self._pos.create_group('test')

    def select_hdf5_group(self, set_name):
        """
        This method selects the set in which new data should be added.

        Parameters
        ----------
        set_name: string
                name of the set, can only be one of ('train','valid','test').

        """
        DatasetSaver.validate_set_name(set_name)
        self._cur_g = self._fdb[set_name]
        if set_name in ['train', 'valid', 'test']:
            self._cur_p = self._pos[set_name]
        else:
            self._cur_p = None

    def create_hdf5_db(self, img_name, data):
        """
        This method appends data to the currently selected set with the img_name.

        Parameters
        ----------
        img_name: string
                name of the image (ex: filename), must be unique.

        data: list
                This list contains the data and the ground truth as ndarray.
        """
        try:
            self._cur_g.create_group(img_name)
        except RuntimeError:
            LOGGER.debug(
                'Group {0} already exists.'.format(self._cur_g.name))

        LOGGER.debug('Saving {0}.'.format(self._cur_g[img_name]))

        self._cur_g[img_name].create_dataset('img', data=data[0],
                                             dtype=np.float32)

        self._cur_g[img_name].create_dataset('gt', data=data[1],
                                             dtype=np.float32)

    def create_hdf5_position(self, pos, shuffle=False):
        """
        Create position hdf5 file to have an easy way to handle
        position during training.

        Parameters
        ----------
        pos: dictionary
                dictionary containing for each "classes" a list of
                tuple looking like this ('name', x, y, z).

        shuffle: boolean
                boolean if we need to shuffle each dataset before creation.
        """
        if not isinstance(pos, (dict,)):
            raise ValueError("pos parameter must be a dictionary.")

        # Sometimes, ground truth data is not available and therefore we do not have a position file.
        if self._cur_p == None:
            return

        # Make every dictionary values iterable
        pos = {k: iter(v) for k, v in pos.iteritems()}
        for k in pos:
            if k not in self._cur_p.keys():
                self._cur_p.create_dataset(k, shape=(0,),
                                           maxshape=(None,),
                                           compression='gzip',
                                           shuffle=True,
                                           dtype=self.kdt)
            nb_max = 10000000
            turn = 0
            # Need to split the dataset in parts because sometimes the number of 
            # positions are too big to fit multiple times in memory
            while True:
                # Select the position to add
                arr = np.array([val
                                for i, val in it.izip(xrange(nb_max),
                                                      pos[k])],
                               dtype=self.kdt)

                olen = len(self._cur_p[k])
                self._cur_p[k].resize((olen + len(arr),))
                self._cur_p[k][olen:olen + len(arr)] = arr
                turn += 1
                if len(arr) != nb_max:
                    break

    def done(self, shuffle=True):
        """
        Used to close the file and after that the training step will start.

        Parameters
        ----------
        shuffle: boolean
        	Shuffling the position for each set.
        """

        self._fdb.close()
        if shuffle:
            print("Shuffling")

            name = ['train', 'valid', 'test']
            for n in name:
                for d in self._pos[n].keys():

                    category_name = '{0}/{1}'.format(n, d)
                    nb_pos = self._pos[category_name].shape[0]
                    print(
                        "Doing '{} ({} in total)'".format(category_name, nb_pos))

                    # We make sure we shuffle enough the data.
                    # If there are too many positions, the shuffle won't be perfect.
                    nb_max = 10000000
                    step = nb_max / 2

                    for i in range(nb_pos / step + 1):

                        idx = range(
                            i * step, min(nb_max + i * step, nb_pos - 1))
                        sub_data = self._pos[category_name][idx]

                        shuf_idx = range(len(sub_data))
                        self.rng.shuffle(shuf_idx)
                        self._pos[category_name][idx] = sub_data[shuf_idx]
        self._pos.close()

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value + '.hdf5'

    @property
    def pos_name(self):
        d_name = os.path.dirname(self._name)
        b_name = os.path.basename(self._name)
        return os.path.join(d_name, "Pos_{}".format(b_name))

