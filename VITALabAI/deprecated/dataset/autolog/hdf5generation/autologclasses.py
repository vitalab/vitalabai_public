"""
Classes used to generate the hdf5 dataset file and to run our models.
"GrubHoles" and "SawCut" were removed since there was not enough data.
"""
CLASSES = [
"BarkPocket",
#"GrubHoles",
"HoneyComb",
#"SawCut",
"SoundWood",
"VisualSkip",
"BlueStain",
"HearthStain",
"Knot",
"Shake",
"UnsoundWood",
"WhiteSpeck"]
