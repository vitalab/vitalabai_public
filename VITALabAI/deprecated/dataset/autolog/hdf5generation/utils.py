"""
This is the type of the position to insert into the hdf5 file
"""  # pylint: disable=W0105

import numpy as np


DATA_POS_TYPE = np.dtype([('name', np.string_, 32),
                          ('x', np.int64),
                          ('y', np.int64),
                          ('z', np.int64)])
