# The MIT License (MIT)
#
# Copyright (c) <2015> <Clement Zotti> <Francis Dutil>
#
# Permission is hereby granted, free of charge, to an_y person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import numpy as np
import h5py
import json
import os


class GenericDataset(object):

    def __init__(self, mini_batch_num, distribution):
        """
        Default class for iteration on hdf5 dataset.

        Parameters
        ----------
        mini_batch_num: int
                Number of batches inside an epoch

        distribution: dict
                dictionary containing the distribution of classes.
        """

        self.mini_batch_num = mini_batch_num
        self.distribution = distribution

        # Try to get an environment variable to find the dataset if it is not found in the current folder
        self.bpath = os.path.expanduser(os.environ.get('SAVE_DATASET', '.'))

        # Set up the rng for the iterator
        self.rng = np.random.RandomState(1)

        # Prepare hdf5 file inside the iterator.
        self._load_hdf5()
        self._total_view = 0
        self.distribution = json.loads(self.distribution)
        self._cur_index = {k: 0 for k in self.distribution.keys()}

    @property
    def num_classes(self):
        # Get the number of classes
        return self._num_classes

    @num_classes.setter
    def num_classes(self, nclasses):
        # Set the number of classes
        self._num_classes = nclasses

    @property
    def preprocessing(self):
        # Fetches the preprocessing array to apply on the images
        return self._preproc

    @preprocessing.setter
    def preprocessing(self, lst):
        if not isinstance(lst, (list, tuple)):
            raise TypeError(
                "The parameter sould be a list, it's a {}".format(type(lst)))
        self._preproc = lst

    def get_num_examples(self):
        return self.mini_batch_num * self.batch_size

    def __iter__(self):
        """
        Default python method to get iterator object.

        Return
        ------
        object, object with implementation of __next__() method.
        """
        self._total_view = 0
        return self

    def n_next(self):
        """
        Method called during iteration, should be overriden by subclass.
        """
        raise NotImplementedError("Abstract class.")

    def __next__(self):
        """
        Default iterator method called by python.
        """
        return self.n_next()

    def _load_hdf5(self):
        """
        Method to load both hdf5 files and initialize the iterator.
        """
        # Get dataset file
        self.f = h5py.File(os.path.join(self.bpath, self.data), 'r')

        # Get position file
        filename = os.path.join(self.bpath, os.path.dirname(self.data),
                                "Pos_{}".format(os.path.basename(self.data)))

        # _data contains all the data, _pdata contains all the positions to extract patches
        self._data = self.f[self.which_set]
        self._pdata = h5py.File(filename, 'r')
