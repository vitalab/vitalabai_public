"""
VITAL Lab - Universite de Sherbrooke

Pierre-Marc Jodoin
Karl-Etienne Perron
Clement Zotti
"""

from ...dataset.autolog.hdf5generation.autologclasses import CLASSES
from .dataset2D import Dataset2D
from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract

import json
import numpy as np
import ordered_set
import re


class AutologDataset(VITALabAiDatasetAbstract):
    """
    autolog project dataset class, used to access the autolog dataset's data. The autolog data
    contains several ROI of wood characteristics which help autolog classify the wood's quality.
    These ROI can be accessed as a whole or via patches.The ROI are organized by categories based
    on these characteristics. The dataset itself is organized by sets of ROI: the test set,
    the training set and the validation set. The training, validation and testing sets contain
    unique images from each plank category. Since there is more data than most computer's memory
    can store, we access it through iterators which, each time, gather just enough data for a
    forward pass.
    """

    # ----------------------------------------------------------------------------------------------
    # PUBLIC METHODS

    def __init__(self, batch_size, dataset_filepath, axes, input_shape, nb_mini_batch,
                 nb_channels, preprocessing, strides, **distribution):
        """
        Dataset constructor

        Parameters
        ----------
        batch_size : int
            Number of examples processed per forward pass;
        dataset_filepath : str
            Full path to the hdf5 dataset file;
        axes : tuple
            Dataset axes (for example ('b', 'c', 0, 1))
                b: batch axes;
                c: channel axes;
                0: shape width;
                1: shape height;
           Dataset order. The
                iterator will give you a data with the defined axis.
        input_shape : (int,int)
            Input data shape
        nb_mini_batch : int
            Number of forward passes to do;
        nb_channels : int
            Number of channels to use during training and testing.
            3 = RGB
            4 = 3 + depth
            5 = 4 + angle
            6 = 5 + eccentricity
        preprocessing : <preprocess> list
            List of preprocessing functions to apply on the data. See
            ".../dataset/autolog/preprocess.py for details;
        strides : (int,int)
            Filtering stride on each axis;
        distribution : ... (REST OF PARAMETERS)
            The rest of the parameters should be used to provide the desired distribution percentage
            for each category of planks. To do so, provide "class name=distribution percentage"
            parameters. For example, when providing parameters to main.py:
            python3 main.py ...,SoundWood=10.0,Knot=10.0,BlueStain=10.0,...

            Make sure that these percentages sum up to 100.0

            Any other parameter that is not a plank category will simply be ignored;

        Returns
        -------
        None
        """

        self.axes = axes
        self.batch_size = int(batch_size)
        self.dataset_filepath = dataset_filepath
        self.input_shape = input_shape
        self.nb_mini_batch = nb_mini_batch
        self.nb_channels = nb_channels
        self.preprocessing = preprocessing
        self.strides = strides

        # We take the rest of the parameters and drop any that does not provide a proper class
        # distribution
        args_distribution = {k: distribution[k] for k in set(CLASSES).intersection(distribution)}

        distribution_sum = np.sum(list(args_distribution.values()))
        assert distribution_sum >= 99.9999, ("Sorry, the total distribution percentage must be "
                                             "greater than or equal to 100%. You asked for a total "
                                             "distribution of: {0}%".format(distribution_sum))

        # We create the non-zero distribution string necessary to the iterator's creation
        self.distribution = ({k: v for k, v in args_distribution.items() if not np.isclose(v, 0)}
                             .__str__().replace('\'', '\"'))

        # From the distributions string, we compute the number of classes
        self.nb_classes = len(re.findall("[\"|\']*[\"|\']:", self.distribution))

        self.data_classes = None
        self.test_data = None
        self.train_data = None
        self.valid_data = None

    def generate_dataset(self):
        """
        This methods fetches the training, validation and test data from the dataset file.

        Returns
        -------
        None
        """

        self._get_data_classes()

        self.train_data = self._get_data('train')
        self.valid_data = self._get_data('valid')
        self.test_data = self._get_data('test')

    def preprocess_dataset(self):
        """
        This method applies preprocessing to the loaded data.
        (Not applicable in our case since data preprocessing is done automatically in the
        data iterator)

        Returns
        -------
        None
        """

        pass

    def next(self):
        """
        This methods gets the next batch of data from the dataset. This is useful when the dataset
        has more data than memory can store.
        (Not applicable in our case since data iteration is done automatically in the data iterator)

        Returns
        -------
        None
        """

        pass

    def get_entire_dataset(self):
        """
        This method obtains all the available training, validation and test data.
        (Not applicable in our case since the entire dataset does not fit in memory)

        Returns
        -------
        None
        """

        pass

    # ----------------------------------------------------------------------------------------------
    # PRIVATE METHODS

    def _trim_data_classes(self, gt):
        """
        This private method trims the ground truth vector in order to only keep the wanted classes.

        Parameters
        ----------
        gt : <float> arrays
            Ground truth vectors;

        Returns
        -------
        : <float> arrays
            Trimmed ground truth vectors;
        """

        return (np.asarray([[i[1][j] for j in [self.data_classes[0]]] for i in enumerate(gt)])
                .reshape(len(gt), self.nb_classes))

    def _get_data_classes(self):
        """
        This private method specifies the plank categories used for training.

        Parameters
        ----------
        gt : <float> arrays
            Ground truth vectors;

        Returns
        -------
        None
        """

        data_classes_index = ordered_set.OrderedSet()

        # Remove the superfluous characters in order to retreive data from the distribution string
        d = json.loads(self.distribution)
        data_classes_index = [CLASSES.index(class_name) for class_name in d.keys()]
        data_classes_index = sorted(data_classes_index)

        self.data_classes = [data_classes_index, [CLASSES[i] for i in data_classes_index]]

    def _get_data(self, set_name):
        """
        This private method creates a data iterator for a specific number of examples from a
        specific set.

        Parameters
        ----------
        nb_mini_batch : int
            Number of forward passes to do;
        set_name : str
            Name of the dataset set onto which we want to iterate ('test','train','valid');

        Returns
        -------
        : dataset iterator
            The configured dataset iterator;
        """
        it = Dataset2D(which_set=set_name, data=self.dataset_filepath, patch_size=self.input_shape,
                       strides=self.strides,
                       batch_size=self.batch_size,
                       num_channel=self.nb_channels,
                       axis=self.axes,
                       mini_batch_num=self.nb_mini_batch,
                       cache_limit=10000,
                       distribution=self.distribution)
        it.preprocessing = self.preprocessing
        it.num_classes = self.nb_classes

        it.trim_classes = self._trim_data_classes

        return it
