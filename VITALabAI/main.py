"""
main.py file

This file contains the main functions that will call every function of
the interface.
"""

import argparse
import re
from ast import literal_eval
import matplotlib
matplotlib.use('Agg')


def get_arguments():
    """
    This function is used to create the arguments parser.

    Returns
    -------
    An argument parser object, similar to a dictionary.
    """
    parser = argparse.ArgumentParser(
        description="Main function of the pipeline to work with everyone's model.")
    parser.add_argument("model", action="store", help="model to import")
    parser.add_argument("-o", "--option", type=str, nargs='+', default=[],
                        help="all this option will be pass to the model as **kawrgs")
    parser.add_argument("--testonly", action='store_false', dest="do_train", help="Skip training and do test phase",
                        default=True)
    args = parser.parse_args()
    args.option = ''.join(args.option)
    return args


def get_model(model_str):
    """
    Get the model given as parameters of the script.

    Parameters
    ----------
    model_str: String
        It's a string that will be imported dynamically and we create the model
        from that.

    Returns
    -------
    Class model given in parameters
    """
    # the model_str is somethink like that model.dijon.dijonmodel.DijonModel
    # So we split it to extract the DijonModel class
    model_split = model_str.split('.')

    # Redo the inverse of the split without the class name
    package = ".".join(model_split[:-1])

    # Dynamically import the package
    model = __import__(package, fromlist=[model_split[-1]])

    # Get the class from the imported package
    model = getattr(model, model_split[-1])

    return model


def get_options(arg):
    """
    Extract all the options from the parameters given in the command line
    The option line should look like:
         -o "my_int=42,my_float=3.14,my_tuple=(13,37),my_string='yolo'"

    You need to surround all options with quotes, and further surround strings with different quotes, as above.

    Parameters
    ----------
    arg : String
        A comma separated string for each argument.

    Returns
    -------
    Dictionary with key being the first part of = and the value is the
    second part.
    """

    # Protection against empty option parameters
    if arg == '':
        return {}

    """We split all the comma saparated parameters
       ([^=]+)     # key
        =          # equals is how we tokenise the original string
        ([^=]+)    # value
        (?:,|$)    # value terminator, either comma or end of string
    """
    options = re.findall(r'([^=]+)=([^=]+)(?:,|$)', arg)

    # Create dict and eval values
    return {k.strip(): literal_eval(v) for k, v in options}


def run(model, do_train=True):
    """
    This function is the default one to run the class given in parameter of
    the script.

    Parameters
    ----------
    model: Object
        Child of VITALAiAbstract class
    do_train: Boolean
        When False, training is skipped. Default: True
    """
    model.load_dataset()
    model.build_model()
    if do_train:
        model.train()
    model.test()
    if do_train:
        model.save_model()
    model.export_results()


def main():
    """
    Main function that will get the model, create it with the parameters and
    run it.
    """
    args = get_arguments()
    class_model = get_model(args.model)
    options = get_options(args.option)
    model = class_model(**options)
    run(model, do_train=args.do_train)


if __name__ == "__main__":
    main()
