from keras.models import load_model

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiAbstract import VITALabAiAbstract, Backend


class VITALabAiKerasAbstract(VITALabAiAbstract):
    """
    Estimator for a *Keras* Model.

    Notes:
        For more information on the parameters, see keras.Model.compile.
    """

    def __init__(self, dataset: VITALabAiDatasetAbstract, loss_fn=None,
                 optimizer=None, metrics=None, loss_weights=None):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            loss_weights: list of weights for the loss functions
        """
        super().__init__(dataset)
        if None not in [loss_fn, optimizer]:
            self.model.compile(optimizer=optimizer, loss=loss_fn, metrics=metrics, loss_weights=loss_weights)

    def get_backend(self):
        return Backend.KERAS

    def train(self, **kwargs):
        """Train the model.

        Args:
            **kwargs: dict with keywords to be used.
        """
        try:
            print(("Press ctrl-c to stop the training and continue "
                   "the pipeline."))
            # Fit the model
            print("Fitting model...")
            self.model.fit_generator(self.get_train_set(),
                                     validation_data=self.get_validation_set(),
                                     **kwargs)
        except KeyboardInterrupt as ki:
            print("\nTraining cancelled!\n")

    def predict(self, **kwargs):
        """Predict on the test set using the model.

        Args:
            **kwargs: dict with keywords to be used.
        """
        return self.model.predict_generator(self.dataset.get_test_set(), **kwargs)

    def predict_on_batch(self, batch):
        """Predict on a single batch"""
        return self.model.predict_on_batch(batch)

    def save(self):
        """Save the model"""
        return self.model.save('{}.h5'.format(self.__class__.__name__))

    def load_model(self, filepath, **kwargs):
        """Load the model

        Args:
            filepath: Filepath to the model
            kwargs: Additional parameters
        """
        self.model = load_model(filepath, **kwargs)

    def load_weights(self, filepath, **kwargs):
        """Load the model's weights

        Args:
            filepath: Filepath to the weights
            kwargs: Additional parameters
        """
        self.model.load_weights(filepath, **kwargs)

    def get_preprocessing(self):
        return {'scale': 1,
                'shift': (0, 0, 0),
                'use_bgr': False}

    def get_train_set(self):
        """ Overriding this method allows the model to access the sequence before it is used for training.
        """
        return self.dataset.get_train_set()

    def get_validation_set(self):
        """ Overriding this method allows the model to access the sequence before it is used for training.
        """
        return self.dataset.get_validation_set()

    def get_test_set(self):
        """ Overriding this method allows the model to access the sequence before it is used for testing.
        """
        return self.dataset.get_test_set()

    def get_model(self):
        return self.model
