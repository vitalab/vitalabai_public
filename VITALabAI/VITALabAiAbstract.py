#
# Author: Clement ZOTTI (clement.zotti@usherbrooke.ca), Frédéric B-Charron, Thierry Judge
#
from enum import Enum

from VITALabAI.VITALabAiDatasetAbstract import VITALabAiDatasetAbstract

"""
VITALabAiAbstract class
"""


class Backend(Enum):
    KERAS = 1
    PYTORCH = 2


class VITALabAiAbstract():
    """
    This class is the default interface that each model has to inherit.
    The class intends to set a 'regular' way of developing ML within the
    context of the VITAL lab.
    """

    def __init__(self, dataset: VITALabAiDatasetAbstract):
        self.dataset = dataset
        self.model = self.build_model()

    def get_backend(self):
        raise NotImplementedError

    def build_model(self):
        """Function that instantiate the Model.

        Returns:
            A Model object to be used.
        """
        raise NotImplementedError

    def train(self, **kwargs):
        """Train the model

        Args:
            **kwargs: dict with keywords to be used.
        """
        raise NotImplementedError

    def predict(self, **kwargs):
        """Predict on the test set using the model.

        Args:
            **kwargs: dict with keywords to be used.
        """
        raise NotImplementedError

    def predict_on_batch(self, batch):
        """Predict on a single batch.

        Args:
            batch: Numpy array ready to be fed to the Model.
        """
        raise NotImplementedError

    def evaluate(self, **kwargs):
        """Evaluate the model"""
        raise NotImplementedError

    def save(self):
        """Save the model"""
        raise NotImplementedError

    def load_model(self, filepath, **kwargs):
        """Load the model

        Args:
            filepath: Filepath to the model
            kwargs: Additional parameters
        """
        raise NotImplementedError

    def load_weights(self, filepath, **kwargs):
        """Load the model's weights

        Args:
            filepath: Filepath to the weights
            kwargs: Additional parameters
        """
        raise NotImplementedError
