import datetime
import os

import numpy as np
import scipy.misc
from skimage.feature import hog
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support
from tqdm import tqdm

from VITALabAI.VITALabAiAbstract import VITALabAiAbstract


def evaluate(estimator: VITALabAiAbstract, save_errors=False):
    """Evaluate the `estimator` object on the classification task.

    Args:
        estimator: VITALabAiAbstract with the weights already loaded.
        save_errors: If True, wrongly classified images will be saved in the
                    class_errors/<datetime> folder.

    Returns:
        tuple of train, validation and test set loss and accuracy values.
    """
    y_true = []
    y_pred = []
    test_seq = estimator.dataset.get_test_set()
    for i in tqdm(range(len(test_seq))):
        x, y = test_seq[i]
        y_true.extend(np.argmax(y, -1).ravel())
        y_pred.extend(estimator.predict_on_batch(x).argmax(-1).ravel())

    y_true = np.array(y_true).reshape([-1])
    y_pred = np.array(y_pred).reshape([-1])
    precision, recall, f1, _ = precision_recall_fscore_support(y_true, y_pred)
    print(confusion_matrix(y_true, y_pred,
                           labels=range(estimator.dataset.get_num_classes())))
    print("Precision: {}, Recall: {}, F1: {}".format(precision, recall, f1))
    print("Mean precision: " + str(np.mean(precision)))
    print("Mean recall: " + str(np.mean(recall)))
    print("Mean f1: " + str(np.mean(f1)))

    print("Testing on train set...")
    train_value = estimator.model.evaluate_generator(estimator.dataset.get_train_set(), verbose=1)
    print("Train : Loss: {}, Accuracy: {}".format(train_value[0], train_value[1]))

    print("Testing on validation set...")
    val_value = estimator.model.evaluate_generator(estimator.dataset.get_validation_set(), verbose=1)
    print("Val : Loss: {}, Accuracy: {}".format(val_value[0], val_value[1]))

    print("Testing on test set...")
    test_value = estimator.model.evaluate_generator(estimator.dataset.get_test_set(), verbose=1)
    print("Test : Loss: {}, Accuracy: {}".format(test_value[0], test_value[1]))

    if save_errors:
        save_test_errors(estimator)

    return train_value, val_value, test_value


def save_test_errors(estimator: VITALabAiAbstract):
    """Save wrongly classified images.

    Args:
        estimator: VITALabAiAbstract with the weights already loaded.
    """
    test_seq = estimator.dataset.get_test_set()

    now = datetime.datetime.now()
    save_dir = os.path.join('class_errors', now.strftime("%Y-%m-%d-%H:%M"))

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    print("Saving wrongly classified images in {}...".format(save_dir))

    bad_count = 0
    for i in tqdm(range(len(test_seq))):
        x, y = test_seq[i]
        y_true = np.argmax(y, -1).ravel()
        y_pred = estimator.predict_on_batch(x).argmax(-1).ravel()

        for j in range(len(y_true)):
            if y_true[j] != y_pred[j]:
                scipy.misc.imsave(os.path.join(save_dir, '{}_{}({}).jpg'.format(
                    estimator.dataset.get_classes()[y_true[j]],
                    estimator.dataset.get_classes()[y_pred[j]],
                    bad_count)), x[j].squeeze())
                bad_count += 1


def compute_features(array, feature=None):
    """This method computes the features needed to train a linear classifier.

    Args:
        array: ndarray to compute the features
        feature: string, feature to compute

    Returns:
        A ndarray, with the computed features
    """

    if feature == 'hog':
        array = np.array([hog(img, orientations=4, pixels_per_cell=(3, 3)) for img in array])

    return array
