import keras.backend as K
import numpy as np
from keras.layers import Input, Conv2D, Lambda, MaxPooling2D, Flatten, Dense, Dropout, Activation, Concatenate
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.regularizers import l2

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.model.classification.classification_utils import *


class VITALAlexnet(VITALabAiKerasAbstract):
    """Implementation of Krizhevsky et al. 2012, with a few differences:
        - Conv-ReLU have been replaced by Conv-BatchNorm-ReLU
        - Biases initialized at a non-zero value are set to 0.1 instead of 1*
        - Dense layers at the end are not split in two branches*

    Pretrained weights are available on the shared folder:
    /datasets/vitalab/datasets/mio-tcd/classification_challenge/model_weights/alexnet_imagenet.h5

    Those weights have been produced by a network which is a Theano replication of
    https://github.com/BVLC/caffe/tree/master/models/bvlc_alexnet.

    *Those modifications are also present in the implementation that produced the weights.
    """

    def __init__(self, dataset: VITALabAiDatasetAbstract, loss_fn=None, optimizer=None,
                 metrics=None, weights=None):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            loss_fn: The objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            weights: Pre-trained weights for the model
        """
        self.weights = weights
        super().__init__(dataset, loss_fn=loss_fn, optimizer=optimizer, metrics=metrics)

    def evaluate(self, save_errors=False, **kwargs):
        """Evaluate the current model as a Classification task."""
        return evaluate(self, save_errors=save_errors)

    def build_model(self):
        """Build AlexNet as a Keras model.

        Returns:
           keras.Model object *not* compiled.

        # Raises:
            ValueError: Input must be of shape (227, 227, 3)
        """

        assert K.image_data_format() == 'channels_last'

        if self.dataset.get_input_shape() != (227, 227, 3):
            raise ValueError('With the AlexNet model, the input shape must be (227,227,3).')

        num_c = self.dataset.get_num_classes()
        l2_reg = 0.0005  # From Krizhevsky et al. 2012
        w = make_random_weights(num_c)

        def bn_relu():
            def f(x):
                x = BatchNormalization(axis=-1, gamma_regularizer=l2(l2_reg))(x)
                x = Activation('relu')(x)
                return x

            return f

        input_tensor = Input(self.dataset.get_input_shape())

        x = Conv2D(96, (11, 11), strides=(4, 4), weights=get_wb(w, 'conv1'),
                   input_shape=self.dataset.get_input_shape(),
                   kernel_regularizer=l2(l2_reg))(input_tensor)  # --> 55
        x = bn_relu()(x)
        x = MaxPooling2D((3, 3), strides=(2, 2))(x)  # --> 27

        # This network was designed to run on two GPU's, hence the two 'branches' in the network.
        x1 = Lambda(lambda t: t[:, :, :, :48])(x)
        x2 = Lambda(lambda t: t[:, :, :, 48:])(x)
        x1 = Conv2D(128, (5, 5), padding='same', weights=get_wb(w, 'conv2a'),
                    kernel_regularizer=l2(l2_reg))(x1)
        x2 = Conv2D(128, (5, 5), padding='same', weights=get_wb(w, 'conv2b'),
                    kernel_regularizer=l2(l2_reg))(x2)
        x = Concatenate(axis=-1)([x1, x2])  # --> 256 maps
        x = bn_relu()(x)
        x = MaxPooling2D((3, 3), strides=(2, 2))(x)  # --> 13

        # Here there is 'communication' between the two branches
        x = Conv2D(384, (3, 3), padding='same', weights=get_wb(w, 'conv3'), kernel_regularizer=l2(l2_reg))(x)
        x = bn_relu()(x)

        x1 = Lambda(lambda t: t[:, :, :, :192])(x)
        x2 = Lambda(lambda t: t[:, :, :, 192:])(x)
        x1 = Conv2D(192, (3, 3), padding='same', weights=get_wb(w, 'conv4a'),
                    kernel_regularizer=l2(l2_reg))(x1)
        x2 = Conv2D(192, (3, 3), padding='same', weights=get_wb(w, 'conv4b'),
                    kernel_regularizer=l2(l2_reg))(x2)
        x1 = bn_relu()(x1)
        x2 = bn_relu()(x2)

        x1 = Conv2D(128, (3, 3), padding='same', weights=get_wb(w, 'conv5a'),
                    kernel_regularizer=l2(l2_reg))(x1)
        x2 = Conv2D(128, (3, 3), padding='same', weights=get_wb(w, 'conv5b'),
                    kernel_regularizer=l2(l2_reg))(x2)
        x = Concatenate(axis=-1)([x1, x2])  # --> 256 maps
        x = bn_relu()(x)
        x = MaxPooling2D((3, 3), strides=(2, 2))(x)  # --> 6

        x = Flatten()(x)
        x = Dense(4096, weights=get_wb(w, 'dense1'), kernel_regularizer=l2(l2_reg))(x)
        x = bn_relu()(x)
        x = Dropout(0.5)(x)
        codes = Dense(4096, weights=get_wb(w, 'dense2'), kernel_regularizer=l2(l2_reg))(x)
        x = bn_relu()(codes)
        x = Dropout(0.5)(x)
        preds = Dense(num_c, activation='softmax', weights=get_wb(w, 'dense3'),
                      kernel_regularizer=l2(l2_reg))(x)

        model = Model(input_tensor, preds)

        # Original optimizer from paper (except they lowered lr on plateaus)
        # sgd = SGD(lr=0.01, momentum=0.9, decay=5e-4)

        if self.weights is not None:
            model.load_weights(self.weights)

        return model

    def get_preprocessing(self):
        return {'scale': 1,
                'shift': (-122.22585154, -116.20915924, -103.56548431),
                'use_bgr': False}


def make_random_weights(num_classes):
    """Initialization scheme from paper, except non-zero biases are scaled to 0.1

    Args:
        num_classes: int, number of classes for the output layer

    Returns:
        dict containing the weights for the Alexnet model
    """
    return {
        'conv1_W': np.random.randn(11, 11, 3, 96) * 0.01,
        'conv1_b': np.zeros(96),
        'conv2a_W': np.random.randn(5, 5, 48, 128) * 0.01,
        'conv2b_W': np.random.randn(5, 5, 48, 128) * 0.01,
        'conv2a_b': np.ones(128) * 0.1,
        'conv2b_b': np.ones(128) * 0.1,
        'conv3_W': np.random.randn(3, 3, 256, 384) * 0.01,
        'conv3_b': np.zeros(384),
        'conv4a_W': np.random.randn(3, 3, 192, 192) * 0.01,
        'conv4b_W': np.random.randn(3, 3, 192, 192) * 0.01,
        'conv4a_b': np.ones(192) * 0.1,
        'conv4b_b': np.ones(192) * 0.1,
        'conv5a_W': np.random.randn(3, 3, 192, 128) * 0.01,
        'conv5b_W': np.random.randn(3, 3, 192, 128) * 0.01,
        'conv5a_b': np.ones(128) * 0.1,
        'conv5b_b': np.ones(128) * 0.1,
        'dense1_W': np.random.randn(36 * 256, 4096) * 0.01,
        'dense1_b': np.zeros(4096),
        'dense2_W': np.random.randn(4096, 4096) * 0.01,
        'dense2_b': np.zeros(4096),
        'dense3_W': np.random.randn(4096, num_classes) * 0.01,
        'dense3_b': np.zeros(num_classes)
    }


def get_wb(w, key):
    """Extract [kernel, bias] weight pair from a dictionary"""
    return [w[key + '_W'], w[key + '_b']]
