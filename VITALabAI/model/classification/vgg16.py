from keras import layers, models
from keras.applications import VGG16

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.model.classification.classification_utils import *


class VITALVGG16(VITALabAiKerasAbstract):
    """
        Estimator for a VGG16 model.

        Notes:
            For more information on the parameters, see keras.Model.compile.
        """

    def __init__(self, dataset: VITALabAiDatasetAbstract, loss_fn=None, optimizer=None,
                 metrics=None, weights='imagenet'):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            weights: Pre-trained weights for the VGG16 model

        """
        self.weights = weights
        super().__init__(dataset, loss_fn=loss_fn, optimizer=optimizer, metrics=metrics)

    def evaluate(self, save_errors=False):
        """Evaluate the current model as a Classification task."""
        return evaluate(self, save_errors=save_errors)

    def build_model(self):
        """Build VGG-16, requires an image size of 48 or more.

        Returns:
            keras.Model object *not* compiled.
        """
        inp = layers.Input(self.dataset.get_input_shape())
        vgg = VGG16(include_top=False, input_tensor=inp, weights=self.weights)
        x = vgg.output
        x = layers.Flatten(name='flatten')(x)
        x = layers.Dense(4096, activation='relu', name='fc1')(x)
        x = layers.Dropout(0.5)(x)
        x = layers.Dense(4096, activation='relu', name='fc2')(x)
        x = layers.Dropout(0.5)(x)
        x = layers.Dense(self.dataset.get_num_classes(), activation='softmax',
                         name='predictions')(x)
        model = models.Model(inp, x)
        return model

    # TODO: check why model accuracy is bad with preprocessing
    # @staticmethod
    # def get_preprocessing():
    #     return {'scale': 1,
    #             'shift': (-103.939, -116.779, -123.68),
    #             'use_bgr': True}

    def get_embedding_model(self):
        vec_layer = self.model.layers[-3]  # 4096 dense (fc2)
        model = models.Model(self.model.inputs, vec_layer.output)
        return model


if __name__ == '__main__':
    from VITALabAI.dataset.classification.mnist import VITALMnist
    from keras.callbacks import CSVLogger

    ds = VITALMnist(48)
    mnist_model = VITALVGG16(ds, optimizer='sgd', loss_fn='categorical_crossentropy',
                             metrics=['accuracy'])
    mnist_model.train(epochs=1, callbacks=[CSVLogger('vgg_model.csv')], workers=1)
    mnist_model.evaluate()
