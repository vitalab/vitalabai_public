from keras import layers, models
from keras.applications import ResNet50

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.model.classification.classification_utils import *


class VITALResnet(VITALabAiKerasAbstract):
    """
    Estimator for a Resnet model.

    Notes:
        For more information on the parameters, see keras.Model.compile.
    """

    def __init__(self, dataset: VITALabAiDatasetAbstract, loss_fn=None, optimizer=None,
                 metrics=None, weights='imagenet'):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            loss_fn: The objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            weights: Pre-trained weights for the Resnet model
        """
        self.weights = weights
        super().__init__(dataset, loss_fn=loss_fn, optimizer=optimizer, metrics=metrics)

    def evaluate(self, save_errors=False):
        """Evaluate the current model as a Classification task."""
        return evaluate(self, save_errors=save_errors)

    def build_model(self):
        """Build ResNet-50, requires an image size of 197 or more.

        Returns:
            keras.Model object *not* compiled.
        """
        inp = layers.Input(self.dataset.get_input_shape())
        resnet = ResNet50(include_top=False, weights=self.weights,
                          input_tensor=inp,
                          input_shape=self.dataset.get_input_shape(),
                          classes=self.dataset.get_num_classes(), pooling='avg')
        x = resnet.output
        x = layers.Dense(self.dataset.get_num_classes(), activation='softmax',
                         name='softmax')(x)
        model = models.Model(inp, x)
        return model

    @staticmethod
    def get_preprocessing():
        return {'scale': 1,
                'shift': (-103.939, -116.779, -123.68),
                'use_bgr': True}

    def get_embedding_model(self):
        vec_layer = self.model.layers[-2]  # Resnet output
        model = models.Model(self.model.inputs, vec_layer.output)
        model.summary()
        return model


if __name__ == '__main__':
    from VITALabAI.dataset.classification.cifar import VITALCifar10
    from keras.callbacks import CSVLogger
    import keras

    ds = VITALCifar10(input_size=197)
    optim = keras.optimizers.Adam(lr=0.0001)

    mnist_model = VITALResnet(ds, optimizer=optim, loss_fn='categorical_crossentropy',
                              metrics=['accuracy'], weights='imagenet')
    mnist_model.train(epochs=20, callbacks=[CSVLogger('resnet_model.csv')], workers=1)
    mnist_model.evaluate()
