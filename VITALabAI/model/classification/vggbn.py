from keras import Input, Model, models
from keras.layers import BatchNormalization, Activation, Conv2D, MaxPooling2D, Flatten, Dense
from keras.utils import get_file
from keras_applications.vgg19 import WEIGHTS_PATH_NO_TOP

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.model.classification.vgg19 import VITALVGG19


class VITALVGG19BN(VITALVGG19):
    """This architecure has been adapted from keras.applications' VGG19.

        Here is the change that has been made: Conv-ReLU has been replaced by Conv-BatchNorm-ReLU.
        VGGBN means VGG + BatchNorm.
    """

    def __init__(self, dataset: VITALabAiDatasetAbstract, loss_fn=None, optimizer=None,
                 metrics=None, pretrained=True):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            pretrained: bool, Use the pretrained weights from keras_application

        """
        self.pretrained = pretrained
        super().__init__(dataset, loss_fn=loss_fn, optimizer=optimizer, metrics=metrics)

    def build_model(self):
        """Build VGG-19 with additional batch-norm layers, requires an image size of 48 or more.

        Returns:
            keras.Model object *not* compiled.
        """

        def bn_relu(x):
            x = BatchNormalization()(x)
            x = Activation('relu')(x)
            return x

        def conv_bn_relu(*conv_args, **conv_kwargs):
            def f(x):
                x = Conv2D(*conv_args, **conv_kwargs)(x)
                x = bn_relu(x)
                return x

            return f

        img_input = Input(self.dataset.get_input_shape())

        # Block 1
        x = conv_bn_relu(64, (3, 3), padding='same', name='block1_conv1')(img_input)
        x = conv_bn_relu(64, (3, 3), padding='same', name='block1_conv2')(x)
        x = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x)

        # Block 2
        x = conv_bn_relu(128, (3, 3), padding='same', name='block2_conv1')(x)
        x = conv_bn_relu(128, (3, 3), padding='same', name='block2_conv2')(x)
        x = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x)

        # Block 3
        x = conv_bn_relu(256, (3, 3), padding='same', name='block3_conv1')(x)
        x = conv_bn_relu(256, (3, 3), padding='same', name='block3_conv2')(x)
        x = conv_bn_relu(256, (3, 3), padding='same', name='block3_conv3')(x)
        x = conv_bn_relu(256, (3, 3), padding='same', name='block3_conv4')(x)
        x = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x)

        # Block 4
        x = conv_bn_relu(512, (3, 3), padding='same', name='block4_conv1')(x)
        x = conv_bn_relu(512, (3, 3), padding='same', name='block4_conv2')(x)
        x = conv_bn_relu(512, (3, 3), padding='same', name='block4_conv3')(x)
        x = conv_bn_relu(512, (3, 3), padding='same', name='block4_conv4')(x)
        x = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x)

        # Block 5
        x = conv_bn_relu(512, (3, 3), padding='same', name='block5_conv1')(x)
        x = conv_bn_relu(512, (3, 3), padding='same', name='block5_conv2')(x)
        x = conv_bn_relu(512, (3, 3), padding='same', name='block5_conv3')(x)
        x = conv_bn_relu(512, (3, 3), padding='same', name='block5_conv4')(x)
        x = MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool')(x)

        # Classification block
        x = Flatten(name='flatten')(x)
        x = Dense(4096, name='fc1')(x)
        x = bn_relu(x)
        codes = Dense(4096, name='fc2')(x)
        x = bn_relu(codes)
        preds = Dense(self.dataset.get_num_classes(), activation='softmax', name='preds')(x)

        model = Model(img_input, preds, name='vggbn')

        if self.pretrained:
            weights_path = get_file('vgg19_weights_tf_dim_ordering_tf_kernels_notop.h5',
                                    WEIGHTS_PATH_NO_TOP,
                                    cache_subdir='weights_cache')
            model.load_weights(weights_path, by_name=True)

        return model

    def get_embedding_model(self):
        vec_layer = self.model.get_layer("fc2")  # 4096 dense (fc2)
        return models.Model(self.model.inputs, vec_layer.output)


if __name__ == '__main__':
    from VITALabAI.dataset.classification.cifar import VITALCifar10
    from keras.callbacks import CSVLogger

    ds = VITALCifar10(48)
    mnist_model = VITALVGG19BN(ds, optimizer='sgd', loss_fn='categorical_crossentropy',
                               metrics=['accuracy'])
    mnist_model.train(epochs=1, callbacks=[CSVLogger('vgg_model.csv')], workers=1)
    mnist_model.evaluate()
