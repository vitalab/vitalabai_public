import time

import numpy as np
from sklearn.externals import joblib
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import classification_report, recall_score, precision_score, cohen_kappa_score, \
    accuracy_score

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.model.classification.classification_utils import compute_features


class VITALSvm(VITALabAiKerasAbstract):
    """Create a standard SVM Model."""

    def __init__(self, dataset: VITALabAiDatasetAbstract, features=None):
        super().__init__(dataset)
        self.features = features

    def build_model(self):
        return SGDClassifier(n_jobs=6)

    def train(self, **kwargs):
        """Train the model

        Args:
            **kwargs: additional parameters
        """

        print('Training...')
        x, y = self.dataset.X_train, self.dataset.y_train
        x = x.squeeze()
        x = compute_features(x, self.features)
        x = x.reshape((x.shape[0], -1))
        y = np.argmax(y, axis=1)
        self.model.fit(x, y)

    def evaluate(self, **kwargs):
        """Predict on the test set, then print results

        Args:
            **kwargs: additional parameters
        """

        print('Predicting on test set...')
        x, y = self.dataset.X_test, self.dataset.y_test
        x = x.squeeze()
        x = compute_features(x, self.features)
        x = x.reshape((x.shape[0], -1))
        y = np.argmax(y, -1).ravel()

        y_pred = self.model.predict(x)
        y_true = y

        # Compute metrics
        self.metrics = {
            'accuracy': accuracy_score(y_true, y_pred),
            'mean_precision': precision_score(y_true, y_pred, average='macro'),
            'mean_recall': recall_score(y_true, y_pred, average='macro'),
            'cohen_kappa': cohen_kappa_score(y_true, y_pred)
        }
        self.text_report = classification_report(y_true, y_pred)

        # Save predictions to disk
        csvdata = np.stack([y_true, y_pred], axis=1)
        np.savetxt('miosvm_predict_test.csv', csvdata, fmt='%i', delimiter=',')

        # Write results in console
        print('\n====== RESULTS ======\n')
        self.write_results()

    def write_results(self, write_func=None):
        """This method writes metrics and text report, either in the console or in a file.

        Args:
            write_func: function to write the data. To write the results in a file, open it and give
                        <file>.write as the write_func argument.
        """

        if write_func is None:
            def write_func(s):
                print(s, end='')

        write_func('Metrics:\n')
        for name, value in self.metrics.items():
            write_func('{:<20} {:.3f}\n'.format(name, value))
        write_func('\nClassification report:\n')
        write_func(self.text_report)

    def load_model(self, filepath, **kwargs):
        """Load the model.

        Args:
           filepath: Filepath to the model
           kwargs: Additional parameters
        """
        print('Loading model at ' + filepath)
        self.classifier = joblib.load(filepath)

    def save_model(self):
        """Save the LinearSVC classifier to disk.
        """
        print('Saving model...')
        filename = 'miosvm_{}.pkl'.format(time.strftime("%Y-%m-%d_%H-%M-%S"))
        joblib.dump(self.classifier, filename)  # Equivalent to pickle.dump
        print('Model saved as: ' + filename)


if __name__ == '__main__':
    from VITALabAI.dataset.classification.mnist import VITALMnist

    ds = VITALMnist(28)
    mnist_model = VITALSvm(ds, features='hog')
    mnist_model.train()
    mnist_model.evaluate()
