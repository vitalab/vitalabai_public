import keras.applications
from keras import models
from keras.layers import Input, Dense
from keras.models import Model

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.model.classification.classification_utils import *


class VITALDenseNet(VITALabAiKerasAbstract):
    """Create a standard DenseNet model."""

    def __init__(self, dataset: VITALabAiDatasetAbstract, loss_fn=None, optimizer=None,
                 metrics=None, weights=None):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            loss_fn: The objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            weights: Pre-trained weights for the model
        """
        self.weights = weights
        super().__init__(dataset, loss_fn=loss_fn, optimizer=optimizer, metrics=metrics)

    def evaluate(self, save_errors=False):
        """Evaluate the current model as a Classification task."""
        return evaluate(self, save_errors=save_errors)

    def build_model(self):
        """Build the DenseNet model

        Returns:
           keras.Model object *not* compiled.
        """

        x = Input(self.dataset.get_input_shape())
        model = keras.applications.DenseNet121(include_top=False, weights='imagenet', input_tensor=x,
                                               input_shape=self.dataset.get_input_shape(), pooling='avg')
        preds = Dense(units=self.dataset.get_num_classes(), activation='softmax')(model.output)

        model = Model(model.input, preds)

        if self.weights is not None:
            model.load_weights(self.weights)

        return model

    def get_embedding_model(self):
        vec_layer = self.model.layers[-2]  # avg pooling layer
        model = models.Model(self.model.inputs, vec_layer.output)
        return model


if __name__ == '__main__':
    from VITALabAI.dataset.classification.cifar import VITALCifar10
    from keras.callbacks import CSVLogger

    ds = VITALCifar10(224)
    model = VITALDenseNet(ds, optimizer='sgd', loss_fn='categorical_crossentropy',
                          metrics=['accuracy'])

    model.train(epochs=1, callbacks=[CSVLogger('vgg_model.csv')], workers=1)
    model.evaluate()
