"""
Defines ResNet18 working with the VITALabAiPytorchAbstract interface.

Running this script trains a Resnet18 on CIFAR10.
"""
import torch.optim
import torch.utils.model_zoo as model_zoo
from ignite.metrics import CategoricalAccuracy, Loss
from torchvision.models.resnet import resnet18, model_urls

from VITALabAI.VITALabAiPytorchAbstract import VITALabAiPytorchAbstract, optimizer_setup
from VITALabAI.dataset.classification import cifar10_pytorch


class ResNet18(VITALabAiPytorchAbstract):
    """ Estimator for a Pytorch Resnet-18 model. """

    def build_model(self, pretrained=False):
        """Build ResNet-18 model.

        Args:
            pretrained: bool, if True, pretrained weights are used

        Returns:
            Pytorch resnet-18 model
        """
        num_classes = self.dataset.get_num_classes()
        model = resnet18(num_classes=num_classes, pretrained=False)

        if pretrained:
            weights = model_zoo.load_url(model_urls['resnet18'])
            if model.fc.weight.shape != weights['fc.weight'].shape:
                # If we dont want to train with 1000 classes, use initial weights for FC layer
                weights['fc.weight'] = model.fc.weight.data
                weights['fc.bias'] = model.fc.bias.data
            model.load_state_dict(weights)

        return model


if __name__ == '__main__':
    # In the following, we train a simple dataset (cifar10) with a pretrained ResNet model.
    # The available model is made for ImageNet; due to its depth, it cannot take as input 32x32 images.
    # Hence we upsample cifar10 images to 224x224.
    dataset = cifar10_pytorch.get_cifar10_dataset(input_size=224)
    loss_fn = torch.nn.CrossEntropyLoss()

    model_trainer = ResNet18(dataset=dataset,
                             loss_fn=loss_fn,
                             optimizer_factory=optimizer_setup(torch.optim.Adam, lr=1e-3),
                             metrics={'loss': Loss(loss_fn), 'accuracy': CategoricalAccuracy()}
                             )
    model_trainer.train(max_epochs=10)
    model_trainer.evaluate()
