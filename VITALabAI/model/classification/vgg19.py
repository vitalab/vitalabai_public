from VITALabAI.model.classification.vgg16 import VITALVGG16
from keras import layers, models
from keras.applications import VGG19


class VITALVGG19(VITALVGG16):
    """ Estimator for a VGG19 model.
        """

    def build_model(self):
        """Build VGG-19, requires an image size of 48 or more.

        Returns:
            keras.Model object *not* compiled.
        """
        inp = layers.Input(self.dataset.get_input_shape())
        vgg = VGG19(include_top=False, input_tensor=inp, weights=self.weights)
        x = vgg.output
        x = layers.Flatten(name='flatten')(x)
        x = layers.Dense(4096, activation='relu', name='fc1')(x)
        x = layers.Dropout(0.5)(x)
        x = layers.Dense(4096, activation='relu', name='fc2')(x)
        x = layers.Dropout(0.5)(x)
        x = layers.Dense(self.dataset.get_num_classes(), activation='softmax',
                         name='predictions')(x)
        model = models.Model(inp, x)
        return model


if __name__ == '__main__':
    from VITALabAI.dataset.classification.mnist import VITALMnist
    from keras.callbacks import CSVLogger

    ds = VITALMnist(48)
    mnist_model = VITALVGG19(ds, optimizer='sgd', loss_fn='categorical_crossentropy',
                             metrics=['accuracy'])
    mnist_model.train(epochs=1, callbacks=[CSVLogger('vgg_model.csv')], workers=1)
    mnist_model.evaluate()
