from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.model.classification.classification_utils import evaluate
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.models import Sequential


class VITALCnn(VITALabAiKerasAbstract):
    """Create a standard CNN Model."""

    def evaluate(self, save_errors=False):
        """Evaluate the current model as a Classification task."""
        return evaluate(self, save_errors=save_errors)

    def build_model(self):
        """Build the standard CNN model.

        Returns:
           keras.Model object *not* compiled.
        """

        model = Sequential()

        model.add(Conv2D(32, (3, 3), padding='same',
                         input_shape=(self.dataset.get_input_shape())))
        model.add(Activation('relu'))
        model.add(Conv2D(32, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Conv2D(64, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(Conv2D(64, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Flatten())
        model.add(Dense(512))
        model.add(Activation('relu'))
        model.add(Dropout(0.5))
        model.add(Dense(self.dataset.get_num_classes()))
        model.add(Activation('softmax'))

        return model


if __name__ == '__main__':
    from VITALabAI.dataset.classification.mnist import VITALMnist
    from keras.callbacks import CSVLogger

    ds = VITALMnist(28)
    mnist_model = VITALCnn(ds, optimizer='sgd', loss_fn='categorical_crossentropy',
                           metrics=['accuracy'])
    mnist_model.train(epochs=1, callbacks=[CSVLogger('cnn_model.csv')])
    mnist_model.evaluate()
