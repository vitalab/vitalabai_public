# ACDC project

## Contributors

* Clément Zotti (clement.zotti@usherbrooke.ca)

## Description

This is a project for the cardiac segmentation of MRI images using various Convolutionnal Neural Networks. 
Each CNN is encapsulated inside a class and each class is derived from a parent class called ACDCBase. 
Each model has been developed, trained and tested in conjunction with the [ACDC](www.creatis.insa-lyon.fr/Challenge/acdc/) 2017 dataset (Automatic Cardiac Delineation Challenge).

## Requirements

** Tensorflow not suported **

* keras (with --no-deps)
* scikit-image
* pandas
* nibabel
* medpy (python3 version should be install with pip github link)
* tqdm
* natsort
* theano (0.9)
* [pygpu](https://github.com/Theano/libgpuarray) (0.6.2)
* pygame (optional)


## Generate dataset 


The file `VITALabAI/dataset/acdc/acdcdatasetgenerator.py` can be used to
generate the dataset into the hdf5 format.

### How to use `acdcdatasetgenerator.py`

```bash
python3 acdcdatasetgenerator.py path/of/the/raw_MRI_nifti_data output.hdf5 -d
```
#### Parameters explanation

* `path/of/the/data `, the parent directory of all the pathologies.
* `output.hdf5`, save the dataset into this hdf5 file.
* `-d`, add data augmentation to the dataset (rotation -60 to 60).


## How to train and test on the ACDC hdf5 dataset.

```bash
python main.py model.acdc.acdcgridnet_multires_dice.ACDCGridNet -o \
"path='path/of/output.hdf5', \
nb_epochs=100, \
contour_weights=0.01"
```

### Parameters explanation

* `path`, path of the h5 file that contains the ACDC dataset.
* `batch_size`, is optional (default `10`), choose the size of the batch feeded to the network.
* `nb_epochs`, is optional (default `100`), number of epoch to train on the dataset.
* `nb_feature_maps`, is optional (default `32`), number of features maps used on the first layer (automaticly scaled for the following layers).
* `contour_weights`, is optional (default `0.01`), choose the weight to apply on the contour loss.
* `pretrained_weights`, is optional (default None), use to load weights to initialize the model.
* `train`, is optional (default `True`), used to skip the training, default it is equal to `True`, set it to `False` to skip the training step.
* `display`, is optional (defautl `False`), display the output segmentation map.
* `screen`, is optional (default `False`), this cannot be used without display=True, save screenshot of the display into the `SCREEN` folder.
-------------------------------------------------------------------------------------

# acdcunet.py / conv_deconv.py / dens_unet.py / denslynet.py

## Contributors

* Faezeh Amjadi

## Description

This is a project for the cardiac segmentation of MRI images using:

 *U_net
 *conve_deconve Neural Network
 *densely Neural Networks
 *densly block with connections in U_net


### How to use `U_net.py`

```bash
ppython3 main.py model.acdc.acdcunet.ACDC_UNet -o "path='path/of/output.hdf5'"
```

### How to use `conv_deconv.py`

```bash
python3 main.py model.acdc.conv_deconv.ACDC_Convdec -o "path='/path/of/output.hdf5'"
```

### How to use `denslynet.py`

```bash
python3 main.py model.acdc.denselynet.Denselynet_ACDC -o "path='/path/of/output.hdf5'"
```
### How to use `dens_unet.py`

```bash
python3 main.py model.acdc.dens_unet.Dens_Unet_ACDC -o "path='/path/of/output.hdf5'"
```
## How to train and test on the ACDC hdf5 dataset, for example on dens_unet.py  .

```bash
python main.py model.acdc.dens_unet.Dens_Unet_ACDC -o \
"path='path/of/output.hdf5', \
nb_epochs=100"
```

### Parameters explanation

* `path`, path of the h5 file that contains the ACDC dataset.
* `batch_size`, is optional (default `10`), choose the size of the batch feeded to the network.
* `nb_epochs`, is optional (default `40`), number of epoch to train on the dataset.
* `nb_feature_maps`, is optional (default `32`).
* `pretrained_weights`, is optional (default None), use to load weights to initialize the model.
* `train`, is optional (default `True`), used to skip the training, default it is equal to `True`, set it to `False` to skip the training step.
* 'learning rate in U_net and conv_deconve is ': 3e-4
* 'learning rate in Densely_net and Dens_Unet is ': 5e-4
