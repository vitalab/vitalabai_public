# -*- coding: utf-8 -*-
"""
uspreprocess.py
author: Sarah LECLERC from Clement Zotti's code
Implementations of ultrasound images pre-processing

"""

import numpy as np


class PreProcessing(object):
    """
    This is the basic class for all preprocessing classes.
    """

    name = None

    def __call__(self, data):
        """
        All PreProcessing subclasses must override this method.
        This is called by the prediction part of this pipeline.
        :param data: ndarray: data to process
        :return ndarray : processed data
        :return tuple : dice and loss of the training for one epochs
        """

        raise NotImplementedError("Abstract class.")


class PreProcessSTD(PreProcessing):
    """
    Standard deviation normalization class
    """

    name = 'std'

    def __call__(self, data):

        # If data are all zeros
        if np.all(data == 0):
            return data

        data = data.astype(np.float32)
        data = data - data.mean()

        std = data.std()

        # Avoid division by zero
        if std == 0:
            return data

        return data / std