"""
ae_utils.py
author: Sarah LECLERC
This file implements functions useful for AE
"""

from os.path import join

import keras.backend as K
import numpy as np
import tensorflow as tf
from PIL import Image
from VITALabAI.model.camus.autoencoder import AE
from keras.models import Model
from keras.models import load_model
from keras.objectives import mean_squared_error, mean_absolute_error, categorical_crossentropy

def load_and_run_encoder(tensor, path, image_size, dataset, subfold, kwargs):
    """
     This function is used for computing the ACNN loss.
     :param tensor: tensor: input tensor fed to the encoder
     :param path: string: path to the data for model initialization
     :param image_size: tuple: size of the input images
     :param dataset: USDataset: data for building the model
     :param subfold: int: subfold number
     :param kwargs: list: parameters used in AE
     :return codes : ? : codes obtained from the AE
     """

    # load AE
    mode = kwargs["mode"]
    ae = AE(path=path, **kwargs)
    ae.image_size = image_size
    ae.dataset = dataset
    ae.build_model()
    ae.model.load_weights(join(ae.name + str(subfold), "valid_model_best_loss.hdf5"), by_name=False)

    # load encoder, run on tensor and freeze
    kwargs["mode"] = "encoder_only"
    kwargs["input_tensor"] = tensor
    enc = AE(path=path, **kwargs)
    enc.image_size = image_size
    enc.dataset = dataset
    enc.build_model()
    aux_model = enc.model
    aux_model.set_weights(ae.model.get_weights())

    # Freeze the weights
    aux_model.trainable = False
    for layer in aux_model.layers:
        layer.trainable = False

    codes = aux_model.outputs[0]
    kwargs["mode"] = mode
    return codes

def run_decoder(model, code):
    """
     This function predicts reconstructions from codes as arrays
     :param model: keras model: decoder
     :param code: numpy array: code to interpret
     :return segmentation : numpy array : reconstructed mask
     """

    predicted_output = model.predict(code)
    segmentation = np.argmax(predicted_output, axis=3).astype('uint8')

    return segmentation

def compute_shapes(decoder, Z, nbv):
    """
     This function predicts reconstructions from codes as arrays
     :param decoder: keras model: decoder
     :param Z: list: codes obtained from the data
     :param nbv: int: number of components used for shape analysis
     :return shapes : list : mean shape and standard deformations
     """

    Z = np.asarray(Z)
    z_mean = np.mean(Z, axis=0)
    z_mean_shape = run_decoder(decoder, z_mean[np.newaxis, :])
    shapes = [z_mean_shape[0,: :]]

    z_sigma = np.cov(Z, rowvar=False)
    eigv, vv = np.linalg.eig(z_sigma)
    peigv = np.argsort(eigv)[-nbv:][::-1]  # first principal eigen values

    for comp in peigv:
        zl = z_mean - eigv[comp] * vv[:, comp]
        zl_shape = run_decoder(decoder, zl[np.newaxis, :])
        shapes.append(zl_shape[0,:,:])
        zr= z_mean + eigv[comp] * vv[:, comp]

        zr_shape = run_decoder(decoder, zr[np.newaxis, :])
        shapes.append(zr_shape[0,:,:])

    return shapes

def save_shape_space(path, defs, nbv):
    """
     This function saves a visualisation of the shape space computed from the data
     :param path: string: where to save the visuals
     :param defs: list: mean shape and standard deformations
     :param nbv: int: number of components used for shape analysis
     """

    defs = np.asarray(defs)
    mean_shape = defs[0, :, :]/3*255
    mean_image = Image.fromarray(mean_shape.astype('uint8'))
    mean_image.save(join(path,'mean.png'))
    defs = defs[1:, :, :]/3*255

    for it in range(nbv):
        resarray = np.concatenate((defs[it*2, :, :], mean_shape, defs[it*2 + 1, :, :]), axis = 1)
        imres =  Image.fromarray(resarray.astype('uint8'))
        imres.save(join(path,'defs' + str(it) + '.png'))