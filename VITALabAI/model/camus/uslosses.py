"""
uslosses.py
author: Sarah LECLERC from Clement ZOTTI's code
This file contains segmentation objective functions
"""

import numpy as np
import tensorflow as tf
from keras import backend as k
from keras import losses as kl

class USLosses:
    """
    Class that implement the segmentation losses for the U-Net.
    """

    def __init__(self, nb_classes):

        self.nb_classes = nb_classes

    def MSE(self, y_true, y_pred):

        return k.mean(k.square(y_true - y_pred), axis=-1)

    def _dice(self, y_true, y_pred):

        flat_y_true = k.flatten(y_true)
        flat_y_pred = k.flatten(y_pred)

        # flat_y_true is a binary vector
        intersect = k.sum(flat_y_true * flat_y_pred)
        s_true = k.sum(flat_y_true)
        s_pred = k.sum(flat_y_pred)
        return (2. * intersect + 1.) / (s_true + s_pred + 1.)

    def classes_dice(self, y_true, y_pred):
        """
        This function computes dice on each classes
        function on training or validation data.
        :param y_true: keras_var: true value of the prediction
        :param y_pred: keras_var: model prediction
        :return float : the sum of "dice" scores over all the classes
        """

        res = k.variable(0., name='dice_classes')
        nb_channels = y_pred.shape[3]

        for i in range(nb_channels):
            res += self._dice(y_true[:, :, :, i], y_pred[:, :, :, i])

        nb_channels = k.cast(nb_channels, 'float32')

        return res / nb_channels

    def classes_dice_loss(self, y_true, y_pred):
        """
        This function computes the Dice loss
        :param y_true: keras_var: true value of the prediction
        :param y_pred: keras_var: model prediction
        :return float : 1 - dice(y_true, y_pred)
        """

        return 1 - self.classes_dice(y_true, y_pred)

    def classes_dice_and_hd_loss(self, y_true, y_pred):

        dice_loss = self.classes_dice_loss(y_true, y_pred)
        return dice_loss + self.classes_hausdorff_loss(y_true, y_pred)

    def classes_crossentropy(self, y_true, y_pred):
        """
        This function computes cross_entropy with given class weights
        :param y_true: keras_var: true value of the prediction
        :param y_pred: keras_var: model prediction
        :return float : crossentropy over all the classes
        """

        return kl.categorical_crossentropy(y_true, y_pred)

    def EFS_loss(self, y_true, y_pred):
        """
        This function computes a loss based on the surfacic approximation of the LV ejection fraction (EFS)
        :param y_true: keras_var: true value of the prediction
        :param y_pred: keras_var: model prediction
        :return float : regression loss on the EFS
        """

        # extract channels of LV segmentations
        y_true_ED = y_true[:, :, :, 1]
        y_true_ES = y_true[:, :, :, 5]
        y_pred_ED = y_pred[:, :, :, 1]
        y_pred_ES = y_pred[:, :, :, 5]

        # compute EFS
        SED_true = k.sum(k.sum(y_true_ED, axis=1), axis=1)
        EFS_true = (k.sum(k.sum(y_true_ES, axis=1), axis=1) - SED_true) / SED_true

        SED_pred = k.sum(k.sum(y_pred_ED, axis=1), axis=1)
        EFS_pred = (k.sum(k.sum(y_pred_ES, axis=1), axis=1) - SED_pred) / SED_pred

        # optimization of EFS and the relative sizes of the LV (unique solution)
        loss = 2 * self.MSE(EFS_pred, EFS_true) + \
               2 - self._dice(y_pred_ED , y_true_ED) - self._dice(y_pred_ES , y_true_ES)

        return loss


    def classes_hausdorff_loss(self, y_true, y_pred):
        """
        This function computes a loss based on an approximation of the Hausdorff distance
        :param y_true: keras_var: true value of the prediction
        :param y_pred: keras_var: model prediction
        :return float : result
        """

        res = k.variable(0., name='hausdorff_classes')
        nb_channels = y_pred.shape.as_list()[3]

        for i in range(nb_channels):
            res += self.hd_loss(y_true[:, :, :, i], y_pred[:, :, :, i])

        return res / nb_channels

    def body_dt(self, count, nbel, it, dt, filter):
        """
        This function is the body of the while loop. All variables have to be in and outs.
        It performs one iteration of the while loop.
        """

        eps = k.variable(0.1)

        dt = tf.nn.conv2d(dt, filter, [1, 1, 1, 1], "SAME")  # apply filter once
        dt = dt / (tf.reduce_max(dt) + eps)  # renormalize dt
        count = tf.to_float(tf.count_nonzero(dt))  # update count
        it = it + 1  # keep track of the number of applications
        return count, nbel, it, dt, filter

    def cond_dt(self, count, nbel, it, dt, filter):
        """
        This function is the condition of the while loop, all variables have to be inputs, the output is a boolean.
        The distance map is ready when the diffusion reaches the edges and dt does not contain any zeros.
        For safety -in case dt contains only zeros- there is a stopping criteria on it.
        """
        return tf.logical_and(count < nbel, it < tf.sqrt(nbel))

    def tensor_distance_map(self, image):
        """
        This function builds a differentiable distance map
        :param image: keras_var: image to process
        :return dtlog: keras_var: smooth and normalized distance map
        """
        eps = k.variable(0.1)

        # Applied dilating filter
        f = k.constant([[1, 1, 1], [1, 1, 1], [1, 1, 1]], dtype=tf.float32)
        filter = k.expand_dims(tf.expand_dims(f, 2), 3)

        # initialize the distance map with the image
        dt = k.identity(image)

        # number of elements in an image (nbel)
        nbel = tf.to_float(tf.size(image))  # size is not differentiable

        # other loop variables : counter of non zero elements (count), and number of iterations (it)
        count = k.constant(0, dtype=tf.float32)
        it = k.constant(0, dtype=tf.float32)


        # get a normalized energy diffusion map (max = 1)
        # Necessary to use a tf.while_loop because it is conditioned by a tensor
        (count, nbel, it, dt, filter) = tf.while_loop(self.cond_dt, self.body_dt, (count, nbel, it, dt, filter),
                                                      back_prop=True, swap_memory=True)  # switch swap_memory to True for memory issues

        # get a distance map with smooth evolution (min = 0)
        dtlog = k.log(dt)
        dtlog = k.abs(dtlog)

        # get a normalized distance map (max = 1)
        dtlog = dtlog / (tf.reduce_max(dtlog) + eps)

        return dtlog

    def __surface_distances(self, y_true, y_pred):
        """
        This function computes the distances between the surface of binary objects in result and their
        nearest partner surface of a binary object in reference. #inspired from medpy.hd
        """

        eps = k.variable(0.1)

        # normalized distance map
        dt = self.tensor_distance_map(y_true)

        # extract only 1-pixel border line of objects
        f = k.constant([[1, 1, 1], [1, 1, 1], [1, 1, 1]], dtype=tf.float32)
        filter = k.expand_dims(tf.expand_dims(f, 2), 3)
        result_dilate = tf.nn.conv2d(y_pred, filter, [1, 1, 1, 1], "SAME")
        result_dilate = result_dilate / (tf.reduce_max(result_dilate)+ eps)

        result_border = tf.abs(tf.subtract(result_dilate, y_pred))
        result_border = result_border / (tf.reduce_max(result_border)+ eps)

        # get distance map values for result borders indices
        # we extract the values of the distance map weighted by the blurry border
        sds = tf.multiply(dt, result_border)

        return tf.reduce_max(tf.reduce_max(sds, axis=1), axis=1)

    def hd_loss(self, y_true, y_pred):
        """
        Approximation of the real Hausdorff Distance, where the distance map is built by series of convolutions
        from the segmentation in y_true.
        """
        ### after further examinations, it seems that though gradients flow perfectly, the optimal solution tends to be a null prediction
        ### because of the *, approximation of sampling values from dt
        ### need to find a way to suppress this local optimum

        # reshape to 4D tensors
        y_true = k.expand_dims(y_true, 3)
        y_pred = k.expand_dims(y_pred, 3)

        hd2 = tf.reduce_mean(self.__surface_distances(y_true, y_pred))

        return hd2

    def ACNN_loss(self, path, image_size, dataset, subfold, kwargs):

        def dice_shape_model_loss(y_true, y_pred):
            return self.classes_dice_loss(y_true, y_pred) + self.shape_model_loss(path, image_size, dataset, subfold, kwargs)(y_true, y_pred)

        return dice_shape_model_loss

    def shape_model_loss(self, path, image_size, dataset, subfold, kwargs):
        """
        Reconstruction loss approximated by mean difference in segmentation codes
        """
        ### after further examinations, it is likely that it acts as a metric (broken chain of gradients)
        ### Imports are impossible without putting it here. Necessary to rearrange functions for better clarity.
        from VITALabAI.model.camus.ae_utils import load_and_run_encoder

        alpha = kwargs["alpha"]

        def code_loss(y_true, y_pred):
            return alpha * k.mean(k.square(load_and_run_encoder(y_true, path, image_size, dataset, subfold, kwargs) -
                                           load_and_run_encoder(y_pred, path, image_size, dataset, subfold, kwargs)),
                                  axis=-1)

        return code_loss


    # inspired from https://gist.github.com/irhumshafkat
    def dice_vae(self, args):
        """
        Loss to train VAE by adding a KL divergence term
        """

        mean = args[0]
        log_stddev = args[1]

        # compute the KL loss
        KL_loss = - 0.5 * k.sum(1 + log_stddev - k.square(mean) - k.exp(log_stddev), axis=-1)

        # compute the average MSE error, then scale it up, ie. simply sum on all axes
        def vae_loss(y_true, y_pred):

            #reconstruction_loss = K.sum(K.square(y_pred - y_true))
            # return the average loss over all images in batch
            return self.classes_dice_loss(y_true, y_pred) + k.mean(KL_loss)

        return vae_loss

