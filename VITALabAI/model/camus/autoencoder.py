"""
autoencoder.py
author: Sarah LECLERC
This file implements an autoencoder architecture
"""

from VITALabAI.model.camus.usbase import USbase
from VITALabAI.model.camus.uslosses import USLosses
from VITALabAI.utils import utils
from keras import backend as K
from keras.layers import (
    Input, Conv2D, Flatten, Dense, Reshape, UpSampling2D, Lambda
)
from keras.models import Model
from keras.optimizers import Adam
from keras.regularizers import l2
LOGGER = utils.get_logger(__file__)
import os
from os.path import join
import h5py
from keras.utils import to_categorical
import numpy as np
import scipy.misc as sc

K.set_image_data_format("channels_last")


class AE(USbase):
    """
    Class that implement the autoencoder for the ultrasound CAMUS dataset. Several modes allow to access subparts
    of the model for dedicated uses.

    ELU activations were used rather than ReLU since they allowed a higher performance (up to 30% boost) for a similar capacity.
    Plus they allow negative activations, which is interesting for coding (intuition) and remain unbounded for positive outputs.

    Activity regularization on the code layer is especially useful for a AE model that will be used to provide
    an auxiliary loss (ACNN) to ensure a low magnitude of loss no matter the training data

    """

    def __init__(self, **kwargs):

        self.name = "model/camus/AE"
        self.input = kwargs.get('input_tensor', None)  # for use on a test tensor

        if kwargs.get('code_length'):
            self.code_length = kwargs['code_length']
        elif self.code_length is None:
            self.code_length = 32

        if kwargs.get('mode'):
            self.mode = kwargs['mode']
        elif self.mode is None:
            self.mode = "ae"

        super(AE, self).__init__(**kwargs)

    def sampler(self, args):
        """
        This layer uses the reparametrizaion trick to sample the code in VAE.
        stddev=0.0 means training a classical AE, 1 is very noisy
        """

        mean = args[0]
        log_dev = args[1]
        std_norm = K.random_normal(shape=(K.shape(mean)[0], self.code_length), mean=0.0, stddev=0.1)

        return mean + K.exp(0.5 * log_dev) * std_norm

    def build_model(self):
        """
        This function builds the baseline CNN self.model for US.
        """

        nb_feature_maps = 48

        # self.model_input
        if self.mode == "decoder_only":
            input_layer = Input(shape=(self.code_length,))
            xc = input_layer
            meanc = xc
            log_stdc = xc

        else:
            input_layer = Input(shape=(self.image_size[0], self.image_size[1], self.nb_channels), tensor=self.input)

            # encoder
            x = Conv2D(nb_feature_maps, (3, 3), activation='elu', strides=(2, 2),
                       kernel_initializer='glorot_normal')(input_layer)
            
            x = Conv2D(nb_feature_maps, (3, 3), activation='elu', kernel_initializer='glorot_normal')(x)
            
            x = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', strides=(2, 2),
                       kernel_initializer='glorot_normal')(x)
            
            x = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', kernel_initializer='glorot_normal')(x)
            
            x = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu', strides=(2, 2),
                       kernel_initializer='glorot_normal')(x)

            x = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu', kernel_initializer='glorot_normal')(x)

            x = Conv2D(1, (3, 3), activation='elu', strides=(2, 2),
                       kernel_initializer='glorot_normal')(x)

            # code
            x = Flatten()(x)

            # addition for VAE
            if self.mode == "vae":
                meanc = Dense(self.code_length, activation='elu')(x)
                log_stdc = Dense(self.code_length, activation='elu')(x)
                xc = Lambda(self.sampler)([meanc, log_stdc])

            else:
                # regularized so as to avoid high and unstable coefficients
                xc = Dense(self.code_length, activation='elu', activity_regularizer=l2(0.0001))(x)

        # decoder
        if self.mode != "encoder_only":
            x = Dense(16 * 16, activation='elu')(xc)

            x = Reshape((16, 16, 1), name='reshape_1')(x)

            x = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu', padding='same',
                       kernel_initializer='glorot_normal')(x)

            x = Conv2D(nb_feature_maps * 4, (3, 3), activation='elu', padding='same',
                       kernel_initializer='glorot_normal')(x)

            x = UpSampling2D(size=(2, 2))(x)

            x = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', padding='same',
                       kernel_initializer='glorot_normal')(x)

            x = UpSampling2D(size=(2, 2))(x)

            x = Conv2D(nb_feature_maps * 2, (3, 3), activation='elu', padding='same',
                       kernel_initializer='glorot_normal')(x)

            x = UpSampling2D(size=(2, 2))(x)

            x = Conv2D(nb_feature_maps, (3, 3), activation='elu', padding='same',
                       kernel_initializer='glorot_normal')(x)

            x = Conv2D(nb_feature_maps, (3, 3), activation='elu', padding='same',
                       kernel_initializer='glorot_normal')(x)

            x = UpSampling2D(size=(2, 2))(x)
            
            xo = Conv2D(self.dataset.nb_classes, (3, 3), activation='softmax', padding='same')(x)

        # model compilation and summary

        if self.mode == "encoder_only":
            self.model = Model(inputs=input_layer, outputs=xc)

        else:
            self.model = Model(inputs=input_layer, outputs=xo)

            uslosses = USLosses(self.dataset.nb_classes)
            if self.mode == "vae":
                loss = [uslosses.dice_vae([meanc, log_stdc])]
            else:
                loss = [uslosses.classes_dice_loss]

            metrics = [uslosses.classes_dice]
            opti = Adam(lr=3e-4, amsgrad=True)

            self.model.compile(loss=loss,
                            optimizer=opti,
                            metrics=metrics)

        if not self.kwargs.get('aux_model'):
            LOGGER.warning("Creating AE self.model.")
            self.model_summarize()

    def test(self):
        """
        This methods generates prediction on the test set
        """
        from VITALabAI.model.camus.ae_utils import compute_shapes, save_shape_space

        if not self.kwargs.get('predict', True):
            return

        # Loads the weights saved during training.
        self.model.load_weights(join(self.name + str(self.subfold), "valid_model_best_loss.hdf5"))
        self.save = 0

        # get encoder
        ae = self.model

        self.mode = "encoder_only"
        self.build_model()
        enc = self.model
        enc.set_weights(ae.get_weights())
        self.model = ae

        # Create the path to save the result if needed
        if not os.path.isdir(self.name + str(self.subfold)):
            os.mkdir(self.name + str(self.subfold))

        # Build the keras functions used to make the predictions
        keras_fct = K.function([K.learning_phase(), ] + ae.inputs,
                               ae.outputs)

        keras_fct_enc = K.function([K.learning_phase(), ] + enc.inputs,
                               enc.outputs)

        # Select the test dataset
        self.dataset.select_set('test')

        # Create a dataset file to store the predictions of the model.
        tmp_h5py = h5py.File(join(self.name + str(self.subfold),
                                  'res_tmp.hdf5'),
                            'w')

        # Loop to make all the predictions.
        codes = []

        for name, img, gt, info in self.dataset:

           LOGGER.debug("Processing: {}".format(name))

           # Resize the image to have the same size as a training one
           gte = to_categorical(gt)
           x_in = gte[np.newaxis, :, :, :]

            # Call the prediction function
           pred = keras_fct([0, ] + [x_in])
           code = keras_fct_enc([0, ] + [x_in])
           codes.append(code)

           # Select the class with higher probability to generate the segmentation.
           pred = np.squeeze(np.argmax(pred[0], axis=-1))

           count = self.dataset.im_count
           pname = name

           # keep track of the patient images
           # could be improved later on

           if count == 1:
               name = name + '_2CH_ED'
           elif count == 2:
               name = name + '_2CH_ES'
           elif count == 3:
               name = name + '_4CH_ED'
           else:
               name = name + '_4CH_ES'

           # resize everything to the original image size
           size = info[:3].astype('int')
           pred_resize = sc.imresize(np.squeeze(pred), (size[0], size[1]), interp='nearest')
           pred = np.round(pred_resize / np.max(pred_resize) * np.max(pred))[..., np.newaxis]
           img_resize = sc.imresize(np.squeeze(img), (size[0], size[1]), interp='nearest')
           img = np.round(img_resize / np.max(img_resize) * np.max(img))[..., np.newaxis]
           gt_resize = sc.imresize(np.squeeze(gt), (size[0], size[1]), interp='nearest')
           gt = np.round(gt_resize / np.max(gt_resize) * np.max(gt))[..., np.newaxis]

           # Save the result into the database file.
           tmp_h5py.create_group(name)
           tmp_h5py[name].create_dataset('IMG', data=img)
           tmp_h5py[name].create_dataset('GT', data=gt)
           tmp_h5py[name].create_dataset('PRED', data=pred)
           tmp_h5py[name].create_dataset('info', data=info)

           # Apply all needed post processing
           post = self.apply_post_processing(pred)
           tmp_h5py[name].create_dataset('PRED_Post', data=post)

           # save unprocessed segmentation images as .mhd
           if self.save_mhd_flag:
               segpath = "model/camus/AE/reconstructions/"
               self.save_mhd(pred, name, info, segpath, self.subfold, pname)

        # get decoder
        LOGGER.debug("Computing shape space")
        self.mode = "decoder_only"
        self.build_model()
        dec = self.model
        if self.mode == "vae":
            dec.set_weights(ae.get_weights()[18:])
        else:
            dec.set_weights(ae.get_weights()[16:])

        # compute shape space : standard deformations from the mean shape that was learnt
        nbv = 8  # number of components to analyze
        codes = np.squeeze(np.asarray(codes))

        shapes = compute_shapes(dec, codes, nbv)
        shape_space_path = "model/camus/AE/shape_space/" + str(self.subfold)

        if not os.path.isdir(shape_space_path):
            os.makedirs(shape_space_path)
        save_shape_space(shape_space_path, shapes, nbv)

        tmp_h5py.close()
        K.clear_session()

