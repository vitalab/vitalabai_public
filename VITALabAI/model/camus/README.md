## Model template and U-Net model applied to multiclass segmentation on the CAMUS database
usbase.py : Provide the framework for deep learning models dedicated to cardiac segmentation

usdataset.py : Manage the dataset stored in a hdf5 file to use with DL models

uspostprocess.py : Contains dedicated postprocessing methods

uspreprocess.py : Contains dedicated preprocessing methods

uslosses.py : Contains dedicated segmentation losses

usscores.py : Computes metrics

usunet.py : U-NET slightly modified 

Contact Sarah LECLERC (sarah.leclerc@insa-lyon.fr)

# single model

```bash
python3 main.py model.camus.usunet.USUNet -o "path='dataset/camus/datahdf5fold3.hdf5', batch_size=10, temporal=1, nb_epochs =30, train=1, save_feat=1, save_mhd=1"
```

path: string : path to the dataset

batch_size: int : number of elements per batch

nb_channels: int : number of channels to use

temp: bool : whether to process ED and ES jointly

nb_epochs: int : number of epochs to train

train : bool : whether to train the model before test

save_feat: bool : save intermediary features (or not)

save_mhd: bool : save results as mhd (or not)
        
code_length : int : Latent space size [optional : AE] 

mode : string : mode of the AE ("encoder_only", "decoder_only", "AE") [optional : AE]   

# cross validation 1 image

```bash
for sub in {1..10} # 1..num subfolds
do 
python3 main.py model.camus.usunet.USUNet -o "path='dataset/camus/datahdf5fold$sub.hdf5', batch_size=10, temporal=0, nb_epochs =50, train=1"
done
```

# cross validation 2 images

```bash
for sub in {1..10} # 1..num subfolds
do 
python3 main.py model.camus.usunet.USUNet -o "path='dataset/camus/datahdf5fold$sub.hdf5', batch_size=10, temporal=1, nb_epochs =50, train=1"
done
```

# cross validation Learn all auto-encoders

```bash
for sub in {1..10} # 1..num subfolds
do 
python3 main.py model.camus.autoencoder.AE -o "path='dataset/camus/datahdf5fold$sub.hdf5', batch_size=10, temporal=0, nb_epochs =300, train=1, save_mhd=1, code_length=64, mode='ae' "
done
```

# cross validation Learn all variational auto-encoders

```bash
for sub in {1..10} # 1..num subfolds
do 
python3 main.py model.camus.autoencoder.AE -o "path='dataset/camus/datahdf5fold$sub.hdf5', batch_size=10, temporal=0, nb_epochs =300, train=1, save_mhd=1, code_length=64, mode='vae' "
done
```

# cross validation ACNNs

```bash
for sub in {1..10} # 1..num subfolds
do 
python3 main.py model.camus.usunet.USUNet -o "path='dataset/camus/datahdf5fold$sub.hdf5', batch_size=10, temporal=0, nb_epochs =50, train=1, aux_model=1, alpha=100, code_length=64, mode='ae' "
done