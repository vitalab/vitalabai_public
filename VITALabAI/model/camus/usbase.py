# -*- coding: utf-8 -*-
"""
usbase.py
author: Sarah LECLERC from Clement Zotti's code

This file contains the USbase parent class from which every model derives from.

"""

import gc
import os
from os.path import join

import SimpleITK
import h5py
import numpy as np
import scipy.io
import scipy.misc as sc
import tensorflow as tf
from PIL import Image
from VITALabAI.VITALabAiAbstract import VITALabAiAbstract
from VITALabAI.dataset.camus.usdataset import USDataset
from VITALabAI.model.camus.uspostprocess import Post2dBigBlob
from VITALabAI.model.camus.uspreprocess import PreProcessSTD
from VITALabAI.model.camus.usscores import (
    MergeScoreIntoCsv, HausdorffScore, DiceScore
)
from VITALabAI.utils import utils
from keras import backend as K
from keras.models import Model
from keras.utils import generic_utils, plot_model, to_categorical

LOGGER = utils.get_logger(__file__)
np.random.seed(707)


class USbase(VITALabAiAbstract):
    """
    Base class for training network on the CAMUS dataset.
    """

    def __init__(self, path, batch_size=10, nb_epochs=30, temporal=0, save_feat=False, save_mhd=True, **kwargs):
        """
        Initializer
        :param path: string : path to the dataset
        :param batch_size: int : number of elements per batch
        :param nb_channels: int : number of channels to use
        :param nb_epochs: int : number of epochs to train
        :param temporal: bool : whether to process ED and ES jointly
        :param save_feat: bool : save intermediate feature maps (or not)
        :param save_mhd: bool : save results as mhd (or not)
        :param kwargs : optional parameters - for more complex models (ex:AE)
        """

        self.path = path
        self.subfold = int(os.path.basename(self.path)[-6])
        if self.subfold == 0:
            self.subfold = 10
        self.temporal = temporal
        self.batch_size = int(batch_size)

        if self.temporal:
            self.nb_channels = 2
        elif self.name == "model/camus/AE":
            self.nb_channels = 4
        else:
            self.nb_channels = 1

        self.nb_epochs = int(nb_epochs)
        self.model = None
        self.dataset = None
        self.image_size = None
        self.kwargs = kwargs
        self.save_feat = save_feat
        self.save_mhd_flag = save_mhd

        if self.name is None:
            self.name = "USbase"



    def build_model(self):
        # Not implemented here. This function is specific to the derived class.
        raise NotImplementedError("Abstract class.")

    def load_model(self, path):
        # Not implemented here. This function is specific to the derived class.
        raise NotImplementedError("Abstract class.")

    def load_dataset(self):
        """
        This function loads the dataset iterator.
        """

        LOGGER.debug("Loading the US datasets")

        self.dataset = USDataset(
            self.path,
            self.temporal,
            self.batch_size)
        self.image_size = self.dataset.image_size

    def _function_on_batch(self, on_batch, prefix):
        """
        This function applies the keras "train_on_batch" or "test_on_batch"
        function on training or validation data.
        :param on_batch: call-back function: keras function (train_on_batch or test_on_batch)
        :param prefix: string: prefix used to display from which dataset the result comes from
        :return tuple : dice and loss of the training for one epoch
        """

        # Set up the progress bar
        p_bar = generic_utils.Progbar(self.dataset.get_num_examples())
        all_loss, all_dice, all_aux = [], [], []
        old_loss, old_dice = float('inf'), float('-inf')

        # Loop on example x_data and y_data contains the data of one batch
        for x_data, y_data in self.dataset:

            # apply preprocessing
            for b in range(len(x_data)):
                x_data[b] = self.apply_pre_processing(x_data[b])

            x_data = np.array(x_data)
            y_data = np.array(y_data)

            if self.temporal:

                y_data1 = to_categorical(y_data[:, :, :, 0])
                y_data2 = to_categorical(y_data[:, :, :, 1])
                y_data = [y_data1, y_data2, np.concatenate([y_data1, y_data2], axis=3)]
                tloss, lossim1, lossim2, lossim3, diceim1, diceim2, _ = on_batch(x_data, y_data)  # run model

                loss = tloss
                dice = (diceim1 + diceim2) / 2
                aux = lossim3

            elif self.name == "model/camus/AE":
                y_datac = to_categorical(y_data)
                loss, dice = on_batch(np.copy(y_datac), y_datac)  # run model
                aux = 0.0
            else:
                y_data = to_categorical(y_data)
                if self.kwargs.get('aux_model'):
                    loss, dice, aux = on_batch(x_data, y_data)  # run model
                else:
                    loss, dice, aux = on_batch(x_data, y_data)  # run model

            # visualize
            all_loss.append(loss)
            all_dice.append(dice)
            all_aux.append(aux)

            # early saving dice
            if dice > old_dice:
                self.f_name = "{}_model_best_dice.hdf5".format(prefix)
                self.save_model()
                old_dice = dice

            # early saving loss
            if loss < old_loss:
                self.f_name = "{}_model_best_loss.hdf5".format(prefix)
                self.save_model()
                old_loss = loss

            # Progress bar update
            if self.temporal:
                nbp = x_data.shape[0] / 2
            else:
                nbp = x_data.shape[0] / 4

            p_bar.add(nbp, values=[
                ('{}_loss'.format(prefix), loss),
                ('{}_dice'.format(prefix), dice),
                ('{}_aux'.format(prefix), aux)
            ])
            gc.collect()

        return np.array(all_loss).mean(axis=0), np.array(all_dice).mean(axis=0), np.array(all_aux).mean(axis=0)

    def train(self):
        """
        This methods trains the model.
        """

        # By default do the training
        if not self.kwargs.get('train', True):
            return

        if not os.path.isdir(self.name + str(self.subfold)):
            os.mkdir(self.name + str(self.subfold))

        # Epochs counter
        epochs = 0
        self.save = 1  # allows to update model weights

        # keep a track of loss and dice for every epochs
        loss = {
            "train": [],
            "valid": []
        }
        dice = {
            "train": [],
            "valid": []
        }
        EFS = {
            "train": [],
            "valid": []
        }

        # Epochs loop alternate between training and validation
        while epochs < self.nb_epochs:
            self.dataset.select_set('train')
            mean_loss, mean_dice, mean_aux = self._function_on_batch(self.model.train_on_batch, 'train')
            loss['train'].append(mean_loss)
            dice['train'].append(mean_dice)
            EFS['train'].append(mean_aux)

            self.dataset.select_set('valid')
            mean_loss, mean_dice, mean_aux = self._function_on_batch(self.model.test_on_batch, 'valid')
            loss['valid'].append(mean_loss)
            dice['valid'].append(mean_dice)
            EFS['valid'].append(mean_aux)
            epochs += 1

        self.save = 0  # deactivate the update of the model

        LOGGER.debug("Saving scores and models in {}".format(self.name + str(self.subfold)))
        for k in loss.keys():
            # Save the training scores into a csv
            np.savetxt(join(self.name + str(self.subfold), "{}.csv".format(k)),
                       np.vstack((loss[k], dice[k], EFS[k])).T,
                       delimiter=',',
                       header=','.join(['loss', 'dice', 'aux']),
                       comments='',
                       fmt="%.5f")

    def test(self):
        """
        This methods generates prediction on the test set
        """

        if not self.kwargs.get('predict', True):
            return

        # Loads the weights saved during training.
        self.model.load_weights(join(self.name + str(self.subfold), "valid_model_best_loss.hdf5"))
        self.save = 0

        # Create the path to save the result if needed
        if not os.path.isdir(self.name + str(self.subfold)):
            os.mkdir(self.name + str(self.subfold))

        # Build the keras function used to make the predictions
        keras_fct = K.function([K.learning_phase(), ] + self.model.inputs,
                               self.model.outputs)

        # Select the test dataset
        self.dataset.select_set('test')

        # Create a dataset file to store the predictions of the model.
        tmp_h5py = h5py.File(join(self.name + str(self.subfold),
                                  'res_tmp.hdf5'),
                             'w')

        # Loop to make all the predictions.
        for name, img, gt, info in self.dataset:

            LOGGER.debug("Processing: {}".format(name))

            # Resize the image to have the same size as a training one

            x_in = img[np.newaxis, :, :, :]
            # apply preprocessing
            x_in = self.apply_pre_processing(x_in)

            # Call the prediction function
            pred = keras_fct([0, ] + [x_in])

            instants = ['ES', 'ED']
            if self.temporal:

                count = self.dataset.im_count
                if count > 2:
                    view = '4CH'
                else:
                    view = '2CH'

                # decompose prediction into images
                for c, instant in enumerate(instants):

                    im_name = name + '_' + str(view) + '_' + str(instant)

                    # Get the data from the dataset

                    x_img = img[:, :, c]
                    y_img = gt[:, :, c]
                    pred_img = pred[c][0, :, :, :]
                    pred_img = np.argmax(pred_img, axis=-1)

                    # resize everything
                    size = info[:3].astype('int')
                    pred_resize = sc.imresize(np.squeeze(pred_img), (size[0], size[1]), interp='nearest')
                    pred_img = np.round(pred_resize / np.max(pred_resize) * np.max(pred_img))[..., np.newaxis]
                    img_resize = sc.imresize(np.squeeze(x_img), (size[0], size[1]), interp='nearest')
                    x_img = np.round(img_resize / np.max(img_resize) * np.max(x_img))[..., np.newaxis]
                    gt_resize = sc.imresize(np.squeeze(y_img), (size[0], size[1]), interp='nearest')
                    y_img = np.round(gt_resize / np.max(gt_resize) * np.max(y_img))[..., np.newaxis]

                    # save the results
                    tmp_h5py.create_group(im_name)
                    tmp_h5py[im_name].create_dataset('IMG', data=x_img)
                    tmp_h5py[im_name].create_dataset('GT', data=y_img)
                    tmp_h5py[im_name].create_dataset('PRED', data=pred_img)
                    tmp_h5py[im_name].create_dataset('info', data=info)

                    post = self.apply_post_processing(pred_img.astype('int64'))
                    tmp_h5py[im_name].create_dataset('PRED_Post', data=post)

                    if self.save_mhd_flag:
                        segpath = "model/camus/cross_validation/"
                        self.save_mhd(pred_img, im_name, info, segpath, self.subfold, name)

            else:

                # Select the class with higher probability to generate the segmentation.
                pred = np.squeeze(np.argmax(pred[0], axis=-1))[..., np.newaxis]

                count = self.dataset.im_count
                pname = name

                # keep track of the patient images
                # could be improved later on
                if count == 1:
                    name = name + '_2CH_ED'
                elif count == 2:
                    name = name + '_2CH_ES'
                elif count == 3:
                    name = name + '_4CH_ED'
                else:
                    name = name + '_4CH_ES'

                # Save last feature maps
                if self.save_feat:
                    self.save_intermediate_output(x_in, name, -4)

                # resize everything
                size = info[:3].astype('int')
                pred_resize = sc.imresize(np.squeeze(pred), (size[0], size[1]), interp='nearest')
                pred = np.round(pred_resize / np.max(pred_resize) * np.max(pred))[..., np.newaxis]
                img_resize = sc.imresize(np.squeeze(img), (size[0], size[1]), interp='nearest')
                img = np.round(img_resize / np.max(img_resize) * np.max(img))[..., np.newaxis]
                gt_resize = sc.imresize(np.squeeze(gt), (size[0], size[1]), interp='nearest')
                gt = np.round(gt_resize / np.max(gt_resize) * np.max(gt))[..., np.newaxis]

                # Save the result into the database file.

                tmp_h5py.create_group(name)
                tmp_h5py[name].create_dataset('IMG', data=img)
                tmp_h5py[name].create_dataset('GT', data=gt)
                tmp_h5py[name].create_dataset('PRED', data=pred)
                tmp_h5py[name].create_dataset('info', data=info)

                # Apply all needed post processing
                post = self.apply_post_processing(pred)
                tmp_h5py[name].create_dataset('PRED_Post', data=post)

                # save unprocessed segmentation images as .mhd
                if self.save_mhd_flag:
                    segpath = "model/camus/cross_validation/"
                    self.save_mhd(pred, name, info, segpath, self.subfold, pname)

        tmp_h5py.close()
        K.clear_session()

    def save_mhd(self, input, name, info, segpath, subfold, pname):
        """
        This function saves raw and mhd file from the original image info
        :param input: ndarray : image to save
        :param name: string: image name
        :param segpath: string: path to the storage folder for mhd results
        """

        mhd_path = os.path.join(segpath + str(subfold), pname)

        if not os.path.isdir(mhd_path):
            os.makedirs(mhd_path)

        # get info
        origin = info[3:6]
        spacing = info[6:9]
        direction = info[9:]

        # save png()
        input = input.astype(np.uint8)
        im = Image.fromarray((input[:, :, 0] / 3 * 255).astype('uint8'))
        im.save(os.path.join(segpath, name + ".png"))

        # copy metainformation in seg
        seg = SimpleITK.GetImageFromArray(input)
        seg.SetOrigin(origin)
        seg.SetSpacing(spacing)
        seg.SetDirection(direction)

        SimpleITK.WriteImage(seg, os.path.join(mhd_path, name + ".mhd"))

    def save_intermediate_output(self, input, input_name, ind_layer):
        """
        This function allows to save an intermediate output to use as features in an other ML algorithm
        It is also really interesting to visualize learnt features at a given level

        :param input: ndarray : image to extract features on
        :param input_name: string: image name
        :param ind_layer: int: layer to extract feature maps from
        """

        # Select feature maps layer
        layers = self.model.layers
        feat_layer = layers[ind_layer]

        # Create a new model copying the first but stopping at the wanted layer
        intermediate_layer_model = Model(inputs=self.model.input,
                                         outputs=feat_layer.output)
        feat_maps = intermediate_layer_model.predict(input)
        feat_maps = np.squeeze(feat_maps, axis=0)

        # Save the layer outputs
        if not os.path.isdir("model/camus/autofeat_test/"):
            os.mkdir("model/camus/autofeat_test")

        scipy.io.savemat("model/camus/autofeat_test/{}.mat".format(input_name),
                         mdict={'learnt_features': feat_maps})

    def apply_post_processing(self, pred):
        """
        This function applies all post processing on the prediction image
        :param pred: ndarray : image to process
        :return postpred: ndarray: image processed
        """
        postpred = pred
        for post in self.get_postprocessing():
            postpred = post(postpred)

        return postpred

    def apply_pre_processing(self, im):
        """
        This function applies all pre processing on the prediction image
        :param pred: ndarray : image to process
        :return prepred: ndarray: image preprocessed
        """
        prepred = im
        for pre in self.get_preprocessing():
            prepred = pre(prepred)

        return prepred

    @staticmethod
    def get_preprocessing():
        """
        This function retrieves the preprocessing list
        :return preproc: tuple : list of preprocessing functions
        """

        preproc = [PreProcessSTD()]  # PreProcessSTD()
        return preproc

    @staticmethod
    def get_postprocessing():
        """
        This function retrieves the postprocessing list
        :return postproc: tuple : list of postprocessing functions
        """
        postproc = [Post2dBigBlob(), ]
        return postproc

    @staticmethod
    def get_scores():
        """
        This function retrieves the scores list
        :return scores: tuple : list of evaluation functions
        """
        scores = [
            MergeScoreIntoCsv(
                list_score=[
                    DiceScore(),
                    HausdorffScore(),
                ]   # SaveNifti()
            )
        ]
        return scores

    def export_results(self):
        """
        This function computes predictions results
        """

        # Save the result of the prediction.
        if not os.path.isdir(self.name + str(self.subfold)):
            os.mkdir(self.name + str(self.subfold))
        if not os.path.isdir(join(self.name + str(self.subfold), 'scores')):
            os.mkdir(join(self.name + str(self.subfold), 'scores'))

        tmp_h5py = h5py.File(join(self.name + str(self.subfold),
                                  'res_tmp.hdf5'),
                             'r')

        # Compute all the test scores.
        for score in self.get_scores():
            score(tmp_h5py, join(self.name + str(self.subfold), 'scores'))

    def save_model(self):
        """
        This function saves the model weights
        """
        if self.save :
            self.model.save_weights(join(self.name + str(self.subfold), self.f_name), overwrite=True)


    def model_summarize(self):
        """
        This function prints and saves a summary of the keras CNN model.
        """
        if not os.path.isdir(self.name):
            os.mkdir(self.name)

        plot_model(self.model, join(self.name, 'network_architecture.png'), show_shapes=True, show_layer_names=True)
        self.model.summary()

### Part in development : how to visualize gradients to see  if a loss is functional ###
    def get_gradients(self):
        """Return the gradient of every trainable weight in model

        Parameters
        -----------
        model : a keras model instance

        First, find all tensors which are trainable in the model. Surprisingly,
        `model.trainable_weights` will return tensors for which
        trainable=False has been set on their layer (last time I checked), hence the extra check.
        Next, get the gradients of the loss with respect to the weights.

        """

        model = self.model
        weights = [tensor for tensor in model.trainable_weights if model.get_layer(tensor.name.split("/")[0]).trainable]
        optimizer = self.model.optimizer

        return optimizer.get_gradients(self.model.total_loss, weights)
