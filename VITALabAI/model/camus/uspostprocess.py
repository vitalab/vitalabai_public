# -*- coding: utf-8 -*-
"""
uspostprocess.py
author: Sarah LECLERC from Clement ZOTTI's code
Implementations of multi-class segmentation post-processing
"""

import numpy as np
from scipy.ndimage.measurements import label
from skimage.morphology import convex_hull_image


class PostProcessing(object):
    """
    This is the base class for all the post processing.
    """

    def __call__(self, data, *kwargs):
        """
        All the postprocessing subclasses must override this method.
        :param data: ndarray: data to process
        :return ndarray : processed data
        """

        raise NotImplementedError("Abstract class.")


class Post2dBigBlob(PostProcessing):
    """
    This class finds the biggest blob over each slice of a 3d image and
    returns an image with only this blob.
    """

    def __call__(self, img, *kwargs):

        if len(img.shape) == 4:
            img = np.squeeze(img, axis=0)
        if len(img.shape) == 3:
            img = np.squeeze(img, axis=2)

        return np.expand_dims(self.process_2d_image(img), axis=2)

    def process_2d_image(self, img):
        """
        Method to process the 2D image
        :param img: ndarray: 2D label field
        :return ndarray : processed image
        """

        res = np.zeros_like(img)
        classes = np.unique(img.flat)

        for c in range(1, len(classes)):

            timg = np.copy(img)
            timg[timg != c] = 0

            # Find each blob in the image
            lbl, num = label(timg)

            # Count the number of elements per label
            count = np.bincount(lbl.flat)

            if not np.any(count[1:]):
                continue

            # Select the largest blob
            maxi = np.argmax(count[1:]) + 1

            # Remove the other blobs
            lbl[lbl != maxi] = 0

            # Rescale the result
            lbl = np.clip(lbl, 0, 1)

            res += lbl * c
            res[res > c] = c

        return res


class PostConvexHull(PostProcessing):
    """
    Postprocessing consisting on computing a convexHull along the first
    dimension of an array.
    """

    def __call__(self, img, *kwargs):

        if len(img.shape) == 4:
            img = np.squeeze(img, axis=0)
        if len(img.shape) == 3:
            img = np.squeeze(img, axis=2)

        return np.expand_dims(self.process_2d_image(img), axis=2)

    def process_2d_image(self, img):
        """
        Method to process the 2D image
        :param img: ndarray: 2D label field
        :return ndarray : processed image
        """

        res = np.zeros_like(img)

        for c in [1, 2, 3]:
            c_img = np.copy(img, order='C')
            c_img[c_img != c] = 0
            hull = np.array([convex_hull_image(c_img)])
            res += np.squeeze(hull, axis=0) * c

            if c == 2:
                res[res == 3] = 1
            res[res > c] = c

        return res
