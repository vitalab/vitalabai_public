"""
usunet.py
author: Sarah LECLERC from Clement ZOTTI's code
This file implements a U-Net based architecture
"""

from VITALabAI.model.camus.usbase import USbase
from VITALabAI.model.camus.uslosses import USLosses
from VITALabAI.utils import utils
from keras import backend as k
from keras.layers import (
    Input, Conv2D, MaxPooling2D,
    Permute, Activation, UpSampling2D, LeakyReLU
)
from keras.layers.merge import Concatenate
from keras.models import Model
from keras.optimizers import Adam

LOGGER = utils.get_logger(__file__)
k.set_image_data_format("channels_last")


class USUNet(USbase):
    """
    Class that implement the UNet for the ultrasound CAMUS dataset.

    This is inspired by the UNet work:
    https://arxiv.org/abs/1505.04597

    The input size, the number and types of layers are different. The number of feature maps per
    layer is also different.
    """

    def __init__(self, **kwargs):
        self.name = "model/camus/USUNet"
        super(USUNet, self).__init__(**kwargs)

    def build_model(self):
        """
        This function builds the baseline CNN model for US.
        """

        LOGGER.warning("Creating US model.")

        # model_input
        input_img = Input(shape=(self.image_size[0], self.image_size[1], self.nb_channels))

        if self.temporal:
            nb_feature_maps = 48
        else:
            nb_feature_maps = 32

        # segmentation network architecture
        x = Conv2D(nb_feature_maps, (3, 3), activation = 'elu', padding="same")(input_img)
        x1 = Conv2D(nb_feature_maps, (3, 3), activation = 'elu', padding="same")(x)

        x = MaxPooling2D()(x1)

        x = Conv2D(nb_feature_maps, (3, 3), activation = 'elu', padding="same")(x)
        x2 = Conv2D(nb_feature_maps, (3, 3), activation = 'elu', padding="same")(x)

        x = MaxPooling2D()(x2)

        x = Conv2D(nb_feature_maps * 2, (3, 3), activation = 'elu', padding="same")(x)
        x3 = Conv2D(nb_feature_maps * 2, (3, 3), activation = 'elu', padding="same")(x)

        x = MaxPooling2D()(x3)

        x = Conv2D(nb_feature_maps * 4, (3, 3), activation = 'elu', padding="same")(x)
        x4 = Conv2D(nb_feature_maps * 4, (3, 3), activation = 'elu', padding="same")(x)

        x = MaxPooling2D()(x4)

        x = Conv2D(nb_feature_maps * 4, (3, 3), activation = 'elu', padding="same")(x)
        x5 = Conv2D(nb_feature_maps * 4, (3, 3), activation = 'elu', padding="same")(x)

        x = MaxPooling2D()(x5)

        x = Conv2D(nb_feature_maps * 4, (3, 3), activation = 'elu', padding="same")(x)
        x = Conv2D(nb_feature_maps * 4, (3, 3), activation = 'elu', padding="same")(x)

        # 512
        x = UpSampling2D(size=(2, 2))(x)
        x = Concatenate(axis=3)([x, x5])

        x = Conv2D(nb_feature_maps * 4, (3, 3), activation = 'elu', padding="same")(x)
        x = Conv2D(nb_feature_maps * 4, (3, 3), activation = 'elu', padding="same")(x)

        # 512
        x = UpSampling2D(size=(2, 2))(x)
        x = Concatenate(axis=3)([x, x4])

        x = Conv2D(nb_feature_maps * 4, (3, 3), activation = 'elu', padding="same")(x)
        x = Conv2D(nb_feature_maps * 4, (3, 3), activation = 'elu', padding="same")(x)

        # 256

        x = UpSampling2D(size=(2, 2))(x)
        x = Concatenate(axis=3)([x, x3])

        x = Conv2D(nb_feature_maps * 2, (3, 3), activation = 'elu', padding="same")(x)
        x = Conv2D(nb_feature_maps * 2, (3, 3), activation = 'elu', padding="same")(x)

        # 128

        x = UpSampling2D(size=(2, 2))(x)
        x = Concatenate(axis=3)([x, x2])

        x = Conv2D(nb_feature_maps, (3, 3), activation = 'elu', padding="same")(x)
        x = Conv2D(nb_feature_maps, (3, 3), activation = 'elu', padding="same")(x)

        # 64

        x = UpSampling2D(size=(2, 2))(x)
        x = Concatenate(axis=3)([x, x1])

        x = Conv2D(nb_feature_maps, (3, 3), activation = 'elu', padding="same")(x)
        x = Conv2D(nb_feature_maps, (3, 3), activation = 'elu', padding="same")(x)

        # output

        uslosses = USLosses(self.dataset.nb_classes)

        if self.temporal:

            xo1 = Conv2D(self.dataset.nb_classes, (1, 1))(x)
            xo2 = Conv2D(self.dataset.nb_classes, (1, 1))(x)

            o1 = Activation("softmax", name = "o1")(xo1)
            o2 = Activation("softmax", name="o2")(xo2)

            o3 = Concatenate(name = "o3", axis=3)([o1, o2])

            loss = {"o1":uslosses.classes_dice_loss, "o2":uslosses.classes_dice_loss, "o3":uslosses.EFS_loss}
            weighting = [0.0, 0.0, 1.0]
            metrics = [uslosses.classes_dice]
            outputs = [o1, o2, o3]

        else:

            xf = Conv2D(self.dataset.nb_classes, (1, 1))(x)
            output = Activation("softmax")(xf)

            weighting = [1.0]
            outputs = [output]

            # ACNN loss
            if self.kwargs.get('aux_model'):
                metrics = [uslosses.classes_dice, uslosses.shape_model_loss(self.path, self.image_size, self.dataset,
                                                                            self.subfold, self.kwargs)]

                loss = [uslosses.ACNN_loss(self.path, self.image_size, self.dataset,
                                        self.subfold, self.kwargs)]

            else:
                #metrics = [uslosses.classes_dice, uslosses.classes_hausdorff_loss]
                #loss = [uslosses.classes_dice_and_hd_loss]
                metrics = [uslosses.classes_dice, uslosses.classes_dice]
                loss = [uslosses.classes_dice_loss,]

        # compile model and plot summary
        self.model = Model(inputs=[input_img, ], outputs=outputs)
        opti = Adam(lr=3e-4, amsgrad=True)

        self.model.compile(loss=loss,
                           loss_weights=weighting,
                           optimizer=opti,
                           metrics=metrics)

        self.model_summarize()

