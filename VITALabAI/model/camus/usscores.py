# -*- coding: utf-8 -*-
"""
scores.py
author: Sarah LECLERC from Clement ZOTTI's code
Implementations of scores computation
"""

import os

import nibabel as nib
import numpy as np
import pandas as pd
from tqdm import tqdm
from VITALabAI.utils import utils
from medpy.metric.binary import hd, dc, assd, precision, recall
from skimage.morphology import binary_erosion

LOGGER = utils.get_logger(__file__)


class BaseScore(object):
    """
    Base class used for each score class.
    """

    _name = None

    def __call__(self, h5file, save_path):
        """
        Must be implemented in child classes.
        :param h5file : hdf5 : file containing Ground Truth and Prediction
        :param save_path: string : the path where to save the outputs if necessary
        """

        raise NotImplementedError("Abstract class.")


class SaveNifti(BaseScore):
    """
    This class provides the ability to save the h5py into nifti format
    Saves only the first modality of the image
    """

    _name = 'Nifti'

    def __call__(self, h5file, save_path):

        # Iterator on the hdf5 file for each predicted images.
        for name in h5file.keys():

            #LOGGER.debug("Generating a .nii.gz file for {}".format(name))

            pred = h5file[name]['PRED']
            pred_post = h5file[name]['PRED_Post']
            ground = h5file[name]['GT']
            img = h5file[name]['IMG']
            voxel = h5file[name]['info'][6:9]

            # It's ('s', 0, 1, 'time'), the time dimension is always of size 1 and for visualization
            # purpose we can remove it.
            pred = pred[..., 0]
            pred_post = pred_post[..., 0]
            ground = ground[..., 0]
            img = img[..., 0]

            # We rotate the image to have the right orientation when visualizing results
            pred = np.rot90(pred.transpose((1, 0)), 2)
            pred_post = np.rot90(pred_post.transpose((1, 0)), 2)
            ground = np.rot90(ground.transpose((1, 0)), 2)
            img = np.rot90(img.transpose((1, 0)), 2)

            #LOGGER.debug("Save {0}".format(os.path.join(save_path, name)))

            datas = [pred, pred_post, ground, img]
            tags = ["prediction", "Post_prediction",
                    "groundtruth", "image"]

            for data, tag in zip(datas, tags):

                save_nii_file(data,
                              os.path.join(save_path,
                                           "{}_{}.nii.gz".format(name, tag)),
                              zoom=voxel)

            contours = self._generate_contour(pred_post)
            for i, contour in enumerate(contours):
                save_nii_file(contour,
                              os.path.join(save_path,
                                           "{}_contour_{}.nii.gz".format(name, i)),
                              zoom=voxel)

    def _generate_contour(self, pred):
        """
        Method to generate the contour of the prediction, used for visualisation.
        :param pred: ndarray: the prediction made by the model
        :return labels_contour: list : list of array with a contour for each class
        """

        labels_contour = []

        # Get the maximum label contain in the image
        for i in range(1, pred.max() + 1):
            # Choose only the current class and discard the other
            lbl = (pred == i).astype(np.int)
            lbl -= binary_erosion(lbl)
            labels_contour.append(lbl)

        return labels_contour


class MergeScoreIntoCsv(BaseScore):
    """
    Basic class to merge each score that can fit into a csv.
    The scores usually contain all the metrics, distances or numerical results.
    """

    def __init__(self, list_score, ext_name=""):
        """
        Initializer
        :param list_score: list: a list of BaseCsvScore classes
        :param ext_name: string: a suffix added inside the csv file name
        """

        self.list_score = list_score
        self.ext_name = ext_name

    def __call__(self, h5file, save_path):

        # Get all the scores given at object creation from the list_score
        columns = [score._name for score in self.list_score]

        for c in np.arange(1, 4):
            columns = columns + [score._name+'_struct{}'.format(c) for score in self.list_score]

        # Generate the path to save the csv file
        path = 'merge_score{}.csv'.format(self.ext_name
                                          if self.__dict__.get("ext_name", None)
                                          else "")
        path = os.path.join(save_path, path)

        rows = list(h5file.keys())
        res = []

        # Iterate through image from the result hdf5 file
        print("Writing scores in csv file : ")
        for name in tqdm(h5file.keys(), total=len(rows), unit="Patients"):

            # Remove channel dim (always 1) don't need in prediction

            preds = np.array(h5file[name]['PRED_Post']).astype(np.int16)
            gts = np.array(h5file[name]['GT']).astype(np.int16)

            # Iterate on each BaseCsvScore object and set the voxel spacing
            for f in self.list_score:
                f.voxel = h5file[name]['info'][6:9]

            # Compute metrics for all classes and total scores
            la = np.array([f.image_score([preds, gts]) for f in self.list_score])
            for c in np.arange(1, np.max(preds)+1):
                pred = preds == c
                gt = gts == c
                la = np.concatenate((la, np.array([f.image_score([pred, gt])
                                    for f in self.list_score])), axis=0)

            res.append(la)

        try:

            # compute mean and std
            mean = np.mean(np.asarray(res), axis=0)
            std = np.std(np.asarray(res), axis=0)
            res.append(mean)
            res.append(std)
            rows.append("mean")
            rows.append("std")

            # Create the DataFrame and save it
            df = pd.DataFrame(res, index=rows, columns=columns)
            df.to_csv(path)

        except (AssertionError, ValueError):

            print("An error occured while saving results. The model may not predict all awaited classes or metrics for all patients.")


class BaseCsvScore:
    """
    Base class to have the score saved into its own csv file.
    """

    def image_score(self, data):
        """
        This method should be implemented in each child of this class.
        :param  data: tuple: the 1st element is the prediction and the 2nd is the groundtruth
        :return the score of the prediction
        """


class BasicMetrics(BaseCsvScore):

    def image_score(self, data):

        preds = data[0]
        gts = data[1]
        res = []

        # Compute the metric for each class in the image
        for i in range(1, np.amax(gts) + 1):
            tgt = np.copy(gts)
            tpred = np.copy(preds)
            tgt[tgt != i] = 0
            tpred[tpred != i] = 0
            res.append(self.metrics(tgt, tpred))

        res = np.array(res)
        res[np.where(res == np.array(None))] = -1
        res = res.mean(axis=0)
        return res


class PrecisionScore(BasicMetrics):
    """
    Precision class
    """

    def __init__(self):
        self._name = 'Precision'
        self.metrics = precision


class RecallScore(BasicMetrics):
    """
    Recall class
    """

    def __init__(self):
        self._name = 'Recall'
        self.metrics = recall


class DiceScore(BasicMetrics):
    """
    Dice class
    """

    def __init__(self):
        self._name = 'Dice'
        self.metrics = dc


class BaseDistance(BaseCsvScore):
    def image_score(self, data):
        """
        This method computes a metric distance for a multi-channel image.
        :param  data: list, tuple: a list or tuple of only two elements, first is the prediction and second is the groundtruth
        :return the metric distance score
        """
        res = []

        if data[1].sum() != 0 and data[0].sum() != 0:
            img = data[0]
            gt = data[1]

            # Compute the metric distance for each class in the image
            for i in range(1, np.amax(gt) + 1):
                tgt = np.copy(gt)
                tpred = np.copy(img)
                tgt[tgt != i] = 0
                tpred[tpred != i] = 0

                if tpred.sum() != 0 and tgt.sum() != 0:
                    res.append(self.distance(tpred, tgt, self.voxel))

        res = np.array(res)
        if len(res) == 0:
            return float('inf')
        return res.max()


class HausdorffScore(BaseDistance):
    """
    Hausdorff class
    """

    def __init__(self):

        self._name = 'Hausdorff'
        self.distance = hd


class ASSDScore(BaseDistance):
    """
    ASSD class
    """

    def __init__(self):
        self._name = 'ASSD'
        self.distance = assd


def save_nii_file(array, output_filepath, affine=np.eye(4), header=None,
                  zoom=(1, 1), dtype=np.float32):
    """
    This method save an numpy array in a nii image.
    :param  array: ndarray: image to save
    :param  output_filepath: string: path to store the nii image
    :param  affine: 2D matrix: transformation mapping voxel and world axes
    :param  header: dictionary: NIFTI image metadata
    :param  zoom: int[]: scaling coefficients
    :param  dtype: dtype: data type in which the data is stored
    """

    img = nib.Nifti1Image(array.astype(dtype), affine, header=header)
    img.header.set_zooms(zoom)
    img.set_data_dtype(dtype)
    nib.save(img, output_filepath)
