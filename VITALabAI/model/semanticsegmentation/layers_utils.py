"""
layer_utils.py
author: Charles Authier (charles.authier@usherbrooke.ca)
date: 03/08/2018

This code regroups all layer used in the different network of this project.
"""
from keras import backend as K
from keras import regularizers
from keras.layers import (
    Add, BatchNormalization, Conv2D, Conv2DTranspose, Dropout,
    concatenate, MaxPooling2D,
    Activation)
from keras.layers.advanced_activations import LeakyReLU

assert K.image_data_format() == 'channels_last'


def conv2D_bn_leakyrelu(inputs, feature_maps=16, bn_axis=None, kernel_size=3, alpha=1e-05, stride=1,
                        relu=True, bn=True):
    """This function applies a batch normalization to a Conv2D.

    Args:
        inputs: tensor, This tensor represents the input given to this block.
        feature_maps: int, Number of feature maps for the convolution layer.
        bn_axis: int, Axis on which to apply the batch normalization.
        kernel_size: int or tuple/list of 2 integers, Height and width of the 2D convolution window.
            Can be a single integer to specify the same value for all spatial dimensions.
        alpha: float, Alpha in the regularizer.
        stride: int, Strides of the convolution along the height and width.
        relu: bool, Applies the LeakyRelu function.
        bn: bool, Applies the batch normalization function.

    Returns:
        layer block
    """
    conv = Conv2D(
        feature_maps,
        kernel_size=kernel_size,
        strides=(stride, stride),
        padding='same',
        kernel_regularizer=regularizers.l2(alpha),
    )(inputs)
    if bn:
        conv = BatchNormalization(axis=bn_axis)(conv)
    if relu:
        conv = LeakyReLU()(conv)
    return conv


def conv2Dtranspose_leakyrelu_bn(inputs, feature_maps=16, bn_axis=None, alpha=1e-05, stride=2, relu=True,
                                 bn=True):
    """This function applies a batch normalization to a Conv2D.

    Args:
        inputs: tensor, This tensor represents the input given to this block.
        feature_maps: int, Number of feature maps for the convolution layer.
        bn_axis: int, Axis on which to apply the batch normalization.
        alpha: float, Alpha in the regularizer.
        stride: int, Strides of the convolution along the height and width.
        relu: bool, Applies the LeakyRelu function.
        bn: bool, Applies the batch normalization function.

    Returns:
        layer block
    """
    conv = Conv2DTranspose(
        feature_maps,
        (2, 2),
        strides=(stride, stride),
        padding='same',
        kernel_regularizer=regularizers.l2(alpha),
    )(inputs)
    if relu:
        conv = LeakyReLU()(conv)
    if bn:
        conv = BatchNormalization(axis=bn_axis)(conv)
    return conv


def three_conv_res_block_leakyrelu(inputs, input_channel, output_channel, stride=1, alpha=1e-05,
                                   bn_axis=None, bn=True):
    """This function makes residual layer with a batch normalization to a Conv2D with specific kernal sizes
        for pyramidnet.

    Args:
        inputs: tensor, This tensor represents the input given to this block.
        input_channel: int, Number of feature maps for the convolution layer.
        output_channel: int, Number of feature maps for the convolution layer.
        stride: int, Strides of the convolution along the height and width.
        alpha: float, Alpha in the regularizer.
        bn_axis: int, Axis on which to apply the batch normalization.
        bn: bool, Applies the batch normalization function.

    Returns:
        residual layer
    """
    conv1 = conv2D_bn_leakyrelu(
        inputs, input_channel, kernel_size=1,
        alpha=alpha, bn_axis=bn_axis, bn=bn,
    )
    conv2 = conv2D_bn_leakyrelu(
        conv1, output_channel, kernel_size=3, stride=stride,
        alpha=alpha, bn_axis=bn_axis, bn=bn
    )
    conv3 = conv2D_bn_leakyrelu(
        conv2, output_channel, kernel_size=1, relu=False,
        alpha=alpha, bn_axis=bn_axis, bn=bn
    )

    if input_channel == output_channel and stride == 1:
        answer = Add()([conv3, inputs])

    else:
        conv1_ = conv2D_bn_leakyrelu(
            inputs, output_channel, kernel_size=1, relu=False,
            stride=stride, alpha=alpha, bn_axis=bn_axis, bn=bn
        )
        answer = Add()([conv3, conv1_])

    answer = LeakyReLU()(answer)
    return answer


def res_conv_layer(inputs, input_channel, output_channel, stride=1, alpha=1e-05, bn_axis=None,
                   leaky_relu=True, bn=True):
    """This function makes residual layer with a batch normalization to a Conv2D.

    Args:
        inputs: tensor, This tensor represents the input given to this block.
        input_channel: int, Number of feature maps for the convolution layer.
        output_channel: int, Number of feature maps for the convolution layer.
        stride: int, Strides of the convolution along the height and width.
        alpha: float, Alpha in the regularizer.
        bn_axis: int, Axis on which to apply the batch normalization.
        leaky_relu: bool, Applies the LeakyRelu function.
        bn: bool, Applies the batch normalization function.

    Returns:
        residual layer
    """
    if input_channel == output_channel and stride == 1:
        conv1 = conv2D_bn_leakyrelu(
            inputs, input_channel, alpha=alpha, bn_axis=bn_axis, bn=bn
        )
        conv2 = conv2D_bn_leakyrelu(
            conv1, input_channel, alpha=alpha, bn_axis=bn_axis, bn=bn
        )
        conv3 = conv2D_bn_leakyrelu(
            conv2, input_channel, alpha=alpha, bn_axis=bn_axis, bn=bn
        )
        answer = Add()([conv3, inputs])
        if leaky_relu:
            return LeakyReLU()(answer)
        else:
            return answer

    else:
        conv1 = conv2D_bn_leakyrelu(
            inputs, input_channel, stride=stride, alpha=alpha, bn_axis=bn_axis, bn=bn
        )
        conv2 = conv2D_bn_leakyrelu(
            conv1, output_channel, alpha=alpha, bn_axis=bn_axis, bn=bn
        )
        conv3 = conv2D_bn_leakyrelu(
            conv2, output_channel, relu=False, alpha=alpha, bn_axis=bn_axis, bn=bn
        )
        conv1_ = conv2D_bn_leakyrelu(
            inputs, output_channel, relu=False, stride=stride, alpha=alpha, bn_axis=bn_axis, bn=bn
        )
        answer = Add()([conv1_, conv3])
        if leaky_relu:
            return LeakyReLU()(answer)
        else:
            return answer


def bn_relu_conv2D_dp(inputs, feature_maps=16, bn_axis=None, dropout_prob=0.1, alpha=1e-05, stride=1,
                      relu=True, bn=True):
    """This function applies a batch normalization, a LeakyReLu and a dropout to a Conv2D.
    Typical of the TiramitsuNet.
    (c.f. Table 1, Layer(left one), in ref. paper, link in the header of the TiramisuNet)

    Args:
        inputs: tensor, This tensor represents the input given to this block.
        feature_maps: int, Number of feature maps for the convolution layer.
        bn_axis: int, Axis on which to apply the batch normalization.
        dropout_prob: float, Dropout rate between 0 and 1.
        alpha: float, Alpha in the regularizer.
        stride: int, Strides of the convolution along the height and width.
        relu: bool, Applies the LeakyRelu function.
        bn: bool, Applies the batch normalization function.

    Returns:
        block layer
    """
    conv = inputs
    if relu:
        conv = LeakyReLU()(conv)
    if bn:
        conv = BatchNormalization(axis=bn_axis)(conv)

    conv = Conv2D(
        feature_maps,
        (3, 3),
        strides=(stride, stride),
        padding='same',
        kernel_regularizer=regularizers.l2(alpha),
    )(conv)

    if dropout_prob is not None:
        conv = Dropout(dropout_prob)(conv)
    return conv


def dense_block(inputs, nb_layers, feature_maps=16, bn_axis=None, dropout_prob=0.1, alpha=1e-05):
    """This function creates a dense block typical of the TiramitsuNet.
    (c.f. Fig.2 in ref. paper, link in the header of the TiramisuNet)

    Args:
        inputs: tensor, This tensor represents the input given to this block.
        nb_layers: int, Number of layers.
        feature_maps: int, Number of feature maps for the convolution layer.
        bn_axis: int, Axis on which to apply the batch normalization.
        dropout_prob: float, Dropout rate between 0 and 1.
        alpha: float, Alpha in the regularizer.

    Returns:
        dense block
    """
    layer_outputs = []
    conv = inputs
    for layer_idx in range(nb_layers):
        layer_output = bn_relu_conv2D_dp(
            conv, feature_maps=feature_maps, bn_axis=bn_axis,
            dropout_prob=dropout_prob, alpha=alpha,
        )
        conv = concatenate([conv, layer_output])
        layer_outputs.append(layer_output)
    block_output = concatenate(layer_outputs)
    block_size = nb_layers * feature_maps
    return block_output, block_size


def trans_down(inputs, nb_classes, bn_axis=None, dropout_prob=0.1, alpha=1e-05, relu=True, bn=True):
    """This function applies a batch normalization, dropout and maxpooling to a Conv2D. Typical of the
    TiramitsuNet.
    (c.f. Table 1, TD(middle one), in ref. paper, link in the header of the TiramisuNet)

    Args:
        inputs: tensor, This tensor represents the input given to this block.
        nb_classes: int, Number of classes.
        bn_axis: int, Axis on which to apply the batch normalization.
        dropout_prob: float, Dropout rate between 0 and 1.
        alpha: float, Alpha in the regularizer.
        relu: bool, Applies the LeakyRelu function.
        bn: bool, Applies the batch normalization function.

    Returns:
        block layer
    """
    conv = inputs
    if bn:
        conv = BatchNormalization(axis=bn_axis)(conv)
    if relu:
        conv = LeakyReLU()(conv)

    conv = Conv2D(nb_classes, (1, 1), kernel_regularizer=regularizers.l2(alpha))(conv)

    if dropout_prob is not None:
        conv = Dropout(dropout_prob)(conv)
    conv = MaxPooling2D(pool_size=(2, 2))(conv)
    return conv


def trans_up(inputs, feature_maps, alpha=1e-05):
    """This function applies a batch normalization, dropout and upsampling to a Conv2D.
     Typical of the TiramitsuNet.
    (c.f. Table 1, TU(right one), in ref. paper, link in the header of the TiramisuNet)

    Args:
        inputs: tensor, This tensor represents the input given to this block.
        feature_maps: int, Number of feature maps for the convolution layer.
        alpha: float, Alpha in the regularizer.

    Returns:
        layer block
    """
    conv = Conv2DTranspose(
        feature_maps,
        (3, 3),
        strides=(2, 2),
        padding='same',
        kernel_regularizer=regularizers.l2(alpha),
    )(inputs)
    return conv


def conv_relu(x, featuremaps=16, l2reg=0):
    """ Basic block of convolution, batchnorm and leaklyrelu.

    Args:
        x: tensor, This tensor represent the input given to this block.
        featuremaps: int, Number of featuremaps for the convolution layer.
        l2reg: float, regularization factor

    Returns:
        conv_relu layer
    """
    x_c = Conv2D(featuremaps, (3, 3), padding="same", kernel_regularizer=regularizers.l2(l2reg))(x)
    x_lr = Activation('relu')(x_c)
    return x_lr


def conv_bn_relu(x, featuremaps=16, bn_axis=3, l2reg=0):
    """ Basic block of convolution, batchnorm and leaklyrelu.

    Args:
        x: tensor, This tensor represent the input given to this block.
        featuremaps: int, Number of featuremaps for the convolution layer.
        bn_axis: int, Which axis to apply the batchnormalization.
        l2reg: float, kernel regularizer l2 regularization factor

    Returns:
        conv bn relu layer
    """
    x_c = Conv2D(featuremaps, (3, 3), padding="same", kernel_regularizer=regularizers.l2(l2reg))(x)
    x_b = BatchNormalization(axis=bn_axis)(x_c)
    x_lr = Activation('relu')(x_b)
    return x_lr


def elu_conv(x, nb_feature_maps, k_size):
    act = Activation("elu")(x)
    return Conv2D(nb_feature_maps, k_size, padding="same",
                  kernel_regularizer=regularizers.l2(1e-5))(act)


def bn_act_conv(x, nb_feature_maps, k_size, act="relu"):
    bn = BatchNormalization(axis=1)(x)
    act = Activation(act)(bn)
    conv = Conv2D(nb_feature_maps, k_size, padding="same")(act)
    return conv


def bn_conv(x, nb_feature_maps, k_size):
    bn = BatchNormalization(axis=1)(x)
    conv = Conv2D(nb_feature_maps, k_size, padding="same")(bn)
    return conv


def resnet_block(x, nb_feature_maps, is_first=False):
    """ Create resnet block layer

    Args:
        x: keras input
        nb_feature_maps: int, number of feature maps
        is_first: bool

    Returns:
        resnet block
    """
    bottleneck = nb_feature_maps // 4
    if is_first:
        c1 = Conv2D(bottleneck, 3, padding="same",
                    kernel_regularizer=regularizers.l2(1e-5))(x)
    else:
        c1 = bn_conv(x, bottleneck, 1)
    c2 = bn_act_conv(c1, bottleneck, 3)
    c3 = bn_act_conv(c2, nb_feature_maps, 1)

    input_features = K.int_shape(x)[1]
    if input_features != nb_feature_maps:
        x = Conv2D(nb_feature_maps, 1, padding="same")(x)
    add = Add()([x, c3])
    return add


def bottleneck_block(x, nb_feature_maps):
    bottleneck = nb_feature_maps // 4
    c1 = bn_conv(x, bottleneck, 1)
    c2 = bn_act_conv(c1, bottleneck, 3)
    c3 = bn_act_conv(c2, nb_feature_maps, 1)
    return c3
