"""
pyramidnet.py
author: Charles Authier (charles.authier@usherbrooke.ca)
date: 06/08/2018
Refactored:
    author: Thierry Judge (thierry.judge@usherbrooke.ca)
    date: 05/11/2018

This model is the implementation of a Feature Pyramid Networks for semantic image segmentation.
Adaptation of https://github.com/YudeWang/FPN_semantic_segmentation.
Reference: Tsung-Yi Lin, and al. 2016 (https://arxiv.org/abs/1612.03144).
Differences:
- Conv2D has been converted in a conv2D_bn_leakyrelu.
- The ReLu has been replaced by LeackyReLu.
- Bilinear upsampling replaces a nearest neighbor upsampling like in the paper.
"""

import cv2
import numpy as np
import tensorflow as tf
from keras.layers import (
    Input, Conv2D, Lambda, Add
)
from keras import regularizers
from keras.models import Model
from keras.utils import multi_gpu_model, Sequence
from keras.utils.generic_utils import to_list
from scipy.stats import mode
from sklearn.metrics import accuracy_score
from tqdm import tqdm

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.model.semanticsegmentation.layers_utils import (
    conv2D_bn_leakyrelu, three_conv_res_block_leakyrelu
)


class PyramidNetSequenceWrapper(Sequence):
    """ Wrap the sequence to return the ground truth with the same scale as the predictions.
        Note, "sequence" is structure guarantees that the network will only train once on each sample per
        epoch, use for "generator" function from keras.
    """

    def __init__(self, seq, dims):
        self.seq = seq
        self.dims = dims

    def __len__(self):
        """This method returns the number of elements in the sequence."""
        return len(self.seq)

    def __getitem__(self, idx):
        """This method reads and rescales the ground truth.

        Args:
            idx: int, Index of the sequence.

        Returns:
            array, Array with a size of (batch_size, height, width, channels).
            list, Out_gt from the scale of the three predictions, each scale is an array of size (
                batch_size, height, width, nb of class).
        """
        out_images, out_gt = self.seq[idx]
        out_gt_scale1 = out_gt
        out_gt_scale2 = np.ascontiguousarray(
            [cv2.resize(img, (self.dims[0], self.dims[0]), interpolation=cv2.INTER_NEAREST) for img in
             out_gt],
            dtype=np.float32
        )
        out_gt_scale3 = np.ascontiguousarray(
            [cv2.resize(img, (self.dims[1], self.dims[1]), interpolation=cv2.INTER_NEAREST) for img in
             out_gt],
            dtype=np.float32
        )
        out_gt_scale4 = np.ascontiguousarray(
            [cv2.resize(img, (self.dims[2], self.dims[2]), interpolation=cv2.INTER_NEAREST) for img in
             out_gt],
            dtype=np.float32
        )
        return out_images, [out_gt_scale1, out_gt_scale2, out_gt_scale3, out_gt_scale4]


class VITALPyramidNet(VITALabAiKerasAbstract):
    """Create a standard Pyramid semantic segmentation Model."""

    def __init__(self, dataset: VITALabAiDatasetAbstract, loss_fn=None, optimizer=None,
                 metrics=None, nb_feature_maps=32, alpha=1e-5, G=1):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            nb_feature_maps: int, number of feature maps for the convolution layers
            alpha: float, Alpha in the regularizer.
            G: int, number of gpus to use
        """
        self.nb_feature_maps = nb_feature_maps
        self.alpha = alpha
        self.G = G
        self.p1_dim, self.p2_dim, self.p3_dim, self.p4_dim = None, None, None, None
        loss_fn = [loss_fn, loss_fn, loss_fn, loss_fn]  # Four outputs, four losses
        super().__init__(dataset, loss_fn=loss_fn, optimizer=optimizer, metrics=metrics)

    def evaluate(self, best_class_fn=(lambda x, **kwargs: x), **kwargs):
        """ Evaluate model after training.

        Args:
            best_class_fn: function, applied after argmax to adjust segmentation classes
            **kwargs: additional parameters
        """

        def accuracy_test(seq):
            y_true = []
            all_outs = []
            for i in tqdm(range(len(seq))):
                x, y = seq[i]
                y_true.extend(y[0])
                outs = to_list(self.predict_on_batch(x))
                if not all_outs:
                    for out in outs:
                        all_outs.append([])
                for i, out in enumerate(outs):
                    all_outs[i].append(out)

            result = [np.concatenate(out) for out in all_outs]

            y_pred = []
            h, w = result[0].shape[1], result[0].shape[2]
            for i in range(result[0].shape[0]):
                p1 = best_class_fn(result[0][i].argmax(axis=-1).squeeze())
                p2 = best_class_fn(np.array([cv2.resize(result[1][i], (h, w),
                                                        interpolation=cv2.INTER_LINEAR_EXACT)]).argmax(
                    axis=-1).squeeze())
                p3 = best_class_fn(np.array([cv2.resize(result[2][i], (h, w),
                                                        interpolation=cv2.INTER_LINEAR_EXACT)]).argmax(
                    axis=-1).squeeze())
                p4 = best_class_fn(np.array([cv2.resize(result[3][i], (h, w),
                                                        interpolation=cv2.INTER_LINEAR_EXACT)]).argmax(
                    axis=-1).squeeze())

                pred = np.stack([p1, p2, p3, p4], axis=-1)
                pred = mode(pred, axis=-1)[0]
                y_pred.append(pred.squeeze())

            y_pred = np.array(y_pred)
            y_true = best_class_fn(np.array(y_true).argmax(axis=-1).squeeze())

            return accuracy_score(y_true.flatten(), y_pred.flatten())

        print("Testing on train set...")
        train_value = self.model.evaluate_generator(self.get_train_set(), verbose=1)
        print("Train : Loss: {}".format(train_value[0]))
        print("Training accuracy: {}".format(accuracy_test(self.get_train_set())))

        print("Testing on validation set...")
        val_value = self.model.evaluate_generator(self.get_validation_set(), verbose=1)
        print("Val : Loss: {}".format(val_value[0]))
        print("Validation accuracy: {}".format(accuracy_test(self.get_validation_set())))

        print("Testing on test set...")
        test_value = self.model.evaluate_generator(self.get_test_set(), verbose=1)
        print("Test : Loss: {}".format(test_value[0]))
        print("Testing accuracy: {}".format(accuracy_test(self.get_test_set())))

    def build_model(self):
        """Build PyramidNet as a Keras model.

        Returns:
           keras.Model object *not* compiled.
        """
        input_image = Input(
            shape=self.dataset.get_input_shape(), name="input_image"
        )

        # Working axis, axis on which to apply the batch normalization.
        working_axis = -1

        # Bottom-Up
        c1 = conv2D_bn_leakyrelu(
            input_image, self.nb_feature_maps, kernel_size=7, stride=2,
            bn_axis=working_axis, alpha=self.alpha
        )
        c2 = three_conv_res_block_leakyrelu(
            c1, self.nb_feature_maps, self.nb_feature_maps * 2,
            stride=1, bn_axis=working_axis, alpha=self.alpha
        )
        c3 = three_conv_res_block_leakyrelu(
            c2, self.nb_feature_maps * 2, self.nb_feature_maps * 4,
            stride=2, bn_axis=working_axis, alpha=self.alpha
        )
        c4 = three_conv_res_block_leakyrelu(
            c3, self.nb_feature_maps * 4, self.nb_feature_maps * 8,
            stride=2, bn_axis=working_axis, alpha=self.alpha
        )
        c5 = three_conv_res_block_leakyrelu(
            c4, self.nb_feature_maps * 8, self.nb_feature_maps * 8,
            stride=2, bn_axis=working_axis, alpha=self.alpha
        )

        # Top-down
        p5 = Conv2D(self.nb_feature_maps * 4, kernel_size=1, strides=1,
                    kernel_regularizer=regularizers.l2(self.alpha))(c5)
        p4 = Lambda(
            lambda image: tf.image.resize_bilinear(image, [c4.shape[1], c4.shape[2]])  # (image, H, W)
        )(p5)  # bilinear upsampling
        lateral1 = Conv2D(self.nb_feature_maps * 4, kernel_size=1, strides=1,
                          kernel_regularizer=regularizers.l2(self.alpha))(c4)
        p4 = Add()([p4, lateral1])
        p3 = Lambda(
            lambda image: tf.image.resize_bilinear(image, [c3.shape[1], c3.shape[2]])  # (image, H, W)
        )(p4)  # bilinear upsampling
        lateral2 = Conv2D(self.nb_feature_maps * 4, kernel_size=1, strides=1,
                          kernel_regularizer=regularizers.l2(self.alpha))(c3)
        p3 = Add()([p3, lateral2])
        p2 = Lambda(
            lambda image: tf.image.resize_bilinear(image, [c2.shape[1], c2.shape[2]])  # (image, H, W)
        )(p3)  # bilinear upsampling
        lateral3 = Conv2D(self.nb_feature_maps * 4, kernel_size=1, strides=1,
                          kernel_regularizer=regularizers.l2(self.alpha))(c2)
        p2 = Add()([p2, lateral3])
        p1 = Lambda(
            lambda image: tf.image.resize_bilinear(image,
                                                   [self.dataset.get_input_shape()[0],
                                                    self.dataset.get_input_shape()[1]]))(p2)  # (image, H, W)

        p4 = conv2D_bn_leakyrelu(
            p4, self.nb_feature_maps * 4, alpha=self.alpha, relu=False, bn=False,
        )
        p3 = conv2D_bn_leakyrelu(
            p3, self.nb_feature_maps * 4, alpha=self.alpha, relu=False, bn=False,
        )
        p2 = conv2D_bn_leakyrelu(
            p2, self.nb_feature_maps * 4, alpha=self.alpha, relu=False, bn=False,
        )
        p1 = conv2D_bn_leakyrelu(
            p1, self.nb_feature_maps * 4, alpha=self.alpha, relu=False, bn=False,
        )

        # Prediction
        out4 = Conv2D(self.dataset.get_num_classes(), (1, 1), activation='softmax', name="predict_4")(p4)
        out3 = Conv2D(self.dataset.get_num_classes(), (1, 1), activation='softmax', name="predict_3")(p3)
        out2 = Conv2D(self.dataset.get_num_classes(), (1, 1), activation='softmax', name="predict_2")(p2)
        out1 = Conv2D(self.dataset.get_num_classes(), (1, 1), activation='softmax', name="predict_1")(p1)
        # Output dimension
        self.p1_dim, self.p2_dim, self.p3_dim, self.p4_dim = (out1.shape[1], out2.shape[1], out3.shape[1],
                                                              out4.shape[1])

        # Model
        # check to see if we are compiling using just a single GPU
        if self.G <= 1:
            print("[INFO] training with 1 GPU...")
            self.model = Model(inputs=[input_image], outputs=[out1, out2, out3, out4])
        # otherwise, we are compiling using multiple GPUs
        else:
            print("[INFO] training with {} GPUs...".format(self.G))

            # we'll store a copy of the model on *every* GPU and then combine
            # the results from the gradient updates on the CPU
            with tf.device("/cpu:0"):
                # initialize the model
                self.model = Model(inputs=[input_image], outputs=[out1, out2, out3, out4])
            # make the model parallel
            self.model = multi_gpu_model(self.model, gpus=self.G)

        return self.model

    def get_train_set(self):
        return PyramidNetSequenceWrapper(self.dataset.get_train_set(),
                                         [self.p2_dim, self.p3_dim, self.p4_dim])

    def get_validation_set(self):
        return PyramidNetSequenceWrapper(self.dataset.get_validation_set(),
                                         [self.p2_dim, self.p3_dim, self.p4_dim])

    def get_test_set(self):
        return PyramidNetSequenceWrapper(self.dataset.get_test_set(),
                                         [self.p2_dim, self.p3_dim, self.p4_dim])

    def get_dims(self):
        return self.p1_dim, self.p2_dim, self.p3_dim, self.p4_dim

    def save(self):
        """Save the model.
        Because of Lambda layers, the regular save model method does not work."""
        return self.model.save_weights('{}.h5'.format(self.__class__.__name__))


if __name__ == '__main__':
    from VITALabAI.dataset.semanticsegmentation.isprs.isprs import VITALIsprs
    import argparse
    from keras.callbacks import CSVLogger
    import keras
    from VITALabAI.model.semanticsegmentation.losses import *

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the dataset", type=str)
    aparser.add_argument("--weight", type=str, dest="weight", help="Weights h5 file", default=None)
    args = aparser.parse_args()

    ds = VITALIsprs(path=args.path, batch_size=32)
    optim = keras.optimizers.Adam(lr=0.0001)

    losses = dice_crossentropy_loss

    model = VITALPyramidNet(ds, loss_fn=losses, optimizer=optim, metrics=['accuracy'])

    if args.weight is not None:
        print("Loading weights...")
        model.load_weights(args.weight)
        print("Done.")

    model.train(epochs=20, callbacks=[CSVLogger('pyramidnet.csv')], workers=1)
    model.evaluate(best_class_fn=(lambda x: x + 1))  # +1 because no class 0 in ISPRS
    model.save()
