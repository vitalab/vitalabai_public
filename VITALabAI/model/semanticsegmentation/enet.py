from keras import Input, Model
from keras import regularizers
from keras.layers import (
    Add, BatchNormalization, Conv2D, Conv2DTranspose, SpatialDropout2D,
    concatenate, MaxPooling2D, ZeroPadding2D, UpSampling2D, Permute,
    Activation)
from keras.layers.advanced_activations import PReLU

from VITALabAI.utils.layers import MaxPoolingWithArgmax2D, MaxUnpooling2D


class VITALENet:
    """Create a standard ENet semantic segmentation Model.

    This is inspired by the ENet work:
    https://arxiv.org/abs/1606.02147
    """

    @classmethod
    def network(cls, input_shape, num_classes, nb_feature_maps,
                encoder_relu=True, decoder_relu=True,
                l2reg=1e-4, dropout=0.1):
        """Build ENet as a Keras model.

        Returns:
           keras.Model object *not* compiled.
        """
        input_image = Input(input_shape, name="input_image")

        # E-Net encoder
        x, max_indices = cls.enet_encoder(input_image, input_shape, nb_feature_maps, encoder_relu, l2reg, dropout)

        # E-Net decoder
        out = cls.enet_decoder(x, num_classes, nb_feature_maps, max_indices, decoder_relu, l2reg, dropout)

        return Model(inputs=[input_image], outputs=[out])

    @classmethod
    def conv_bn_act(cls, x, nb_feature_maps, kernel_size, strides=(1, 1),
                    padding='valid', dilation=(1, 1), bias=True, relu=True, l2reg=0.0):
        """ Utility function used for ENet blocks combining a convolution, a BatchNorm and an Activation function.

        Args:
            x: input
            nb_feature_maps: the number of output channels.
            kernel_size: the kernel size of the filters used in the convolution layer.
            strides: An integer or tuple/list of a single integer,
                specifying the stride length of the convolution. Default: (1, 1)
            padding: type of padding used for the convolution. Default: "valid".
            dilation: spacing between kernel elements for the convolution described
                in item 2 of the extension branch. Default: 1.
            bias: Adds a learnable bias to the output if ``True``. Default: False.
            relu: When ``True`` ReLU is used as the activation function; otherwise, PReLU is used. Default: True.
            l2reg: L2 Regularization coefficient. Default: 0 ( no regularization ).

        Returns:
            Output of applying convolution, batch normalization and an activation function on x.
        """
        x = Conv2D(nb_feature_maps, kernel_size=kernel_size, strides=strides, padding=padding,
                   dilation_rate=dilation, use_bias=bias, kernel_regularizer=regularizers.l2(l2reg))(x)
        x = BatchNormalization(momentum=0.1)(x)
        x = Activation("relu")(x) if relu else PReLU()(x)
        return x

    @classmethod
    def initial_block(cls, x, nb_feature_maps, nb_input_channels, kernel_size=3, padding="same", bias=False,
                      relu=True, l2reg=0.0):
        """ The initial block is composed of two branches:
            1. a main branch which performs a regular convolution with stride 2.
            2. an extension branch which performs max-pooling.
            Doing both operations in parallel and concatenating their results
            allows for efficient downsampling and expansion. The main branch
            outputs nb_feature_maps feature maps while the extension branch outputs nb_input_channels, for a
            total of nb_feature_maps+nb_input_channels feature maps after concatenation.

        Args:
            x: input.
            nb_feature_maps: the number output channels.
            nb_input_channels: the number of input channels.
            kernel_size: the kernel size of the filters used in the convolution layer. Default: 3.
            padding: type of padding used for the convolution. Default: "same".
            bias: Adds a learnable bias to the output if ``True``. Default: False.
            relu: When ``True`` ReLU is used as the activation function; otherwise, PReLU is used. Default: True.
            l2reg: L2 Regularization coefficient. Default: 0 ( no regularization ).

        Returns:
            Initial block of ENet applied to x.
        """
        # Main branch - As stated above the number of output channels for this
        # branch is the total minus the number of channels in the input, since the remaining channels come from
        # the extension branch
        main_branch = Conv2D(nb_feature_maps - nb_input_channels, kernel_size=kernel_size, strides=2, padding=padding,
                             use_bias=bias, kernel_regularizer=regularizers.l2(l2reg))(x)

        # Extension branch
        ext_branch = MaxPooling2D(pool_size=kernel_size, strides=2, padding=padding)(x)

        # Concatenate branches
        cat = concatenate([main_branch, ext_branch], axis=-1)

        # Apply batch normalization
        norm = BatchNormalization(momentum=0.1)(cat)

        act = Activation("relu")(norm) if relu else PReLU()(norm)

        return act

    @classmethod
    def regular_bottleneck(cls, x, nb_feature_maps, internal_ratio=4, kernel_size=3, padding="same", dilation=1,
                           asymmetric=False, dropout_prob=0., bias=False, relu=True, l2reg=0.0):
        """ Regular bottlenecks are the main building block of ENet.
        Main branch:
            1. Shortcut connection.
        Extension branch:
            1. 1x1 convolution which decreases the number of channels by
               ``internal_ratio``, also called a projection;
            2. regular, dilated or asymmetric convolution;
            3. 1x1 convolution which increases the number of channels back to
               ``channels``, also called an expansion;
            4. dropout as a regularizer.

        Args:
            x: input.
            nb_feature_maps: the number of output channels.
            internal_ratio: a scale factor applied to ``channels`` used to compute the number of channels
                after the projection.
                eg. given ``channels`` equal to 128 and internal_ratio equal to 2
                the number of channels after the projection is 64. Default: 4.
            kernel_size: the kernel size of the filters used in the convolution layer described above
            in item 2 of the extension branch. Default: 3.
            padding: type of padding used for the convolution. Default: "same".
            dilation: spacing between kernel elements for the convolution described
                in item 2 of the extension branch. Default: 1.
            asymmetric: flags if the convolution described in item 2 of the extension branch
                is asymmetric or not. Default: False.
            dropout_prob: probability of an element to be zeroed. Default: 0 (no dropout).
            bias: Adds a learnable bias to the output if ``True``. Default: False.
            relu: When ``True`` ReLU is used as the activation function; otherwise, PReLU is used. Default: True.
            l2reg: L2 Regularization coefficient. Default: 0 ( no regularization ).

        Returns:
            Regular bottleneck of ENet applied to x.

        Raises:
            Runtime error when internal_ratio is not valid i.e outside [1, nb_feature_maps]
        """
        # Check in the internal_scale parameter is within the expected range
        # [1, channels]
        if internal_ratio <= 1 or internal_ratio > nb_feature_maps:
            raise RuntimeError(
                "Value out of range. Expected value in the "
                "interval [1, {0}], got internal_scale={1}."
                    .format(nb_feature_maps, internal_ratio)
            )

        internal_channels = nb_feature_maps // internal_ratio

        main = x

        conv1 = cls.conv_bn_act(x, internal_channels, kernel_size=1, strides=1, bias=bias, relu=relu, l2reg=l2reg)

        if asymmetric:
            if type(kernel_size) != int:
                raise RuntimeError(
                    "When asymmetric convolution, the kernel_size should be of type int"
                    "got {0}".format(type(kernel_size))
                )
            conv2 = cls.conv_bn_act(conv1, internal_channels, kernel_size=(kernel_size, 1), strides=1, padding=padding,
                                    dilation=dilation, bias=bias, relu=relu, l2reg=l2reg)
            conv2 = cls.conv_bn_act(conv2, internal_channels, kernel_size=(1, kernel_size), strides=1, padding=padding,
                                    dilation=dilation, bias=bias, relu=relu, l2reg=l2reg)
        else:
            conv2 = cls.conv_bn_act(conv1, internal_channels, kernel_size=kernel_size, strides=1, padding=padding,
                                    dilation=dilation, bias=bias, relu=relu, l2reg=l2reg)

        conv3 = cls.conv_bn_act(conv2, nb_feature_maps, kernel_size=1, strides=1, bias=bias, relu=relu, l2reg=l2reg)

        drop = SpatialDropout2D(dropout_prob)(conv3)

        out = Add()([main, drop])

        act = Activation("relu")(out) if relu else PReLU()(out)
        return act

    @classmethod
    def down_bottleneck(cls, x, nb_feature_maps, internal_ratio=4, kernel_size=3, padding="same",
                        return_indices=False, dropout_prob=0., bias=False, relu=True, l2reg=0.0):
        """ Downsampling bottlenecks further downsample the feature map size.
        Main branch:
            1. max pooling with stride 2; indices are saved to be used for
               unpooling later.
        Extension branch:
            1. 2x2 convolution with stride 2 that decreases the number of channels
               by ``internal_ratio``, also called a projection;
            2. regular convolution (by default, 3x3);
            3. 1x1 convolution which increases the number of channels to
               ``out_channels``, also called an expansion;
            4. dropout as a regularizer.

        Args:
            x: input.
            nb_feature_maps: the number of output channels.
            internal_ratio: a scale factor applied to ``channels`` used to compute the number of channels
                after the projection.
                eg. given ``channels`` equal to 128 and internal_ratio equal to 2
                the number of channels after the projection is 64. Default: 4.
            kernel_size: the kernel size of the filters used in the convolution layer described above
            in item 2 of the extension branch. Default: 3.
            padding: type of padding used for the convolution. Default: "same".
            return_indices: if ``True``, will return the max indices along with the outputs. Useful when unpooling
                later.
            dropout_prob: probability of an element to be zeroed. Default: 0 (no dropout).
            bias: Adds a learnable bias to the output if ``True``. Default: False.
            relu: When ``True`` ReLU is used as the activation function; otherwise, PReLU is used. Default: True.
            l2reg: L2 Regularization coefficient. Default: 0 ( no regularization ).

        Returns:
            Downsampling bottleneck of ENet applied to x.

        Raises:
            Runtime error when internal_ratio is not valid i.e outside [1, nb_feature_maps]
        """
        # Check in the internal_scale parameter is within the expected range
        # [1, channels]
        if internal_ratio <= 1 or internal_ratio > nb_feature_maps:
            raise RuntimeError(
                "Value out of range. Expected value in the "
                "interval [1, {0}], got internal_scale={1}."
                    .format(nb_feature_maps, internal_ratio)
            )

        internal_channels = nb_feature_maps // internal_ratio

        if return_indices:
            main_max1, max_indices = MaxPoolingWithArgmax2D(pool_size=kernel_size, strides=2, padding=padding)(x)
        else:
            main_max1 = MaxPooling2D(pool_size=kernel_size, strides=2, padding=padding)(x)

        ext_conv1 = cls.conv_bn_act(x, internal_channels, kernel_size=2, strides=2, bias=bias, relu=relu, l2reg=l2reg)
        ext_conv2 = cls.conv_bn_act(ext_conv1, internal_channels, kernel_size=kernel_size, strides=1,
                                    padding=padding, bias=bias, relu=relu, l2reg=l2reg)
        ext_conv3 = cls.conv_bn_act(ext_conv2, nb_feature_maps, kernel_size=1, strides=1,
                                    bias=bias, relu=relu, l2reg=l2reg)
        drop = SpatialDropout2D(dropout_prob)(ext_conv3)

        # Compute channel padding
        ext_shape = drop.get_shape().as_list()
        main_shape = main_max1.get_shape().as_list()
        ch_padding = ext_shape[-1] - main_shape[-1]

        # Permute to be able to pad channels
        main_padded = Permute((1, 3, 2))(main_max1)
        main_padded = ZeroPadding2D(padding=((0, 0), (0, ch_padding)))(main_padded)
        main_padded = Permute((1, 3, 2))(main_padded)

        out = Add()([main_padded, drop])
        act = Activation("relu")(out) if relu else PReLU()(out)
        return (act, max_indices) if return_indices else act

    @classmethod
    def up_bottleneck(cls, x, nb_feature_maps, internal_ratio=4, kernel_size=3, padding="same", dropout_prob=0.,
                      bias=False, relu=True, max_indices=None, l2reg=0.0):
        """ The upsampling bottleneck upsamples the feature map resolution using max
        pooling indices stored from the corresponding downsampling bottleneck.

        Main branch:
            1. 1x1 convolution with stride 1 that decreases the number of channels
               by ``internal_ratio``, also called a projection;
            2. max unpool layer using the max pool indices from the corresponding
               downsampling max pool layer.
        Extension branch:
            1. 1x1 convolution with stride 1 that decreases the number of channels
               by ``internal_ratio``, also called a projection;
            2. transposed convolution (by default, 3x3);
            3. 1x1 convolution which increases the number of channels to
               ``out_channels``, also called an expansion;
            4. dropout as a regularizer.

        Args:
            x: input.
            nb_feature_maps: the number of output channels.
            internal_ratio: a scale factor applied to ``channels`` used to compute the number of channels
                after the projection.
                eg. given ``channels`` equal to 128 and internal_ratio equal to 2
                the number of channels after the projection is 64. Default: 4.
            kernel_size: the kernel size of the filters used in the convolution layer described above
            in item 2 of the extension branch. Default: 3.
            padding: type of padding used for the convolution. Default: "same".
            dropout_prob: probability of an element to be zeroed. Default: 0 (no dropout).
            bias: Adds a learnable bias to the output if ``True``. Default: False.
            relu: When ``True`` ReLU is used as the activation function; otherwise, PReLU is used. Default: True.
            max_indices: the max indicies returned from the bottleneck block. Default: None (no MaxUnpooling).
            l2reg: L2 Regularization coefficient. Default: 0 ( no regularization ).

        Returns:
            Upsampling bottleneck of ENet applied to x.

        Raises:
            Runtime error when internal_ratio is not valid i.e outside [1, nb_feature_maps]
        """
        # Check in the internal_scale parameter is within the expected range
        # [1, channels]
        if internal_ratio <= 1 or internal_ratio > nb_feature_maps:
            raise RuntimeError(
                "Value out of range. Expected value in "
                "the interval [1, {0}], got internal_scale={1}. "
                    .format(nb_feature_maps, internal_ratio)
            )

        internal_channels = nb_feature_maps // internal_ratio

        main_conv1 = Conv2D(nb_feature_maps, kernel_size=1, padding=padding,
                            use_bias=bias, kernel_regularizer=regularizers.l2(l2reg))(x)
        main_conv1 = BatchNormalization(momentum=0.1)(main_conv1)

        if max_indices is not None:
            main_unpool = MaxUnpooling2D(size=2)([main_conv1, max_indices])
        else:
            main_unpool = UpSampling2D(size=2)(main_conv1)

        ext_conv1 = cls.conv_bn_act(x, internal_channels, kernel_size=1, bias=bias, relu=relu, padding=padding)
        ext_conv2 = Conv2DTranspose(internal_channels, kernel_size=kernel_size, strides=2, padding=padding,
                                    output_padding=1, use_bias=bias, kernel_regularizer=regularizers.l2(l2reg))(
            ext_conv1)
        ext_conv2 = BatchNormalization(momentum=0.1)(ext_conv2)
        ext_conv2 = Activation("relu")(ext_conv2) if relu else PReLU()(ext_conv2)

        ext_conv3 = cls.conv_bn_act(ext_conv2, nb_feature_maps, kernel_size=1, bias=bias, relu=relu,
                                    padding=padding, l2reg=l2reg)
        drop = SpatialDropout2D(dropout_prob)(ext_conv3)

        out = Add()([main_unpool, drop])
        act = Activation("relu")(out) if relu else PReLU()(out)
        return act

    @classmethod
    def enet_encoder(cls, x, input_shape, nb_feature_maps, encoder_relu=True, l2reg=1e-4, dropout=0.1):
        """ Block making up the encoder half of a E-Net.
        Args:
            x: tensor, input image or feature maps to down-sample.
            input_shape: tuple, shape of the input tensor for each axis.
            nb_feature_maps: int, number of feature maps at beginning of the network, automatic scaling for the
                            following layers.
            encoder_relu: bool, When ``True`` ReLU is used as the activation function; otherwise, PReLU is used.
                          Default: True.
            l2reg: float, alpha in the regularizer.
            dropout: float, probability of an element to be zeroed. Default: 0 (no dropout).
        Returns:
            tuple of:
                x: tensor, feature maps downsampled to the point of reaching the bottleneck of the network.
                max_indices: list, the max indices returned from the 2 down bottleneck blocks.
        """

        x = cls.initial_block(x, nb_feature_maps, nb_input_channels=input_shape[-1], relu=encoder_relu,
                              l2reg=l2reg)

        # Stage 1 - Encoder
        x, max_indices1_0 = cls.down_bottleneck(x, nb_feature_maps * 2, dropout_prob=0.1 * dropout,
                                                relu=encoder_relu, l2reg=l2reg, return_indices=True)
        for i in range(4):
            x = cls.regular_bottleneck(x, nb_feature_maps * 2, dropout_prob=0.1 * dropout, relu=encoder_relu,
                                       l2reg=l2reg)

        # Stage 2 - Encoder
        x, max_indices2_0 = cls.down_bottleneck(x, nb_feature_maps * 4,
                                                dropout_prob=0.1 * dropout,
                                                relu=encoder_relu, l2reg=l2reg, return_indices=True)
        for i in range(2):
            x = cls.regular_bottleneck(x, nb_feature_maps * 4, dropout_prob=dropout,
                                       relu=encoder_relu, l2reg=l2reg)
            x = cls.regular_bottleneck(x, nb_feature_maps * 4, dilation=2 * 4 ** i, dropout_prob=dropout,
                                       relu=encoder_relu, l2reg=l2reg)
            x = cls.regular_bottleneck(x, nb_feature_maps * 4, kernel_size=5, asymmetric=True,
                                       dropout_prob=dropout, relu=encoder_relu, l2reg=l2reg)
            x = cls.regular_bottleneck(x, nb_feature_maps * 4, dilation=4 * 4 ** i, dropout_prob=dropout,
                                       relu=encoder_relu, l2reg=l2reg)

        # Stage 3 - Encoder
        for i in range(2):
            x = cls.regular_bottleneck(x, nb_feature_maps * 4, dropout_prob=dropout,
                                       relu=encoder_relu, l2reg=l2reg)
            x = cls.regular_bottleneck(x, nb_feature_maps * 4, dilation=2 * 4 ** i, dropout_prob=dropout,
                                       relu=encoder_relu, l2reg=l2reg)
            x = cls.regular_bottleneck(x, nb_feature_maps * 4, kernel_size=5, asymmetric=True,
                                       dropout_prob=dropout, relu=encoder_relu, l2reg=l2reg)
            x = cls.regular_bottleneck(x, nb_feature_maps * 4, dilation=4 * 4 ** i, dropout_prob=dropout,
                                       relu=encoder_relu, l2reg=l2reg)

        return x, [max_indices1_0, max_indices2_0]

    @classmethod
    def enet_decoder(cls, x, num_classes, nb_feature_maps, max_indices, decoder_relu=True, l2reg=1e-4, dropout=0.1,
                     name: str = None):
        """ Block making up the decoder half of a E-Net.
        Args:
            x: tensor, the encoded sample given as input to this block.
            num_classes: int, number of classes and channels to output.
            nb_feature_maps: int, factor used to compute the number of feature maps for the convolution layers.
            max_indices: list, the max indices returned from the bottleneck block
            decoder_relu: bool, when ``True`` ReLU is used as the activation function; otherwise, PReLU is used.
                Default: True.
            l2reg: float, alpha in the regularizer.
            dropout: float, probability of an element to be zeroed. Default: 0 (no dropout).
            name: str, name of the output layer (useful to link loss to a specific output in a multi-output model).
       Returns:
           tensor, ENet's segmentation
        """
        # Stage 4 - Decoder
        x = cls.up_bottleneck(x, nb_feature_maps * 2, dropout_prob=dropout,
                              relu=decoder_relu, l2reg=l2reg, max_indices=max_indices[1])
        x = cls.regular_bottleneck(x, nb_feature_maps * 2, dropout_prob=dropout,
                                   relu=decoder_relu, l2reg=l2reg)
        x = cls.regular_bottleneck(x, nb_feature_maps * 2, dropout_prob=dropout,
                                   relu=decoder_relu, l2reg=l2reg)
        # Stage 5 - Decoder
        x = cls.up_bottleneck(x, nb_feature_maps, dropout_prob=dropout,
                              relu=decoder_relu, l2reg=l2reg, max_indices=max_indices[0])
        x = cls.regular_bottleneck(x, nb_feature_maps, dropout_prob=dropout,
                                   relu=decoder_relu, l2reg=l2reg)
        # Output head
        out = Conv2DTranspose(num_classes, kernel_size=3, strides=2, padding='same', use_bias=False,
                              activation='softmax', name=name)(x)
        return out
