"""
convdeconv.py
author: Charles Authier (charles.authier@usherbrooke.ca)
date: 03/08/2018
Refactored:
    author: Thierry Judge (thierry.judge@usherbrooke.ca)
    date: 02/11/2018


This model is strongly inspired by the Fully Convolutional Networks (ConvDeconv).
Reference: Jonathan Long and al. 2014 (https://arxiv.org/abs/1411.4038).
Differences:
- Conv2D has been converted in a conv2D_bn_leakyrelu.
- Conv2DTranspose has been converted in a conv2Dtranspose_leakyrelu_bn.
- The ReLu is replaced by LeackyReLu.
"""

import tensorflow as tf
from keras.layers import (
    Input, Conv2D, MaxPooling2D
)
from keras.models import Model
from keras.utils import multi_gpu_model

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.model.semanticsegmentation.layers_utils import (
    conv2D_bn_leakyrelu, conv2Dtranspose_leakyrelu_bn
)


class VITALConvDeconv(VITALabAiKerasAbstract):
    """Create a standard ConvDeconv semantic segmentation model."""
    def __init__(self, dataset: VITALabAiDatasetAbstract, loss_fn=None, optimizer=None,
                 metrics=None, nb_feature_maps=32, alpha=1e-5, G=1):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            nb_feature_maps: int, Number of feature maps at beginning of the network, automatic scaling for
                            the following layers.
            alpha: float, Alpha in the regularizer
            G: int, Number of GPU to run on.
        """
        self.nb_feature_maps = nb_feature_maps
        self.alpha = alpha
        self.G = G
        super().__init__(dataset, loss_fn=loss_fn, optimizer=optimizer, metrics=metrics)

    def evaluate(self, **kwargs):
        """ Evaluate model after training.

        Args:
            **kwargs: additional parameters
        """
        print("Testing on train set...")
        train_value = self.model.evaluate_generator(self.dataset.get_train_set(), verbose=1)
        print("Train : Loss: {}, Accuracy: {}".format(train_value[0], train_value[1]))
        print("Testing on validation set...")
        val_value = self.model.evaluate_generator(self.dataset.get_validation_set(), verbose=1)
        print("Val : Loss: {}, Accuracy: {}".format(val_value[0], val_value[1]))
        print("Testing on test set...")
        test_value = self.model.evaluate_generator(self.dataset.get_test_set(), verbose=1)
        print("Test : Loss: {}, Accuracy: {}".format(test_value[0], test_value[1]))

    def build_model(self):
        """Build ConvDeconv as a Keras model

        Returns:
           keras.Model object *not* compiled.
        """
        # Init input
        input_image = Input(self.dataset.get_input_shape(), name="input_image")

        # Working axis, axis on which to apply the batch normalization.
        working_axis = -1

        # first layer
        conv1 = conv2D_bn_leakyrelu(input_image, self.nb_feature_maps, working_axis, alpha=self.alpha)
        conv1 = conv2D_bn_leakyrelu(conv1, self.nb_feature_maps, working_axis, alpha=self.alpha)
        pool1 = MaxPooling2D()(conv1)
        # second layer
        conv2 = conv2D_bn_leakyrelu(pool1, self.nb_feature_maps * 2, working_axis, alpha=self.alpha)
        conv2 = conv2D_bn_leakyrelu(conv2, self.nb_feature_maps * 2, working_axis, alpha=self.alpha)
        pool2 = MaxPooling2D()(conv2)
        # third layer
        conv3 = conv2D_bn_leakyrelu(pool2, self.nb_feature_maps * 4, working_axis, alpha=self.alpha)
        conv3 = conv2D_bn_leakyrelu(conv3, self.nb_feature_maps * 4, working_axis, alpha=self.alpha)
        pool3 = MaxPooling2D()(conv3)
        # fourth layer
        conv4 = conv2D_bn_leakyrelu(pool3, self.nb_feature_maps * 8, working_axis, alpha=self.alpha)
        conv4 = conv2D_bn_leakyrelu(conv4, self.nb_feature_maps * 8, working_axis, alpha=self.alpha)
        pool4 = MaxPooling2D()(conv4)
        # fifth layer
        conv5 = conv2D_bn_leakyrelu(pool4, self.nb_feature_maps * 16, working_axis, alpha=self.alpha)
        conv5 = conv2D_bn_leakyrelu(conv5, self.nb_feature_maps * 16, working_axis, alpha=self.alpha)
        # Up sampling - fifth layer
        up6 = conv2Dtranspose_leakyrelu_bn(conv5, self.nb_feature_maps * 8, working_axis, alpha=self.alpha)
        conv6 = conv2D_bn_leakyrelu(up6, self.nb_feature_maps * 8, working_axis, alpha=self.alpha)
        conv6 = conv2D_bn_leakyrelu(conv6, self.nb_feature_maps * 8, working_axis, alpha=self.alpha)
        # Up sampling - fourth layer
        up7 = conv2Dtranspose_leakyrelu_bn(conv6, self.nb_feature_maps * 4, working_axis, alpha=self.alpha)
        conv7 = conv2D_bn_leakyrelu(up7, self.nb_feature_maps * 4, working_axis, alpha=self.alpha)
        conv7 = conv2D_bn_leakyrelu(conv7, self.nb_feature_maps * 4, working_axis, alpha=self.alpha)
        # Up sampling - third layer
        up8 = conv2Dtranspose_leakyrelu_bn(conv7, self.nb_feature_maps * 2, working_axis, alpha=self.alpha)
        conv8 = conv2D_bn_leakyrelu(up8, self.nb_feature_maps * 2, working_axis, alpha=self.alpha)
        conv8 = conv2D_bn_leakyrelu(conv8, self.nb_feature_maps * 2, working_axis, alpha=self.alpha)
        # Up sampling - second layer
        up9 = conv2Dtranspose_leakyrelu_bn(conv8, self.nb_feature_maps, working_axis, alpha=self.alpha)
        conv9 = conv2D_bn_leakyrelu(up9, self.nb_feature_maps, working_axis, alpha=self.alpha)
        conv9 = conv2D_bn_leakyrelu(conv9, self.nb_feature_maps, working_axis, alpha=self.alpha)
        # output layer
        out = Conv2D(self.dataset.get_num_classes(), (1, 1), activation='softmax', name="final_output")(conv9)

        # Model
        # check to see if we are compiling using just a single GPU
        if self.G <= 1:
            print("[INFO] training with 1 GPU...")
            model = Model(inputs=[input_image], outputs=[out])
        # otherwise, we are compiling using multiple GPUs
        else:
            print("[INFO] training with {} GPUs...".format(self.G))
            # we'll store a copy of the model on *every* GPU and then combine
            # the results from the gradient updates on the CPU
            with tf.device("/cpu:0"):
                # initialize the model
                model = Model(inputs=[input_image], outputs=[out])
            # make the model parallel
            model = multi_gpu_model(model, gpus=self.G)

        return model


if __name__ == '__main__':
    from VITALabAI.dataset.semanticsegmentation.isprs.isprs import VITALIsprs
    import argparse
    from keras.callbacks import CSVLogger
    import keras
    from VITALabAI.model.semanticsegmentation.losses import *

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the dataset", type=str)
    aparser.add_argument("--weight", type=str, dest="weight", help="Weights h5 file", default=None)
    args = aparser.parse_args()

    ds = VITALIsprs(path=args.path, batch_size=32)
    optim = keras.optimizers.Adam(lr=0.0001)

    losses = dice_crossentropy_loss

    model = VITALConvDeconv(ds, loss_fn=losses, optimizer=optim, metrics=['accuracy'])

    if args.weight is not None:
        print("Loading weights...")
        model.load_weights(args.weight)
        print("Done.")

    model.train(epochs=20, callbacks=[CSVLogger('convdeconv.csv')], workers=1)
    model.evaluate()
