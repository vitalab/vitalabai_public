"""
tiramisu.py
author: Charles Authier (charles.authier@usherbrooke.ca)
date: 06/08/2018
Refactored:
    author: Thierry Judge (thierry.judge@usherbrooke.ca)
    date: 05/11/2018

This model is the implementation of an FC-DenseNets name TiramisuNet
The One Hundred Layers Tiramisu: Fully Convolutional DenseNets for Semantic Segmentation.
Reference: Simon Jegou and al. 2016 (https://arxiv.org/abs/1611.09326).
Differences:
- The ReLu has been replaced by LeackyReLu.
"""

import tensorflow as tf
from keras.layers import (
    Input, Conv2D, concatenate
)
from keras.models import Model
from keras.utils import multi_gpu_model

from VITALabAI import VITALabAiDatasetAbstract
from VITALabAI.VITALabAiKerasAbstract import VITALabAiKerasAbstract
from VITALabAI.model.semanticsegmentation.layers_utils import (
    conv2D_bn_leakyrelu, dense_block, trans_up, trans_down
)


class VITALTiramisu(VITALabAiKerasAbstract):
    """Create a standard Tiramisu semantic segmentation Model."""

    def __init__(self, dataset: VITALabAiDatasetAbstract, loss_fn=None, optimizer=None,
                 metrics=None, nb_feature_maps=32, alpha=1e-5, G=1, down_blocks=None, up_blocks=None,
                 dropout_prob=0.2):
        """
        Args:
            dataset: VITALabAiDatasetAbstract, the dataset to use.
            loss_fn: the objective function.
            optimizer: Optimizer to use.
            metrics: List of metrics to monitor.
            nb_feature_maps: int, number of feature maps for the convolution layers
            alpha: float, Alpha in the regularizer.
            G: int, number of gpus to use
            down_blocks: list of down blocks
            up_blocks: list of up blocks
            dropout_prob: float, dropout probability
        """

        self.up_blocks = [12, 10, 7, 5, 4] if up_blocks is None else up_blocks
        self.down_blocks = [4, 5, 7, 10, 12, 15] if down_blocks is None else down_blocks
        self.dropout_prob = dropout_prob

        self.nb_feature_maps = nb_feature_maps
        self.alpha = alpha
        self.G = G
        super().__init__(dataset, loss_fn=loss_fn, optimizer=optimizer, metrics=metrics)

    def evaluate(self, **kwargs):
        """ Evaluate model after training.

        Args:
            **kwargs: additional parameters
        """
        print("Testing on train set...")
        train_value = self.model.evaluate_generator(self.dataset.get_train_set(),
                                                    verbose=1)
        print("Train : Loss: {}, Accuracy: {}".format(train_value[0], train_value[1]))
        print("Testing on validation set...")
        val_value = self.model.evaluate_generator(self.dataset.get_validation_set(),
                                                  verbose=1)
        print("Val : Loss: {}, Accuracy: {}".format(val_value[0], val_value[1]))
        print("Testing on test set...")
        test_value = self.model.evaluate_generator(self.dataset.get_test_set(),
                                                   verbose=1)
        print("Test : Loss: {}, Accuracy: {}".format(test_value[0], test_value[1]))

    def build_model(self):
        """Build Tiramisu as a Keras model.

        Returns:
           keras.Model object *not* compiled.
        """
        # Init input
        input_image = Input(
            self.dataset.get_input_shape(), name="input_image"
        )
        conv_size = self.dataset.get_input_shape()[2] * self.nb_feature_maps
        nb_filters = self.dataset.get_input_shape()[2] * self.nb_feature_maps
        skips = []  # Skip connection
        block_idx = 0

        # Working axis, axis on which to apply the batch normalization.
        working_axis = -1

        # first layer
        conv = conv2D_bn_leakyrelu(
            input_image, nb_filters, working_axis, alpha=self.alpha, relu=False, bn=False
        )
        # The layers - down
        for block_idx, nb_layers in enumerate(self.down_blocks):
            block_output, block_size = dense_block(
                conv, nb_layers, self.nb_feature_maps, working_axis, self.dropout_prob, self.alpha
            )
            if block_idx < len(self.down_blocks) - 1:
                conv = concatenate([conv, block_output])
                conv_size += block_size
                skips.append(conv)
                conv = trans_down(
                    conv, self.dataset.get_num_classes(), working_axis, self.dropout_prob, self.alpha
                )
            else:
                conv = block_output
                conv_size = block_size
        # The layers - up
        for nb_layers, skip in zip(self.up_blocks, reversed(skips)):
            block_idx += 1
            conv = trans_up(conv, conv_size, self.alpha)
            conv = concatenate([conv, skip])
            conv, block_size = dense_block(
                conv, nb_layers, self.nb_feature_maps, working_axis, self.dropout_prob, self.alpha
            )
            conv_size = block_size

        out = Conv2D(self.dataset.get_num_classes(), (1, 1), activation='softmax')(conv)

        # Model
        # check to see if we are compiling using just a single GPU
        if self.G <= 1:
            print("[INFO] training with 1 GPU...")
            self.model = Model(inputs=[input_image], outputs=[out])
        # otherwise, we are compiling using multiple GPUs
        else:
            print("[INFO] training with {} GPUs...".format(self.G))
            # we'll store a copy of the model on *every* GPU and then combine
            # the results from the gradient updates on the CPU
            with tf.device("/cpu:0"):
                # initialize the model
                self.model = Model(inputs=[input_image], outputs=[out])
            # make the model parallel
            self.model = multi_gpu_model(self.model, gpus=self.G)

        return self.model


if __name__ == '__main__':
    from VITALabAI.dataset.semanticsegmentation.isprs.isprs import VITALIsprs
    import argparse
    from keras.callbacks import CSVLogger
    import keras
    from VITALabAI.model.semanticsegmentation.losses import *

    aparser = argparse.ArgumentParser()
    aparser.add_argument("--path", help="Path to the dataset", type=str)
    aparser.add_argument("--weight", type=str, dest="weight", help="Weights h5 file", default=None)
    args = aparser.parse_args()

    ds = VITALIsprs(path=args.path, batch_size=3)
    optim = keras.optimizers.Adam(lr=0.0001)

    losses = dice_crossentropy_loss

    model = VITALTiramisu(ds, loss_fn=losses, optimizer=optim, metrics=['accuracy'])

    if args.weight is not None:
        print("Loading weights...")
        model.load_weights(args.weight)
        print("Done.")

    model.train(epochs=20, callbacks=[CSVLogger('tiramisu.csv')], workers=1)
    model.evaluate()
