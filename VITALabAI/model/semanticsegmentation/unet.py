"""
unet.py
author: Charles Authier (charles.authier@usherbrooke.ca)
date: 03/08/2018
Refactored:
    author: Thierry Judge (thierry.judge@usherbrooke.ca)
    date: 02/11/2018
Refactored:
    author: Audrey Duran (audrey.duran@creatis.insa-lyon.fr)
    date: 07/01/2020

This model is the implementation of an U-Net.
Reference: Ronneberger and al. 2015 (https://arxiv.org/abs/1505.04597).
Differences:
- Conv2D has been converted in a conv2D_bn_leakyrelu.
- Conv2DTranspose has been converted into a conv2Dtranspose_leakyrelu_bn.
- The ReLu has been replaced by LeackyReLu.
"""

from keras.layers import (
    Input, Conv2D, MaxPooling2D, concatenate
)
from keras.models import Model

from VITALabAI.model.semanticsegmentation.layers_utils import (
    conv2D_bn_leakyrelu, conv2Dtranspose_leakyrelu_bn
)


class VITALUnet:
    """Create a standard Unet semantic segmentation Model.

    This is inspired by the original U-Net work:
    https://arxiv.org/abs/1505.04597
    """

    @classmethod
    def network(cls, input_shape, num_classes, nb_feature_maps=32, alpha=1e-5):
        """Build Unet as a Keras model.

        Returns:
           keras.Model object *not* compiled.
        """
        # Init input
        input_image = Input(input_shape, name="input_image")

        # Working axis, axis on which to apply the batch normalization.
        working_axis = -1

        # Downsampling half
        x, connected_feature_maps = cls.unet_encoder(input_image, nb_feature_maps, working_axis, alpha)

        # Upsampling half
        out = cls.unet_decoder(x, num_classes, nb_feature_maps, connected_feature_maps, working_axis, alpha)

        return Model(inputs=[input_image], outputs=[out])

    @classmethod
    def downsampling_block(cls, x, nb_feature_maps: int, working_axis, alpha):
        """ Block that downsamples by a factor of 2 the input feature maps.

        Args:
            x: tensor, input feature maps to downsample.
            nb_feature_maps: int, number of feature maps for the convolution layers.
            working_axis: int, axis on which to apply the batch normalization.
            alpha: float, L2 Regularization coefficient.

        Returns:
             tuple of:
                MaxPooling2D()(x): tensor, the downsampled feature maps.
                x: tensor, the feature maps before the dowsampling
        """
        x = conv2D_bn_leakyrelu(x, nb_feature_maps, working_axis, alpha=alpha)
        x = conv2D_bn_leakyrelu(x, nb_feature_maps, working_axis, alpha=alpha)
        return MaxPooling2D()(x), x

    @classmethod
    def upsampling_block(cls, x, connected_feature_maps, nb_feature_maps: int, working_axis, alpha):
        """ Block that upsamples by a factor 2 the input feature maps.

        Args:
            x: tensor, input feature maps to upsample.
            connected_feature_maps: tensor, connected feature maps from the corresponding downsampling step to
                                    concatenate to the result of the upsampling.
            nb_feature_maps: int, number of feature maps for the convolution layers.
            working_axis: int, axis on which to apply the batch normalization.
            alpha: float, L2 Regularization coefficient.

        Returns:
            tensor, upsampled feature maps concatenated with the feature maps from the corresponding downsampling
            step.
        """
        x = concatenate([
            conv2Dtranspose_leakyrelu_bn(x, nb_feature_maps, working_axis, alpha=alpha),
            connected_feature_maps], axis=-1)
        x = conv2D_bn_leakyrelu(x, nb_feature_maps, working_axis, alpha=alpha)
        x = conv2D_bn_leakyrelu(x, nb_feature_maps, working_axis, alpha=alpha)
        return x

    @classmethod
    def unet_encoder(cls, x, nb_feature_maps: int, working_axis=-1, alpha=1e-5):
        """ Block making up the downsampling half (downsample by a factor 16) of a UNet.

        Args:
            x: tensor, input image or feature maps to downsample.
            nb_feature_maps:  int, number of feature maps at beginning of the network, automatic scaling for the
                              following layers.
            working_axis: int, axis on which to apply the batch normalization.
            alpha: float, L2 Regularization coefficient. Default: 1e-5.

        Returns:
            tuple of:
                x: tensor, feature maps downsampled to the point of reaching the bottleneck of the network.
                connected_feature_maps: list, connected feature maps from each downsampling step to concatenate to the
                    result of the upsampling
        """
        # first layer
        x, down_factor_2 = cls.downsampling_block(x, nb_feature_maps, working_axis, alpha)
        # second layer
        x, down_factor_4 = cls.downsampling_block(x, nb_feature_maps * 2, working_axis, alpha)
        # third layer
        x, down_factor_8 = cls.downsampling_block(x, nb_feature_maps * 4, working_axis, alpha)
        # fourth layer
        x, down_factor_16 = cls.downsampling_block(x, nb_feature_maps * 8, working_axis, alpha)
        # fifth layer
        # Convolutional block keeping the same image resolution at the bottleneck of the network
        x = conv2D_bn_leakyrelu(x, nb_feature_maps * 16, working_axis, alpha=alpha)
        x = conv2D_bn_leakyrelu(x, nb_feature_maps * 16, working_axis, alpha=alpha)

        return x, [down_factor_2, down_factor_4, down_factor_8, down_factor_16]

    @classmethod
    def unet_decoder(cls, x, num_classes, nb_feature_maps: int, connected_feature_maps, working_axis=-1, alpha=1e-5,
                     name: str = 'final_output'):
        """ Block making up the upsampling half (upsample by a factor 16) of a UNet.

        Args:
            x: tensor, feature maps, at the bottleneck of the UNet, to upsample.
            num_classes: int, number of classes and channels to output.
            nb_feature_maps:  int, number of feature maps at beginning of the network, automatic scaling for the
                              following layers.
            connected_feature_maps: list, connected feature maps from each downsampling step to concatenate to the
                                    corresponding upsampling steps of the upsampling half.
            working_axis: int, axis on which to apply the batch normalization.
            alpha: float, L2 Regularization coefficient. Default: 1e-5.
            name: str, name of the output layer (useful to link loss to a specific output in a multi-output model).

        Returns:
            tensor, UNet's segmentation.
        """
        # Skip Connection - fifth layer
        x = cls.upsampling_block(x, connected_feature_maps[3], nb_feature_maps * 8, working_axis, alpha)
        # Skip Connection - fourth layer
        x = cls.upsampling_block(x, connected_feature_maps[2], nb_feature_maps * 4, working_axis, alpha)
        # Skip Connection - third layer
        x = cls.upsampling_block(x, connected_feature_maps[1], nb_feature_maps * 2, working_axis, alpha)

        # Skip Connection - second layer
        x = cls.upsampling_block(x, connected_feature_maps[0], nb_feature_maps, working_axis, alpha)

        # output layer
        out = Conv2D(num_classes, (1, 1), activation='softmax', name=name)(x)

        return out
