"""
losses.py
author: Charles Authier (charles.authier@usherbrooke.ca)
date: 05/09/2018

File containing segmentation losses.
"""

from keras import backend as K
from keras import losses


def dice_coef(y_true, y_pred, smooth=1.):
    """Here is a dice loss for keras which is smoothed to approximate a linear (L1) loss.
    It ranges from 1 to 0 (no error), and returns results similar to binary crossentropy
    Dice = (2*|X & Y|)/ (|X|+ |Y|)
         =  2*sum(|A*B|)/(sum(A^2)+sum(B^2))
    ref: https://arxiv.org/pdf/1606.04797v1.pdf

    Args:
        y_true: keras_var, True value of the prediction.
        y_pred: keras_var, Model prediction.
        smooth: float

    Returns:
        dice coefficient
    """

    intersection = K.sum(K.abs(y_true * y_pred), axis=-1)

    return (2. * intersection + smooth) / (K.sum(K.square(y_true), -1) + K.sum(K.square(y_pred), -1) + smooth)


def dice_coef_loss(y_true, y_pred):
    """Return the dice loss from the `dice_coef`.
    """
    return 1 - dice_coef(y_true, y_pred)


def dice_crossentropy_loss(y_true, y_pred):
    """Combine the categorical crossentropy loss with the dice loss.
    """
    cc = losses.categorical_crossentropy(y_true, y_pred)
    dc = dice_coef_loss(y_true, y_pred)
    return cc + dc
