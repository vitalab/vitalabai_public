# VITALabAI

Welcome to the **PUBLIC** git repo of the [Videos & Images Theory and Analytics
Laboratory (VITAL)](http://vital.dinf.usherbrooke.ca/ "VITAL home page") of Sherbrooke
University, headed by Professor [Pierre-Marc Jodoin](http://info.usherbrooke.ca/pmjodoin/).

The purpose of this repository is threefold :

1. Keep a record of students projects, especially after they left the lab.

2. Provide reusable machine learning code.

3. Enforce a project architecture and coding standards, to promote consistency between projects and facilitate student collaboration.

Each project created by a member of the lab is located inside the *project* directory
with its own directory name like project/motorway, project/cifar and project/mio_tcd.
The *model* directory contains generic models organized by task like
model/classification/cnn.py. The *dataset* directory contains the code required to
load the data of any dataset used in our lab. Like the *model* directory, the *dataset*
directory is organized by task. The *utils* directory contains code that may be used by
several models.

Projects and datasets in the *deprecated* directory are no longer supported.

Everyone who wishes to contribute has to comply to both of these interfaces:

* VITALabAiAbstract.py
* VITALabAiDatasetAbstract.py

# How to install it
## Prerequisites

#### Anaconda environment

Download the Miniconda installation script for python 3
https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh

Execute the downloaded file and follow the prompt instructions

```bash
bash Anaconda-latest-Linux-x86_64.sh
```

Make sure you have this line in your `~/.bashrc` file:
```bash
. <anaconda/miniconda installation path>/etc/profile.d/conda.sh
```

Restart your terminal and test your installation:
```bash
conda --version
```

Create a Conda environment (Do this only once per environment):
```bash
conda create -n <EnvironmentName> python pip
```

Type ```conda info --envs``` to make sure your environment was correctly installed

Start your Conda environment (You will need to do this every time you want to execute some code):
```bash
conda activate <EnvironmentName>
```

#### Common dependencies

Complete the environment setup by installing the dependencies required by
**VITALabAI**:

1. Install the **conda** dependencies using the shell script:
```bash
install_conda_packages.sh
```
2. Install the **pip** dependencies running:
```bash
pip install -r pip-requirements.txt
```

Note that you might need to add additional packages for specific projects.

#### Deep learning libraries

Install what you need from the following:

```bash
conda install tensorflow-gpu  # 'tensorflow' is also available (cpu only)
pip install keras
conda install pytorch torchvision ignite -c pytorch
```

You should be able to successfully import your library (don't forget to use your conda environment):

```bash
import tensorflow
import keras
import torch
```

#### Specific library versions

It is also possible to install a specific version of Cuda, Cudnn and/or Tensorflow using the following command with your desired versions.

```bash
conda install -c anaconda cudatoolkit==8.0
conda install -c anaconda cudnn==6.0
conda install -c anaconda tensorflow-gpu==1.4
```

## Installation of the VITALab library

The installation of our library is straightforward. Open a terminal and do:

```bash
cd PATH_TO_YOUR_DEVELOPMENT_VITALAB_PROJECT
git clone https://bitbucket.org/vitalab/vitalabai_public.git
cd vitalabai_public
python setup.py develop
```

These commands will clone the repository, and link this directory inside your python environment.
The "develop" parameter implies that every change inside the repository will be available inside the python environment.

## Test installation

Now that the package is installed, let's test it.
Let's create a python file `test.py` or open a `python3` or `ipython3` command line and copy this.

```python
from VITALabAI.VITALabAiAbstract import VITALabAiAbstract
```

This should work.

**YOU ARE NOW READY TO GO, ENJOY!**

# How to commit

In order to commit, your code must respect the Pep8 and documentation tests. To
automatically check your code, add the following
[hook](https://bitbucket.org/snippets/pierremarcjodoin/aezAjK) .

To install this hook:

* Download the file and copy it to `vitalabai_public/.git/hooks/pre-commit`
* Run this command `chmod +x pre-commit`

The hook will run the tests locally every time you commit. You can also run the test
manually from the `vitalabai_public` directory:

```bash
pytest --pep8 -m pep8 -n0 <target_dir>
pytest tests/
```

It is possible that the Pytest fails because of unknown arguments. In that case, it is possible that the
wrong version of Pytest is being used. In some cases, the Pytest version of the default Python is used
instead of the version installed in your Anaconda environment. To fix this, uninstall Pytest from your
default Python.

When committing you must be in your Anaconda environment, if not the test will probably fail because of
import errors.  
